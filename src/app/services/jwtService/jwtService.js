import FuseUtils from '@fuse/utils/FuseUtils';
import axios from 'axios';
import jwtDecode from 'jwt-decode';
import { ENTRYPOINT } from 'config/entrypoint';
/* eslint-disable camelcase */

class JwtService extends FuseUtils.EventEmitter {
	init() {
		this.setInterceptors();
		this.handleAuthentication();
	}

	setInterceptors = () => {
		axios.interceptors.response.use(
			response => {
				return response;
			},
			err => {
				return new Promise((resolve, reject) => {
					if (err.response.status === 401 && err.config && !err.config.__isRetryRequest) {
						// if you ever get an unauthorized response, logout the user
						this.emit('onAutoLogout', 'Invalid access_token');
						this.setSession(null);
					}
					throw err;
				});
			}
		);
	};

	handleAuthentication = () => {
		const access_token = this.getAccessToken();

		if (!access_token) {
			this.emit('onNoAccessToken');

			return;
		}

		if (this.isAuthTokenValid(access_token)) {
			this.setSession(access_token);
			this.emit('onAutoLogin', true);
		} else {
			this.setSession(null);
			this.emit('onAutoLogout', 'access_token expired');
		}
	};

	createUser = data => {
		return new Promise((resolve, reject) => {
			const { displayName, password, email } = data;
			axios
				.post(`${ENTRYPOINT}/openapi/security/register/`, {
					email,
					password,
					confirm_password: password,
					name: displayName
				})
				.then(res => {
					if (res.data.user) {
						this.setSession(res.data.access_token);
						resolve(res.data.user);
					} else {
						reject(res.data.error);
					}
				})
				.catch(error => reject(error));
		});
	};

	signInWithEmailAndPassword = (email, password) => {
		console.log(email, password);
		return new Promise((resolve, reject) => {
			axios
				.post(`${ENTRYPOINT}/openapi/security/authentication_token/`, {
					email,
					password
				})
				.then(res => {
					console.log(`signInWithEmailAndPassword response: ${res}`);
					if (res.data.user) {
						console.log('Saving Token');
						this.setSession(res.data.access_token);
						resolve(res.data.user);
					} else {
						console.log(`Something Wrong ${res.data.error}`);
						reject(res.data.error);
					}
				})
				.catch(error => {
					console.log(`Something Wrong: ${error}`);
					reject(error);
				});
		});
	};

	signInWithToken = () => {
		return new Promise((resolve, reject) => {
			axios
				.get(
					`${ENTRYPOINT}/openapi/security/authentication_token/check_access/?accessToken=${this.getAccessToken()}`
				)
				.then(res => {
					console.log(`signInWithToken then: ${res}`);
					if (res.data.user) {
						console.log('Saving Token');
						this.setSession(res.data.access_token);
						resolve(res.data.user);
					} else {
						console.log(`Something Wrong ${res.data.error}`);
						reject(res.data.error);
					}
				})
				.catch(error => {
					console.log(`Something Wrong: ${error}`);
					reject(error);
				});
		});
	};

	resetPassword = email => {
		return new Promise((resolve, reject) => {
			try {
				axios
					.post(`${ENTRYPOINT}/openapi/security/password/reset/request/`, { email })
					.then(res => {
						if (
							res.hasOwnProperty('data') &&
							res.data.hasOwnProperty('status') &&
							res.data.status === 'ok'
						) {
							this.setSession(res.data.access_token);
							resolve(res.data.user);
						} else {
							reject(res.data.error);
						}
					})
					.catch(error => reject(error));
			} catch (error) {
				reject(error);
			}
		});
	};

	updateUserData = user => {
		return axios.post(`${ENTRYPOINT}/api/auth/user/update`, {
			user
		});
	};

	setSession = access_token => {
		if (access_token) {
			localStorage.setItem('jwt_access_token', access_token);
			axios.defaults.headers.common.Authorization = `Bearer ${access_token}`;
		} else {
			localStorage.removeItem('jwt_access_token');
			delete axios.defaults.headers.common.Authorization;
		}
	};

	logout = () => {
		this.setSession(null);
	};

	isAuthTokenValid = access_token => {
		if (!access_token) {
			return false;
		}
		const decoded = jwtDecode(access_token);
		const currentTime = Date.now() / 1000;
		if (decoded.exp < currentTime) {
			console.warn('access token expired');
			return false;
		}

		return true;
	};

	getAccessToken = () => {
		return window.localStorage.getItem('jwt_access_token');
	};
}

const instance = new JwtService();

export default instance;
