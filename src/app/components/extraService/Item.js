import React , { useState , useEffect } from 'react'
import {default as FusePageCarded} from '@fuse/core/FusePageCarded'
import { Link , useHistory } from 'react-router-dom'

import { useTranslation } from 'react-i18next'

import * as ExtraService from '../../utils/ExtraServices'

import MessageSnackbar from '../utils/MessageSnackbar'
import GenericPrompt from '../utils/GenericPrompt'

import {
    Button,
    Typography,
    // TextField,
    Grid,
    Box
} from '@material-ui/core'

import {
    ArrowBack
} from '@material-ui/icons'

const Item = props => {

    const history = useHistory()

    const {t} = useTranslation()

    const [fetching,setFetching] = useState(false)

    const [dialog,setDialog] = useState({
        open: false,
        dataAction: -1
    })

    const [extraService,setExtraService] = useState({
                                id: 0,
                                name: '',
                                description: '',
                                activeStatus: '',
                                addedValue: '',
                                currency: '',
                                icon: '',
                            })

    useEffect(() => {
        if(props.match.params.id){
            setFetching(true)
            ExtraService.getExtraServiceItem(props.match.params.id)
                .then(res => {
                    setExtraService({
                        ...extraService,
                        id: res.data.id,
                        name: res.data.name,
                        description: res.data.description,
                        activeStatus: res.data.activeStatus ? t('general.yes') : t('general.not'),
                        addedValue: res.data.addedValue,
                        currency: res.data.currency,
                        icon: res.data.icon,
                    })
                    setFetching(false)
                })
                .catch(error => console.error(error))
        }
    },[])

    const info = [
        {
            name: 'name',
            value: extraService.name,
            label: t('register.inputs.name')
        },
        {
            name: 'description',
            value: extraService.description,
            label: t('tableHeads.description')
        },
        {
            name: 'activeStatus',
            value: extraService.activeStatus,
            label: t('tableHeads.activeStatus')
        },
        {
            name: 'addedValue',
            value: extraService.addedValue,
            label: t('tableHeads.addedValue')
        },
        {
            name: 'currency',
            value: extraService.currency,
            label: t('tableHeads.currency')
        },
        {
            name: 'icon',
            value: extraService.icon,
            label: t('general.icon')
        },
    ]

    const openDialog = () => setDialog({
        open: true,
        dataAction: props.match.params.id
    })

    const closeDialog = () => setDialog({
        open: false,
        dataAction: -1
    })

    const handleDelete = extra_service_id => {
        closeDialog()
        setFetching(true)
        ExtraService.deleteExtraService(extra_service_id)
            .then(res => {
                setFetching(false)
                history.push('/extra_services')
            })
            .catch(error => console.error(error))            
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: 'p-0',
                header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
            }}
            header={
                <div className='flex flex-1 w-full items-center justify-between'>
                    <div className='flex flex-col items-start max-w-full'>
                        <div className='flex items-center max-w-full'>
                            <div className='flex flex-col min-w-0'>
                                <Link to='/extra_services' style={{textDecoration: 'none',marginBottom: '10px'}}>
                                    <ArrowBack /> {t('general.back')}
                                </Link>
                                <Typography variant='h4'>
                                    { extraService.name }
                                </Typography>
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        {/* pagination */}
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        <Link to={`/extra_services/update/${props.match.params.id}`} style={{textDecoration: 'none'}}>
                            <Button className='whitespace-no-wrap' variant='contained'>
                                {t('adminPanel.updateExtraServices')}
                            </Button>
                        </Link>
                        <Button 
                            style={{marginLeft: '10px'}} 
                            className='whitespace-no-wrap' 
                            color='secondary' 
                            variant='contained'
                            onClick={openDialog}
                        >
                            {t('adminPanel.deleteExtraServices')}
                        </Button>
                    </div>
                </div>
            }
            content={
                <Box p={4} mt={2} mb={2}>
                    {
                        fetching && <MessageSnackbar variant='info' message='Loading Data...' />
                    }
                    {
                        !fetching && <Grid
                                        container
                                        direction='row'
                                        justify='flex-start'
                                        alignItems='flex-start'
                                        spacing={2}
                                    >
                                        {
                                            info.length
                                                ?   info.map(data => <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                                            <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                                                {data.label}
                                                                            </Typography>
                                                                            <Typography variant='body1'>
                                                                                {data.value}
                                                                            </Typography>
                                                                        </Grid>)
                                                :   <Grid />
                                        }
                                    </Grid>
                    }
                    <GenericPrompt 
                        title='Delete Extra Service'
                        message='Are you sure?'
                        close={closeDialog}
                        action={handleDelete}
                        data={dialog}
                    />
                </Box>
            }
            innerScroll
        >
        </FusePageCarded>
    )
}

export default Item