import React,{ useState , useEffect } from 'react'
import { Link } from 'react-router-dom'
import GenericTable from '../utils/GenericTable'
import {default as FusePageCarded} from '@fuse/core/FusePageCarded'

import { useTranslation } from 'react-i18next'

import * as ExtraServices from '../../utils/ExtraServices'

import MessageSnackbar from '../utils/MessageSnackbar'
import GenericPrompt from '../utils/GenericPrompt'

import {
    Button,
    Typography,
} from '@material-ui/core'

const List = props => {

    const [extraServices,setExtraServices] = useState([])

    const [fetching,setFetching] = useState(false)

    const [dialog,setDialog] = useState({
        open: false,
        dataAction: -1
    })

    const {t,i18n} = useTranslation()

    const tableHeaders = [
        'ID',
        t('register.inputs.name'),
        t('tableHeads.description'),
        t('tableHeads.activeStatus'),
        t('tableHeads.currency'),
        t('tableHeads.actions')
    ]

    const openDialog = id => setDialog({
        open: true,
        dataAction: id
    })

    const closeDialog = () => setDialog({
        open: false,
        dataAction: -1
    })

    const handleDelete = extra_service_id => {
        setDialog({
            open: false,
            dataAction: -1
        })
        setFetching(true)
        ExtraServices.deleteExtraService(extra_service_id)
            .then(res => {})
            .catch(error => console.error(error))
        
        getExtraServicesCollection()
    }

    const getExtraServicesCollection = () => {
        setFetching(true)
        ExtraServices.getExtraServices()
            .then(res => {
                let rows = []

                res.map(data => {
                    let row = [
                        {
                            link: true,
                            path: `/extra_services/${data.id}`,
                            message: data.id
                        },
                        {
                            link: false,
                            message: data.name
                        },
                        {
                            link: false,
                            message: data.description
                        },
                        {
                            link: false,
                            message: data.activeStatus
                        },
                        {
                            link: false,
                            message: data.currency
                        },
                        {
                            action: true,
                            icons: [
                                {
                                    icon: 'search',
                                    path: `/extra_services/${data.id}`
                                },
                                {
                                    icon: 'edit',
                                    path: `/extra_services/update/${data.id}`
                                },
                                {
                                    icon: 'delete',
                                    onClick: openDialog,
                                    id: data.id
                                }
                            ]
                        }
                    ]

                    rows.push(row)
                })

                setExtraServices(rows)
                setFetching(false)
            })
            .catch(error => console.error(error))
    }

    useEffect(() => {
        getExtraServicesCollection()
    },[])

    return (
        <FusePageCarded
            classes={{
                toolbar: 'p-0',
                header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
            }}
            header={
                <div className='flex flex-1 w-full items-center justify-between'>
                    <div className='flex flex-col items-start max-w-full'>
                        <div className='flex items-center max-w-full'>
                            <div className='flex flex-col min-w-0'>
                                <Typography variant='h4'>
                                    {t('adminPanel.extraServices')}
                                </Typography>
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        {/* pagination */}
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        <Link to='/extra_services/create' style={{textDecoration: 'none'}}>
                            <Button className='whitespace-no-wrap' variant='contained'>
                                {t('adminPanel.createExtraServices')}
                            </Button>
                        </Link>
                    </div>
                </div>
            }
            content={
                <div className='p-16 sm:p-24' style={{maxHeight: '400px'}}>
                    {
                        fetching && <MessageSnackbar variant='info' message='Loading Data...' />
                    }
                    {
                        !fetching && <GenericTable headers={tableHeaders} body={extraServices} />
                    } 
                    <GenericPrompt 
                        title={t('adminPanel.deleteExtraServices')}
                        message={t('general.areYouSureToDoThis')}
                        close={closeDialog}
                        action={handleDelete}
                        data={dialog}
                    />                    
                </div>
            }
            innerScroll
        >
        </FusePageCarded>
    )
}

export default List