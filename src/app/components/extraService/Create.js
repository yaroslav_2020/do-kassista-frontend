import React , { useState } from 'react'
import {default as FusePageCarded} from '@fuse/core/FusePageCarded'
import { Link , useHistory } from 'react-router-dom'

import { useTranslation } from 'react-i18next'

import * as ExtraService from '../../utils/ExtraServices' 

import MessageSnackbar from '../utils/MessageSnackbar'

import {
    Button,
    Typography,
    TextField,
    Grid,
    Box,
    Switch,
    Select,
    MenuItem
} from '@material-ui/core'

import {
    ArrowBack
} from '@material-ui/icons'

const Create = props => {

    const [extraService,setExtraService] = useState({
                                name: '',
                                description: '',
                                activeStatus: false,
                                addedValue: '',
                                currency: '',
                                icon: '',
                            })
    
    const [fetching,setFetching] = useState(false)

    const {t,i18n} = useTranslation()

    const info = [
        {
            name: 'name',
            value: extraService.name,
            label: t('register.inputs.name'),
            type: 'text'
        },
        {
            name: 'description',
            value: extraService.description,
            label: t('tableHeads.description'),
            type: 'text'
        },
        {
            name: 'addedValue',
            value: extraService.addedValue,
            label: t('tableHeads.addedValue'),
            type: 'number'
        },
        {
            name: 'currency',
            value: extraService.currency,
            label: t('tableHeads.currency'),
            type: 'select',
            options: [
                {
                    key: 'EUR',
                    label: 'EUR'
                },
                {
                    key: 'USD',
                    label: 'USD'
                }
            ]
        },
        {
            name: 'icon',
            value: extraService.icon,
            label: t('general.icon'),
            type: 'select',
            options: [
                {
                    key: 'general_cleanning',
                    label: 'Limpieza General'
                },
                {
                    key: 'planchar_ropa',
                    label: 'Planchar Ropa'
                },
                {
                    key: 'limpieza_electrodomesticos',
                    label: 'Limpieza Electrodomesticos'
                },
                {
                    key: 'interior_muebles',
                    label: 'Interior Muebles'
                },
                {
                    key: 'interior_ventanas',
                    label: 'Interior Ventanas'
                },
                {
                    key: 'hacer_comida',
                    label: 'Hacer La Comida'
                },
                {
                    key: 'interior_frigorifico',
                    label: 'Interior Frigorifico'
                },
                {
                    key: 'compra_supermercado',
                    label: 'Compra Supermercado'
                },
            ]
        },
        {
            name: 'activeStatus',
            value: extraService.activeStatus,
            label: t('tableHeads.activeStatus'),
            type: 'switch'
        },
    ]

    const handleChange = input => {
        setExtraService({
            ...extraService,
            [input.target.name]: input.target.value
        })
    }

    const handleSubmit = async btn => {
        setFetching(true)
        let response = await ExtraService.createExtraService({
            ...extraService,
            activeStatus: extraService.activeStatus ? 1 : 0,
            addedValue: parseFloat(extraService.addedValue)
        })

        setFetching(false)

        setExtraService({
            id: 0,
            name: '',
            description: '',
            activeStatus: '',
            addedValue: '',
            currency: '',
            icon: '',
        })     
    }

    const handleSwitchChange = () => {
        setExtraService({
            ...extraService,
            activeStatus: !extraService.activeStatus
        })
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: 'p-0',
                header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
            }}
            header={
                <div className='flex flex-1 w-full items-center justify-between'>
                    <div className='flex flex-col items-start max-w-full'>
                        <div className='flex items-center max-w-full'>
                            <div className='flex flex-col min-w-0'>
                                <Link to='/extra_services' style={{textDecoration: 'none',marginBottom: '10px'}}>
                                    <ArrowBack /> {t('general.back')}
                                </Link>
                                <Typography variant='h4'>
                                    {t('adminPanel.createExtraServices')}
                                </Typography>
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        {/* pagination */}
                    </div>
                </div>
            }
            content={
                <Box p={4} mt={2} mb={2}>
                    {
                        fetching && <MessageSnackbar variant='info' message='Loading Data...' />
                    }
                    {
                        !fetching && <Grid
                                        container
                                        direction='row'
                                        justify='flex-start'
                                        alignItems='flex-start'
                                        spacing={2}
                                    >
                                        {
                                            info.length
                                                ?   info.map(data => {
                                                        if(data.type !== 'switch' && data.type !== 'select'){
                                                            return  <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                                        <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                                            {data.label}
                                                                        </Typography>
                                                                        <TextField
                                                                            type={data.type}
                                                                            name={data.name}
                                                                            value={data.value}
                                                                            variant='outlined'
                                                                            onChange={handleChange}
                                                                            fullWidth
                                                                        />
                                                                    </Grid>
                                                        }else if(data.type === 'select'){
                                                            return  <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                                        <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                                            {data.label}
                                                                        </Typography>
                                                                        <Select
                                                                            variant='outlined'
                                                                            value={data.value}
                                                                            onChange={handleChange}
                                                                            name={data.name}
                                                                            fullWidth
                                                                        >
                                                                            <MenuItem value=''>None</MenuItem>
                                                                            {
                                                                                data.options.map((option,id) => <MenuItem key={id} value={option.key}>{option.label}</MenuItem> )
                                                                            }
                                                                        </Select>
                                                                    </Grid>
                                                        }{
                                                            return  <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                                        <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                                            {data.label}
                                                                        </Typography>
                                                                        <Switch
                                                                            checked={data.value ? true : false}
                                                                            onChange={handleSwitchChange}
                                                                            name={data.name}
                                                                        />
                                                                    </Grid>
                                                        }
                                                    })
                                                :   <Grid />
                                        }
                                        <Grid item xs={12} md={12} sm={12}>
                                            <Button
                                                variant='contained'
                                                color='primary'
                                                onClick={handleSubmit}
                                                fullWidth
                                            >
                                                {t('general.save')}
                                            </Button>
                                        </Grid>
                                    </Grid>
                    }
                </Box>
            }
            innerScroll
        >
        </FusePageCarded>
    )
}

export default Create