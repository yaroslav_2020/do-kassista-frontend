export const optionsList = (settings, t) => [
    {
        name: 'name',
        value: settings.name,
        label: t('OfferStep2.name'),
        type: 'select',
        options: [
            {
                value: t('Settings.standard_price'),
                key: 'standard_price'
            },
            {
                value: t('Settings.standard_price_independent_worker'),
                key: 'standard_price_independent_worker'
            },
            {
                value: t('Settings.job_link_validity_in_hours'),
                key: 'job_link_validity_in_hours'
            },
            {
                value: t('Settings.allow_voucher_apply'),
                key: 'allow_voucher_apply'
            },
            {
                value: t('Settings.room_cleaning_duration_time'),
                key: 'room_cleaning_duration_time'
            },
            {
                value: t('Settings.bathroom_cleaning_duration_time'),
                key: 'bathroom_cleaning_duration_time'
            },
            {
                value: t('Settings.kitchen_cleaning_duration_time'),
                key: 'kitchen_cleaning_duration_time'
            },
            {
                value: t('Settings.living_cleaning_duration_time'),
                key: 'living_cleaning_duration_time'
            },
            {
                value: t('Settings.max_hours'),
                key: 'max_hours'
            },
            {
                value: t('Settings.min_hours'),
                key: 'min_hours'
            },
            {
                value: t('Settings.max_price_per_hour'),
                key: 'max_price_per_hour'
            },
            {
                value: t('Settings.min_price_per_hour'),
                key: 'min_price_per_hour'
            },
            {
                value: t('Settings.price_per_cleanning_tools'),
                key: 'price_per_cleanning_tools'
            },
            {
                value: t('Settings.start_labor_schedule'),
                key: 'start_labor_schedule'
            },
            {
                value: t('Settings.end_labor_schedule'),
                key: 'end_labor_schedule'
            },
            {
                value: t('Settings.hours_buffer'),
                key: 'hours_buffer'
            },
            {
                value: t('Settings.time_buffer_to_block_iteration'),
                key: 'time_buffer_to_block_iteration'
            },
            {
                value: t('Settings.time_buffer_to_realease_blocked_payment'),
                key: 'time_buffer_to_realease_blocked_payment'
            },
            {
                value: t('Settings.dev_mode'),
                key: 'dev_mode'
            },
        ]
    },
    {
        name: 'value',
        value: settings.value,
        label: t('general.value'),
        type: 'text',
    },
    {
        name: 'currency',
        value: settings.currency,
        label: t('tableHeads.currency'),
        type: 'text',
    },
    {
        name: 'description',
        value: settings.description,
        label: t('tableHeads.description'),
        type: 'text'
    },
    {
        name: 'category',
        value: settings.category,
        label: t('general.category'),
        type: 'text'
    },
]