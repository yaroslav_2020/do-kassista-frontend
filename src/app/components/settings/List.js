import React , { useState , useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import GenericTable from '../utils/GenericTable'
import {default as FusePageCarded} from '@fuse/core/FusePageCarded'

import MessageSnackbar from '../utils/MessageSnackbar'

import * as Settings from '../../utils/Settings'

import {
    Button,
    Typography,
} from '@material-ui/core'
import PaginationControlled from '../utils/PaginationControlled'

const List = () => {

    const {t} = useTranslation()

    const [fetching,setFetching] = useState(false)

    const [settings,setSettings] = useState({
        data: [],
        lastPage: '',
        currentPage: '',
    })

    const [page, setPage] = useState(1);

    const getConfigs = async() => {
        try {
            const configs = (!!settings.currentPage) ? await Settings.getSettings(settings.currentPage) : await Settings.getSettings();
            const rows = configs['hydra:member'].map( config => [
                {
                    link: true,
                    path: `/settings/${config.id}`,
                    message: config.id
                },
                {
                    link: false,
                    message: config.name
                },
                {
                    link: false,
                    message: config.value
                },
                {
                    link: false,
                    message: config.category
                },
                {
                    link: false,
                    message: config.description
                },
                {
                    action: true,
                    icons: [
                        {
                            icon: 'search',
                            path: `/settings/${config.id}`
                        },
                        {
                            icon: 'edit',
                            path: `/settings/update/${config.id}`
                        },
                        {
                            icon: 'delete',
                            path: '/login'
                        }
                    ]
                }
            ]);
            setSettings({
                data: rows,
                lastPage: (!!configs['hydra:view']) ? configs['hydra:view']['hydra:last'] : '',
                currentPage: (!!configs['hydra:view']) ? configs['hydra:view']['@id'] : '',
            })
            setFetching( false );
        } catch (error) {
            console.log( 'configs error: ', error );
            setFetching( false );        
        }
    }

    useEffect(() => {
        setFetching(true)
        getConfigs();
    },[ page ])

    const tableHeaders = [
        'ID',
        t('register.inputs.name'),
        t('general.value'),
        t('general.category'),
        t('tableHeads.description'),
        t('tableHeads.actions'),
    ]

    return (
        <FusePageCarded
            classes={{
                toolbar: 'p-0',
                header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
            }}
            header={
                <div className='flex flex-1 w-full items-center justify-between'>
                    <div className='flex flex-col items-start max-w-full'>
                        <div className='flex items-center max-w-full'>
                            <div className='flex flex-col min-w-0'>
                                <Typography variant='h4'>
                                    {t('general.settings')}
                                </Typography>
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        {/* pagination */}
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        <Link to='/settings/create' style={{textDecoration: 'none'}}>
                            <Button className='whitespace-no-wrap' variant='contained'>
                                {t('adminPanel.createSettings')}
                            </Button>
                        </Link>
                    </div>
                </div>
            }
            content={
                <div className='p-16 sm:p-24' style={{maxHeight: '400px'}}>
                    {
                        fetching && <MessageSnackbar variant='info' message='Loading Data...' />
                    }
                    {
                        !fetching &&
                        <>
                            <GenericTable headers={tableHeaders} body={settings.data} /> 
                            <PaginationControlled
                                count={settings.lastPage.slice(-1)}
                                page={page}
                                onPageChange={setPage}
                                handleUrlChange={setSettings}
                            />
                        </>
                    }                   
                </div>
            }
            innerScroll
        >
        </FusePageCarded>
    )
}

export default List