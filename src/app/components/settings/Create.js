import React , { useState } from 'react'
import {default as FusePageCarded} from '@fuse/core/FusePageCarded/index'
import { Link , useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import * as Settings from '../../utils/Settings'

import MessageSnackbar from '../utils/MessageSnackbar'

import {
    Button,
    Typography,
    TextField,
    Grid,
    Box,
    Select,
    MenuItem
} from '@material-ui/core'

import {
    ArrowBack
} from '@material-ui/icons'
import { optionsList } from './helpers/optionsList'

const Create = () => {
    
    const {t} = useTranslation()
    const [fetching,setFetching] = useState(false)
    const [settings,setSettings] = useState({
        name: 'standard_price',
        value: '',
        currency: '',
        description: '',
        category: '',
    })

    const info = optionsList(settings, t);

    const handleChange = (input) => {
        setSettings({
            ...settings,
            [input.target.name]: input.target.value
        })
    }

    const handleClick = async () => {
        setFetching(true)
        await Settings.createSettings({
            name: settings.name,
            data: {
                value: settings.value,
                currency: settings.currency
            },
            description: settings.description,
            category: settings.category
        })
        setFetching(false)
        setSettings({
            name: '',
            value: '',
            currency: '',
            description: '',
            category: '',
        })
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: 'p-0',
                header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
            }}
            header={
                <div className='flex flex-1 w-full items-center justify-between'>
                    <div className='flex flex-col items-start max-w-full'>
                        <div className='flex items-center max-w-full'>
                            <div className='flex flex-col min-w-0'>
                                <Link to='/settings' style={{textDecoration: 'none',marginBottom: '10px'}}>
                                    <ArrowBack /> {t('general.back')}
                                </Link>
                                <Typography variant='h4'>
                                    {t('adminPanel.createSettings')}
                                </Typography>
                            </div>
                        </div>
                    </div>
                </div>
            }
            content={
                <Box p={4} mt={2} mb={2}>
                    <Box mb={2}>
                        {
                            fetching && <MessageSnackbar variant='info' message='Loading Data...' />
                        }
                    </Box>
                    <Grid
                        container
                        direction='row'
                        justify='flex-start'
                        alignItems='flex-start'
                        spacing={2}
                    >
                        {
                            info.length
                            ?   info.map(data => {
                                    if(data.type === 'select'){
                                        return  <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                    <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                        {data.label}
                                                    </Typography>
                                                    <Select
                                                        value={data.value}
                                                        name={data.name}
                                                        variant='outlined'
                                                        fullWidth
                                                        onChange={handleChange}
                                                    >
                                                        <MenuItem value="">
                                                            <em>None</em>
                                                        </MenuItem>
                                                        {
                                                            data.options.map(option => <MenuItem  key={data.options.indexOf(option)} value={option.key}>{option.value}</MenuItem>)
                                                        }
                                                    </Select>
                                                </Grid>
                                    }else{
                                        return  <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                    <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                        {data.label}
                                                    </Typography>
                                                    <TextField
                                                        type={data.type}
                                                        name={data.name}
                                                        value={data.value}
                                                        variant='outlined'
                                                        onChange={handleChange}
                                                        fullWidth
                                                    />
                                                </Grid>
                                    }
                            })
                            :   <Grid />
                        }

                        <Grid item xs={12} md={12} sm={12}>
                            <Button
                                variant='contained'
                                color='primary'
                                onClick={handleClick}
                                fullWidth
                            >
                                {t('general.save')}
                            </Button>
                        </Grid>

                    </Grid>
                </Box>
            }
            innerScroll
        >
        </FusePageCarded>
    )
}

export default Create