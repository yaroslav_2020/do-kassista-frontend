import React , { useState , useEffect } from 'react'
import {default as FusePageCarded} from '@fuse/core/FusePageCarded/index'
import { Link , useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import * as Settings from '../../utils/Settings'

import MessageSnackbar from '../utils/MessageSnackbar'
import GenericPrompt from '../utils/GenericPrompt'

import {
    Button,
    Typography,
    Grid,
    Box
} from '@material-ui/core'

import {
    ArrowBack
} from '@material-ui/icons'

const Item = props => {

    const history = useHistory()

    const [fetching,setFetching] = useState(false)

    const [dialog,setDialog] = useState({
        open: false,
        dataAction: -1
    })

    const {t,i18n} = useTranslation()

    const [setting,setSetting] = useState({
        id: 0,
        name: '',
        value: 0,
        description: '',
        category: ''
    })

    useEffect(() => {
        console.log('test, ', props.match.params.id);
        if(props.match.params.id){
            setFetching(true)
            Settings.getSettingItem(props.match.params.id)
                .then(res => {
                    setSetting({
                        ...setting,
                        id: res.data.id,
                        name: res.data.name,
                        value: res.data.value,
                        description: res.data.description,
                        category: res.data.category
                    })
                    setFetching(false)
                })
                .catch(error => console.error(error))
        }
    },[])

    const info = [
        {
            name: 'name',
            value: setting.name,
            label: t('OfferStep2.name')
        },
        {
            name: 'value',
            value: setting.value,
            label: t('general.value')
        },
        {
            name: 'category',
            value: setting.category,
            label: t('general.category')
        },
        {
            name: 'description',
            value: setting.description,
            label: t('tableHeads.description')
        },
    ]

    const openDialog = () => setDialog({
        open: true,
        dataAction: props.match.params.id
    })

    const closeDialog = () => setDialog({
        open: false,
        dataAction: -1
    })

    const handleDelete = setting_id => {
        closeDialog()
        setFetching(true)
        setting.deletesetting(setting_id)
            .then(res => {
                setFetching(false)
                history.push('/settings')
            })
            .catch(error => console.error(error))
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: 'p-0',
                header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
            }}
            header={
                <div className='flex flex-1 w-full items-center justify-between'>
                    <div className='flex flex-col items-start max-w-full'>
                        <div className='flex items-center max-w-full'>
                            <div className='flex flex-col min-w-0'>
                                <Link to='/settings' style={{textDecoration: 'none',marginBottom: '10px'}}>
                                    <ArrowBack /> {t('general.back')}
                                </Link>
                                <Typography variant='h4'>
                                    { setting.name }
                                </Typography>
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        {/* pagination */}
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        <Link to={`/settings/update/${props.match.params.id}`} style={{textDecoration: 'none'}}>
                            <Button className='whitespace-no-wrap' variant='contained'>
                                {t('adminPanel.updateSettings')}
                            </Button>
                        </Link>
                        <Button 
                            style={{marginLeft: '10px'}} 
                            className='whitespace-no-wrap' 
                            color='secondary' 
                            variant='contained'
                            onClick={openDialog}
                        >
                            {t('adminPanel.deleteSettings')}
                        </Button>
                    </div>
                </div>
            }
            content={
                <Box p={4} mt={2} mb={2}>
                    {
                        fetching && <MessageSnackbar variant='info' message='Loading Data...' />
                    }
                    {
                        !fetching && 
                        <Grid
                            container
                            direction='row'
                            justify='flex-start'
                            alignItems='flex-start'
                            spacing={2}
                        >
                            {
                                info.length
                                ?   info.map(data => 
                                        <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                            <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                {data.label}
                                            </Typography>
                                            <Typography variant='body1'>
                                                {data.value}
                                            </Typography>
                                        </Grid>
                                    )
                                :   <Grid />
                            }
                        </Grid>
                    }
                    <GenericPrompt 
                        title={t('adminPanel.deletesetting')}
                        message={t('general.areYouSureToDoThis')}
                        close={closeDialog}
                        action={handleDelete}
                        data={dialog}
                    />
                </Box>
            }
            innerScroll
        >
        </FusePageCarded>
    )
}

export default Item