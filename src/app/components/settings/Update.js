import React , { useState , useEffect } from 'react'
import {default as FusePageCarded} from '@fuse/core/FusePageCarded'
import { Link , useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import * as Settings from '../../utils/Settings'

import MessageSnackbar from '../utils/MessageSnackbar'
import GenericPrompt from '../utils/GenericPrompt'

import {
    Button,
    Typography,
    TextField,
    Grid,
    Box,
    Select,
    MenuItem,
    // Switch
} from '@material-ui/core'

import {
    ArrowBack
} from '@material-ui/icons'

const Update = props => {

    const history = useHistory()

    const [fetching,setFetching] = useState(true)

    const {t} = useTranslation()

    const [settings,setSettings] = useState({
        name: '',
        value: '',
        currency: '',
        description: '',
        category: '',
    })

    const [dialog,setDialog] = useState({
        open: false,
        dataAction: -1,
        action: () => {}
    })

    useEffect(() => {
        if(props.match.params.id){
            setFetching(true)
            Settings.getSettingItem(props.match.params.id)
                .then(res => {
                    setSettings({
                        ...settings,
                        name: res.data.name,
                        value: res.data.value,
                        currency: res.data.currency,
                        description: res.data.description,
                        category: res.data.category,
                    })
                    setFetching(false)
                })
                .catch(error => console.error(error))
        }
    },[])

    const info = [
        {
            name: 'name',
            value: settings.name,
            label: t('OfferStep2.name'),
            type: 'text'
        },
        {
            name: 'value',
            value: settings.value,
            label: t('general.value'),
            type: 'text'
        },
        {
            name: 'category',
            value: settings.category,
            label: t('tableHeads.category'),
            type: 'text'
        },
        {
            name: 'description',
            value: settings.description,
            label: t('tableHeads.description'),
            type: 'text'
        },
    ]

    const openDialog = action => setDialog({
        open: true,
        dataAction: props.match.params.id,
        action
    })

    const closeDialog = () => setDialog({
        open: false,
        dataAction: -1,
        action: () => {}
    })

    const handleChange = input => {
        setSettings({
            ...settings,
            [input.target.name]: input.target.value
        })
    }

    const handleClick = async (btn) => {
        try {
            setFetching(true)
            let response = await Settings.updateSettings(props.match.params.id,{
                name: settings.name,
                data: {
                    value: settings.value,
                    currency: settings.currency
                },
                description: settings.description,
                category: settings.category
            })
            setFetching(false)
            closeDialog()
            history.push('/settings')
        } catch (error) {
            console.log('Error in Update: ', error);
        }
    }

    const handleDelete = settings_id => {
        closeDialog()
        setFetching(true)
        settings.deletesettings(settings_id)
            .then(res => {
                setFetching(false)
                history.push('/settings')
            })
            .catch(error => console.error(error))
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: 'p-0',
                header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
            }}
            header={
                <div className='flex flex-1 w-full items-center justify-between'>
                    <div className='flex flex-col items-start max-w-full'>
                        <div className='flex items-center max-w-full'>
                            <div className='flex flex-col min-w-0'>
                                <Link to='/settings' style={{textDecoration: 'none',marginBottom: '10px'}}>
                                    <ArrowBack /> {t('general.back')}
                                </Link>
                                <Typography variant='h4'>
                                    {t('adminPanel.updateSettings')}
                                </Typography>
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        {/* pagination */}
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        <Link to='/settings/create' style={{textDecoration: 'none'}}>
                            <Button className='whitespace-no-wrap' variant='contained'>
                                {t('adminPanel.createSettings')}
                            </Button>
                        </Link>
                        <Button 
                            style={{marginLeft: '10px'}} 
                            className='whitespace-no-wrap' 
                            color='secondary' 
                            variant='contained'
                            onClick={() => openDialog(handleDelete)}
                        >
                            {t('adminPanel.deleteSettings')}
                        </Button>
                    </div>
                </div>
            }
            content={
                <Box p={4} mt={2} mb={2}>
                    {
                        fetching && <MessageSnackbar variant='info' message='Loading Data...' />
                    }
                    <Grid
                        container
                        direction='row'
                        justify='flex-start'
                        alignItems='flex-start'
                        spacing={2}
                    >
                        {
                            info.length
                                ?   info.map(data => {
                                        if(data.type === 'select'){
                                            return  <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                        <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                            {data.label}
                                                        </Typography>
                                                        <Select
                                                            value={data.value}
                                                            name={data.name}
                                                            variant='outlined'
                                                            fullWidth
                                                            onChange={handleChange}
                                                        >
                                                            <MenuItem value="">
                                                                <em>None</em>
                                                            </MenuItem>
                                                            {
                                                                data.options.map(option => <MenuItem  key={data.options.indexOf(option)} value={option.key}>{'option.value'}</MenuItem>)
                                                            }
                                                        </Select>
                                                    </Grid>
                                        }else{
                                            return  <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                        <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                            {data.label}
                                                        </Typography>
                                                        <TextField
                                                            type={data.type}
                                                            name={data.name}
                                                            value={data.value}
                                                            variant='outlined'
                                                            onChange={handleChange}
                                                            fullWidth
                                                        />
                                                    </Grid>
                                        }
                                    })
                                :   <Grid />
                        }
                        <Grid item xs={12} md={12} sm={12}>
                            <Button
                                variant='contained'
                                color='primary'
                                onClick={() => openDialog(handleClick)}
                                fullWidth
                            >
                                {t('general.save')}
                            </Button>
                        </Grid>
                    </Grid>
                    <GenericPrompt 
                        title='Action for settings'
                        message='Are you sure?'
                        close={closeDialog}
                        action={dialog.action}
                        data={dialog}
                    />
                </Box>
            }
            innerScroll
        >
        </FusePageCarded>
    )
}

export default Update