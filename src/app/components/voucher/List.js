import React , { useState , useEffect } from 'react'
import { Link } from 'react-router-dom'
import GenericTable from '../utils/GenericTable'
import {default as FusePageCarded} from '@fuse/core/FusePageCarded'
import { useTranslation } from 'react-i18next'

import * as Voucher from '../../utils/Voucher'

import MessageSnackbar from '../utils/MessageSnackbar'
import GenericPrompt from '../utils/GenericPrompt'

import {
    Button,
    Typography,
} from '@material-ui/core'

const List = ( ) => {

    const [vouchers,setVouchers] = useState([])
    const [fetching,setFetching] = useState(false)
    const [dialog,setDialog] = useState({
        open: false,
        dataAction: -1
    })

    const {t} = useTranslation()

    const tableHeaders = [
        'ID',
        t('tableHeads.title'),
        t('tableHeads.code'),
        t('tableHeads.value'),
        t('tableHeads.usageType'),
        t('tableHeads.usageCount'),
        t('tableHeads.readyToBeUsed'),
        t('tableHeads.activeStatus'),
        t('tableHeads.actions')
    ]

    const getVouchersCollection = () => {
        setFetching(true)
        Voucher.getVouchers()
            .then(res => {

                const rows = res.map(data => [
                    {
                        link: true,
                        path: `/vouchers/${data.id}`,
                        message: data.id
                    },
                    {
                        link: false,
                        message: data.title
                    },
                    {
                        link: false,
                        message: <pre style={{userSelect: 'text'}}> {data.code}</pre>
                    },
                    {
                        link: false,
                        message: `${data.discount} ${(data.type === 'fixed') ? ' \u20AC' : ' %'}`
                    },                        
                    {
                        link: false,
                        message: t("voucher." + data.usageType)
                    },
                    {
                        link: false,
                        message: data.usageCount
                    },
                    {
                        link: false,
                        message: data.readyToBeUsed ? t('general.yes') : t('general.Not')
                    },
                    {
                        link: false,
                        message: data.activeStatus
                    },
                    {
                        action: true,
                        icons: [
                            {
                                icon: 'search',
                                path: `/vouchers/${data.id}`
                            },                          
                            (data.canBeUsedOnlyOneTime && data.usageCount  >  0) ? { icon: '' } : {                                    
                                icon: 'edit',
                                path: `/vouchers/update/${data.id}`
                            }
                            // {
                            //     icon: 'delete',
                            //     onClick: openDialog,
                            //     id: data.id
                            // }
                        ]
                    }
                ])

                setVouchers(rows)
                setFetching(false)
            })
    }

    const openDialog = id => setDialog({
        open: true,
        dataAction: id
    })

    const closeDialog = () => setDialog({
        open: false,
        dataAction: -1
    })

    const handleDelete = voucher_id => {
        setDialog({
            open: false,
            dataAction: -1
        })
        setFetching(true)
        Voucher.deleteVoucher(voucher_id)
        .then(res => {
            setFetching(false)
            getVouchersCollection()
        })
        .catch(error => console.error(error))
    }

    useEffect(() => {
        getVouchersCollection()
    },[])

    return (
        <FusePageCarded
            classes={{
                toolbar: 'p-0',
                header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
            }}
            header={
                <div className='flex flex-1 w-full items-center justify-between'>
                    <div className='flex flex-col items-start max-w-full'>
                        <div className='flex items-center max-w-full'>
                            <div className='flex flex-col min-w-0'>
                                <Typography variant='h4'>
                                    {t('adminPanel.vouchers')}
                                </Typography>
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        {/* pagination */}
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        <Link to='/vouchers/create' style={{textDecoration: 'none'}}>
                            <Button className='whitespace-no-wrap' variant='contained'>
                                {t('adminPanel.createVoucher')}
                            </Button>
                        </Link>
                    </div>
                </div>
            }
            content={
                <div className='p-16 sm:p-24' style={{maxHeight: '400px'}}>
                    {
                        fetching  && <MessageSnackbar variant='info' message='Loading Data...' />
                    }
                    {
                        !fetching && <GenericTable headers={tableHeaders} body={vouchers} />  
                    }  
                    <GenericPrompt 
                        title={t('adminPanel.deleteVoucher')}
                        message={t('general.areYouSureToDoThis')}
                        close={closeDialog}
                        action={handleDelete}
                        data={dialog}
                    />                
                </div>
            }
            innerScroll
        >
        </FusePageCarded>
    )
}

export default List