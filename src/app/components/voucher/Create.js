import React , { useEffect, useState } from 'react'
import {default as FusePageCarded} from '@fuse/core/FusePageCarded'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import * as Voucher from '../../utils/Voucher'

import MessageSnackbar from '../utils/MessageSnackbar'
import { getUsersByRole } from 'app/utils/UserAdminPanel'



import {
    Button,
    Typography,
    TextField,
    Grid,
    Box,
    Select,
    MenuItem
} from '@material-ui/core'

import {
    ArrowBack
} from '@material-ui/icons'


const Create = props => {

    const [fetching,setFetching] = useState(false)

    const {t,i18n} = useTranslation()
    
    const [users, setUsers] = useState([])

    const [voucher,setVoucher] = useState({
        title: '',
        type: '',
        discount: 0,    
        usageType: '',
        user: '',
        startDate: '',
        endDate: '',
    })

    const [showUser, setShowUser] = useState(false)


    const info = [
        {
            name: 'title',
            value: voucher.title,
            label: t('tableHeads.title'),
            type: 'text',
            show: true
        },
        {
            name: 'type',
            value: voucher.type,
            label: t('general.type'),
            type: 'select',
            show: true,
            options: [
                {
                    key: 'fixed',
                    value: 'Fixed'
                },
                {
                    key: 'percent',
                    value: 'Percent'
                }
            ]
        },
        {
            name: 'discount',
            value: voucher.discount,
            label: t('tableHeads.value'),
            type: 'number',
            show: true
        },        
        {
            name: 'usageType',
            value: voucher.usageType,
            label: t('tableHeads.usageType'),
            type: 'select',
            show: true,
            options: [
                {
                    key: 'USAGE_TYPE_BY_ANY_CLIENT_ANY_TIME',
                    value: t('voucher.anyClientAtAnyTime')
                },
                {
                    key: 'USAGE_ONE_TIME_BY_FIRST_CLIENT',
                    value: t('voucher.oneTimeByFirstClient')
                },
                {
                    key: 'USAGE_ONE_TIME_BY_SPECIFIC_CLIENT',
                    value: t('voucher.oneTimeBySpecificClient')
                }                
            ]
        },
        {   
            name: 'user',
            value: voucher.user,
            label: t('tableHeads.user'),
            type: 'select',
            show: showUser,
            options: users                
        },
        {
            name: 'startDate',
            value: voucher.startDate,
            label: t('Step1Frequency.startDateTitle'),
            type: 'date',
            show: true
        },
        {
            name: 'endDate',
            value: voucher.endDate,
            label: t('voucher.endDate'),
            type: 'date',
            show: true
        },
    ]

    useEffect(() => {
        getUsersByRole()
            .then( (users) => (               
                setUsers(
                    users.map((user) => ({                    
                        key: user['@id'],
                        value: user.username                    
                    }))
                ) 
            ))
    }, [])

    useEffect(() => {
        if(voucher.usageType === 'USAGE_ONE_TIME_BY_SPECIFIC_CLIENT'){
            setShowUser(true)
            setVoucher( (v) => ({
                ...v,
                user: '',
            }))        
        }
        else {
            setShowUser(false)
            setVoucher({
                title: voucher.title,
                type: voucher.type,
                discount: voucher.discount,    
                usageType: voucher.usageType,            
                startDate: voucher.startDate,
                endDate: voucher.endDate,
            })
        }

    }, [voucher.usageType])

    const handleChange = input => {    
        setVoucher({
            ...voucher,
            [input.target.name]: input.target.value
        })
    }

    const handleClick = async btn => {
        setFetching(true)
        
        voucher.discount = parseFloat(voucher.discount);
        let response = await Voucher.createVoucher(voucher)
        setFetching(false)
        setVoucher({
            title: '',
            type: '',
            discount: 0,    
            usageType: '',            
            startDate: '',
            endDate: '',
        })
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: 'p-0',
                header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'        
            }}
            header={
                <div className='flex flex-1 w-full items-center justify-between'>
                    <div className='flex flex-col items-start max-w-full'>
                        <div className='flex items-center max-w-full'>
                            <div className='flex flex-col min-w-0'>
                                <Link to='/vouchers' style={{textDecoration: 'none',marginBottom: '10px'}}>
                                    <ArrowBack /> {t('general.back')}
                                </Link>
                                <Typography variant='h4'>
                                    {t('adminPanel.createVoucher')}
                                </Typography>
                            </div>
                        </div>
                    </div>
                </div>
            }
            content={
                <Box p={4} mt={2} mb={2}>
                    <Box mb={2}>
                        {
                            fetching && <MessageSnackbar variant='info' message='Loading Data...' />
                        }
                    </Box>
                    <Grid
                        container
                        direction='row'
                        justify='flex-start'
                        alignItems='flex-start'
                        spacing={2}                                          
                    >
                        {
                            info.length
                                ?   info.map(data => {
                                        if(data.type === 'select'){
                                            return  <>
                                            {
                                                data.show &&
                                                <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                            <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                                {data.label}
                                                            </Typography>
                                                            <Select
                                                                value={data.value}
                                                                name={data.name}
                                                                variant='outlined'
                                                                fullWidth
                                                                onChange={handleChange}
                                                            >
                                                                <MenuItem value="">
                                                                    <em>None</em>
                                                                </MenuItem>
                                                                {
                                                                    data.options.map(option => <MenuItem  key={data.options.indexOf(option)} value={option.key}>{option.value}</MenuItem>)
                                                                }
                                                            </Select>
                                                        </Grid>

                                            }
                                            </>

                                        }else{
                                            return  <>
                                                <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                    <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                        {data.label}
                                                    </Typography>
                                                    <TextField
                                                        type={data.type}
                                                        name={data.name}
                                                        value={data.value}
                                                        variant='outlined'
                                                        onChange={handleChange}
                                                        fullWidth
                                                    />
                                                </Grid>
                                            </>
                                        }
                                    })
                                :   <Grid />
                        }
                        <Grid item xs={12} md={12} sm={12}>
                            <Button
                                variant='contained'
                                color='primary'
                                onClick={handleClick}
                                fullWidth
                            >
                                {t('general.save')}
                            </Button>
                        </Grid>
                    </Grid>
                </Box>
            }
            innerScroll
        >
        </FusePageCarded>
    )
}

export default Create