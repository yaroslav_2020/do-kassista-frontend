import React , { useState , useEffect } from 'react'
import {default as FusePageCarded} from '@fuse/core/FusePageCarded'
import { Link , useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import * as Voucher from '../../utils/Voucher'
import moment from 'moment'

import MessageSnackbar from '../utils/MessageSnackbar'
import GenericPrompt from '../utils/GenericPrompt'

import {
    Button,
    Typography,
    TextField,
    Grid,
    Box,
    Select,
    MenuItem,
    Switch
} from '@material-ui/core'

import {
    ArrowBack
} from '@material-ui/icons'
import { getUsersByRole } from 'app/utils/UserAdminPanel'

const Update = props => {
    const header = document.querySelector('#headerTop')

    const history = useHistory()

    const [fetching,setFetching] = useState(true)

    const [error, setError] = useState(false)

    const {t,i18n} = useTranslation()

    const [voucher,setVoucher] = useState({
        code: '',
        title: '',
        type: '',
        discount: 0,    
        usageType: '',
        user: '',
        startDate: '',
        endDate: '',
        activeStatus: false
    })

    const [currentVoucher, setCurrentVoucher] = useState({})

    const [showUser, setShowUser] = useState(false)

    const [users, setUsers] = useState([])

    const [dialog,setDialog] = useState({
        open: false,
        dataAction: -1,
        action: () => {}
    })

    useEffect(() => {
        if(props.match.params.id){        

            getUsersByRole()
            .then( (userList) => {
                setUsers(
                    userList.map((user) => ({                    
                        key: user['@id'],
                        value: user.username                    
                    }))
                )
            })
            .catch(error => console.error(error))
        }
    },[])

    useEffect(() => {
        if (users.length > 0) {
            Voucher.getVoucherItem(props.match.params.id)
                    .then(res => {                                       
                        setVoucher({
                            ...voucher,
                            code: res.data.code,
                            title: res.data.title,
                            type: res.data.type,
                            discount: res.data.discount,                                                
                            usageType: res.data.usageType,                
                            user: res.data.user,
                            activeStatus: Boolean(res.data.activeStatus),
                            startDate: moment(res.data.startDate).format('YYYY-MM-DD'),
                            endDate: moment(res.data.endDate).format('YYYY-MM-DD')
                        })
                        
                        setCurrentVoucher({
                            code: res.data.code,                
                            title: res.data.title,
                            type: res.data.type,
                            discount: res.data.discount,                                                
                            usageType: res.data.usageType,                
                            user: res.data.user,
                            activeStatus: Boolean(res.data.activeStatus),
                            startDate: moment(res.data.startDate).format('YYYY-MM-DD'),
                            endDate: moment(res.data.endDate).format('YYYY-MM-DD')
                        })

                        setFetching(false)
                                                
                    })
                    .catch(error => console.error(error))            
        }        

    }, [users])

    
    const info = [        
        {
            name: 'code',
            value: voucher.code,
            label: t('tableHeads.code'),
            type: 'text'
        },
        {
            name: 'title',
            value: voucher.title,
            label: t('tableHeads.title'),
            type: 'text'
        },        
        {
            name: 'type',
            value: voucher.type,
            label: t('general.type'),
            type: 'select',
            options: [
                {
                    key: 'fixed',
                    value: 'Fixed'
                },
                {
                    key: 'percent',
                    value: 'Percent'
                }
            ]
        },
        {
            name: 'discount',
            value: voucher.discount,
            label: t('tableHeads.value'),
            type: 'number',
        },
        {
            name: 'usageType',
            value: voucher.usageType,
            label: t('tableHeads.usageType'),
            type: 'select',
            options: [
                {
                    key: 'USAGE_TYPE_BY_ANY_CLIENT_ANY_TIME',
                    value: t('voucher.anyClientAtAnyTime')
                },
                {
                    key: 'USAGE_ONE_TIME_BY_FIRST_CLIENT',
                    value: t('voucher.oneTimeByFirstClient')
                },
                {
                    key: 'USAGE_ONE_TIME_BY_SPECIFIC_CLIENT',
                    value: t('voucher.oneTimeBySpecificClient')
                }                
            ]
        },
        {   
            name: 'user',
            value: voucher.user,
            label: t('tableHeads.user'),
            type: 'select',
            show: showUser,
            options: users       

        },
        {
            name: 'startDate',
            value: voucher.startDate,
            label: t('Step1Frequency.startDateTitle'),
            type: 'date'
        },
        {
            name: 'endDate',
            value: voucher.endDate,
            label: t('voucher.endDate'),
            type: 'date'
        },
        {
            name: 'activeStatus',
            value: voucher.activeStatus,
            label: t('tableHeads.activeStatus'),
            type: 'switch'
        },
    ]

    useEffect(() => {
        if (voucher.usageType) {
            if(voucher.usageType === 'USAGE_ONE_TIME_BY_SPECIFIC_CLIENT'){
                setShowUser(true)
                if (!voucher.user) {
                    setVoucher( {
                        ...voucher,
                        user: currentVoucher.user,
                    })                    
                }
            }
            else {
                setShowUser(false)
                setVoucher({
                    code: voucher.code,
                    title: voucher.title,
                    type: voucher.type,                    
                    discount: voucher.discount,    
                    usageType: voucher.usageType,                
                    startDate: moment(voucher.startDate).format('YYYY-MM-DD'),
                    endDate: moment(voucher.endDate).format('YYYY-MM-DD'),
                    activeStatus: voucher.activeStatus
                })
            }            
        } else {
            setShowUser(false)
        }

    }, [voucher.usageType])    

    const openDialog = action => setDialog({
        open: true,
        dataAction: props.match.params.id,
        action
    })

    const closeDialog = () => setDialog({
        open: false,
        dataAction: -1,
        action: () => {}
    })

    const handleChange = input => {
        setVoucher({
            ...voucher,
            [input.target.name]: input.target.value
        })
    }

    const handleClick = async btn => {

        setFetching(true)

        Voucher.updateVoucher(props.match.params.id, {
            ...voucher,
            code: voucher.code,
            discount: parseFloat(voucher.discount),
            startDate: moment(voucher.startDate).utc().format(),
            endDate: moment(voucher.endDate).utc().format(),
            activeStatus: voucher.activeStatus ? 1 : 0
        })
            .then( (resp) => {                
                setFetching(false)
                if(!!resp)
                    history.push('/vouchers')
                else{
                    setVoucher( {
                        ...voucher,
                        code: currentVoucher.code,
                    })                
                    setError(true)                        
                    header.scrollIntoView()
                    setTimeout(() => {
                        setError(false)                        
                    }, 2000);
                }
            })

        closeDialog()
    }

    const handleSwitchChange = () => {
        setVoucher({
            ...voucher,
            activeStatus: !voucher.activeStatus
        })
    }

    const handleDelete = voucher_id => {
        closeDialog()
        setFetching(true)
        Voucher.deleteVoucher(voucher_id)
            .then(res => {
                setFetching(false)
                history.push('/vouchers')
            })
            .catch(error => console.error(error))
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: 'p-0',
                header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
            }}
            header={
                <div className='flex flex-1 w-full items-center justify-between'>
                    <div className='flex flex-col items-start max-w-full'>
                        <div className='flex items-center max-w-full'>
                            <div className='flex flex-col min-w-0'>
                                <Link to='/vouchers' style={{textDecoration: 'none',marginBottom: '10px'}}>
                                    <ArrowBack /> {t('general.back')}
                                </Link>
                                <Typography variant='h4'>
                                    {t('adminPanel.updateVoucher')}
                                </Typography>
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        {/* pagination */}
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        <Link to='/vouchers/create' style={{textDecoration: 'none'}}>
                            <Button className='whitespace-no-wrap' variant='contained'>
                                {t('adminPanel.createVoucher')}
                            </Button>
                        </Link>
                        {/* <Button 
                            style={{marginLeft: '10px'}} 
                            className='whitespace-no-wrap' 
                            color='secondary' 
                            variant='contained'
                            onClick={() => openDialog(handleDelete)}
                        >
                            {t('adminPanel.deleteVoucher')}
                        </Button> */}
                    </div>
                </div>
            }
            content={
                <Box id="headerTop" p={4} mt={2} mb={2}>
                    {
                        fetching && <MessageSnackbar variant='info' message='Loading Data...' />
                    }
                    {
                        error && <MessageSnackbar variant='error' message={t('Advices.notEqualVoucher')} />
                    }
                    <Grid
                        container
                        direction='row'
                        justify='flex-start'
                        alignItems='flex-start'
                        spacing={2}
                    >
                        {
                            info.length
                                ?   info.map(data => {
                                        if(data.type === 'select' && data.name === 'user') {
                                            return  <>
                                            {
                                                data.show &&
                                                <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                            <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                                {data.label}
                                                            </Typography>
                                                            <Select
                                                                value={data.value}
                                                                name={data.name}
                                                                variant='outlined'
                                                                fullWidth
                                                                onChange={handleChange}
                                                            >
                                                                <MenuItem value="">
                                                                    <em>None</em>
                                                                </MenuItem>
                                                                {
                                                                    data.options.map(option => <MenuItem  key={data.options.indexOf(option)} value={option.key}>{option.value}</MenuItem>)
                                                                }
                                                            </Select>
                                                        </Grid>

                                            }
                                            </>

                                        } else if(data.type === 'select'){
                                            return  <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                        <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                            {data.label}
                                                        </Typography>
                                                        <Select
                                                            value={data.value}
                                                            name={data.name}
                                                            variant='outlined'
                                                            fullWidth
                                                            onChange={handleChange}
                                                        >
                                                            <MenuItem value="">
                                                                <em>None</em>
                                                            </MenuItem>
                                                            {
                                                                data.options.map(option => <MenuItem  key={data.options.indexOf(option)} value={option.key}>{option.value}</MenuItem>)
                                                            }
                                                        </Select>
                                                    </Grid>
                                        }else if(data.type === 'switch'){
                                            return  <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                        <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                            {data.label}
                                                        </Typography>
                                                        <Switch
                                                            checked={data.value}
                                                            onChange={handleSwitchChange}
                                                            name={data.name}
                                                        />
                                                    </Grid>
                                        }else{
                                            return  <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                        <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                            {data.label}
                                                        </Typography>
                                                        <TextField
                                                            type={data.type}
                                                            name={data.name}
                                                            value={data.value}
                                                            variant='outlined'
                                                            onChange={handleChange}
                                                            fullWidth
                                                        />
                                                    </Grid>
                                        }
                                    })
                                :   <Grid />
                        }
                        <Grid item xs={12} md={12} sm={12}>
                            <Button
                                variant='contained'
                                color='primary'
                                onClick={() => openDialog(handleClick)}
                                fullWidth
                            >
                                {t('general.save')}
                            </Button>
                        </Grid>
                    </Grid>
                    <GenericPrompt 
                        title={t('adminPanel.updateVoucher')}
                        message={t('general.areYouSureToDoThis')}
                        close={closeDialog}
                        action={dialog.action}
                        data={dialog}
                    />
                </Box>
            }
            innerScroll
        >
        </FusePageCarded>
    )
}

export default Update