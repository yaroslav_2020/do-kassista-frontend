import React , { useState , useEffect } from 'react'
import {default as FusePageCarded} from '@fuse/core/FusePageCarded'
import { Link , useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import * as Voucher from '../../utils/Voucher'

import MessageSnackbar from '../utils/MessageSnackbar'
import GenericPrompt from '../utils/GenericPrompt'
import moment from 'moment'


import {
    Button,
    Typography,
    Grid,
    Box
} from '@material-ui/core'

import {
    ArrowBack
} from '@material-ui/icons'
import { getUserInfo } from 'app/utils/User'

const Item = props => {

    const history = useHistory()

    const [fetching,setFetching] = useState(true)

    const [dialog,setDialog] = useState({
        open: false,
        dataAction: -1
    })

    const {t,i18n} = useTranslation()

    const [voucher,setVoucher] = useState({
        id: 0,
        title: '',
        code: '',
        type: '',
        discount: 0,
        use: '',
        user: '',                                
        activeStatus: '',
        usageType: '',
        usageCount: '',
        startDate: '',
        endDate: '',
        readyToBeUsed: ''
        // canByUsedByClientAnyTime: '',
        // canBeUsedOnlyOneTime: '',
        // usedAtLeastOneTime: '',
        // active: '',
        // notActive: ''
    })
                                
    useEffect(() => {
        if(props.match.params.id){
            Voucher.getVoucherItem(props.match.params.id)
                .then(res => {

                    if (!res.data.user) {
                        setVoucher({
                            ...voucher,
                            id: res.data.id,
                            title: res.data.title,
                            code: res.data.code,                        
                            type: res.data.type,
                            discount: res.data.discount,
                            use: res.data.usageType,                        
                            activeStatus: res.data.activeStatus,
                            usageType: res.data.usageType,
                            usageCount: res.data.usageCount,
                            startDate: res.data.startDate,
                            endDate: res.data.endDate,
                            readyToBeUsed: res.data.readyToBeUsed
                            // canByUsedByClientAnyTime: res.data.canByUsedByClientAnyTime ? t('general.yes') : t('general.Not'),
                            // canBeUsedOnlyOneTime: res.data.canBeUsedOnlyOneTime ? t('general.yes') : t('general.Not'),
                            // usedAtLeastOneTime: res.data.usedAtLeastOneTime,
                            // active: res.data.active ? t('general.yes') : t('general.not'),
                            // notActive: res.data.notActive ? t('general.yes') : t('general.not')
                        }) 
                        setFetching(false)
                    } else {
                        getUserInfo(res.data.user)
                        .then( ({username}) => {                            
                            setVoucher({
                                ...voucher,
                                id: res.data.id,
                                title: res.data.title,
                                code: res.data.code,                        
                                type: res.data.type,
                                discount: res.data.discount,
                                use: res.data.usageType,                        
                                user: username,
                                activeStatus: res.data.activeStatus,
                                usageType: res.data.usageType,
                                usageCount: res.data.usageCount,
                                startDate: res.data.startDate,
                                endDate: res.data.endDate,
                                readyToBeUsed: res.data.readyToBeUsed
                                // canByUsedByClientAnyTime: res.data.canByUsedByClientAnyTime ? t('general.yes') : t('general.Not'),
                                // canBeUsedOnlyOneTime: res.data.canBeUsedOnlyOneTime ? t('general.yes') : t('general.Not'),
                                // usedAtLeastOneTime: res.data.usedAtLeastOneTime,
                            })
                            setFetching(false)
                        })
                    }

                    
                })
                .catch(error => console.error(error))
        }
    },[])

    const info = [
        {
            name: 'title',
            value: voucher.title,
            label: t('tableHeads.title')
        },
        {
            name: 'code',
            value: voucher.code,
            label: t('tableHeads.code')
        },
        {
            name: 'type',
            value: t('general.' + voucher.type),
            label: t('general.type')
        },
        {
            name: 'amount',
            value: voucher.discount,
            label: t('tableHeads.value')
        },
        {
            name: 'usageType',
            value: t("voucher." + voucher.usageType),
            label: t('tableHeads.usageType')
        },
        {        
            name: 'user',
            value: voucher.user,
            label: t('tableHeads.user')
        },        
        {
            name: 'activeStatus',
            value: (voucher.activeStatus) ? t('general.active') : t('general.notActive'),
            label: t('tableHeads.activeStatus')
        },
        {
            name: 'usageCount',
            value: voucher.usageCount,
            label: t('tableHeads.usageCount')
        },    
        // {
        //     name: 'canByUsedByClientAnyTime',
        //     value: (voucher.canByUsedByClientAnyTime) ? t('general.yes') : t('general.Not'),
        //     label: t('voucher.canByUsedByClientAnyTime')
        // },
        // {
        //     name: 'canBeUsedOnlyOneTime',
        //     value: (voucher.canBeUsedOnlyOneTime) ? t('general.yes') : t('general.Not'),
        //     label: t('voucher.canBeUsedOnlyOneTime')
        // },
        // {
        //     name: 'usedAtLeastOneTime',
        //     value: (voucher.usedAtLeastOneTime) ? t('general.yes') : t('general.Not'),
        //     label: t('voucher.usedAtLeastOneTime')
        // },        
        {
            name: 'endDate',
            value: moment(voucher.endDate).format('DD-MM-YYYY'),
            label: t('voucher.endDate')            
        },
        {
            name: 'readyToBeUse',
            value: (voucher.readyToBeUsed) ? t('general.yes') : t('general.Not'),
            label: t('tableHeads.readyToBeUsed')
        },        
    ]

    const openDialog = () => setDialog({
        open: true,
        dataAction: props.match.params.id
    })

    const closeDialog = () => setDialog({
        open: false,
        dataAction: -1
    })

    const handleDelete = voucher_id => {
        closeDialog()
        setFetching(true)
        Voucher.deleteVoucher(voucher_id)
            .then(res => {
                setFetching(false)
                history.push('/vouchers')
            })
            .catch(error => console.error(error))
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: 'p-0',
                header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
            }}
            header={
                <div className='flex flex-1 w-full items-center justify-between'>
                    <div className='flex flex-col items-start max-w-full'>
                        <div className='flex items-center max-w-full'>
                            <div className='flex flex-col min-w-0'>
                                <Link to='/vouchers' style={{textDecoration: 'none',marginBottom: '10px'}}>
                                    <ArrowBack /> {t('general.back')}
                                </Link>
                                <Typography variant='h4'>
                                    { voucher.title }
                                </Typography>
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        {/* pagination */}
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        <Link to={`/vouchers/update/${props.match.params.id}`} style={{textDecoration: 'none'}}>
                            <Button className='whitespace-no-wrap' variant='contained'>
                                {t('adminPanel.updateVoucher')}
                            </Button>
                        </Link>
                        {/* <Button 
                            style={{marginLeft: '10px'}} 
                            className='whitespace-no-wrap' 
                            color='secondary' 
                            variant='contained'
                            onClick={openDialog}
                        >
                            {t('adminPanel.deleteVoucher')}
                        </Button> */}
                    </div>
                </div>
            }
            content={
                <Box p={4} mt={2} mb={2}>
                    {
                        fetching && <MessageSnackbar variant='info' message='Loading Data...' />
                    }
                    {
                        !fetching &&    <Grid
                                            container
                                            direction='row'
                                            justify='flex-start'
                                            alignItems='flex-start'
                                            spacing={2}
                                        >                                            
                                            {
                                                info.length
                                                    ?   info.map(data => (

                                                        (data.name === 'user')
                                                        ?   (data.value) && 
                                                            <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                                <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                                    {data.label}
                                                                </Typography>
                                                                <Typography variant='body1'>
                                                                    {data.value}
                                                                </Typography>
                                                            </Grid>
                                                        :   <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                                <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                                    {data.label}
                                                                </Typography>
                                                                <Typography variant='body1'>
                                                                    {data.value}
                                                                </Typography>
                                                            </Grid>
                                                    ))
                                                    :   <Grid />
                                            }
                                        </Grid>
                    }
                    <GenericPrompt 
                        title={t('adminPanel.deleteVoucher')}
                        message={t('general.areYouSureToDoThis')}
                        close={closeDialog}
                        action={handleDelete}
                        data={dialog}
                    />
                </Box>
            }
            innerScroll
        >
        </FusePageCarded>
    )
}

export default Item