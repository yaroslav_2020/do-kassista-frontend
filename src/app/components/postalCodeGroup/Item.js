import React , { useState , useEffect } from 'react'
import {default as FusePageCarded} from '@fuse/core/FusePageCarded'
import { Link , useHistory } from 'react-router-dom'

import * as PostalCodeGroups from '../../utils/PostalCodeGroups'

import { useTranslation } from 'react-i18next'

import MessageSnackbar from '../utils/MessageSnackbar'
import GenericPrompt from '../utils/GenericPrompt'

import {
    Button,
    Typography,
    Grid,
    Box,
} from '@material-ui/core'

import {
    ArrowBack
} from '@material-ui/icons'

const Item = props => {

    const history = useHistory()

    const [fetching,setFetching] = useState(false)

    const [dialog,setDialog] = useState({
        open: false,
        dataAction: -1
    })

    const {t,i18n} = useTranslation()

    const [postalCodeGroups,setPostalCodeGroups] = useState({
                                title: '',
                                preferredPrice: 0,
                                currencyPreferredPrice: '',
                            })

    useEffect(() => {
        if(props.match.params.id){
            setFetching(true)
            PostalCodeGroups.getPostalCodeGroupsId(props.match.params.id)
                .then(res => {
                    setPostalCodeGroups({
                        ...postalCodeGroups,
                        title: res.title,
                        preferredPrice: res.preferredPrice,
                        currencyPreferredPrice: res.currencyPreferredPrice,
                    })
                    setFetching(false)
                })
                .catch(error => console.error(error))
        }
    },[])

    const info = [
        {
            name: 'title',
            value: postalCodeGroups.title,
            label: t('tableHeads.title'),
            type: 'text'
        },
        {
            name: 'preferredPrice',
            value: postalCodeGroups.preferredPrice,
            label: t('tableHeads.preferedPrice'),
            type: 'number',
        },
        {
            name: 'currencyPreferredPrice',
            value: postalCodeGroups.currencyPreferredPrice,
            label: t('tableHeads.currency'),
            type: 'text',
        },
    ]

    const openDialog = () => setDialog({
        open: true,
        dataAction: props.match.params.id
    })

    const closeDialog = () => setDialog({
        open: false,
        dataAction: -1
    })

    const handleDelete = postal_code_groups_id => {
        closeDialog()
        setFetching(true)
        PostalCodeGroups.deletePostalCodeGroups(postal_code_groups_id)
            .then(res => {
                setFetching(false)
                history.push('/postal_code_groups')
            })
            .catch(error => console.error(error)) 
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: 'p-0',
                header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
            }}
            header={
                <div className='flex flex-1 w-full items-center justify-between'>
                    <div className='flex flex-col items-start max-w-full'>
                        <div className='flex items-center max-w-full'>
                            <div className='flex flex-col min-w-0'>
                                <Link to='/postal_code_groups' style={{textDecoration: 'none',marginBottom: '10px'}}>
                                    <ArrowBack /> {t('general.back')}
                                </Link>
                                <Typography variant='h4'>
                                    { postalCodeGroups.title }
                                </Typography>
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        {/* pagination */}
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        <Link to={`/postal_code_groups/update/${props.match.params.id}`} style={{textDecoration: 'none'}}>
                            <Button className='whitespace-no-wrap' variant='contained'>
                                {t('adminPanel.updatePostalCodeGroups')}
                            </Button>
                        </Link>
                        <Button 
                            style={{marginLeft: '10px'}} 
                            className='whitespace-no-wrap' 
                            color='secondary' 
                            variant='contained'
                            onClick={openDialog}
                        >
                            {t('adminPanel.deletePostalCodeGroups')}
                        </Button>
                    </div>
                </div>
            }
            content={
                <Box p={4} mt={2} mb={2}>
                    {
                        fetching && <MessageSnackbar variant='info' message='Loading Data...' />
                    }
                    {
                        !fetching && <Grid
                                        container
                                        direction='row'
                                        justify='flex-start'
                                        alignItems='flex-start'
                                        spacing={2}
                                    >
                                        {
                                            info.length
                                                ?   info.map(data => <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                                            <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                                                {data.label}
                                                                            </Typography>
                                                                            <Typography variant='body1'>
                                                                                {data.value}
                                                                            </Typography>
                                                                        </Grid>)
                                                :   <Grid />
                                        }
                                    </Grid>
                    }
                    <GenericPrompt 
                        title={t('adminPanel.deletePostalCodeGroups')}
                        message={t('general.areYouSureToDoThis')}
                        close={closeDialog}
                        action={handleDelete}
                        data={dialog}
                    />
                </Box>
            }
            innerScroll
        >
        </FusePageCarded>
    )
}

export default Item