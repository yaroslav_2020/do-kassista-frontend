import React, { useEffect, useState } from 'react'

import { Button, CircularProgress, FormControl, Grid, InputBase, makeStyles, NativeSelect, Typography, withStyles } from '@material-ui/core';
import { updateOfferFullSchedule } from 'app/utils/OfferFullSchedule';
import { useTranslation } from 'react-i18next'
import moment from 'moment'

import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import { connect } from 'react-redux';

import GenericSnackbar from 'app/utils/GenericSnackbar'

const BootstrapInput = withStyles((theme) => ({
    root: {
      'label + &': {
        marginTop: theme.spacing(3),
      },
    },
    input: {
      borderRadius: 4,
      position: 'relative',
      backgroundColor: theme.palette.background.paper,
      border: '1px solid #ced4da',
      fontSize: 16,
      padding: '10px 26px 10px 12px',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      '&:focus': {
        borderRadius: 4,
        borderColor: '#80bdff',
        boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      },
    },
  }))(InputBase);

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    loaderCont: {
        display: 'flex',
        width: '100vw',
        height: '100vh',
        position: 'fixed',
        background: 'rgba(0,0,0,.1)',
        zIndex: '1000',
        justifyContent: 'center',
        boxSizing: 'border-box',
        alignItems: 'center',
        top: '0',
        left: '0'
    },
    button: {
        margin: theme.spacing(1),
    },
}));

const OfferStatus = ({dateTimeStart, id, clientOfferStatus, user_role, workerOfferStatus, paymentStatus}) => {
    const classes = useStyles();

    const {t} = useTranslation();

    const [fetching, setFetching] = useState(false)
    
    const [snackbar,setSnackbar] = useState({
        open: false,
        message: '', 
        severity: 'success',
    })

    const handleCloseSnackbar = ( value ) => setSnackbar( value );

    const [status, setStatus] = useState({})
    const [currentStatus, setCurrentStatus] = useState({})


    useEffect(() => {
        if( user_role[0] === 'ROLE_CLIENT') { setStatus(clientOfferStatus); setCurrentStatus(clientOfferStatus) }
        else if( user_role[0] === 'ROLE_EMPLOYEE_WORKER') { setStatus(workerOfferStatus); setCurrentStatus(workerOfferStatus) }    
        else if( user_role[0] === 'ROLE_INDEPENDENT_WORKER') { setStatus(workerOfferStatus); setCurrentStatus(workerOfferStatus) }    
    }, [])


    const handleChange = (e) => {
        setStatus(e.target.value);
    };

    const handleStatusUpdate = (id, status, e) => {        
        e.preventDefault();
        setFetching(true)

        if (status !== currentStatus && user_role[0] === 'ROLE_CLIENT') {            
            const update = {
                clientOfferStatus: status,
                dateTimeUpdated: moment()
            }
        
            updateOfferFullSchedule(id, update)
                .finally(() => {
                    setFetching(false)
                    setCurrentStatus(status)
                    setSnackbar({
                        open: true,
                        message: t('general.updateData'), 
                        severity: 'success',
                    })
                })            
        } else if ( status !== currentStatus && (user_role[0] === 'ROLE_EMPLOYEE_WORKER' || user_role[0] === 'ROLE_INDEPENDENT_WORKER')) {
            const update = {
                workerOfferStatus: status,
                dateTimeUpdated: moment()           
            }
            updateOfferFullSchedule(id, update)
                .finally(() => {
                    setFetching(false)
                    setCurrentStatus(status)
                    setSnackbar({
                        open: true,
                        message: t('general.updateData'), 
                        severity: 'success',
                    })
                })            
     
        }else {
            setFetching(false)            
        }
    }

    return (
    <>
        {
            fetching && 
            <div className={classes.loaderCont} >
                <CircularProgress />
            </div>
        }
        <GenericSnackbar 
            handleClose={handleCloseSnackbar} 
            {...snackbar}
        />
        <Grid item md={2} sm={12} >
            <Typography variant='subtitle1' gutterBottom>
                { t( `Schedule.days.${ moment( dateTimeStart ).format('dddd').toLowerCase() }`) }
            </Typography>
        </Grid>
        <Grid item md={2} sm={12} >
            <Typography variant='subtitle1' gutterBottom>
                { moment(dateTimeStart).format("DD-MM-YYYY") }
            </Typography>
        </Grid>

        <Grid item md={3} sm={12} >
        {
            moment().isSame(dateTimeStart, 'day')&&
            <FormControl className={classes.formControl}>
                {/* <InputLabel htmlFor="customized-select">Status</InputLabel> */}
                <NativeSelect 
                    id="customized-select"
                    value={status}
                    input={<BootstrapInput />}                
                    onChange={handleChange}                            
                >
                    {/* <option aria-label="None" >{}</option> */}
                    <option value ='STATUS_PENDING'>{t('offerStatus.STATUS_PENDING')}</option>
                    <option value ='STATUS_AMOUNT_BLOCKED'>{t('offerStatus.STATUS_AMOUNT_BLOCKED')}</option>
                    <option value ='STATUS_IN_PROGRESS'>{t('offerStatus.STATUS_IN_PROGRESS')}</option>
                    <option value ='STATUS_COMPLETED'>{t('offerStatus.STATUS_COMPLETED')}</option>
                    <option value ='STATUS_CANCELED'>{t('offerStatus.STATUS_CANCELED')}</option>
                    <option value ='STATUS_WITH_ISSUES'>{t('offerStatus.STATUS_WITH_ISSUES')}</option>                    
                </NativeSelect>
            </FormControl>                                            
        }
        {
            moment().isAfter(dateTimeStart, 'day')&&
            <Typography variant='subtitle1' gutterBottom>
                {t(`offerStatus.${clientOfferStatus}`)} 
            </Typography>

        }
        </Grid>                          

        <Grid item md={2} sm={12}>
            <Typography variant='subtitle1' gutterBottom>
                {t(`offerStatus.${paymentStatus}`)}
            </Typography>
        </Grid>

        <Grid item md={3} sm={12} >
        {
            moment().isSame(dateTimeStart, 'day')&&
            <Button
                variant="contained"
                color="default"
                className={classes.button}
                onClick={(e) => handleStatusUpdate(id, status, e)}
            >
                {t('general.update')}
            </Button>                                        
        }
        </Grid>
    </>
    )
}

const mapState = state => ({
    user_role: state.auth.user.role
})

export default connect(mapState)(OfferStatus)
