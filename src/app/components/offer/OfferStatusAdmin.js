import React, { useState } from 'react'

import { Button, CircularProgress, FormControl, Grid, InputBase, makeStyles, NativeSelect, Typography, withStyles } from '@material-ui/core';
import { updateOfferFullSchedule } from 'app/utils/OfferFullSchedule';
import { useTranslation } from 'react-i18next'
import moment from 'moment'

import GenericSnackbar from 'app/utils/GenericSnackbar'

const BootstrapInput = withStyles((theme) => ({
    root: {
      'label + &': {
        marginTop: theme.spacing(3),
      },
    },
    input: {
      borderRadius: 4,
      position: 'relative',
      backgroundColor: theme.palette.background.paper,
      border: '1px solid #ced4da',
      fontSize: 16,
      padding: '10px 26px 10px 12px',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      '&:focus': {
        borderRadius: 4,
        borderColor: '#80bdff',
        boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      },
    },
  }))(InputBase);

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    loaderCont: {
        display: 'flex',
        width: '100vw',
        height: '100vh',
        position: 'fixed',
        background: 'rgba(0,0,0,.1)',
        zIndex: '1000',
        justifyContent: 'center',
        boxSizing: 'border-box',
        alignItems: 'center',
        top: '0',
        left: '0'
    },
    button: {
        margin: theme.spacing(1),
    },
    snack: {
        position: 'fixed',
        left:'100px',
        top:'200px'
    }
}));

const OfferStatusAdmin = ({dateTimeStart, id, clientOfferStatus, workerOfferStatus, paymentStatus, update = false}) => {
    const classes = useStyles();

    const {t} = useTranslation();

    const [fetching, setFetching] = useState(false)
    
    const [snackbar,setSnackbar] = useState({
        open: false,
        message: '', 
        severity: 'success',
    })

    const handleCloseSnackbar = ( value ) => setSnackbar( value );

    const [clientState, setClientState] = useState(clientOfferStatus)
    const [workerState, setWorkerState] = useState(workerOfferStatus)
    const [currentStatusClient, setCurrentStatusClient] = useState(clientOfferStatus)
    const [currentStatusWorker, setCurrentStatusWorker] = useState(workerOfferStatus)

    const handleChange = (e) => {
        if (e.target.name == 'client') {
            setClientState(e.target.value);            
        } else {
            setWorkerState(e.target.value);               
        }
    };

    const handleStatusUpdate = (id, clientState, workerState, e) => {        
        e.preventDefault();
        setFetching(true)

        if (clientState !== currentStatusClient) {            
            const update = {
                clientOfferStatus: clientState,
                dateTimeUpdated: moment(dateTimeStart)
            }
        
            updateOfferFullSchedule(id, update)
                .finally(() => {
                    setFetching(false)
                    setCurrentStatusClient(clientState)
                    setSnackbar({
                        open: true,
                        message: t('general.updateData'), 
                        severity: 'success',
                    })
                })            
        } else if ( workerState !== currentStatusWorker ) {
            const update = {
                workerOfferStatus: workerState,
                dateTimeUpdated: moment(dateTimeStart)           
            }
            updateOfferFullSchedule(id, update)
                .finally(() => {
                    setFetching(false)
                    setCurrentStatusWorker(workerState)
                    setSnackbar({
                        open: true,
                        message: t('general.updateData'), 
                        severity: 'success',
                    })
                })            
     
        }else {
            setFetching(false)            
        }
    }

    return (
    <>
        {
            fetching && 
            <div className={classes.loaderCont} >
                <CircularProgress />
            </div>
        }
        <GenericSnackbar 
            className={classes.snack}
            handleClose={handleCloseSnackbar} 
            {...snackbar}
        />
        <Grid item md={2} sm={12} >
            <Typography variant='subtitle1' gutterBottom>
                { t( `Schedule.days.${ moment( dateTimeStart ).format('dddd').toLowerCase() }`) }
            </Typography>
        </Grid>
        <Grid item md={2} sm={12} >
            <Typography variant='subtitle1' gutterBottom>
                { moment(dateTimeStart).format("DD-MM-YYYY") }
            </Typography>
        </Grid>

        <Grid item md={2} sm={12} >
        {
            update&&
            <FormControl className={classes.formControl}>
                {/* <InputLabel htmlFor="customized-select">Status</InputLabel> */}
                <NativeSelect 
                    id="customized-select"
                    value={clientState}
                    name='client'
                    input={<BootstrapInput />}                
                    onChange={handleChange}                            
                >
                    {/* <option aria-label="None" >{}</option> */}
                    <option value ='STATUS_PENDING'>{t('offerStatus.STATUS_PENDING')}</option>
                    <option value ='STATUS_AMOUNT_BLOCKED'>{t('offerStatus.STATUS_AMOUNT_BLOCKED')}</option>
                    <option value ='STATUS_IN_PROGRESS'>{t('offerStatus.STATUS_IN_PROGRESS')}</option>
                    <option value ='STATUS_COMPLETED'>{t('offerStatus.STATUS_COMPLETED')}</option>
                    <option value ='STATUS_CANCELED'>{t('offerStatus.STATUS_CANCELED')}</option>
                    <option value ='STATUS_WITH_ISSUES'>{t('offerStatus.STATUS_WITH_ISSUES')}</option>                    
                </NativeSelect>
            </FormControl>                                            
        }
        {
            !update&&
            <Typography variant='subtitle2' gutterBottom>
                {t(`offerStatus.${clientOfferStatus}`)} 
            </Typography>

        }
        </Grid>       

        <Grid item md={2} sm={12} >
        {
            update&&
            <FormControl className={classes.formControl}>
                {/* <InputLabel htmlFor="customized-select">Status</InputLabel> */}
                <NativeSelect 
                    id="customized-select"
                    value={workerState}
                    name="worker"
                    input={<BootstrapInput />}                
                    onChange={handleChange}                            
                >
                    {/* <option aria-label="None" >{}</option> */}
                    <option value ='STATUS_PENDING'>{t('offerStatus.STATUS_PENDING')}</option>
                    <option value ='STATUS_AMOUNT_BLOCKED'>{t('offerStatus.STATUS_AMOUNT_BLOCKED')}</option>
                    <option value ='STATUS_IN_PROGRESS'>{t('offerStatus.STATUS_IN_PROGRESS')}</option>
                    <option value ='STATUS_COMPLETED'>{t('offerStatus.STATUS_COMPLETED')}</option>
                    <option value ='STATUS_CANCELED'>{t('offerStatus.STATUS_CANCELED')}</option>
                    <option value ='STATUS_WITH_ISSUES'>{t('offerStatus.STATUS_WITH_ISSUES')}</option>                    
                </NativeSelect>
            </FormControl>                                            
        }
        {
            !update&&
            <Typography variant='subtitle2' gutterBottom>
                {t(`offerStatus.${workerOfferStatus}`)} 
            </Typography>

        }
        </Grid>                                                     

        <Grid item md={2} sm={12}>
            <Typography variant='subtitle2' gutterBottom>
                {t(`offerStatus.${paymentStatus}`)}
            </Typography>
        </Grid>

        <Grid item md={2} sm={12} >
        {
            update&&
            <Button
                variant="contained"
                color="default"
                className={classes.button}
                onClick={(e) => handleStatusUpdate(id, clientState, workerState, e)}
            >
                {t('general.update')}
            </Button>                                        
        }
        </Grid>
    </>
    )
}

export default (OfferStatusAdmin)
