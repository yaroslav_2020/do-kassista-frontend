import React,{ useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import * as FetchActions from '../../store/actions/FetchActions'

import {
    Grid,
    Typography,
    TableRow,
    useMediaQuery
} from '@material-ui/core'

import { getOffers } from '../../utils/User'

import { makeStyles } from '@material-ui/core/styles'

import RightArrowIcon from '../../assets/images/icons/rightArrowIcon.svg'
import LeftArrowIcon from '../../assets/images/icons/leftArrowIcon.svg'

const useStyles = makeStyles({
    container: {
        background: '#fff',
        borderRadius: '13px',
        boxShadow: '0px 5px 10px rgba(0,0,0,.12)',
    },
    contTitle: {
        background: '#fff',
        padding: '35px'
    },
    table: {
        minWidth: '100%'
    },
    tr: {
        borderBottom: '1px solid #DBDBDB',
        cursor: 'pointer',
        // '& td:last-child':{
        //     paddingRight: '0px'
        // },
        // '& th:last-child':{
        //     paddingRight: '120px'
        // }
    },
    tableCont: {
        width: '100%',
        overflowX: 'scroll'
    },
    paginationButton: {
        width: '42px',
        height: '42px',
        background: '#0097F6',
        color: '#fff',
        borderRadius: '6px',
        margin: '0 9px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    paginationPage: {
        borderRadius: '6px',
        border: '1px solid #DBDBDB',
        width: '42px',
        height: '42px',
        fontSize: '20px',
        fontWeight: '400',
        color: '#0097F6',
        textAlign: 'center',
        lineHeight: '42px',
        margin: '0 4.5px'
    },
    paginationTotalPages: {
        fontSize: '18px',
        fontWeight: '400',
        color: '#0097F6',
        height: '42px',
        textAlign: 'center',
        lineHeight: '42px',
        margin: '0 4.5px'
    },
    tdFlex: {
        display: 'flex', 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    firstTh: {
        padding: '20px 0 20px 20px'
    },
    middleTh: {
        padding: '20px 0 20px 0'
    },
    lastTh: {
        padding: '20px 20px 20px 0'
    },
    generalCell: {
        width: '100%',
        height: '70px',
        background: '#FCFCFC',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        padding: '0 24px',
    },
    firstCell: {
        borderRadius: '10px 0 0 10px'
    },
    lastCell: {
        borderRadius: '0 10px 10px 0'
    },
    link: {
        '& a': {
            width: '100px',
            height: '42px',
            borderRadius: '9px',
            border: '1px solid #0097F6',
            color: '#0097F6',
            fontSize: '12px',
            fontWeight: '500',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            textDecoration: 'none'
        },
        '& a:hover': {
            textDecoration: 'none'
        }
    }
})

const desktopStyles = makeStyles({
    title: {
        fontSize: '25px',
        fontWeight: 'bold',
        color: '#000'
    },
    thBody: {
        fontSize: '24px',
        fontWeight: '500',
        color: '#2F2F2F',
    },
    tdBody: {
        fontSize: '22px',
        fontWeight: '400',
        color: '#000',
        textAlign: 'center',
        padding: '24px 0',
    },
    pagination: {
        width: '100%',
        height: '95px',
        display: 'flex',
        justifyContent: 'flex-end',
        paddingRight: '74px',
        alignItems: 'center'
    },
})

const mobileStyles = makeStyles({
    title: {
        fontSize: '20px',
        fontWeight: 'bold',
        color: '#272727'
    },
    thBody: {
        fontSize: '18px',
        fontWeight: '500',
        color: '#2F2F2F',
    },
    tdBody: {
        fontSize: '16px',
        fontWeight: '400',
        color: '#272727',
        textAlign: 'center',
        padding: '24px',
    },
    pagination: {
        width: '100%',
        height: '95px',
        display: 'flex',
        justifyContent: 'flex-end',
        paddingRight: '19px',
        alignItems: 'center'
    },
})

const List = props => {

    const {t} = useTranslation()

    const handleDate = ( date ) => {
        let dt = new Date(date)
        let months = ['january',
                    'february',
                    'march',
                    'april',
                    'may',
                    'june',
                    'july',
                    'August',
                    'September',
                    'October',
                    'November',
                    'December']
        
        let month = t(`Schedule.shortMonth.${months[dt.getMonth()]}`)
    
        return `${month}${dt.getDate()}, ${dt.getFullYear()}`
    }

    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const matches = useMediaQuery('(max-width:600px)')

    const [offers,setOffers] = useState([])
    const [offersToShow,setOffersToShow] = useState({
        pos: 0,
        page: 1,
        offers: []
    })

    // const history = useHistory()

    useEffect(() => {
        (async () => {
            props.fetching();
            try {
                const offerFetch = await getOffers( props.user_id );

                // console.log( 'respuesta del await ', offerFetch.data );
                
                if ( !!offerFetch.data['hydra:member'].length ) {
                    const offers = offerFetch.data['hydra:member']
                        // .filter( offer => !offer.deleted )
                        .map( offer => ({
                            iri: offer['@id'],
                            id: offer.id,
                            company: offer.company !== null ? offer.company.name : '',
                            status: offer.status,
                            total: offer.pricePerService,
                            createdDate: offer.timestapableCreated
                        }))
                        .sort( (a, b) => {
                            if ( a.id > b.id ) return -1                        
                            if ( a.id < b.id ) return 1
                            return 0
                        })
                    setOffers( offers );
                    setOffersToShow({
                        ...offersToShow,
                        pos: [0,8],
                        page: 1,
                        offers: [...offers.slice(0,8)]
                    })
                } else {
                    console.log('respuesta de offers: no se encontraron offertas cargadas', offersToShow );
                }

                props.success()      

            } catch (error) {
                props.error()
                console.log( error );
            }
        })();
    },[ props.user_id ])

    // useEffect(() => {
    //     console.log('respuesta de offers', offersToShow );

    // }, [ offersToShow ])


    const prevPage = () => {
        if((offersToShow.page - 1) >= 1)
            setOffersToShow({
                ...offersToShow,
                pos: [offersToShow.pos[0] - 8,offersToShow.pos[1] - 8],
                page: offersToShow.page - 1,
                offers: [...offers.slice(offersToShow.pos[0] - 8,offersToShow.pos[1] - 8)]
            })
    }

    const nextPage = () => {
        if((offersToShow.page + 1) <= Math.ceil(offers.length / 8))
            setOffersToShow({
                ...offersToShow,
                pos: [offersToShow.pos[0] + 8,offersToShow.pos[1] + 8],
                page: offersToShow.page + 1,
                offers: [...offers.slice(offersToShow.pos[0] + 8,offersToShow.pos[1] + 8)]
            })
    }

    const statusStyles = status => {
        switch(status){
            case 'pending': 
                return {
                    message: 'PENDIENTE',
                    style: {
                        desktop: {
                            color: '#FFC200',
                            background: '#FFF2D3',
                            borderRadius: '50px',
                            padding: '4px 17px',
                            fontSize: '18px',
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            maxWidth: '150px',
                        },
                        mobile: {
                            color: '#FFC200',
                            background: '#FFF2D3',
                            borderRadius: '50px',
                            padding: '4px 17px',
                            fontSize: '16px',
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            maxWidth: '150px',
                        }
                    }
                }
            default:
                return {
                    message: '',
                    style: {
                        desktop: {
                            color: '#000',
                            background: '#fff',
                            borderRadius: '50px',
                            padding: '4px 17px',
                            fontSize: '18px',
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            maxWidth: '150px',
                        },
                        mobile: {
                            color: '#000',
                            background: '#fff',
                            borderRadius: '50px',
                            padding: '4px 17px',
                            fontSize: '16px',
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            maxWidth: '150px',
                        }
                    }
                }
        }
    }

    return (
        <Grid item xs={12} md={8} sm={6}>
            <div className={classes.contTitle}>
                <Typography className={matches ? mobileClasses.title : desktopClasses.title} gutterBottom>
                    {t('columnMenu.myRequests')}
                </Typography>
            </div>

            <div className={classes.container} >
                <div className={classes.tableCont} >
                    <table className={classes.table} >
                        <thead >
                            <tr className={classes.tr} >
                                <th className={`${matches ? mobileClasses.thBody : desktopClasses.thBody} ${classes.firstTh}`} ><p className={`${classes.generalCell} ${classes.firstCell}`} >{t('general.order')}#</p></th>
                                <th className={`${matches ? mobileClasses.thBody : desktopClasses.thBody} ${classes.middleTh}`} ><p className={classes.generalCell} >{t('general.company')}</p></th>
                                <th className={`${matches ? mobileClasses.thBody : desktopClasses.thBody} ${classes.middleTh}`} ><p className={classes.generalCell} >{t('general.state')}</p></th>
                                <th className={`${matches ? mobileClasses.thBody : desktopClasses.thBody} ${classes.middleTh}`} ><p className={classes.generalCell} >{t('general.total')}</p></th>
                                <th className={`${matches ? mobileClasses.thBody : desktopClasses.thBody} ${classes.middleTh}`} ><p className={classes.generalCell} >{t('general.created')}</p></th>
                                <th className={`${matches ? mobileClasses.thBody : desktopClasses.thBody} ${classes.lastTh}`} ><p className={`${classes.generalCell} ${classes.lastCell}`} ></p></th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                offersToShow.offers.length
                                    ?   offersToShow.offers.map(offer => {
                                            let statusStyle = statusStyles(offer.status)    

                                            return <tr key={offersToShow.offers.indexOf(offer)} className={classes.tr}>
                                                        <td className={matches ? mobileClasses.tdBody : desktopClasses.tdBody} >{offer.id}</td>
                                                        <td className={matches ? mobileClasses.tdBody : desktopClasses.tdBody} >{offer.company}</td>
                                                        <td className={`${matches ? mobileClasses.tdBody : desktopClasses.tdBody} ${classes.tdFlex}`} ><p style={matches ? statusStyle.style.mobile : statusStyle.style.desktop} >{statusStyle.message}</p></td>
                                                        <td className={matches ? mobileClasses.tdBody : desktopClasses.tdBody} >{offer.total} &euro;</td>
                                                        <td className={matches ? mobileClasses.tdBody : desktopClasses.tdBody} >{handleDate(offer.createdDate)}</td>     
                                                        <td className={`${matches ? mobileClasses.tdBody : desktopClasses.tdBody} ${classes.link}`} ><Link to={`/offer/item/${offer.id}`}>VER DETALLES </Link></td>                            
                                                    </tr>
                                        })
                                    :   <TableRow />
                            }
                        </tbody>
                    </table>                    
                </div>
                <div className={matches ? mobileClasses.pagination : desktopClasses.pagination} >
                    <button className={classes.paginationButton} onClick={prevPage} ><img src={LeftArrowIcon} /></button>
                    <p className={classes.paginationPage} >{offersToShow.page}</p>
                    <p className={classes.paginationTotalPages} >De { Math.ceil(offers.length / 8) && offersToShow.page }</p>
                    <button className={classes.paginationButton} onClick={nextPage} ><img src={RightArrowIcon} alt='' /></button>
                </div>
            </div>
        </Grid>
    )
}

const mapState = state => ({
    user_id: state.auth.user.data.id
})

const mapDispatch = {
    ...FetchActions
}

export default connect(mapState,mapDispatch)(List)