import React,{ useState , useEffect , Fragment } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import { calculateOfferFullSchedule, getOfferFullSchedule } from 'app/utils/OfferFullSchedule';

import { offerDetails } from '../../utils/Offer'
import * as FetchActions from '../../store/actions/FetchActions'
import moment from 'moment'
import OfferStatus from 'app/components/offer/OfferStatus';

import {
    Box,
    Card,
    CardContent,
    Grid,
    Divider,
    Typography,
    makeStyles,
} from '@material-ui/core'
import { useParams } from 'react-router'

const useStyles = makeStyles((theme) => ({
formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
},
button: {
    margin: theme.spacing(1),
},
}));

const OfferInfo = (props) => {
    
    const { id } = useParams();

    const {t} = useTranslation();

    const period_detail = value => {
        return t(`Step1Frequency.period.${value}`)
    };

    const [offer,setOffer] = useState({
        id: 0,
        createdAt: new Date(),
        startFromDate: '',
        endDate: '',
        offerBuilding: {
            roomNumber: 0,
            bathRoomNumber: 0,
            kitchenNumber: 0,
            livingNumber: 0
        },
        periodPoints: [],
        extraServices: [],
        hours: 0,
        pricePerDay: 0,
        pricePerService: 0,
        serviceAddress: '',
        billingAddress: '',
        company: null,
        vouchers: []
    })

    const [offerFetch, setOfferFetch] = useState({});
    const [serviceSchedule, setServiceSchedule] = useState({});
    const [fullScheduleStatus, setFullScheduleStatus] = useState({});
    useEffect(() => {
        (async () => {
            try {
                const order = await offerDetails(id);
                setOfferFetch(  order.data );
            } catch (error) {
                console.log( error );
            }
        })();

    }, [ id ])

    useEffect( () => {        
        props.fetching()
        if( Object.keys(offerFetch).length !== 0 )
        {
            const startFromDate = moment( offerFetch.startFromDate ).format( 'YYYY-MM-DDThh:mm:ssZ' );
            const endDate = moment( offerFetch.endDate ).format( 'YYYY-MM-DDThh:mm:ssZ' );
            const startHour = offerFetch.periodPoints[0].startHour;
            const endHour = offerFetch.periodPoints[0].endHour;
            const days = Object.values( offerFetch.periodPoints ).map( ({ day }) => day );
            
            (async () => {
                try {
                    const schedule = await calculateOfferFullSchedule({ startFromDate, endDate, startHour, endHour, days });
                    const data = await getOfferFullSchedule(offerFetch['id']);            
                    setServiceSchedule( schedule );                   
                    setFullScheduleStatus(data['hydra:member']);
                } catch (error) {
                    console.log( error );
                }
            })();
    
        }
    },[ offerFetch ])
    
    useEffect(() => {
        
        if (Object.keys(serviceSchedule).length !== 0) {
            let extraServices = {};
            let hours = 0;
            let billingAddress;
            
            offerFetch.periodPoints.map( pp => {
                if ( hours === 0 ) { hours = pp.durationInMinutes / 60 }
                if(pp.pointServices){
                    pp.pointServices.map(ps => {
                        extraServices = {
                            ...extraServices,
                            [`service_${ps.id}`]: {
                                ...extraServices[`service_${ps.id}`],
                                service: ps.extraService ? ps.extraService.name : '',
                                days: extraServices[`service_${ps.id}`] ? [...extraServices[`service_${ps.id}`].days,pp.day] : [pp.day]
                            }
                        }
                    })
                }
            })
    
    
            if(offerFetch.offerBilling){
                billingAddress = offerFetch.offerBilling.address 
                                    ? `${offerFetch.offerBilling.address.country.name}, ${offerFetch.offerBilling.address.county.name}, ${offerFetch.offerBilling.address.city ? offerFetch.offerBilling.address.city.name : ''} 
                                        ${offerFetch.offerBilling.address.street}, ${t('address.floor')}: ${offerFetch.offerBilling.address.floor}, ${t('address.phoneNumber')}: ${offerFetch.offerBilling.address.phoneNumber}`
                                    : `${offerFetch.company.address.country.name}, ${offerFetch.company.address.county.name}, ${offerFetch.company.address.city ? offerFetch.company.address.city.name : ''} 
                                        ${offerFetch.company.address.street}, ${t('address.floor')}: ${offerFetch.company.address.floor}, ${t('address.phoneNumber')}: ${offerFetch.company.address.phoneNumber}`
            }            

            setOffer({
                id: offerFetch.id,
                createdAt: new Date(offerFetch.timestapableCreated),
                startFromDate: offerFetch.startFromDate,
                endDate: offerFetch.endDate,
                offerBuilding: {
                    roomNumber: offerFetch.offerBuilding.roomNumber,
                    bathRoomNumber: offerFetch.offerBuilding.bathRoomNumber,
                    kitchenNumber: offerFetch.offerBuilding.kitchenNumber,
                    livingNumber: offerFetch.offerBuilding.livingNumber
                },
                period: period_detail(offerFetch.period),
                periodPoints: offerFetch.periodPoints,
                extraServices: Object.values(extraServices),
                serviceAddress: `${offerFetch.serviceAddress.country.name}, ${offerFetch.serviceAddress.county.name}, ${offerFetch.serviceAddress.city ? offerFetch.serviceAddress.city.name : ''} 
                                ${offerFetch.serviceAddress.street}, ${t('address.floor')}: ${offerFetch.serviceAddress.floor}, ${t('address.phoneNumber')}: ${offerFetch.serviceAddress.phoneNumber}`,
                hours,
                pricePerDay: offerFetch.pricePerDay,
                pricePerService: offerFetch.pricePerService,
                company: offerFetch.company,
                billingAddress,
                vouchers: [...offerFetch.vouchers]
            })
            
            props.success()  
        }                                   
        
    }, [ serviceSchedule ])    

    return (

        <Grid item xs={12} md={8} sm={6}>
            <Card>
                <CardContent>

                    <Box p={2} mb={1}>
                        <Grid 
                            container
                            direction='row'
                            justify='space-around'
                            alignItems='center'
                            spacing={3}
                        >
                            <Grid item xs={12} md={6} sm={12}>
                                <Typography variant='h6' gutterBottom>
                                    {`${t('general.order')} #${offer.id}`}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} md={6} align='right' sm={12}>
                                <Typography variant='h6' gutterBottom>
                                    { offer.createdAt.toLocaleDateString() }
                                </Typography>
                            </Grid>

                            <Grid item xs={12} md={12} sm={12}>
                                <Typography variant='h6' gutterBottom>
                                    {t('general.service')}
                                </Typography>
                            </Grid>

                        </Grid>
                    </Box>

                    <Divider />

                    <Box mt={3} p={2} mb={3}>
                        <Grid 
                            container
                            direction='row'
                            justify='space-around'
                            alignItems='center'
                            spacing={3}
                        >
                            <Grid item xs={12} md={12} sm={12}>
                                <Typography variant='subtitle1' gutterBottom>
                                    {t('OfferStep2.serviceAddress')}
                                </Typography>
                            </Grid>
                            <Grid item xs={12} md={12} sm={12}>
                                <Typography variant='subtitle1' gutterBottom>
                                    {offer.serviceAddress}
                                </Typography>
                            </Grid>
                        </Grid>
                    </Box>

                    <Divider />

                    <Box mt={3} p={2} mb={3}>
                        <Grid 
                            container
                            direction='row'
                            justify='space-around'
                            alignItems='center'
                            spacing={3}
                        >
                            <Grid item xs={12} md={8} >
                                <Typography variant='subtitle1' gutterBottom>
                                    {t('offerItem.cleaningType')}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} md={4} >
                                <Typography variant='subtitle1' gutterBottom>
                                    {`${offer.offerBuilding.roomNumber} ${t('offerBuilding.room')}`}<br/>
                                    {`${offer.offerBuilding.bathRoomNumber} ${t('offerBuilding.bathroom')}`}<br/>
                                    {`${offer.offerBuilding.kitchenNumber} ${t('offerBuilding.kitchen')}`}<br/>
                                    {`${offer.offerBuilding.livingNumber} ${t('offerBuilding.living')}`}<br/>
                                </Typography>
                            </Grid>
                        </Grid>
                    </Box>

                    <Divider />

                    <Box mt={3} p={2} mb={3}>
                        <Grid 
                            container
                            direction='row'
                            justify='space-around'
                            alignItems='center'
                            spacing={3}
                        >
                            <Grid item xs={12} md={8} sm={12}>
                                <Typography variant='subtitle1' gutterBottom>
                                    {t('offerItem.serviceFrequency')}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} md={4} sm={12}>
                                <Typography variant='subtitle1' gutterBottom>
                                    { offer.period }
                                </Typography>
                            </Grid>

                        </Grid>
                    </Box>

                    <Divider />

                    <Box mt={3} p={2} mb={3}>
                        <Grid 
                            container
                            direction='row'
                            justify='space-around'
                            alignItems='center'
                            spacing={3}
                        >
                            <Grid item xs={12} md={8} sm={12}>
                                <Typography variant='subtitle1' gutterBottom>
                                    {t('Step1Frequency.startDateTitle')}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} md={4} sm={12}>
                                <Typography variant='subtitle1' gutterBottom>
                                    { moment(offer.startFromDate).format('DD-MM-YYYY') }
                                </Typography>
                            </Grid>

                            <Grid item xs={12} md={8} sm={12}>
                                <Typography variant='subtitle1' gutterBottom>
                                    {t('Step1Frequency.endDateTitle')}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} md={4} sm={12}>
                                <Typography variant='subtitle1' gutterBottom>
                                    { moment(offer.endDate).format('DD-MM-YYYY') }
                                </Typography>
                            </Grid>
                        </Grid>
                    </Box>

                    <Divider />

                    <Box mt={3} p={2} mb={3}>
                        <Grid 
                            container
                            direction='row'
                            justify='space-around'
                            alignItems='center'
                            spacing={3}
                        >
                            <Grid item xs={12} md={3} sm={12}>
                                <Typography variant='subtitle1' gutterBottom>
                                    {t('offerItem.serviceDay')}
                                </Typography>
                            </Grid>
                            <Grid item xs={12} md={3} sm={12}>
                                <Typography variant='subtitle1' gutterBottom>
                                    {t('Schedule.startHour')}
                                </Typography>
                            </Grid>   
                            <Grid item xs={12} md={3} sm={12}>
                                <Typography variant='subtitle1' gutterBottom>
                                    {t('Schedule.endHour')}
                                </Typography>
                            </Grid>                           
                            <Grid item xs={12} md={3} sm={12}>
                                <Typography variant='subtitle1' gutterBottom>
                                    {t('offerItem.serviceHours')}
                                </Typography>
                            </Grid>    
                            {
                                offer.periodPoints.length
                                    ?   offer.periodPoints.map(pp => <Fragment key={offer.periodPoints.indexOf(pp)}>
                                                                        <Grid item xs={12} md={3} sm={12}>
                                                                            <Typography variant='subtitle1' gutterBottom>
                                                                                { t(`Schedule.days.${pp.day.toLowerCase()}`) }
                                                                            </Typography>
                                                                        </Grid>

                                                                        <Grid item xs={12} md={3} sm={12}>
                                                                            <Typography variant='subtitle1' gutterBottom>
                                                                                { pp.startHour }
                                                                            </Typography>
                                                                        </Grid>

                                                                        <Grid item xs={12} md={3} sm={12}>
                                                                            <Typography variant='subtitle1' gutterBottom>
                                                                                { pp.endHour }
                                                                            </Typography>
                                                                        </Grid>

                                                                        <Grid item xs={12} md={3} sm={12}>
                                                                            <Typography variant='subtitle1' gutterBottom>
                                                                                { `${pp.durationInMinutes / 60} ${t('Schedule.hours')}` }
                                                                            </Typography>
                                                                        </Grid>
                                                                    </Fragment>)
                                    : <Grid />
                            }
                        </Grid>
                    </Box>

                    <Divider />

                    <Box mt={3} p={2} mb={3}>
                        <Grid
                            container
                            direction='row'
                            justify='space-around'
                            alignItems='center'
                            spacing={3}
                        >
                            <Grid item md={4} sm={12} >
                                <Typography variant='subtitle1' gutterBottom>
                                    { t( 'general.serviceDaysList' ) }
                                </Typography>
                            </Grid>
                            <Grid item md={3} sm={12} >
                                <Typography variant='subtitle1' gutterBottom>
                                    { t( 'general.status' ) }
                                </Typography>
                            </Grid>                            
                            <Grid item md={5} sm={12} >
                                <Typography variant='subtitle1' gutterBottom>
                                    { t( 'general.payment' ) }
                                </Typography>
                            </Grid>
                            {                                
                                Object.values( fullScheduleStatus ).map( ({ dateTimeStart, id, clientOfferStatus, workerOfferStatus, paymentStatus }) => (
                                    <OfferStatus dateTimeStart={dateTimeStart} id={id} clientOfferStatus={clientOfferStatus} workerOfferStatus={workerOfferStatus} paymentStatus={paymentStatus} />                            
                                ))
                            }
                        </Grid>

                    </Box>

                    <Divider />

                    {
                        ( !!offer.extraServices.length ) && 
                            <>
                                {
                                    offer.extraServices.map(service => 
                                        <Box key={ offer.extraServices.indexOf(service) } mt={3} p={2} mb={3}>
                                            <Grid 
                                                container
                                                direction='row'
                                                justify='space-around'
                                                alignItems='center'
                                                spacing={3}
                                            >
                                                <Grid item xs={12} md={12} sm={12}>
                                                    <Typography variant='subtitle1' gutterBottom>
                                                        {service.service}
                                                    </Typography>
                                                </Grid>
                                                {/* <Grid item xs={12} md={8} sm={12}>
                                                    {
                                                        service.days.length
                                                            ?   service.days.map(day => <Typography key={service.days.indexOf(day)} variant='subtitle1' gutterBottom>
                                                                                            { t(`Schedule.days.${day.toLowerCase()}`) }
                                                                                        </Typography>)
                                                            :   <p/>
                                                    }
                                                </Grid>
                                                <Grid item xs={12} md={2} sm={12}>
                                                    <Typography variant='subtitle1' gutterBottom>
                                                        20
                                                    </Typography>
                                                </Grid>
                                                <Grid item xs={12} md={2} sm={12}>
                                                    <Typography variant='subtitle1' gutterBottom>
                                                        20
                                                    </Typography>
                                                </Grid> */}
                                            </Grid>
                                        </Box>
                                    )
                                }
                                <Divider />
                            </>
                    }
                    
                    <Box mt={3} p={2} mb={3}>
                        <Grid 
                            container
                            direction='row'
                            justify='space-around'
                            alignItems='center'
                            spacing={3}
                        >
                            <Grid item xs={12} md={6} sm={12}>
                                <Typography variant='subtitle1' gutterBottom>
                                    {t('offerItem.serviceTotalHours')}
                                </Typography>
                            </Grid>
                            <Grid item xs={12} md={6} sm={12}>
                                <Typography variant='subtitle1' gutterBottom>
                                    {t('offerItem.serviceTotalDays')}
                                </Typography>
                            </Grid>

                            <Grid item xs={12} md={6} sm={12}>
                                <Typography variant='subtitle1' gutterBottom>
                                    { `${ offer.hours * serviceSchedule.length } ${t('Schedule.hours')}` }
                                </Typography>
                            </Grid>
                            <Grid item xs={12} md={6} sm={12}>
                                <Typography variant='subtitle1' gutterBottom>
                                    { `${ serviceSchedule.length } ${t('general.days')}` }
                                </Typography>
                            </Grid>

                        </Grid>
                    </Box>

                    <Divider />

                    {
                        offer.company &&
                        <>
                            <Box mt={3} p={2} mb={3}>
                                <Grid 
                                    container
                                    direction='row'
                                    justify='space-around'
                                    alignItems='center'
                                    spacing={3}
                                >
                                    <Grid item xs={12} md={12} sm={12}>
                                        <Typography variant='subtitle1' gutterBottom>
                                            {t('offerItem.companyData')}
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12} md={4} sm={12}>
                                        <Typography variant='subtitle2' gutterBottom>
                                            {t('address.fullName')}
                                        </Typography>
                                        <Typography variant='subtitle1' gutterBottom>
                                            {offer.company.name}
                                        </Typography>                                                
                                    </Grid>
                                    <Grid item xs={12} md={4} sm={12}>
                                        <Typography variant='subtitle2' gutterBottom>
                                            {t('register.inputs.bankName')}
                                        </Typography>
                                        <Typography variant='subtitle1' gutterBottom>
                                            {offer.company.bankName}
                                        </Typography>                                                
                                    </Grid>
                                    <Grid item xs={12} md={4} sm={12}>
                                        <Typography variant='subtitle2' gutterBottom>
                                            {t('register.inputs.bankAccount')}
                                        </Typography>
                                        <Typography variant='subtitle1' gutterBottom>
                                            {offer.company.bankAccount}
                                        </Typography>                                                
                                    </Grid>
                                    <Grid item xs={12} md={4} sm={12}>
                                        <Typography variant='subtitle2' gutterBottom>
                                            {t('address.phoneNumber')}
                                        </Typography>
                                        <Typography variant='subtitle1' gutterBottom>
                                            {offer.company.regNumber}
                                        </Typography>                                                
                                    </Grid>
                                    <Grid item xs={12} md={4} sm={12}>
                                        <Typography variant='subtitle2' gutterBottom>
                                            {t('company.swift')}
                                        </Typography>
                                        <Typography variant='subtitle1' gutterBottom>
                                            {offer.company.swift}
                                        </Typography>                                                
                                    </Grid>
                                    <Grid item xs={12} md={4} sm={12}>
                                        <Typography variant='subtitle2' gutterBottom>
                                            {t('company.cif')}
                                        </Typography>
                                        <Typography variant='subtitle1' gutterBottom>
                                            {offer.company.cif}
                                        </Typography>                                                
                                    </Grid>
                                    <Grid item xs={12} md={12} sm={12}>
                                        <Typography variant='subtitle2' gutterBottom>
                                            {t('offerItem.companyAddress')}
                                        </Typography>
                                        <Typography variant='subtitle1' gutterBottom>
                                            {`${offer.company.address.country.name}, ${offer.company.address.county.name}, ${offer.company.address.city ? offer.company.address.city.name : ''} 
                                                ${offer.company.address.street}, ${t('address.floor')}: ${offer.company.address.floor}, ${t('address.phoneNumber')}: ${offer.company.address.phoneNumber}`}
                                        </Typography>                                                
                                    </Grid>
                                </Grid>
                            </Box>

                            <Divider />
                        </>
                    }           

                    {/* <Box mt={3} p={2} mb={3}>
                        <Grid 
                            container
                            direction='row'
                            justify='space-around'
                            alignItems='center'
                            spacing={3}
                        >
                            <Grid item xs={12} md={12} sm={12}>
                                <Typography variant='subtitle1' gutterBottom>
                                    {t('OfferStep2.billingAddress')}
                                </Typography>
                            </Grid>
                            <Grid item xs={12} md={12} sm={12}>
                                <Typography variant='subtitle1' gutterBottom>
                                    {offer.billingAddress}
                                </Typography>
                            </Grid>
                        </Grid>
                    </Box> */}

                    {/* <Divider /> */}

                    <Box mt={3} p={2} mb={3}>
                        <Grid 
                            container
                            direction='row'
                            justify='space-around'
                            alignItems='center'
                            spacing={3}
                        >
                            {
                                (offer.vouchers.length > 0) &&
                                <>
                                    <Grid item xs={12} md={6} sm={12}>
                                        <Typography variant='h7' gutterBottom>
                                            {t('general.subTotal')}
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12} md={6} align='right' sm={12}>
                                        <Typography variant='subtitle1' gutterBottom>
                                            {  
                                                (offer.vouchers[0].type === 'percent') ?
                                                (offer.pricePerService * 100)  / (100 - offer.vouchers[0].discount) :
                                                (offer.pricePerService + offer.vouchers[0].discount)
                                            } &euro;
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={12} md={6} sm={12}>
                                        <Typography variant='h7' gutterBottom>
                                            {t('voucher.code')}
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12} md={6} align='right' sm={12}>
                                        <Typography variant='subtitle2' gutterBottom>
                                            { offer.vouchers[0].code }
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={12} md={6} sm={12}>
                                        <Typography variant='h7' gutterBottom>
                                            {t('general.vouchersValue')}
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12} md={6} align='right' sm={12}>
                                        <Typography variant='subtitle1' gutterBottom>
                                            - {
                                                (offer.vouchers[0].type === 'percent') ?
                                                ((offer.pricePerService)  / (100 - offer.vouchers[0].discount)) * offer.vouchers[0].discount :
                                                (offer.vouchers[0].discount)
                                                
                                            } &euro;
                                        </Typography>
                                    </Grid>
                                </>
                            }
                            
                            <Grid item xs={12} md={6} sm={12}>
                                <Typography variant='subtitle2' gutterBottom>
                                    { t('general.totalPerDay') }
                                </Typography>
                            </Grid>
                            <Grid item xs={12} md={6} align='right' sm={12}>
                                <Typography variant='subtitle1' gutterBottom>
                                    { offer.pricePerDay } &euro;
                                </Typography>
                            </Grid>

                            <Grid item xs={12} md={6} sm={12}>
                                <Typography variant='subtitle2' gutterBottom>
                                    { t('general.totalPerService') }
                                </Typography>
                            </Grid>
                            <Grid item xs={12} md={6} align='right' sm={12}>
                                <Typography variant='subtitle1' gutterBottom>
                                    { offer.pricePerService } &euro;
                                </Typography>
                            </Grid>

                        </Grid>
                    </Box>

                </CardContent>
            </Card>
        </Grid>
    )
}

const mapDispatch = {
    ...FetchActions
}

export default connect(null,mapDispatch)(OfferInfo)