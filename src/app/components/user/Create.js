import React , { useState } from 'react'
import {default as FusePageCarded} from '@fuse/core/FusePageCarded'
import { Link , useHistory } from 'react-router-dom'

import { useTranslation } from 'react-i18next'

import * as User from '../../utils/User'

import MessageSnackbar from '../utils/MessageSnackbar'

import {
    Button,
    Typography,
    TextField,
    Grid,
    Box,
    Select,
    MenuItem
} from '@material-ui/core'

import {
    ArrowBack
} from '@material-ui/icons'

const Create = props => {

    const [user,setUser] = useState({
                                    email: '',
                                    password: '',
                                    name: '',
                                    confirm_password: '',
                                    phone_number: '',
                                    bankAccount: '',
                                    bankName: '',
                                    user_type: '0',
                                    register_mode: 1,
                                })

    const [fetching,setFetching] = useState(false)

    const {t,i18n} = useTranslation()

    const info = [
        {
            name: 'name',
            value: user.name,
            label: t('register.inputs.name'),
            type: 'text'
        },
        {
            name: 'phone_number',
            value: user.phone_number,
            label: t('address.phoneNumber'),
            type: 'text'
        },
        {
            name: 'email',
            value: user.email,
            label: t('OfferStep2.email'),
            type: 'email'
        },
        {
            name: 'password',
            value: user.password,
            label: t('login.inputs.password'),
            type: 'password'
        },
        {
            name: 'confirm_password',
            value: user.confirm_password,
            label: t('login.inputs.confirmPassword'),
            type: 'password'
        },
        {
            name: 'user_type',
            value: user.user_type,
            label: t('tableHeads.roles'),
            type: 'select',
            options: [
                {
                    key: '0',
                    value: 'Admin'
                },
                {
                    key: '1',
                    value: 'Client'
                },
                {
                    key: '4',
                    value: 'Employee Worker'
                },
                {
                    key: '3',
                    value: 'Independent Worker'
                },
            ]
        }
    ]

    const info2 = [
        {
            name: 'bankName',
            value: user.bankName,
            label: t('register.inputs.bankName'),
            type: 'text'
        },
        {
            name: 'bankAccount',
            value: user.bankAccount,
            label: t('register.inputs.bankAccount'),
            type: 'text'
        },
    ]

    const handleChange = input => {
        setUser({
            ...user,
            [input.target.name]: input.target.value
        })
    }

    const handleClick = async btn => {
        if(user.password === user.confirm_password){
            let data = {...user}

            if(data.role !== 'ROLE_INDEPENDENT_WORKER'){
                delete data.bankName
                delete data.bankAccount
            }

            setFetching(true)
            let response = await User.createUser({
                ...data,
                user_type: parseInt(data.user_type)
            })
            setFetching(false)

            setUser({
                email: '',
                password: '',
                name: '',
                phone_number: '',
                confirm_password: '',
                bankAccount: '',
                bankName: '',
                register_mode: 1,
                user_type: '0'
            })
        }
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: 'p-0',
                header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
            }}
            header={
                <div className='flex flex-1 w-full items-center justify-between'>
                    <div className='flex flex-col items-start max-w-full'>
                        <div className='flex items-center max-w-full'>
                            <div className='flex flex-col min-w-0'>
                                <Link to='/users' style={{textDecoration: 'none',marginBottom: '10px'}}>
                                    <ArrowBack /> {t('general.back')}
                                </Link>
                                <Typography variant='h4'>
                                    {t('adminPanel.createUser')}
                                </Typography>
                            </div>
                        </div>
                    </div>
                </div>
            }
            content={
                <Box p={4} mt={2} mb={2}>
                    {
                        fetching && <MessageSnackbar variant='info' message='Loading Data' />
                    }
                    <Grid
                        container
                        direction='row'
                        justify='flex-start'
                        alignItems='flex-start'
                        spacing={2}
                    >
                        {
                            info.length
                                ?   info.map(data => {
                                        if(data.type === 'select'){
                                            return  <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                        <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                            {data.label}
                                                        </Typography>
                                                        <Select
                                                            value={data.value}
                                                            name={data.name}
                                                            variant='outlined'
                                                            fullWidth
                                                            onChange={handleChange}
                                                        >
                                                            <MenuItem value="">
                                                                <em>None</em>
                                                            </MenuItem>
                                                            {
                                                                data.options.map(option => <MenuItem  key={data.options.indexOf(option)} value={option.key}>{option.value}</MenuItem>)
                                                            }
                                                        </Select>
                                                    </Grid>
                                        }else{
                                            return  <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                        <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                            {data.label}
                                                        </Typography>
                                                        <TextField
                                                            type={data.type}
                                                            name={data.name}
                                                            value={data.value}
                                                            variant='outlined'
                                                            onChange={handleChange}
                                                            fullWidth
                                                        />
                                                    </Grid>
                                        }
                                    })
                                :   <Grid />
                        }
                        {
                            user.user_type === '3'
                                ?   (info2.length
                                    ?   info2.map(data => <Grid key={info2.indexOf(data)} item xs={12} md={12} sm={12}>
                                                                <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                                    {data.label}
                                                                </Typography>
                                                                <TextField
                                                                    type={data.type}
                                                                    name={data.name}
                                                                    value={data.value}
                                                                    variant='outlined'
                                                                    onChange={handleChange}
                                                                    fullWidth
                                                                />
                                                            </Grid>)
                                    :   <Grid />)
                                : <Grid />
                        }
                        <Grid item xs={12} md={12} sm={12}>
                            <Button
                                variant='contained'
                                color='primary'
                                onClick={handleClick}
                                fullWidth
                            >
                                {t('general.save')}
                            </Button>
                        </Grid>
                    </Grid>
                </Box>
            }
            innerScroll
        >
        </FusePageCarded>
    )
}

export default Create