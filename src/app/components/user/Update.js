import React , { useState , useEffect } from 'react'
import {default as FusePageCarded} from '@fuse/core/FusePageCarded'
import { Link , useHistory } from 'react-router-dom'

import * as User from '../../utils/User'

import { useTranslation } from 'react-i18next'

import MessageSnackbar from '../utils/MessageSnackbar'
import GenericPrompt from '../utils/GenericPrompt'

import {
    Button,
    Typography,
    TextField,
    Grid,
    Box,
    Select,
    MenuItem
} from '@material-ui/core'

import {
    ArrowBack
} from '@material-ui/icons'

const Create = props => {

    const history = useHistory()

    const [fetching,setFetching] = useState(false)

    const {t,i18n} = useTranslation()

    const [user,setUser] = useState({
                                    name: '',
                                    email: '',
                                    phoneNumber: '',
                                    bankName: '',
                                    bankAccount: '',
                                    role: 'ROLE_ADMIN'
                                })

    const [dialog,setDialog] = useState({
        open: false,
        dataAction: -1,
        action: () => {}
    })

    useEffect(() => {
        if(props.match.params.id){
            setFetching(true)
            User.getUserId(props.match.params.id)
                .then(res => {
                        setUser({
                        ...user,
                        name: res.name,
                        email: res.email,
                        phoneNumber: res.phoneNumber,
                        bankName: res.bankName,
                        bankAccount: res.bankAccount,
                        role: res.roles[0]
                    })
                    setFetching(false)
                })
                .catch(error => console.error(error))
        }
    },[])

    const info = [
        {
            name: 'name',
            value: user.name,
            label: t('register.inputs.name'),
            type: 'text'
        },
        {
            name: 'email',
            value: user.email,
            label: t('OfferStep2.email'),
            type: 'email'
        },
        {
            name: 'phoneNumber',
            value: user.phoneNumber,
            label: t('address.phoneNumber'),
            type: 'text'
        },
        {
            name: 'role',
            value: user.role,
            label: t('tableHeads.roles'),
            type: 'select',
            options: [
                {
                    key: 'ROLE_ADMIN',
                    value: 'Admin'
                },
                {
                    key: 'ROLE_CLIENT',
                    value: 'Client'
                },
                {
                    key: 'ROLE_EMPLOYEE_WORKER',
                    value: 'Employee Worker'
                },
                {
                    key: 'ROLE_INDEPENDENT_WORKER',
                    value: 'Independent Worker'
                },
            ]
        }
    ]

    const info2 = [
        {
            name: 'bankName',
            value: user.bankName,
            label: t('register.inputs.bankName'),
            type: 'text'
        },
        {
            name: 'bankAccount',
            value: user.bankAccount,
            label: t('register.inputs.bankAccount'),
            type: 'text'
        },
    ]

    const openDialog = action => setDialog({
        open: true,
        dataAction: props.match.params.id,
        action
    })

    const closeDialog = () => setDialog({
        open: false,
        dataAction: -1,
        action: () => {}
    })

    const handleChange = input => {
        setUser({
            ...user,
            [input.target.name]: input.target.value
        })
    }

    const handleClick = async btn => {
        closeDialog()

        if(user.password === user.confirm_password){
            let data = {
                ...user,
                roles: [user.role]
            }

            delete data.role

            if(data.role !== 'ROLE_INDEPENDENT_WORKER'){
                delete data.bankName
                delete data.bankAccount
            }

            setFetching(true)
            let response = await User.Update({
                ...data,
                user_iri: `/api/users/${props.match.params.id}`
            })
            setFetching(false)
        }
    }

    const handleDelete = user_id => {
        closeDialog()
        User.deleteUserId(user_id)
            .finally(() => history.push('/users'))
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: 'p-0',
                header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
            }}
            header={
                <div className='flex flex-1 w-full items-center justify-between'>
                    <div className='flex flex-col items-start max-w-full'>
                        <div className='flex items-center max-w-full'>
                            <div className='flex flex-col min-w-0'>
                                <Link to='/users' style={{textDecoration: 'none',marginBottom: '10px'}}>
                                    <ArrowBack /> {t('general.back')}
                                </Link>
                                <Typography variant='h4'>
                                    { user.name }
                                </Typography>
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        {/* pagination */}
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        <Link to='/users/create' style={{textDecoration: 'none'}}>
                            <Button className='whitespace-no-wrap' variant='contained'>
                                {t('adminPanel.createUser')}
                            </Button>
                        </Link>
                        <Button 
                            style={{marginLeft: '10px'}} 
                            className='whitespace-no-wrap' 
                            color='secondary' 
                            variant='contained'
                            onClick={() => openDialog(handleDelete)}
                        >
                            {t('adminPanel.deleteUser')}
                        </Button>
                    </div>
                </div>
            }
            content={
                <Box p={4} mt={2} mb={2}>
                    {
                        fetching && <MessageSnackbar variant='info' message='Loading Data...' />
                    }
                    <Grid
                        container
                        direction='row'
                        justify='flex-start'
                        alignItems='flex-start'
                        spacing={2}
                    >
                        {
                            ( !!info.length ) &&
                            info.map(data => {
                                if ( data.type === 'select' ) {
                                    return  <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                    {data.label}
                                                </Typography>
                                                <Select
                                                    value={data.value}
                                                    name={data.name}
                                                    variant='outlined'
                                                    fullWidth
                                                    onChange={handleChange}
                                                >
                                                    <MenuItem value="">
                                                        <em>None</em>
                                                    </MenuItem>
                                                    {
                                                        data.options.map(option => <MenuItem  key={data.options.indexOf(option)} value={option.key}>{option.value}</MenuItem>)
                                                    }
                                                </Select>
                                            </Grid>
                                }else{
                                    return  <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                    {data.label}
                                                </Typography>
                                                <TextField
                                                    type={data.type}
                                                    name={data.name}
                                                    value={data.value}
                                                    variant='outlined'
                                                    onChange={handleChange}
                                                    fullWidth
                                                />
                                            </Grid>
                                }
                            })
                        }
                        {
                            user.role === 'ROLE_INDEPENDENT_WORKER'
                                ?   (info2.length
                                    ?   info2.map(data => <Grid key={info2.indexOf(data)} item xs={12} md={12} sm={12}>
                                                                <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                                    {data.label}
                                                                </Typography>
                                                                <TextField
                                                                    type={data.type}
                                                                    name={data.name}
                                                                    value={data.value}
                                                                    variant='outlined'
                                                                    onChange={handleChange}
                                                                    fullWidth
                                                                />
                                                            </Grid>)
                                    :   <Grid />)
                                : <Grid />
                        }
                        <Grid item xs={12} md={12} sm={12}>
                            <Button
                                variant='contained'
                                color='primary'
                                onClick={() => openDialog(handleClick)}
                                fullWidth
                            >
                                Save
                            </Button>
                        </Grid>
                    </Grid>
                    <GenericPrompt 
                        title={t('adminPanel.deleteUser')}
                        message={t('general.areYouSureToDoThis')}
                        close={closeDialog}
                        action={dialog.action}
                        data={dialog}
                    />
                </Box>
            }
            innerScroll
        >
        </FusePageCarded>
    )
}

export default Create