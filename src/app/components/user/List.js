import React , { useState , useEffect } from 'react'
import { Link } from 'react-router-dom'
import GenericTable from '../utils/GenericTable'
import {default as FusePageCarded} from '@fuse/core/FusePageCarded'
import GenericPrompt from '../utils/GenericPrompt'

import * as User from '../../utils/UserAdminPanel'
import { deleteUserId } from '../../utils/User'


import { useTranslation } from 'react-i18next'

import MessageSnackbar from '../utils/MessageSnackbar'

import {
    Button,
    Typography,
} from '@material-ui/core'
import PaginationControlled from '../utils/PaginationControlled'

const List = () => {

    const [users,setUsers] = useState({
        data: [],
        lastPage: '',
        currentPage: '',
    })

    const [fetching,setFetching] = useState(true)

    const [dialog,setDialog] = useState({ open: false, dataAction: -1 })
    
    const openDialog = (id) => setDialog({ open: true, dataAction: id })
    
    const closeDialog = () => setDialog({ open: false, dataAction: -1 })

    const [page, setPage] = useState(1);

    const {t} = useTranslation()

    const tableHeaders = [
        'ID',
        t('register.inputs.name'),
        t('OfferStep2.email'),
        t('tableHeads.roles'),
        t('tableHeads.actions'),
    ]

    const getUserList = async() => {
        try {
            const user = (!!users.currentPage) ? await User.getUsers(users.currentPage) : await User.getUsers();
            
            const rows = user['hydra:member'].map(data => [
                {
                    link: true,
                    path: `/users/${data.id}`,
                    message: data.id
                },
                {
                    link: false,
                    message: data.name
                },
                {
                    link: false,
                    message: data.email
                },
                {
                    link: false,
                    message: data.roles[0]
                },
                {
                    action: true,
                    icons: [
                        {
                            icon: 'search',
                            path: `/users/${data.id}`
                        },
                        {
                            icon: 'edit',
                            path: `/users/update/${data.id}`
                        },
                        {
                            icon: 'delete',
                            onClick: openDialog,
                            id: data.id
                        }
                    ]
                }
            ]);
    
            setUsers({
                data: rows,
                lastPage: (!!user['hydra:view']) ? user['hydra:view']['hydra:last'] : '',
                currentPage: (!!user['hydra:view']) ? user['hydra:view']['@id'] : '',
            })
            setFetching(false);
            
        } catch (error) {            
            console.log( 'user error: ', error );
            setFetching(false);        
        }
    
    }    

    const handleDelete = (user_id) => {
        closeDialog()
        setFetching(true)
        deleteUserId(user_id)
            .finally(() => {
                setFetching(false)
                getUserList()                            
            })
    }
    
    useEffect(() => {
        setFetching(true)
        getUserList()
    }, [ page ]);

    return (
        <FusePageCarded
            classes={{
                toolbar: 'p-0',
                header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
            }}
            header={
                <div className='flex flex-1 w-full items-center justify-between'>
                    <div className='flex flex-col items-start max-w-full'>
                        <div className='flex items-center max-w-full'>
                            <div className='flex flex-col min-w-0'>
                                <Typography variant='h4'>
                                    {t('adminPanel.users')}
                                </Typography>
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        {/* pagination */}
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        <Link to='/users/create' style={{textDecoration: 'none'}}>
                            <Button className='whitespace-no-wrap' variant='contained'>
                                {t('adminPanel.createUser')}
                            </Button>
                        </Link>
                    </div>
                </div>
            }
            content={
                <div className='p-16 sm:p-24' style={{maxHeight: '400px'}}>
                    {
                        fetching && <MessageSnackbar variant='info' message='Loading Data...' />
                    }
                    {
                        !fetching &&
                        <>
                            <GenericTable headers={tableHeaders} body={users.data} />
                            <PaginationControlled
                                count={ users.lastPage.slice(-1) }
                                page={page}
                                onPageChange={setPage}
                                handleUrlChange={setUsers}
                            />
                        </>
                    }               
                    <GenericPrompt 
                        title={t('adminPanel.deleteUser')}
                        message={t('general.areYouSureToDoThis')}
                        close={closeDialog}
                        action={handleDelete}
                        data={dialog}
                    />    
                </div>
            }
            innerScroll
        >
        </FusePageCarded>
    )
}

export default List