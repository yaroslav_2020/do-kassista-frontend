import React , { useState , useEffect } from 'react'
import {default as FusePageCarded} from '@fuse/core/FusePageCarded'
import { Link , useHistory } from 'react-router-dom'

import * as User from '../../utils/User'

import { useTranslation } from 'react-i18next'

import MessageSnackbar from '../utils/MessageSnackbar'
import GenericPrompt from '../utils/GenericPrompt'

import {
    Typography,
    Grid,
    Box,
    Table,
    TableHead,
    TableRow,
    TableBody,
    TableCell,
    AppBar,
    Tabs,
    Tab,
    Button,
} from '@material-ui/core'

import {
    ArrowBack
} from '@material-ui/icons'

const TabPanel = props => {
    const { children, value, index, ...other } = props

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {children}
        </div>
    )
}

const a11yProps = index => {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    }
}

const Create = props => {

    const history = useHistory()

    const [tabValue,setTabValue] = useState(0)

    const [fetching,setFetching] = useState(false)

    const [dialog,setDialog] = useState({
        open: false,
        dataAction: -1,
        action: () => {}
    })

    const openDialog = action => setDialog({
        open: true,
        dataAction: props.match.params.id,
        action
    })

    const closeDialog = () => setDialog({
        open: false,
        dataAction: -1,
        action: () => {}
    })

    const {t,i18n} = useTranslation()

    const [user,setUser] = useState({
                                    email: '',
                                    password: '',
                                    name: '',
                                    confirm_password: '',
                                    phoneNumber: '',
                                    bankAccount: '',
                                    bankName: '',
                                    user_type: '0',
                                    register_mode: 1,
                                    addresses: [],
                                    companies: []
                                })
    
    useEffect(() => {
        if(props.match.params.id){
            setFetching(true)

            User.getUserId(props.match.params.id)
                .then(res => {
                    let addresses = res.addresses.map(address => ({
                        id: address.id,
                        data: `${address.country.name}, ${address.county.name}, ${address.city ? address.city.name : ''} 
                                    ${address.street}, ${t('address.floor')}: ${address.floor}, ${t('address.phoneNumber')}: ${address.phoneNumber}`,
                        activeStatus: address.deleted ? t('general.active') : t('general.notActive')
                    }))
                    let companies = res.company.map(company => ({
                        id: company.id,
                        data: `${res.name}, ${t('register.inputs.bankName')}: ${company.bankName}, 
                                    ${t('register.inputs.bankAccount')}: ${company.bankAccount}, 
                                    ${t('company.swift')}: ${company.swift}, ${t('company.cif')}: ${company.cif}, 
                                    ${t('address.phoneNumber')}: ${company.regNumber}` ,
                        activeStatus: company.deleted ? t('general.active') : t('general.notActive')
                    }))
                    setUser({
                        email: res.email,
                        name: res.name,
                        phoneNumber: res.phoneNumber,
                        bankAccount: res.bankAccount,
                        bankName: res.bankName,
                        username: res.username,
                        role: res.roles[0],
                        addresses: [...addresses],
                        companies: [...companies]
                    })
                    setFetching(false)
                })
                .catch(error => console.error(error))
        }
    }, [])

    const info = [
        {
            value: user.name,
            label: t('register.inputs.name'),
        },
        {
            value: user.email,
            label: t('OfferStep2.email'),
        },
        {
            value: user.phoneNumber,
            label: t('address.phoneNumber'),
        },
        {
            value: user.role,
            label: t('tableHeads.roles'),
        },
        {
            value: user.username,
            label: t('general.username'),
        },
    ]

    const info2 = [
        {
            name: 'bankName',
            value: user.bankName,
            label: t('register.inputs.bankName'),
            type: 'text'
        },
        {
            name: 'bankAccount',
            value: user.bankAccount,
            label: t('register.inputs.bankAccount'),
            type: 'text'
        },
    ]

    const handleTabChange = (event, newValue) => {
        setTabValue(newValue);
    }

    const handleDelete = user_id => {
        closeDialog()
        User.deleteUserId(user_id)
            .finally(() => history.push('/users'))
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: 'p-0',
                header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
            }}
            header={
                <div className='flex flex-1 w-full items-center justify-between'>
                    <div className='flex flex-col items-start max-w-full'>
                        <div className='flex items-center max-w-full'>
                            <div className='flex flex-col min-w-0'>
                                <Link to='/users' style={{textDecoration: 'none',marginBottom: '10px'}}>
                                    <ArrowBack /> {t('general.back')}
                                </Link>
                                <Typography variant='h4'>
                                    { user.name }
                                </Typography>
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        {/* pagination */}
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        <Link to={`/users/update/${props.match.params.id}`} style={{textDecoration: 'none'}}>
                            <Button className='whitespace-no-wrap' variant='contained'>
                                {t('adminPanel.updateUser')}
                            </Button>
                        </Link>
                        <Button 
                            style={{marginLeft: '10px'}} 
                            className='whitespace-no-wrap' 
                            color='secondary' 
                            variant='contained'
                            // onClick={() => handleDelete(props.match.params.id)}
                            onClick={() => openDialog(handleDelete)}
                        >
                            {t('adminPanel.deleteUser')}
                        </Button>
                    </div>
                </div>
            }
            content={
                <Box p={4} mt={2} mb={2}>
                    {
                        fetching && <Box mb={2}> <MessageSnackbar variant='info' message='Loading Data...' /> </Box> 
                    }
                    <AppBar position='static' style={{marginBottom: '25px'}}>
                        <Tabs value={tabValue} onChange={handleTabChange}>
                            <Tab label={t('adminPanel.userDetails')} {...a11yProps(0)}/>
                            <Tab label={t('adminPanel.userAddresses')} {...a11yProps(1)}/>
                            <Tab label={t('adminPanel.userCompanies')} {...a11yProps(2)}/>
                        </Tabs>
                    </AppBar>
                    <TabPanel value={tabValue} index={0}>
                        <Grid
                            container
                            direction='row'
                            justify='flex-start'
                            alignItems='flex-start'
                            spacing={2}
                        >
                            {
                                info.length
                                    ?   info.map(data => <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                            <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                                {data.label}
                                                            </Typography>
                                                            <Typography variant='body1'>
                                                                {data.value}
                                                            </Typography>
                                                        </Grid>)
                                    :   <Grid />
                            }
                            {
                                user.role === 'ROLE_INDEPENDENT_WORKER' && info2.length
                                    ?   info2.map(data => <Grid key={info2.indexOf(data)} item xs={12} md={12} sm={12}>
                                                            <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                                {data.label}
                                                            </Typography>
                                                            <Typography variant='body1'>
                                                                {data.value}
                                                            </Typography>
                                                        </Grid>)
                                    :   <Grid />
                            }
                        </Grid>
                    </TabPanel>
                    <TabPanel value={tabValue} index={1}>
                        <Grid
                            container
                            direction='row'
                            justify='flex-start'
                            alignItems='flex-start'
                            spacing={2}
                        >                            
                            <Grid item xs={12} md={12} sm={12}>
                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>ID</TableCell>
                                            <TableCell>{t('general.address')}</TableCell>
                                            <TableCell>{t('tableHeads.activeStatus')}</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                            {
                                                user.addresses.map(address => <TableRow key={user.addresses.indexOf(address)}>
                                                                            <TableCell>{address.id}</TableCell>
                                                                            <TableCell>{address.data}</TableCell>
                                                                            <TableCell>{address.activeStatus}</TableCell>
                                                                        </TableRow>)
                                            }
                                    </TableBody>
                                </Table>
                            </Grid>
                        </Grid>
                    </TabPanel>
                    <TabPanel value={tabValue} index={2}>
                        <Grid
                            container
                            direction='row'
                            justify='flex-start'
                            alignItems='flex-start'
                            spacing={2}
                        >
                            <Grid item xs={12} md={12} sm={12}>
                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>ID</TableCell>
                                            <TableCell>{t('general.company')}</TableCell>
                                            <TableCell>{t('tableHeads.activeStatus')}</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                            {
                                                user.companies.map( company => 
                                                    <TableRow key={user.companies.indexOf(company)}>
                                                        <TableCell>{company.id}</TableCell>
                                                        <TableCell>{company.data}</TableCell>
                                                        <TableCell>{company.activeStatus}</TableCell>
                                                    </TableRow>
                                                )
                                            }
                                    </TableBody>
                                </Table>
                            </Grid>
                        </Grid>
                    </TabPanel>
                    <GenericPrompt 
                        title={t('adminPanel.deleteUser')}
                        message={t('general.areYouSureToDoThis')}
                        close={closeDialog}
                        action={dialog.action}
                        data={dialog}
                    />
                </Box>
            }
            innerScroll
        >
        </FusePageCarded>
    )
}

export default Create