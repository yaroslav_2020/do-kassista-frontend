
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
    table: {
        minWidth: '100%'
    },
    tr: {
        borderBottom: '1px solid #DBDBDB',
        cursor: 'pointer',
        // '& td:last-child':{
        //     paddingRight: '0px'
        // },
        // '& th:last-child':{
        //     paddingRight: '120px'
        // }
    },
    tableCont: {
    },
    firstTh: {
        padding: '20px 0 20px 20px'
    },
    middleTh: {
        padding: '20px 0 20px 0'
    },
    lastTh: {
        padding: '20px 20px 20px 0'
    },
    generalCell: {
        width: '100%',
        height: '70px',
        background: '#FCFCFC',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        padding: '0 24px',
    },
    firstCell: {
        borderRadius: '10px 0 0 10px'
    },
    lastCell: {
        borderRadius: '0 10px 10px 0'
    },
    link: {
        '& a': {
            width: '100px',
            height: '42px',
            borderRadius: '9px',
            border: '1px solid #0097F6',
            color: '#0097F6',
            fontSize: '12px',
            fontWeight: '500',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            textDecoration: 'none'
        },
        '& a:hover': {
            textDecoration: 'none'
        }
    },
    paginationButton: {
        width: '42px',
        height: '42px',
        background: '#0097F6',
        color: '#fff',
        borderRadius: '6px',
        margin: '0 9px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    paginationPage: {
        borderRadius: '6px',
        border: '1px solid #DBDBDB',
        width: '42px',
        height: '42px',
        fontSize: '20px',
        fontWeight: '400',
        color: '#0097F6',
        textAlign: 'center',
        lineHeight: '42px',
        margin: '0 4.5px'
    },
    paginationTotalPages: {
        fontSize: '18px',
        fontWeight: '400',
        color: '#0097F6',
        height: '42px',
        textAlign: 'center',
        lineHeight: '42px',
        margin: '0 4.5px'
    },
})

const desktopStyles = makeStyles({
    contTitle: {
        width: '100%',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        padding: '0 15px',
        marginBottom: '32px'
    },
    title: {
        fontSize: '25px',
        fontWeight: 'bold',
        color: '#000'
    },
    contTabs: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginBottom: '40px'
    },
    tab: {
        padding: '0 0 7px 0',
        marginLeft: '15px',
        marginRight: '60px',
        '& p': {
            fontSize: '24px',
            fontWeight: '400',
            color: '#2F2F2F'
        }
    },
    contTable: {
        borderRadius: '13px',
        boxShadow: '0px 5px 10px rgba(0,0,0,.12)',
        width: '100%',
        overflowX: 'scroll'
    },
    theadTd: {
        padding: '17px 22px',
        fontSize: '24px',
        fontWeight: '500',
        textAlign: 'center',
        color: '#2F2F2F'
    },
    thBody: {
        fontSize: '24px',
        fontWeight: '500',
        color: '#2F2F2F',
    },
    tdBody: {
        fontSize: '22px',
        fontWeight: '400',
        color: '#000',
        textAlign: 'center',
        padding: '24px',
    },
    pagination: {
        width: '100%',
        height: '95px',
        display: 'flex',
        justifyContent: 'flex-end',
        paddingRight: '74px',
        alignItems: 'center'
    },
})

const mobileStyles = makeStyles({
    contTitle: {
        width: '100%',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        padding: '0 15px',
        marginBottom: '32px'
    },
    title: {
        fontSize: '25px',
        fontWeight: 'bold',
        color: '#000'
    },
    contTabs: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginBottom: '40px'
    },
    tab: {
        padding: '0 0 7px 0',
        marginLeft: '15px',
        marginRight: '10px',
        '& p': {
            fontSize: '18px',
            fontWeight: '400',
            color: '#2F2F2F'
        }
    },
    contTable: {
        borderRadius: '13px',
        boxShadow: '0px 5px 10px rgba(0,0,0,.12)',
        width: '100%',
        overflowX: 'scroll'
    },
    theadTd: {
        padding: '17px 22px',
        fontSize: '24px',
        fontWeight: '500',
        textAlign: 'center',
        color: '#2F2F2F',
    },
    thBody: {
        fontSize: '18px',
        fontWeight: '500',
        color: '#2F2F2F',
        padding: '0 24px',
    },
    tdBody: {
        fontSize: '16px',
        fontWeight: '400',
        color: '#272727',
        textAlign: 'center',
        padding: '24px',
    },
    pagination: {
        width: '100%',
        height: '95px',
        display: 'flex',
        justifyContent: 'flex-end',
        paddingRight: '19px',
        alignItems: 'center'
    },
})

export {
    mobileStyles,
    desktopStyles,
    useStyles
}