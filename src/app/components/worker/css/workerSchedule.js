import { makeStyles, TableCell, TableRow, withStyles } from "@material-ui/core"

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: theme.palette.common.white,
        color: theme.palette.common.black,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

const useStyles = makeStyles( theme => ({

    title: {
        fontSize: '18px',
        fontWeight: 'bold',
        color: '#191919',
        marginBottom: '25px'
    },
    section1: {
        margin: theme.spacing(2),   
    },
    container: {
        marginLeft: '15px',
        borderRadius: '13px',
        background: '#fff',
        color: '#191919',
        boxShadow: '0px 5px 10px rgba(0,0,0,.12)'
    },
    contTitle: {
        padding: '42px 60px'
    },
    table: {
        minWidth: '100%'
    },
    paragraph: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(1),
    },
    loaderCont: {
        width: '100vw',
        height: '100vh',
        position: 'fixed',
        background: 'rgba(0,0,0,.1)',
        zIndex: '1000',
        justifyContent: 'center',
        boxSizing: 'border-box',
        alignItems: 'center',
        top: '0',
        left: '0'
    },
}))

const desktopStyles = makeStyles({
    title: {
        fontSize: '25px',
        fontWeight: 'bold',
        color: '#272727'
    },
    inputLabel: {
        fontSize: '18px',
        fontWeight: '500',
        color: '#313131',
        marginBottom: '12px'
    },
    contInputs: {
        width: '80%',
        padding: '20px 0 20px 60px'
    },
    contButton: {
        width: '100%',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    button: {
        background: '#0097F6',
        borderRadius: '6px',
        color: '#fff',
        outline: 'none',
        padding: '14px 45px',
        border: 'none',
        width: '300px',
        height: '75px',
        fontSize: '22px',
        fontWeight: 'bold',
        margin: '20px 0px 30px 60px',
        '&:hover': {
            background: '#0097F6',
            border: 'none'
        }
    },
    textField: {
        border: '1px solid #DBDBDB',
        borderRadius: '9px',
        marginBottom: '35px',
        maxWidth: '400px',
        '& fieldset': {
            border: 'none'
        },
        '& .MuiInputBase-root:hover fieldset': {
            border: 'none'
        },
        '& .MuiInputBase-root:focus fieldset': {
            border: 'none'
        }
    },
})

const mobileStyles = makeStyles({
    title: {
        fontSize: '20px',
        fontWeight: 'bold',
        color: '#272727'
    },
    inputLabel: {
        fontSize: '14px',
        fontWeight: 'bold',
        color: '#313131',
        marginBottom: '12px'
    },
    contInputs: {
        width: '100%',
        padding: '40px 20px 20px 20px'
    },
    contButton: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: '35px'
    },
    button: {
        background: '#0097F6',
        borderRadius: '9px',
        color: '#fff',
        outline: 'none',
        padding: '12px 30px',
        border: 'none',
        width: '120px',
        height: '42px',
        fontSize: '14px',
        fontWeight: '500',
        margin: 'auto',
        '&:hover': {
            background: '#0097F6',
            border: 'none'
        }
    },
    textField: {
        border: '1px solid #DBDBDB',
        borderRadius: '9px',
        marginBottom: '35px',
        '& fieldset': {
            border: 'none'
        },
        '& .MuiInputBase-root:hover fieldset': {
            border: 'none'
        },
        '& .MuiInputBase-root:focus fieldset': {
            border: 'none'
        }
    },
})

export {
    mobileStyles,
    desktopStyles,
    useStyles,
    StyledTableRow,
    StyledTableCell
}