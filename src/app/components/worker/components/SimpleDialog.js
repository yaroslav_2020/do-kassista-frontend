import React, { useEffect, useReducer } from 'react';

import { Button, CircularProgress, Dialog, DialogActions, DialogTitle, IconButton, List, ListItem, ListItemSecondaryAction, ListItemText } from '@material-ui/core';
import { makeStyles, withStyles } from '@material-ui/core/styles';

import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import SaveIcon from '@material-ui/icons/CloudUpload';

import * as WorkerSchedule from 'app/utils/WorkerSchedule'
import DatePicker from '@trd/components/utils/DatePicker'

import moment from 'moment'
import { intervalReducer } from './intervalReducer';
import { indexOf } from 'lodash-es';
import { green, grey } from '@material-ui/core/colors';
import Alert from '@material-ui/lab/Alert';
import { useTranslation } from 'react-i18next'

const useStyles = makeStyles((theme) => ({
    MuiListItemGutters: {
        paddingRight: '32px'
    },
    section1: {
        margin: theme.spacing(2, 0, 2, 1),   
    },
    section2: {
        margin: theme.spacing(0, 0, 1, 2)
    },
    interval: {
        margin: theme.spacing(1, 2),
    },
    formControl: {
        margin: theme.spacing(1, 2),
        minWidth: 120,
    },
    dialog: {
        padding: theme.spacing(2, 4, 3),
    },
    loaderCont: {
        width: '100vw',
        height: '100vh',
        position: 'fixed',
        background: 'rgba(0,0,0,.1)',
        zIndex: '1000',
        justifyContent: 'center',
        boxSizing: 'border-box',
        alignItems: 'center',
        top: '0',
        left: '0'
    },
    deleteButton: {
        alignItems: 'center',
        marginLeft: '80px'
    }
}));

const AddButton = withStyles((theme) => ({
    root: {
      color: theme.palette.getContrastText(green[700]),
      backgroundColor: green[500],
      '&:hover': {
        backgroundColor: green[700],
      },
      padding: '15px 25px',
      marginLeft: '10px'
    },
  }))(Button);

const CreateButton = withStyles((theme) => ({
    root: {
      color: theme.palette.getContrastText(green[700]),
      backgroundColor: green[500],
      '&:hover': {
        backgroundColor: green[700],
      },
      marginLeft: '10px'
    },
  }))(Button);
  
  const CancelButton = withStyles(() => ({
    root: {
      backgroundColor: grey[50],
      '&:hover': {
        backgroundColor: grey[300],
      },
      marginLeft: '10px'
    },
  }))(Button);

function SimpleDialog(props) {
    const classes = useStyles();

    const { onClose, selectedValue, open, init, selectedHour, scheduleReload, initDay, hasSchedulesOnCreate } = props;

    const {t} = useTranslation()

    const [snackbar,setSnackbar] = React.useState({
        open: false,
        message: '', 
        severity: 'success',
    })

    const [fetching,setFetching] = React.useState(false)

    const [selectedDay, setSelectedDay] = React.useState(initDay)
    const [selectedStartHour, setSelectedStartHour] = React.useState(moment('08:00', 'HH:mm'));
    const [selectedEndHour, setSelectedEndHour] = React.useState(moment('22:00', 'HH:mm'));
    const [disabled, setDisabled] = React.useState(false)

    useEffect(() => {
        if(!!init) {
            const hour = hasSchedulesOnCreate ? init : init.filter( interval => interval.startTime !== selectedHour.startTime );  

            hour.map((i) => (
                dispatch(
                {
                    type: 'add',
                    payload: i
                }
                )
            ))

            if (hasSchedulesOnCreate) {
                setSelectedStartHour(moment("00:00", 'HH:mm'));
                setSelectedEndHour(moment("00:00", 'HH:mm'));
            } else if(!!selectedHour.startTime) {
                setSelectedStartHour(moment(selectedHour.startTime, 'HH:mm'));
                setSelectedEndHour(moment(selectedHour.endTime, 'HH:mm'));
                setSelectedDay(selectedHour.day);
                setDisabled(true)
            }
        }
    }, [init])

    useEffect(() => {
        if (!!initDay) {
            setSelectedDay(initDay)
        }
    }, [initDay])

    const handleClose = () => {
        onClose(selectedValue);        
        setSelectedDay('');
        setDisabled(false)
        
        setSelectedStartHour(moment('08:00', 'HH:mm'));
        setSelectedEndHour(moment('22:00', 'HH:mm'));
        
        dispatch({type: 'reset'})
    };
    
    const [intervals, dispatch] = useReducer(intervalReducer, init);


    const handleChange = (event) => {
        setSelectedDay(event.target.value);
    };
    
    
    const handleSubmit = (form) => {
        form.preventDefault()

        if (!!intervals[0]) {

            const data = {
                user: props.user,
                day: selectedDay,
                userScheduleHours: intervals
            }                        

            const response = (!selectedHour.id) ? 
                WorkerSchedule.createUserSchedule(data) : 
                WorkerSchedule.updateDaySchedule(selectedHour.id, data)
            
            response.then(({status}) => {
                if(status === 201){
                    setSnackbar({
                        open: true,
                        message: t('intervals.success.createdSchedule'),
                        severity: 'success',
                    })
    
                    scheduleReload(true)
                    setTimeout(() => {
                        setSnackbar( s => ({...s, open: false}))
                        handleClose();
                    }, 2000);
                } else if (status == 200) {
                    setSnackbar({
                        open: true,
                        message: t('intervals.success.updatedSchedule'),
                        severity: 'success',
                    })
                    scheduleReload(true);
                    setTimeout(() => {
                        setSnackbar( s => ({...s, open: false}))
                        handleClose()
                    }, 2000);
                } else {
                    response.then(({response: {data}}) => {
                        const message = data['hydra:description'].split(': ');
        
                        setSnackbar({
                            open: true,
                            message: `${t(`intervals.error.${message[0]}`)} ${message[1]}`, 
                            severity: 'error',
                        })
                        setTimeout(() => {
                            setSnackbar( s => ({...s, open: false}))
                        }, 2000);
                    })
                }
            })
            
        } else {
            setSnackbar({
                open: true,
                message: t('intervals.error.emptyIntervals'), 
                severity: 'error',
            })
            setTimeout(() => {
                setSnackbar( s => ({...s, open: false}))
            }, 2000);
        }

    }

    // GENERAL SETUP

    const handleHourChange = ( pickerName, pickerValue ) => {
        if ( pickerName === 'startHour' ) { setSelectedStartHour(pickerValue); }
            
        else if ( pickerName === 'endHour' ) { setSelectedEndHour(pickerValue) }
    };

    const handleUpdate = (interval, e) => {
        e.preventDefault()

        setSelectedStartHour(moment(interval.startTime, 'HH:mm'));
        setSelectedEndHour(moment(interval.endTime, 'HH:mm'));

        const action = {
            type: 'delete',
            payload: interval
        }

        dispatch( action );
    }

    const addInterval = (e) => {
        e.preventDefault();

        const startScheduleHour = moment(`${moment().format('YYYY-MM-DD')} 8:00:00`,'YYYY-MM-DDTHH:mm:ss[Z]')
        const endScheduleHour = moment(`${moment().format('YYYY-MM-DD')} 22:00:00`,'YYYY-MM-DDTHH:mm:ss[Z]')

        if (selectedStartHour.valueOf() < selectedEndHour.valueOf()) {

            if ((startScheduleHour.valueOf() <= selectedStartHour.valueOf()) &&
                (endScheduleHour.valueOf() >= selectedEndHour.valueOf()))
            {
                const newInterval = {
                    startTime: moment(selectedStartHour).format('HH:mm'),
                    endTime: moment(selectedEndHour).format('HH:mm')
                }
                
                setSelectedStartHour(moment().startOf('day'));
                setSelectedEndHour(moment().startOf('day'));
    
                const action = {
                    type: 'add',
                    payload: newInterval
                }
        
                dispatch( action );
            } else {
                setSnackbar({
                    open: true,
                    message: t('intervals.error.outOfRange'), 
                    severity: 'error',
                })
                setTimeout(() => {
                    setSnackbar( s => ({...s, open: false}))
                }, 3000);                
            }

        } else {
            setSnackbar({
                open: true,
                message: t('intervals.error.negativeIntervals'), 
                severity: 'error',
            })
            setTimeout(() => {
                setSnackbar( s => ({...s, open: false}))
            }, 3000);
        }

    }

    const deleteInterval = (interval, e) => {
        e.preventDefault();

        const action = {
            type: 'delete',
            payload: interval
        }

        dispatch( action );
    }

    
    return (
        <>
            {
                fetching && 
                <div className={classes.loaderCont} style={{display: props.fetching ? 'flex' : 'none',}}>
                    <CircularProgress />
                </div>
            }
            <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={open}>
                <DialogTitle id="simple-dialog-title">{t('workerOffers.schedule')} - {!!selectedDay && t(`Schedule.days.${selectedDay.toLowerCase()}`).toUpperCase()}</DialogTitle>
                {
                    snackbar.open &&
                    <Alert severity={snackbar.severity}>{snackbar.message}</Alert>

                }
                    <form onSubmit={handleSubmit} className={classes.dialog} >
                        <List>
                            <ListItem className={classes.MuiListItemGutters} >
                                <DatePicker 
                                    type='time'
                                    name='startHour'
                                    label={t('Schedule.startHour')}
                                    handlePicker={ handleHourChange }
                                    value={ selectedStartHour }
                                    className={classes.section1}
                                />

                                <DatePicker 
                                    type='time'
                                    name='endHour'
                                    label={t('Schedule.endHour')}
                                    handlePicker={ handleHourChange }
                                    value={ selectedEndHour }
                                    className={classes.section1}
                                />

                                <AddButton
                                    variant="contained"
                                    onClick={ addInterval }
                                    size="small"
                                    disableElevation
                                >
                                    {t('general.add')}
                                </AddButton>
                                
                            </ListItem>
                            <List className={classes.interval} dense={true}>
                            {
                                intervals &&
                                intervals.map(interval => (
                                    <ListItem
                                        button
                                        key={indexOf(interval)}
                                        className={classes.section2}
                                    >
                                        <ListItemText
                                            onClick={ (e) => handleUpdate(interval, e) }
                                            primary={`${interval.startTime} - ${interval.endTime}`}
                                        />
                                        <ListItemSecondaryAction>
                                            <IconButton edge="end" aria-label="delete">
                                                <HighlightOffIcon color="error" onClick={ (e) => deleteInterval( interval, e ) } />
                                            </IconButton>                                    
                                        </ListItemSecondaryAction>
                                    </ListItem>
                                ))
                            }
                            </List>

                        </List>

                    <DialogActions>
                        <CancelButton onClick={handleClose} variant="contained" disableElevation>
                            {t('general.cancel')}                                                        
                        </CancelButton>
                        {
                            disabled &&
                            <CreateButton type="submit" variant="contained" startIcon={<SaveIcon />}>
                                {t('general.update')}                                                        
                            </CreateButton>
                        }
                        {
                            !disabled &&
                            <CreateButton type="submit" variant="contained" startIcon={<SaveIcon />}>
                                {t('general.create')}                                
                            </CreateButton>
                        }
                    </DialogActions>
                    </form>
            </Dialog>
        </>
    );
}

export default SimpleDialog;