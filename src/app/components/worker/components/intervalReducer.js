
export const intervalReducer = ( state =[], action ) => {
    switch (action.type) {
        case 'add':
            return [ ...state, action.payload ];
    
        case 'delete':
            return state.filter( interval => interval !== action.payload );

        case 'reset':
            return [];

        default:
            return state;
    }
}
