import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import {
    Grid,
    TableRow,
    useMediaQuery
} from '@material-ui/core'

import { useStyles,desktopStyles, mobileStyles } from "./css/offerList";
import { getWorkerOffers } from 'app/utils/User';
import { connect } from 'react-redux';

import * as FetchActions from 'app/store/actions/FetchActions';
import RightArrowIcon from '../../assets/images/icons/rightArrowIcon.svg'
import LeftArrowIcon from '../../assets/images/icons/leftArrowIcon.svg'



const OfferList = (props) => {
    // const [active,setActive] = useState(1)
    const active = 1;

    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()
    const { t } = useTranslation()
    const matches = useMediaQuery('(max-width:600px)')

    const handleDate = ( date ) => {
        let dt = new Date(date)
        let months = ['january',
                    'february',
                    'march',
                    'april',
                    'may',
                    'june',
                    'july',
                    'August',
                    'September',
                    'October',
                    'November',
                    'December']
        
        let month = t(`Schedule.shortMonth.${months[dt.getMonth()]}`)
    
        return `${month}${dt.getDate()}, ${dt.getFullYear()}`
    }

    // FETCHING WORKER'S OFFERS
    const [offers,setOffers] = useState([])
    const [offersToShow,setOffersToShow] = useState({
        pos: 0,
        page: 1,
        offers: []
    })

    useEffect(() => {
        (async () => {
            props.fetching();
            try {
                const offerFetch = await getWorkerOffers( props.user_id );

                console.log( 'respuesta del await ', offerFetch );
                
                if ( !!offerFetch['hydra:member'].length ) {
                    const offers = offerFetch['hydra:member']
                        .filter( offer => !offer.deleted )
                        .map( offer => ({
                            client: offer.client[0].name,
                            id: offer.id,
                            status: offer.status,
                            createdDate: offer.timestapableCreated
                        }))
                        .sort( (a, b) => {
                            if ( a.id > b.id ) return -1                        
                            if ( a.id < b.id ) return 1
                            return 0
                        })
                    setOffers( offers );
                    setOffersToShow({
                        ...offersToShow,
                        pos: [0,8],
                        page: 1,
                        offers: [...offers.slice(0,8)]
                    })
                } else {
                    console.log('respuesta de offers: no se encontraron ofertas cargadas', offersToShow );
                }

                props.success()      

            } catch (error) {
                props.error()
                console.log( error );
            }
        })();
    },[ props.user_id ])
    // END FETCHING

    const prevPage = () => {
        if((offersToShow.page - 1) >= 1)
            setOffersToShow({
                ...offersToShow,
                pos: [offersToShow.pos[0] - 8,offersToShow.pos[1] - 8],
                page: offersToShow.page - 1,
                offers: [...offers.slice(offersToShow.pos[0] - 8,offersToShow.pos[1] - 8)]
            })
    }

    const nextPage = () => {
        if((offersToShow.page + 1) <= Math.ceil(offers.length / 8))
            setOffersToShow({
                ...offersToShow,
                pos: [offersToShow.pos[0] + 8,offersToShow.pos[1] + 8],
                page: offersToShow.page + 1,
                offers: [...offers.slice(offersToShow.pos[0] + 8,offersToShow.pos[1] + 8)]
            })
    }    

    return (
        <Grid item xs={12} md={8} sm={6}>
            <div className={ matches ? mobileClasses.contTitle : desktopClasses.contTitle } >
                <p className={ matches ? mobileClasses.title : desktopClasses.title } >{t('columnMenu.myRequests')}</p>
            </div>
            <div className={ matches ? mobileClasses.contTabs : desktopClasses.contTabs } >
                <div 
                    className={ matches ? mobileClasses.tab : desktopClasses.tab } 
                    style={{
                        borderBottom: active === 0 ? '2px solid #0097F6' : 'none',
                        display: 'none'
                    }} 
                >
                    <p>{t('workerOffers.new')}</p>
                </div>
                <div 
                    className={ matches ? mobileClasses.tab : desktopClasses.tab } 
                    style={{
                        borderBottom: active === 1 ? '2px solid #0097F6' : 'none',
                    }} 
                >
                    <p>{t('workerOffers.accepted')}</p>
                </div>
                <div 
                    className={ matches ? mobileClasses.tab : desktopClasses.tab } 
                    style={{
                        borderBottom: active === 2 ? '2px solid #0097F6' : 'none',
                        display: 'none'
                    }} 
                >
                    <p>{t('workerOffers.refused')}</p>
                </div>
            </div>
            <div className={ matches ? mobileClasses.contTable : desktopClasses.contTable }>
                <table className={classes.table} >
                    <thead >
                        <tr className={classes.tr} >
                            <th className={`${matches ? mobileClasses.thBody : desktopClasses.thBody} ${classes.firstTh}`} ><p className={`${classes.generalCell} ${classes.firstCell}`} >{t('general.order')}#</p></th>
                            <th className={`${matches ? mobileClasses.thBody : desktopClasses.thBody} ${classes.firstTh}`} ><p className={`${classes.generalCell} ${classes.firstCell}`} >{t('general.client')}</p></th>
                            <th className={`${matches ? mobileClasses.thBody : desktopClasses.thBody} ${classes.middleTh}`} ><p className={classes.generalCell} >{t('general.service')}</p></th>
                            <th className={`${matches ? mobileClasses.thBody : desktopClasses.thBody} ${classes.middleTh}`} ><p className={classes.generalCell} ></p></th>
                            <th className={`${matches ? mobileClasses.thBody : desktopClasses.thBody} ${classes.lastTh}`} ><p className={classes.generalCell} ></p></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            offersToShow.offers.length
                                ?   offersToShow.offers.map(offer => {
                                        return <tr key={offersToShow.offers.indexOf(offer)} className={classes.tr}>
                                                    <td className={matches ? mobileClasses.tdBody : desktopClasses.tdBody} >{offer.id}</td>
                                                    <td className={matches ? mobileClasses.tdBody : desktopClasses.tdBody} >{offer.client}</td>
                                                    <td className={matches ? mobileClasses.tdBody : desktopClasses.tdBody} >Servicio de Domicilio</td>
                                                    <td className={matches ? mobileClasses.tdBody : desktopClasses.tdBody} >{handleDate(offer.createdDate)}</td>     
                                                    <td className={`${matches ? mobileClasses.tdBody : desktopClasses.tdBody} ${classes.link}`} ><Link to={`/offer/item/${offer.id}`}>VER DETALLES </Link></td>                            
                                                </tr>
                                    })
                                :   <TableRow />
                        }                    
                    </tbody>
                </table>       
            </div>
            <div className={matches ? mobileClasses.pagination : desktopClasses.pagination} >
                    <button className={classes.paginationButton} onClick={prevPage} ><img src={LeftArrowIcon} /></button>
                    <p className={classes.paginationPage} >{offersToShow.page}</p>
                    <p className={classes.paginationTotalPages} >De { Math.ceil(offers.length / 8) && offersToShow.page }</p>
                    <button className={classes.paginationButton} onClick={nextPage} ><img src={RightArrowIcon} alt='' /></button>
        </div>
        </Grid>
    )
}

const mapState = state => ({
    user_id: state.auth.user.data.id
})

const mapDispatch = {
    ...FetchActions
}

export default connect(mapState, mapDispatch)(OfferList)