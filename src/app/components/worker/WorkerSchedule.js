import { Container, Divider, FormControlLabel, Grid, IconButton, List, ListItem, ListItemSecondaryAction, ListItemText, Paper, Radio, RadioGroup, Table, TableBody, TableContainer, TableHead, TableRow, Typography, useMediaQuery } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { useStyles,desktopStyles, mobileStyles, StyledTableRow, StyledTableCell } from "./css/workerSchedule";
import { useTranslation } from 'react-i18next';

import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';

import * as OfferActions from 'app/store/actions/OfferActions'
import * as FetchActions from 'app/store/actions/FetchActions'
import GenericPrompt from "app/components/utils/GenericPrompt";
import { getUserScheduleCollection, deleteUserScheduleHours, deleteUserSchedule} from 'app/utils/WorkerSchedule';
import { getUserPreference, updateUserPreference } from "app/utils/WorkerPreference";
import GenericSnackbar from 'app/utils/GenericSnackbar'

import { withStyles } from '@material-ui/core/styles';
import { Button, CircularProgress} from '@material-ui/core';
import SimpleDialog from './components/SimpleDialog';
import { green } from '@material-ui/core/colors';
import Step1Service from '@trd/components/Offer/partials/Step1Service';


const days = [
    'MONDAY',
    'TUESDAY',
    'WEDNESDAY',
    'THURSDAY',
    'FRIDAY',
    'SATURDAY',
    'SUNDAY'
];

const StyledRadio = withStyles({
    root: {
        color: '#cacaca',
        '&$checked': {
            color: '#0097F6',
        },
    },
    checked: {},
})((props) => <Radio color='default' {...props} />)

const CreateButton = withStyles((theme) => ({
    root: {
      color: theme.palette.getContrastText(green[700]),
      backgroundColor: green[500],
      '&:hover': {
        backgroundColor: green[700],
      },
      marginLeft: '10px'
    },
  }))(Button);

const getSchedules = async(user_iri) => {
    try {
        const {userSchedules} = await getUserScheduleCollection( `${user_iri}/schedule`);

        const response = userSchedules.map( ({day, id, userScheduleHours}) => ({
            id,
            day,
            userScheduleHours
        }))            
        
        return response;
    } catch (error) {
        console.log( 'userSchedule error: ', error );
    }
}

const getPreferences = async(user_id) => {
    try {
        const userPreferences = await getUserPreference(user_id);

        let response = userPreferences['hydra:member']

        if (response !== 0) {
            response = userPreferences['hydra:member'].map( ({id, petPresence, extraService}) => ({
                id,
                petPresence,
                extraService
            }))
        }
        return response
    } catch (error) {
        console.log( 'userPreferences error: ', error );    
    }
}

const WorkerSchedule = (props) => {
    
    const {t} = useTranslation();    
    
    const matches = useMediaQuery('(max-width:600px)')
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()
    
    const [dense, setDense] = useState(false);
    const [secondary, setSecondary] = useState(false);

    const [dialog,setDialog] = useState({ open: false, dataAction: -1 })
    const openDialog = (id, day) => setDialog({ open: true, dataAction: {id, day} })
    const closeDialog = () => setDialog({ open: false, dataAction: -1 })
    
    const [fetching,setFetching] = useState(true)
    const [fetchingPreferences, setFetchingPreferences] = useState(true)
    const [schedule, setSchedule] = useState([])
    const [preference, setPreference] = useState([])

    const [isUsed, setIsUsed] = useState(false);

    // CREATE MODULE

    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [selectedValue, setSelectedValue] = React.useState(days[1]);

    const [intervals, setIntervals] = useState([])
    const [selectedHour, setSelectedHour] = useState([])

    const [selectedDay, setSelectedDay] = useState('')

    const [snackbar, setSnackbar] = useState([])

    const handleClickOpen = (currentDay) => {    

        const hasSchedules = (schedule.some(({day}) => day === currentDay))    

        if (hasSchedules) {
            const currentSchedule = schedule.find(({day}) => day === currentDay)            
            setIsUsed(hasSchedules)
            handleEdit(currentSchedule.userScheduleHours[currentSchedule.userScheduleHours.length-1], currentSchedule)

        } else {
            setSelectedDay(currentDay)
            setOpen(true);
        }    
    };

    const handleClose = (value) => {
        setOpen(false);
        setSelectedDay('');
        setIsUsed(false);
        setSelectedValue(value);
    };

    // END CREATE MODULE

    useEffect(() => {
        props.getExtraServices()     
        getPreferences(props.user_id)
                .then(setPreference)
                .catch(console.log)
                .finally(() => {
                    setFetchingPreferences(false)
                })
    }, [])

    useEffect(() => {
        if(fetching){                            
            getSchedules(props.user_iri)
                .then(setSchedule)
                .catch(console.log)
                .finally(() => {
                    setFetching(false)
                })
        }
    }, [fetching])

    useEffect(() => {
        if (preference.length !== 0) {            
            const skills = preference
                .map(({extraService, petPresence}) => {
                    if (extraService === null) {
                        props.SaveStep1({
                            pet: petPresence ? 'PET' : 'NOT_PET'
                        })            
                    } else {
                        const id = extraService.split('/');
                        const service = props.pointServices[`service_${id[3]}`]
    
                        return {
                            [`service_${id[3]}`]: {
                                ...service,
                                checked: true
                            }
                        }                            
                    }
                })       
                .reduce((a, v) => ({ ...a, ...v}), {})                             

            props.SavePointService({
                ...skills
            })                 

        }
    }, [preference])  


    const handleChangeSkills = () => {

        setFetchingPreferences(true)        

        let preferences = Object.values(props.pointServices)
            .filter(service => service.checked)
            .map(service => ({
                extraService: service.iri                            
            }
            ))                      

        preferences = {
            userPreferences: [
                {
                    petPresence: props.pet === 'PET'? true : false    
                },
                ...preferences
            ]
        }

        updateUserPreference(props.user_id, preferences)
            .then(({status}) => {
                console.log('DATA INSIDE UPDATE: ', status);
                if (status === 200) {
                    setSnackbar({
                        open: true,
                        message: t('Advices.savedChanges'), 
                        severity: 'success',
                    })
                } else {
                    setSnackbar({
                        open: true,
                        message: t('Advices.somethingWrong'), 
                        severity: 'error',
                    })
                }
            })

        getPreferences(props.user_id)
                .then(setPreference)
                .catch(console.log)
                .finally(() => {
                    setFetchingPreferences(false)
                })
    }

    const handleDelete = ({id, day}) => {
        closeDialog()

        if (Object.keys(day.userScheduleHours).length === 1)
            deleteUserSchedule(day['id']).then(()=>{setFetching(true)})
        else
            deleteUserScheduleHours(id['@id']).then(()=>{setFetching(true)})
        
    }

    const handleCloseSnackbar = value => setSnackbar(value)

    const handleEdit = (hours, day) => {    

        const intervalHours = day.userScheduleHours.map(({startTime, endTime}) => ({
            startTime,
            endTime
        }))

        const selected = {
            id: day.id,
            day: day.day,
            startTime: hours.startTime,
            endTime: hours.endTime
        }

        setSelectedDay(day.day)
        setIntervals(intervalHours)
        setSelectedHour(selected)
        setOpen(true)
    }

    const handleRadioChange = input => {
        props.SaveStep1({
            pet: input.target.value
        })
    }

    return (
        <Grid container spacing={2} direction="column" alignItems="center" xs={12} md={8} sm={6}>
            <GenericSnackbar 
                handleClose={handleCloseSnackbar} 
                {...snackbar} 
            />
                {
                    matches && 
                    <div>
                        <Typography className={mobileClasses.title} gutterBottom>
                            {t('workerOffers.schedule')}
                        </Typography>
                    </div>
                }
                {
                    (fetching || fetchingPreferences) && 
                    <div className={classes.loaderCont} style={{display: props.fetching ? 'flex' : 'none',}}>
                        <CircularProgress />
                    </div>
                }
            <Container className={classes.container}>
                {
                    !matches && 
                    <div className={classes.contTitle} >
                        <Typography className={desktopClasses.title}>
                            {t('workerOffers.schedule')}
                        </Typography>
                    </div>
                }
                <div className={classes.paragraph}>
                    <Typography variant="body1" align="justify" paragraph>
                    {t('Schedule.workerScheduleMessage1')}
                    </Typography>
                    <Typography variant="body1" align="justify" paragraph>
                    {t('Schedule.workerScheduleMessage2')}
                    </Typography>
                    <Typography variant="body1" align="justify" gutterBottom>
                    {t('Schedule.workerScheduleMessage3')}
                    </Typography>
                </div>
                
                <Step1Service title={'Schedule.workerSkills'} />                                

                <div className={matches ? mobileClasses.container : desktopClasses.container} >
                    <p className={classes.title}>{t('Schedule.allowPet')}</p>
                    <RadioGroup
                        name='allow-pet'
                        value={props.pet}
                        onChange={handleRadioChange}
                    >
                        <FormControlLabel value='PET' className={classes.label} control={<StyledRadio />} label={t('general.yes')} />
                        <FormControlLabel value='NOT_PET' className={classes.label} control={<StyledRadio />} label={t('general.Not')} />
                    </RadioGroup>
                </div>

                {/* CREATE/UPDATE PREFERENCES */}
                <CreateButton className={classes.section1} variant="contained" onClick={handleChangeSkills}>
                    {t('buttons.saveChanges')}
                </CreateButton>                

                <TableContainer componen={Paper}>                
                    <Typography variant='h5' className={ matches ? mobileClasses.title : desktopClasses.title } align='center' gutterBottom > 
                        { t('Schedule.workerSchedule') }
                    </Typography>                

                    <Table className={classes.table} size="small">

                        {days.map( day => (
                            <>
                                <TableHead>
                                    <TableRow>
                                        <StyledTableCell align="left" colSpan={3}>        
                                            <List>
                                                <ListItem>
                                                    <ListItemText
                                                        primary={t(`Schedule.days.${day.toLowerCase()}`).toUpperCase()}
                                                    />
                                                    <ListItemSecondaryAction>
                                                        <CreateButton className={classes.section1} variant="contained" onClick={ () => handleClickOpen(day)} endIcon={<AddIcon />} >                                                        
                                                            {t('general.add')}                                                            
                                                        </CreateButton>                                                                                                                                
                                                    </ListItemSecondaryAction>
                                                </ListItem>
                                            </List>                                                                                                                  
                                        </StyledTableCell>                    
                                    </TableRow>                                
                                </TableHead>
                                <TableBody>
                                {
                                    !fetching &&
                                    schedule.filter( sch => sch.day === day )
                                        .map( (day) => (
                                            <StyledTableRow>
                                                <StyledTableCell>
                                                    <List dense={dense}>
                                                    {
                                                        day.userScheduleHours.map( hours => (
                                                                <ListItem>
                                                                    <ListItemText
                                                                        primary={`${hours.startTime} - ${hours.endTime}`}
                                                                        secondary={secondary ? 'Secondary text' : null}
                                                                    />
                                                                    <ListItemSecondaryAction>
                                                                        <IconButton edge="end" aria-label="edit" onClick={() => handleEdit(hours, day)}>
                                                                            <EditIcon />
                                                                        </IconButton>
                                                                        <IconButton onClick={() => openDialog(hours, day)} edge="end" aria-label="delete">
                                                                            <DeleteIcon />
                                                                        </IconButton>
                                                                    </ListItemSecondaryAction>
                                                                </ListItem>                                        
                                                        ))
                                                    }
                                                    </List>                                                         
                                                </StyledTableCell>
                                            </StyledTableRow>
                                        ))
                                }
                                </TableBody>
                            </>
                        ))}
                    </Table>                 
                </TableContainer>                
                {
                    intervals &&
                    <SimpleDialog selectedValue={selectedValue} open={open} onClose={handleClose} user={props.user_iri} init={intervals} selectedHour={selectedHour} scheduleReload={setFetching} initDay={selectedDay} hasSchedulesOnCreate={isUsed}/>
                }
                {
                    !intervals &&
                    <SimpleDialog selectedValue={selectedValue} open={open} onClose={handleClose} user={props.user_iri} scheduleReload={setFetching} initDay={selectedDay}/>
                }
                
            </Container>
            <GenericPrompt 
                title={t('intervals.message.title')}
                message={t('intervals.message.delete')}
                close={closeDialog}
                action={handleDelete}
                data={dialog}
            />
            </Grid>
    )
}

const mapState = state => ({
    user_iri: state.auth.user.data.iri,
    user_id: state.auth.user.data.id,
    period: state.Offer.period,
    startFromDate: state.Offer.startFromDate,
    endDate: state.Offer.endDate,
    startHour: state.Offer.startHour,
    endHour: state.Offer.endHour,
    periodPoints: state.Offer.periodPoints,
    hoursNeeded: state.Offer.hoursNeeded,
    pet: state.Offer.pet,
    pointServices: state.Offer.pointServices
})

const mapDispatch = {
    ...OfferActions,
    ...FetchActions

}

export default connect(mapState,mapDispatch)(WorkerSchedule)