import React,{ useState , useEffect } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import * as User from '../../utils/User'

import *  as FetchActions from '../../store/actions/FetchActions'

import ImgUser from '../../assets/images/icons/user-header.svg'

import {
    Box,
    Typography,
    TextField,
    Button,
    Grid,
    useMediaQuery
} from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
    container: {
        borderRadius: '13px',
        background: '#fff',
        color: '#191919',
        boxShadow: '0px 5px 10px rgba(0,0,0,.12)'
    },
    contTitle: {
        background: '#fff',
        borderBottom: '1px solid #D8D8D8',
        padding: '30px',
        borderRadius: '13px 13px 0px 0px',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        position: 'relative',
    },
    heading: {
        width: '100%',
        padding: '30px 30px 0 30px',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    img: {
        width: '80px',
        height: '80px',
        borderRadius: '100%',
        marginRight: '24px'
    },
    personalInfoCont: {
        marginTop: '20px',
        width: '100%',
        padding: '10px 30px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexWrap: 'wrap'
    },
    textField: {
        border: '1px solid #DBDBDB',
        borderRadius: '9px',
        '& fieldset': {
            border: 'none'
        },
        '& .MuiInputBase-root:hover fieldset': {
            border: 'none'
        },
        '& .MuiInputBase-root:focus fieldset': {
            border: 'none'
        }
    }
})

const desktopStyles = makeStyles({
    title: {
        fontSize: '25px',
        fontWeight: 'bold',
        color: '#272727'
    },
    userFullName: {
        fontSize: '37px',
        color: '#000',
        fontWeight: 'bold'
    },
    changeImgLabel: {
        fontSize: '19px',
        fontWeight: '500',
        color: '#000'
    },
    inputLabel: {
        fontSize: '16px',
        fontWeight: '500',
        color: '#848484',
        marginBottom: '12px'
    },
    detailsLabel: {
        fontSize: '17px',
        fontWeight: '500',
        color: '#313131'
    },
    infoDiv: {
        width: '45%'
    },
    button: {
        background: '#0097F6',
        borderRadius: '6px',
        color: '#fff',
        outline: 'none',
        padding: '14px 45px',
        border: 'none',
        width: '300px',
        height: '75px',
        fontSize: '22px',
        fontWeight: 'bold',
        margin: '40px 0px 20px 0px',
        '&:hover': {
            background: '#0097F6',
            border: 'none'
        }
    },
})

const mobileStyles = makeStyles({
    title: {
        fontSize: '20px',
        fontWeight: 'bold',
        color: '#272727'
    },
    userFullName: {
        fontSize: '29px',
        color: '#000',
        fontWeight: 'bold'
    },
    changeImgLabel: {
        fontSize: '15px',
        fontWeight: '500',
        color: '#000'
    },
    inputLabel: {
        fontSize: '14px',
        fontWeight: '400',
        color: '#272727',
        marginBottom: '16px'
    },
    detailsLabel: {
        fontSize: '14px',
        fontWeight: '400',
        color: '#272727'
    },
    infoDiv: {
        width: '100%',
        marginTop: '20px'
    },
    button: {
        background: '#0097F6',
        borderRadius: '6px',
        color: '#fff',
        outline: 'none',
        padding: '16px 0',
        border: 'none',
        width: '100%',
        height: '60px',
        fontSize: '20px',
        fontWeight: 'bold',
        // margin: '40px 0px 20px 0px',
        '&:hover': {
            background: '#0097F6',
            border: 'none'
        }
    },
})

const ClientInfo = props => {

    const {t} = useTranslation()

    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const [state,setState] = useState({
                                name: '',
                                phoneNumber: '',
                                bankName: '',
                                bankAccount: ''
                            })

    const matches = useMediaQuery('(max-width:600px)')

    const getUserData = async () => {
        props.fetching()
        let response = await User.getUserInfo(props.user)
        props.success()
        setState({
            name: response.name,
            phoneNumber: response.phoneNumber,
            bankName: response.bankName !== null ? response.bankName : '',
            bankAccount: response.bankAccount !== null ? response.bankAccount : ''
        })
    }

    useEffect(() => {
        getUserData()
    },[])

    const personalInfo = [
        {
            name: 'name',
            label: t('general.fullName'),
            value: state.name,
        },
        {
            name: 'phoneNumber',
            label: t('address.phoneNumber'),
            value: state.phoneNumber
        }
    ]

    const independentInfo = [
        {
            name: 'bankName',
            label: t('register.inputs.bankName'),
            value: state.bankName,
        },
        {
            name: 'bankAccount',
            label: t('register.inputs.bankAccount'),
            value: state.bankAccount
        }
    ]

    const handleChange = input => {
        setState({
            ...state,
            [input.target.name]: input.target.value
        })
    }

    const handleSubmit = async btn => {
        props.fetching()
        let response = await User.Update({
            user_iri: props.user,
            ...state
        })

        if(response.status === 200){
            props.success()
            alert(t('general.updateData'))
        }else{
            props.error()
        }            
    }

    return (
        <Grid item xs={12} md={8} sm={6}>
            <div className={classes.container}>
                <div className={classes.contTitle} >
                    <Typography className={matches ? mobileClasses.title : desktopClasses.title} gutterBottom>
                        {t('columnMenu.settings.personalInformation')}
                    </Typography>
                </div>
                <div className={classes.heading} >
                    <img src={ImgUser} className={classes.img} alt=''/>
                    <div>
                        <Typography className={matches ? mobileClasses.userFullName : desktopClasses.userFullName} gutterBottom>
                            {props.displayName}
                        </Typography>
                        <Typography className={matches ? mobileClasses.changeImgLabel : desktopClasses.changeImgLabel} gutterBottom>
                            {t('general.changeAvatar')}
                        </Typography>
                    </div>
                </div>
                <div className={classes.personalInfoCont} >
                    {
                        personalInfo.map((info,id) => <div key={`pi${id}`} className={matches ? mobileClasses.infoDiv : desktopClasses.infoDiv} >
                                                        <Typography className={matches ? mobileClasses.inputLabel : desktopClasses.inputLabel} >
                                                            {info.label}
                                                        </Typography>
                                                        <TextField 
                                                            fullWidth
                                                            type='text'
                                                            name={info.name}
                                                            value={info.value}
                                                            className={classes.textField}
                                                            variant='outlined'
                                                            autoComplete='off'
                                                            onChange={handleChange}
                                                        />
                                                    </div>)
                    }

                    {
                        (independentInfo.length > 0 && (props.role !== undefined ? props.role.indexOf('ROLE_INDEPENDENT_WORKER') > -1 : false))
                            ? independentInfo.map((info,id) => <div key={`pi${id}`} className={matches ? mobileClasses.infoDiv : desktopClasses.infoDiv} >
                                                            <Typography className={matches ? mobileClasses.inputLabel : desktopClasses.inputLabel}>
                                                                {info.label}
                                                            </Typography>
                                                            <TextField 
                                                                type='text'
                                                                fullWidth
                                                                name={info.name}
                                                                value={info.value}
                                                                variant='outlined'
                                                                autoComplete='off'
                                                                onChange={handleChange}
                                                            />
                                                        </div>)
                            : <div/>
                    }
                </div>
                <div className={classes.personalInfoCont} >
                    <div className={matches ? mobileClasses.infoDiv : desktopClasses.infoDiv} >
                        <Typography className={matches ? mobileClasses.inputLabel : desktopClasses.inputLabel} >
                            { t('OfferStep2.email') }
                        </Typography>
                        <Typography className={matches ? mobileClasses.detailsLabel : desktopClasses.detailsLabel} >
                            { props.email }
                        </Typography>
                    </div>
                    <div className={matches ? mobileClasses.infoDiv : desktopClasses.infoDiv} >
                        <Typography className={matches ? mobileClasses.inputLabel : desktopClasses.inputLabel} >
                            { t('general.role') }
                        </Typography>
                        <Typography className={matches ? mobileClasses.detailsLabel : desktopClasses.detailsLabel} >
                            { t(`roles.${props.role[0].toLowerCase()}`) }
                        </Typography>
                    </div>
                </div>
                <Box pl={3} pr={3} pt={2} pb={3} mb={2}>
                    <Button 
                        variant='outlined'
                        onClick={handleSubmit}
                        className={matches ? mobileClasses.button : desktopClasses.button}
                    >
                        {t('buttons.saveChanges')}
                    </Button>
                </Box>
            </div>
        </Grid>
    )
}

const mapState = state => ({
    user: state.auth.user.data.iri,
    displayName: state.auth.user.data.displayName,
    email: state.auth.user.data.email,
    role: state.auth.user.role
})

const mapDispatch = {
    ...FetchActions
}

export default connect(mapState,mapDispatch)(ClientInfo)