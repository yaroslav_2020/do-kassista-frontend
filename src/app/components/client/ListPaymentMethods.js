import React,{ useState , useEffect } from 'react'
import { connect } from 'react-redux'
import { Link , useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import * as AddressActions from '../../store/actions/AddressActions'
import * as FetchActions from '../../store/actions/FetchActions'
import { detachPaymentMethod, listPaymentMethod, setDefaultPaymentMethod } from 'app/utils/PaymentIntent'
import GenericPrompt from "app/components/utils/GenericPrompt";
import GenericSnackbar from 'app/utils/GenericSnackbar'


import {
    Typography,
    Grid,
    Button,
    Accordion,
    AccordionSummary,
    AccordionDetails,
    useMediaQuery,
    Chip
} from '@material-ui/core'

import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

import { makeStyles } from '@material-ui/core/styles'
import moment from 'moment'

const useStyles = makeStyles((theme) => ({
    container: {
        borderRadius: '13px',
        background: '#fff',
        color: '#191919',
    },
    contTitle: {
        background: '#fff',
        padding: '30px 64px',
        borderRadius: '13px 13px 0px 0px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        position: 'relative',
    },
    card: {
        borderRadius: '13px',
        boxShadow: '0px 5px 10px rgba(0,0,0,.12)',
        marginTop: '32px'
    },
    cardHeader: {
        padding: '30px 64px',
        borderBottom: '1px solid #DBDBDB',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    cardTitleCont: {
        width: '50%',
    },
    cardActionButtonsCont: {
        width: '40%',
        display: 'flex',
    },
    cardContent: {
        padding: '25px 64px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        flexWrap: 'wrap'
    }
}))

const desktopStyles = makeStyles({
    title: {
        fontSize: '25px',
        fontWeight: 'bold',
        color: '#242424'
    },
    addButton: {
        borderRadius: '9px',
        background: '#0097F6',
        fontSize: '15px',
        fontWeight: '500',
        color: '#fff',
        padding: '15px',
        '&:hover': {
            background: '#0097F6',
        },
        '& a': {
            textDecoration: 'none',
            fontSize: '15px',
            fontWeight: '500',
            color: '#fff'
        },
        '& a:hover': {
            textDecoration: 'none',
            fontSize: '15px',
            fontWeight: '500',
            color: '#fff'
        }
    },
    cardTitle: {
        fontSize: '20px',
        fontWeight: 'bold',
        color: '#242424'
    },
    blueButton: {
        width: '115px',
        Height: '40px',
        border: '1px solid #0079F6',
        borderRadius: '9px',
        fontSize: '14px',
        fontWeight: '500',
        color: '#0097F6',
        marginRight: '15px',
        padding: '10px 24px'
    },
    blackButton: {
        width: '115px',
        Height: '40px',
        border: '1px solid #000',
        borderRadius: '9px',
        fontSize: '14px',
        fontWeight: '500',
        color: '#000',
        marginRight: '15px',
        padding: '10px 24px'
    },
    cardItem: {
        width: '50%',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginBottom: '25px'
    },
    cardItemLabel: {
        fontSize: '18px',
        fontWeight: '500',
        color: '#A8A8A8',
        minWidth: '160px',
        marginRight: '10px'
    },
    cardItemInfo: {
        fontSize: '18px',
        fontWeight: '500',
        color: '#272727'
    }
})

const mobileStyles = makeStyles({
    title: {
        fontSize: '20px',
        fontWeight: 'bold',
        color: '#272727'
    },
    addButton: {
        fontWeight: '400',
        fontSize: '20px',
        color: '#272727',
        background: '#fff',
        border: 'none',
        boxShadow: 'none',
        textDecoration: 'none',
        '&:hover': {
            textDecoration: 'none',
            fontSize: '20px',
            fontWeight: '400',
            color: '#272727'
        },
        '&:not([role=button])': {
            textDecoration: 'none',
            fontSize: '20px',
            fontWeight: '400',
            color: '#272727'
        }
    },
    plus: {
        fontSize: '20px',
        fontWeight: 'bold',
        color: '#272727'
    },
    container: {
        width: '100%',
    },
    contTitle: {
        width: '100%',
        background: '#fff',
        // borderBottom: '1px solid #D8D8D8',
        padding: '20px 30px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        position: 'relative',
    },
    accordion: {
        boxShadow: '0px 5px 10px rgba(0,0,0,.12)',
        width: '100%',
        borderRadius: '13px',
        marginTop: '25px',
        '&:before': {
            height: '0px'
        }
    },
    accordionSummary: {
        padding: '22px 35px',
        '&:before': {
            height: '0px'
        }
    },
    accordionDetails: {
        padding: '20px 0 0 0',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        flexWrap: 'wrap',
        borderTop: '1px solid #DBDBDB',
    },
    accordionTitle: {
        fontSize: '18px',
        fontWeight: '500',
        color: '#000'
    },
    blueButton: {
        width: '115px',
        Height: '40px',
        border: '1px solid #0079F6',
        borderRadius: '9px',
        fontSize: '14px',
        fontWeight: '500',
        color: '#0097F6',
        marginRight: '15px',
        padding: '10px 24px'
    },
    blackButton: {
        width: '115px',
        Height: '40px',
        border: '1px solid #000',
        borderRadius: '9px',
        fontSize: '14px',
        fontWeight: '500',
        color: '#000',
        marginRight: '15px',
        padding: '10px 24px'
    },
    accordionItem: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginBottom: '20px'
    },
    accordionItemLabel: {
        fontSize: '14px',
        fontWeight: '500',
        color: '#A8A8A8',
        minWidth: '150px',
        marginLeft: '20px',
        marginBottom: '3px'
    },
    accordionItemInfo: {
        fontSize: '16px',
        fontWeight: '400',
        marginLeft: '20px',
        color: '#242424'
    },
    accordionActionButtonsCont: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: '70px',
        padding: '15px 20px',
        borderTop: '1px solid #DBDBDB'
    },
})

const ListPaymentMethods = props => {

    const {t} = useTranslation()

    const history = useHistory()

    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const matches = useMediaQuery('(max-width:600px)')

    const [addresses,setAddresses] = useState([])

    const [paymentMethods, setPaymentMethods] = useState([])

    const [dialog,setDialog] = useState({ open: false, dataAction: -1 })

    const openDialog = (pm) => setDialog({ open: true, dataAction: pm })

    const closeDialog = () => setDialog({ open: false, dataAction: -1 })

    const [snackbar,setSnackbar] = useState({
        open: false,
        message: '', 
        severity: 'success',
    })
    const handleCloseSnackbar = value => setSnackbar(value)

    const getPaymentMethods = () => listPaymentMethod({
        customer: props.clientEmail
    })            
    .then( ({data}) => {
        const defaultPaymentMethod = data.defaultPaymentMethod;

        if(data.paymentMethods !== null) {
            setPaymentMethods(data.paymentMethods.data.map(pm => ({                    
                id: pm.id,
                title: pm.card.brand,      
                exp_month: pm.card.exp_month,
                exp_year: pm.card.exp_year,
                isDefault: (defaultPaymentMethod === pm.id) ? true : false,
                info: [
                    {
                        title: t('general.last_digits'),
                        body: pm.card.last4
                    },
                    {
                        title: t('general.expiration_date'),
                        body: pm.card.exp_month + '/' + pm.card.exp_year
                    },


                ]
            })))
            props.success()
        }
    })
    .catch(error => {
        props.error()
        console.error(error)
    })

    useEffect(() => {
        props.fetching()
        getPaymentMethods()
    }, [])

    const [expanded,setExpanded] = useState('')

    const handleChange = panel => (event, newExpanded) => {
        setExpanded(newExpanded ? panel : false)
    }

    const switchAddressType = addressType => {
        switch(addressType){
            case 'SERVICE_ADDRESS':
                return t('OfferStep2.serviceAddress')
            case 'COMPANY_ADDRESS':
                return t('offerItem.companyAddress')
            case 'BILLING_ADDRESS':
                return t('OfferStep2.billingAddress')
            default:
                return ''
        }
    }

    const handleEditClick = async btn => {
        props.fetching()

        let res = await props.updateAddress(btn.target.name)

        if(res)
            history.push('/client/address/add')
    }

    const handleDelete = pm => {
        const paymentMethod = pm.id
        const isDefault = pm.isDefault

        closeDialog()

        props.fetching()

        if(!isDefault){
            detachPaymentMethod({
                paymentMethod: paymentMethod
            })            
            .then( ({data}) => {
                if(data.paymentMethodDetached !== null) {                
                    getPaymentMethods()
                    setSnackbar({
                        open: true,
                        message: t('payment.stripe.deleted'), 
                        severity: 'success',
                    })
                } else if(data.error){
                    setSnackbar({
                        open: true,
                        message: data.error.message, 
                        severity: 'error',
                    })
                    props.success()
                }
            })
            .catch(error => {
                props.error()
                console.error(error)
            })    
        } else {
            props.success()
            setSnackbar({
                open: true,
                message: t('payment.stripe.error.isDefault'),
                severity: 'error',
            })
        }
    }

    const handleDefault = pm => {
        const paymentMethod = pm

        props.fetching()

        setDefaultPaymentMethod({
            customer: props.clientEmail,
            invoice_settings: {
                default_payment_method: paymentMethod
            }
        })            
        .then( ({data}) => {
            if(data.updatedCustomerPaymentMethod !== null) {
                getPaymentMethods()
            }

        })
        .catch(error => {
            props.error()
            console.error(error)
        })        
    }    

    return (
        <Grid item xs={12} md={8} sm={6}>  
            <GenericSnackbar 
                handleClose={handleCloseSnackbar} 
                {...snackbar} 
            />
            {
                !matches
                    ?   <div className={classes.container}>
                            <div className={classes.contTitle}>
                                <Typography className={desktopClasses.title} gutterBottom>
                                    {t('columnMenu.myPaymentMethods')}
                                </Typography>
                                <Button
                                    variant='contained'
                                    className={desktopClasses.addButton}  
                                    component={Link}
                                    to='/client/payment-methods/add'
                                >
                                    {t('general.addNewPaymentMethods')}
                                </Button>
                            </div>
                            {

                                paymentMethods.map((paymentMethod, id) => 
                                    <div key={`list${id}`} className={classes.card}>
                                        <div className={classes.cardHeader}>
                                            <div className={classes.cardTitleCont}>
                                                <p className={desktopClasses.cardTitle} >{paymentMethod.title.toUpperCase()}</p>
                                                {
                                                    paymentMethod.isDefault
                                                    ?   <Chip
                                                            label="Predeterminado"
                                                        />
                                                    : (moment(moment(`${paymentMethod.exp_month}-${paymentMethod.exp_year}`, 'M/YYYY')).isAfter(new Date())) 
                                                        ?                                                      
                                                            <Chip
                                                                label={t('general.default')}
                                                                variant="outlined"
                                                                onClick={() => handleDefault(paymentMethod.id)}
                                                            />
                                                        : <></>
                                                    
                                                }                                                
                                            </div>
                                            <div className={classes.cardTitleCont}>
                                                
                                            </div>
                                            <div className={classes.cardActionButtonsCont}>
                                                <Button
                                                    className={desktopClasses.blueButton}  
                                                    component={Link}
                                                    to={{
                                                        pathname: '/client/payment-methods/add',
                                                        state: {
                                                            paymentMethod: paymentMethod.id,
                                                            last4: paymentMethod.info[0].body,
                                                            exp_month: paymentMethod.exp_month,
                                                            exp_year: paymentMethod.exp_year                
                                                        }
                                                    }}
                                                >
                                                    {t('general.edit')}
                                                </Button>                                                
                                                
                                                <button 
                                                    className={desktopClasses.blackButton} 
                                                    name={paymentMethod.id}
                                                    onClick={() => openDialog(paymentMethod)}
                                                >{t('general.delete')}</button>                                                                                                                                                 
                                            </div>
                                        </div>
                                        <div className={classes.cardContent} >
                                            {
                                                paymentMethod.info 
                                                    ? paymentMethod.info.map((info,id2) => <div key={`addres${id}sinfo${id2}`} className={desktopClasses.cardItem}>
                                                                                        <p className={desktopClasses.cardItemLabel} >{info.title.toUpperCase()}</p>
                                                                                        <p className={desktopClasses.cardItemInfo}>{info.body}</p>
                                                                                    </div>)
                                                    : <div/>
                                            }
                                        </div>
                                    </div>
                                )

                            }
                        </div>
                    :   <div className={mobileClasses.container} >
                            <div className={mobileClasses.contTitle}>
                                <Typography className={mobileClasses.title}>
                                    {t('columnMenu.myPaymentMethods')}
                                </Typography>
                                <Button
                                    variant='contained'
                                    className={desktopClasses.addButton}  
                                    component={Link}
                                    to='/client/payment-methods/add'
                                >
                                    {t('general.addNewPaymentMethods')}
                                </Button>
                            </div>

                            {

                                paymentMethods.map((paymentMethod, id) => 

                                    <Accordion key={`list${id}`} className={mobileClasses.accordion}>
                                        <AccordionSummary 
                                            expandIcon={<ExpandMoreIcon />}
                                            className={mobileClasses.accordionSummary}
                                        >
                                            <Typography className={mobileClasses.accordionTitle}>
                                                {paymentMethod.title.toUpperCase()}
                                            </Typography>
                                            {
                                                paymentMethod.isDefault
                                                ?   <Chip
                                                        label="Predeterminado"
                                                    />
                                                : (moment(moment(`${paymentMethod.exp_month}-${paymentMethod.exp_year}`, 'M/YYYY')).isAfter(new Date())) 
                                                    ?                                                      
                                                        <Chip
                                                            label={t('general.default')}
                                                            variant="outlined"
                                                            onClick={() => handleDefault(paymentMethod.id)}
                                                        />
                                                    : <></>
                                                
                                            }    
                                        </AccordionSummary>
                                        <AccordionDetails className={mobileClasses.accordionDetails} >  
                                            {
                                                paymentMethod.info 
                                                    ? paymentMethod.info.map((info,id2) => <div key={`list${id}sinfo${id2}`} className={desktopClasses.cardItem}>
                                                                                        <p className={desktopClasses.cardItemLabel} >{info.title.toUpperCase()}</p>
                                                                                        <p className={desktopClasses.cardItemInfo}>{info.body}</p>
                                                                                    </div>)
                                                    : <div/>
                                            }
                                            <div className={mobileClasses.accordionActionButtonsCont}>
                                                <Button
                                                    className={desktopClasses.blueButton}  
                                                    component={Link}
                                                    to={{
                                                        pathname: '/client/payment-methods/add',
                                                        state: {
                                                            paymentMethod: paymentMethod.id,
                                                            last4: paymentMethod.info[0].body,
                                                            exp_month: paymentMethod.exp_month,
                                                            exp_year: paymentMethod.exp_year                
                                                        }
                                                    }}
                                                >
                                                    {t('general.edit')}
                                                </Button>                                                
                                                
                                                <button 
                                                    className={desktopClasses.blackButton} 
                                                    name={paymentMethod.id}
                                                    onClick={() => openDialog(paymentMethod)}
                                                >{t('general.delete')}</button>   
                                            </div>
                                        </AccordionDetails>
                                    </Accordion>    
                                )                                
                            }
                        </div>
            }
            <GenericPrompt 
                title={t('general.deletePaymentMethod')}
                message={t('general.areYouSureToDoThis')}
                close={closeDialog}
                action={handleDelete}
                data={dialog}
            />  
        </Grid>
    )
}

const mapState = state => ({
    clientEmail: state.auth.user.data.email
})

const mapDispatch = {
    ...AddressActions,
    ...FetchActions
}

export default connect(mapState,mapDispatch)(ListPaymentMethods)