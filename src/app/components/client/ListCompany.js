import React,{ useState , useEffect } from 'react'
import { Link , useHistory } from 'react-router-dom'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import * as Company from '../../utils/Company'
import * as CompanyActions from '../../store/actions/CompanyActions'
import * as FetchActions from '../../store/actions/FetchActions'

import {
    Typography,
    Grid,
    Button,
    Accordion,
    AccordionSummary,
    AccordionDetails,
    useMediaQuery
} from '@material-ui/core'

import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
    container: {
        // border: '1px solid #DBDBDB',
        borderRadius: '13px',
        background: '#fff',
        color: '#191919',
        // boxShadow: '0px 5px 10px rgba(0,0,0,.12)',
    },
    contTitle: {
        background: '#fff',
        // borderBottom: '1px solid #D8D8D8',
        padding: '30px 64px',
        borderRadius: '13px 13px 0px 0px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        position: 'relative',
    },
    card: {
        borderRadius: '13px',
        boxShadow: '0px 5px 10px rgba(0,0,0,.12)',
        marginTop: '32px'
    },
    cardHeader: {
        padding: '30px 64px',
        borderBottom: '1px solid #DBDBDB',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    cardTitleCont: {
        width: '50%',
    },
    cardActionButtonsCont: {
        width: '40%',
        display: 'flex',
    },
    cardContent: {
        padding: '25px 64px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        flexWrap: 'wrap'
    }
}))

const desktopStyles = makeStyles({
    title: {
        fontSize: '25px',
        fontWeight: 'bold',
        color: '#242424'
    },
    addButton: {
        borderRadius: '9px',
        background: '#0097F6',
        fontSize: '15px',
        fontWeight: '500',
        color: '#fff',
        padding: '15px',
        '&:hover': {
            background: '#0097F6',
        },
        '& a': {
            textDecoration: 'none',
            fontSize: '15px',
            fontWeight: '500',
            color: '#fff'
        },
        '& a:hover': {
            textDecoration: 'none',
            fontSize: '15px',
            fontWeight: '500',
            color: '#fff'
        }
    },
    cardTitle: {
        fontSize: '20px',
        fontWeight: 'bold',
        color: '#242424'
    },
    blueButton: {
        width: '115px',
        Height: '40px',
        border: '1px solid #0079F6',
        borderRadius: '9px',
        fontSize: '14px',
        fontWeight: '500',
        color: '#0097F6',
        marginRight: '15px',
        padding: '10px 24px'
    },
    blackButton: {
        width: '115px',
        Height: '40px',
        border: '1px solid #000',
        borderRadius: '9px',
        fontSize: '14px',
        fontWeight: '500',
        color: '#000',
        marginRight: '15px',
        padding: '10px 24px'
    },
    cardItem: {
        width: '50%',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginBottom: '25px'
    },
    cardItemLabel: {
        fontSize: '18px',
        fontWeight: '500',
        color: '#A8A8A8',
        minWidth: '200px',
        marginRight: '10px'
    },
    cardItemInfo: {
        fontSize: '18px',
        fontWeight: '500',
        color: '#272727'
    }
})

const mobileStyles = makeStyles({
    title: {
        fontSize: '20px',
        fontWeight: 'bold',
        color: '#272727'
    },
    addButton: {
        fontWeight: '400',
        fontSize: '20px',
        color: '#272727',
        background: '#fff',
        border: 'none',
        boxShadow: 'none',
        textDecoration: 'none',
        '&:hover': {
            textDecoration: 'none',
            fontSize: '20px',
            fontWeight: '400',
            color: '#272727'
        },
        '&:not([role=button])': {
            textDecoration: 'none',
            fontSize: '20px',
            fontWeight: '400',
            color: '#272727'
        }
    },
    plus: {
        fontSize: '20px',
        fontWeight: 'bold',
        color: '#272727'
    },
    container: {
        width: '100%',
    },
    contTitle: {
        width: '100%',
        background: '#fff',
        // borderBottom: '1px solid #D8D8D8',
        padding: '20px 30px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        position: 'relative',
    },
    accordion: {
        boxShadow: '0px 5px 10px rgba(0,0,0,.12)',
        width: '100%',
        borderRadius: '13px',
        marginTop: '25px',
        '&:before': {
            height: '0px'
        }
    },
    accordionSummary: {
        padding: '22px 35px',
        '&:before': {
            height: '0px'
        }
    },
    accordionDetails: {
        padding: '20px 0 0 0',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        flexWrap: 'wrap',
        borderTop: '1px solid #DBDBDB',
    },
    accordionTitle: {
        fontSize: '18px',
        fontWeight: '500',
        color: '#000'
    },
    blueButton: {
        width: '115px',
        Height: '40px',
        border: '1px solid #0079F6',
        borderRadius: '9px',
        fontSize: '14px',
        fontWeight: '500',
        color: '#0097F6',
        marginRight: '15px',
        padding: '10px 24px'
    },
    blackButton: {
        width: '115px',
        Height: '40px',
        border: '1px solid #000',
        borderRadius: '9px',
        fontSize: '14px',
        fontWeight: '500',
        color: '#000',
        marginRight: '15px',
        padding: '10px 24px'
    },
    accordionItem: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginBottom: '20px'
    },
    accordionItemLabel: {
        fontSize: '14px',
        fontWeight: '500',
        color: '#A8A8A8',
        minWidth: '150px',
        marginLeft: '20px',
        marginBottom: '3px'
    },
    accordionItemInfo: {
        fontSize: '16px',
        fontWeight: '400',
        marginLeft: '20px',
        color: '#242424'
    },
    accordionActionButtonsCont: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: '70px',
        padding: '15px 20px',
        borderTop: '1px solid #DBDBDB'
    },
})

const ListCompany = props => {
    const history = useHistory()

    const {t} = useTranslation()

    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const matches = useMediaQuery('(max-width:600px)')

    const [companies,setCompanies] = useState([])

    useEffect(() => {
        props.fetching()
        Company.getCompanies(props.user)
            .then(res => {
                setCompanies([...res.filter(company => !company.deleted).map(company => ({
                    iri: company['@id'],
                    id: company.id,
                    name: company.name,
                    info: [
                        {
                            title: t('OfferStep2.name'),
                            body: company.name
                        },
                        {
                            title: t('register.inputs.bankName'),
                            body: company.bankName
                        },
                        {
                            title: t('register.inputs.bankAccount'),
                            body: company.bankAccount
                        },
                        {
                            title: t('company.swift'),
                            body: company.swift
                        },
                        {
                            title: t('company.cif'),
                            body: company.cif
                        },
                        {
                            title: `${t('address.phone')} - ${t('general.company')}`,
                            body: company.regNumber
                        },
                        {
                            title: t('general.address'),
                            body: company.address.street
                        },
                        {
                            title: t('address.floor'),
                            body: company.address.floor
                        },
                        {
                            title: t('address.phone'),
                            body: company.address.phoneNumber
                        },
                        {
                            title: t('address.postalCode'),
                            body: company.address.customPostCode === '' ? company.address.postalCode.code : company.address.customPostCode
                        },
                        {
                            title: t('address.nr'),
                            body: company.address.nr
                        },
                        {
                            title: t('address.country'),
                            body: company.address.country.name
                        },
                        {
                            title: t('address.county'),
                            body: company.address.county.name
                        },
                        {
                            title: t('address.city'),
                            body: company.address.city !== null ? company.address.city.name : ''
                        },
                    ]
                }))])
                props.success()
            })
            .catch(error => {
                props.error()
                console.error(error)
            })
    },[])

    const [expanded,setExpanded] = useState('')

    const handleChange = panel => (event, newExpanded) => {
        setExpanded(newExpanded ? panel : false);
    }

    const handleUpdateButton = async btn => {
        let response =  await props.updateCompany(btn.target.name)

        if(response)
            history.push('/company/add')
    }

    const handleDeleteButton = async btn => {
        let company_id = btn.target.name

        props.fetching()
        await Company.SoftDelete(company_id)
        props.success()

        setCompanies(companies.filter(company => company.id !== company_id))
        setExpanded('')
    }

    return (
        <Grid item xs={12} md={8} sm={6}>
            {
                !matches
                    ?   <div className={classes.container}>
                            <div className={classes.contTitle}>
                                <Typography className={desktopClasses.title} gutterBottom>
                                    {t('offerItem.companyAddress')}
                                </Typography>
                                <Button
                                    variant='contained' 
                                    className={desktopClasses.addButton}
                                >
                                    <Link to='/company/add' >
                                        {t('general.addNewCompany')}
                                    </Link>
                                </Button>
                            </div>            
                            {
                                companies.map((company,id) => <div key={`company${id}`} className={classes.card}>
                                                                    <div className={classes.cardHeader}>
                                                                        <div className={classes.cardTitleCont}>
                                                                            <p className={desktopClasses.cardTitle} >{ company.name }</p>
                                                                        </div>
                                                                        <div className={classes.cardActionButtonsCont}>
                                                                            <button 
                                                                                name={company.iri}
                                                                                onClick={handleUpdateButton}
                                                                                className={desktopClasses.blueButton} 
                                                                            >{t('general.edit')}</button>
                                                                            <button 
                                                                                className={desktopClasses.blackButton} 
                                                                                name={company.id}
                                                                                onClick={handleDeleteButton}
                                                                            >{t('general.delete')}</button>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.cardContent} >
                                                                        {
                                                                            company.info 
                                                                                ? company.info.map((info,id2) => <div key={`addres${id}sinfo${id2}`} className={desktopClasses.cardItem}>
                                                                                                                    <p className={desktopClasses.cardItemLabel} >{info.title.toUpperCase()}</p>
                                                                                                                    <p className={desktopClasses.cardItemInfo}>{info.body}</p>
                                                                                                                </div>)
                                                                                : <div/>
                                                                        }
                                                                    </div>
                                                                </div>)
                            }
                        </div>
                    :   <div className={mobileClasses.container} >
                            <div className={mobileClasses.contTitle}>
                                <Typography className={mobileClasses.title}>
                                    {t('offerItem.companyAddress')}
                                </Typography>
                                <Link to='/company/add' className={mobileClasses.addButton} >
                                    {t('general.add')} <span className={mobileClasses.plus}>+</span>
                                </Link>
                            </div>
                            {
                                companies.map((company,id) => <Accordion key={`company${id}`} className={mobileClasses.accordion}>
                                                                    <AccordionSummary 
                                                                        expandIcon={<ExpandMoreIcon />}
                                                                        className={mobileClasses.accordionSummary}
                                                                    >
                                                                        <Typography className={mobileClasses.accordionTitle}>
                                                                            {company.name}
                                                                        </Typography>
                                                                    </AccordionSummary>
                                                                    <AccordionDetails className={mobileClasses.accordionDetails} >
                                                                        {
                                                                            company.info 
                                                                                ? company.info.map((info,id2) => <div key={`addres${id}sinfo${id2}`} className={mobileClasses.accordionItem}>
                                                                                                                    <p className={mobileClasses.accordionItemLabel} >{info.title.toUpperCase()}</p>
                                                                                                                    <p className={mobileClasses.accordionItemInfo}>{info.body}</p>
                                                                                                                </div>)
                                                                                : <div/>
                                                                        }
                                                                        <div className={mobileClasses.accordionActionButtonsCont}>
                                                                            <button 
                                                                                name={company.iri}
                                                                                onClick={handleUpdateButton}
                                                                                className={desktopClasses.blueButton} 
                                                                            >{t('general.edit')}</button>
                                                                            <button 
                                                                                className={desktopClasses.blackButton} 
                                                                                name={company.id}
                                                                                onClick={handleDeleteButton}
                                                                            >{t('general.delete')}</button>
                                                                        </div>
                                                                    </AccordionDetails>
                                                                </Accordion>)
                            }
                        </div>
            }
        </Grid>
    )
}

const mapState = state => ({
    user: state.auth.user.data.iri
})

const mapDispatch = {
    ...CompanyActions,
    ...FetchActions
}

export default connect(mapState,mapDispatch)(ListCompany)