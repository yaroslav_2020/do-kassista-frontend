import React,{ useState } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import * as User from '../../utils/User'

import * as FetchActions from '../../store/actions/FetchActions'

import {
    Box,
    Typography,
    TextField,
    Button,
    Grid,
    useMediaQuery
} from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
    container: {
        borderRadius: '13px',
        background: '#fff',
        color: '#191919',
        boxShadow: '0px 5px 10px rgba(0,0,0,.12)'
    },
    contTitle: {
        padding: '42px 60px'
    }
})

const desktopStyles = makeStyles({
    title: {
        fontSize: '25px',
        fontWeight: 'bold',
        color: '#272727'
    },
    inputLabel: {
        fontSize: '18px',
        fontWeight: '500',
        color: '#313131',
        marginBottom: '12px'
    },
    contInputs: {
        width: '80%',
        padding: '20px 0 20px 60px'
    },
    contButton: {
        width: '100%',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    button: {
        background: '#0097F6',
        borderRadius: '6px',
        color: '#fff',
        outline: 'none',
        padding: '14px 45px',
        border: 'none',
        width: '300px',
        height: '75px',
        fontSize: '22px',
        fontWeight: 'bold',
        margin: '20px 0px 30px 60px',
        '&:hover': {
            background: '#0097F6',
            border: 'none'
        }
    },
    textField: {
        border: '1px solid #DBDBDB',
        borderRadius: '9px',
        marginBottom: '35px',
        maxWidth: '400px',
        '& fieldset': {
            border: 'none'
        },
        '& .MuiInputBase-root:hover fieldset': {
            border: 'none'
        },
        '& .MuiInputBase-root:focus fieldset': {
            border: 'none'
        }
    },
})

const mobileStyles = makeStyles({
    title: {
        fontSize: '20px',
        fontWeight: 'bold',
        color: '#272727'
    },
    inputLabel: {
        fontSize: '14px',
        fontWeight: 'bold',
        color: '#313131',
        marginBottom: '12px'
    },
    contInputs: {
        width: '100%',
        padding: '40px 20px 20px 20px'
    },
    contButton: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: '35px'
    },
    button: {
        background: '#0097F6',
        borderRadius: '9px',
        color: '#fff',
        outline: 'none',
        padding: '12px 30px',
        border: 'none',
        width: '120px',
        height: '42px',
        fontSize: '14px',
        fontWeight: '500',
        margin: 'auto',
        '&:hover': {
            background: '#0097F6',
            border: 'none'
        }
    },
    textField: {
        border: '1px solid #DBDBDB',
        borderRadius: '9px',
        marginBottom: '35px',
        '& fieldset': {
            border: 'none'
        },
        '& .MuiInputBase-root:hover fieldset': {
            border: 'none'
        },
        '& .MuiInputBase-root:focus fieldset': {
            border: 'none'
        }
    },
})

const ClientChangePassword = props => {

    const {t} = useTranslation()

    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const matches = useMediaQuery('(max-width:600px)')

    const [state,setState] = useState({
                                prev_password: '',
                                password: '',
                                confirmed_password: '',
                            })

    const [responseMessage,setResponseMessage] = useState('')

    const inputs = [
        {
            name: 'prev_password',
            label: t('changePassword.actualPassword'),
            value: state.prev_password
        },
        {
            name: 'password',
            label: t('changePassword.newPassword'),
            value: state.password
        },
        {
            name: 'confirmed_password',
            label: t('register.inputs.confirmPassword'),
            value: state.confirmed_password
        }
    ]

    const handleChange = input => {
        setState({
            ...state,
            [input.target.name]: input.target.value
        })
    }

    const handleSubmit = async form => {
        form.preventDefault()
        props.fetching()

        if(state.password === state.confirmed_password){
            let response = await User.changePassword(state)

                if(response.data.error){
                    props.error()
                    setResponseMessage(response.data.error)
                }else{
                    setState({
                        prev_password: '',
                        password: '',
                        confirmed_password: '',
                    })
                    setResponseMessage(t('general.updateData'))
                    props.success()
                }                
        }else{
            props.error()
            setResponseMessage(t('changePassword.notMatch'))
        }
    }

    return (
        <Grid item xs={12} md={8} sm={6}>
            {
                matches && <div>
                                <Typography className={mobileClasses.title} gutterBottom>
                                    {t('columnMenu.settings.changePassword')}
                                </Typography>
                            </div>
            }
            <form onSubmit={handleSubmit} className={classes.container} >
                {
                    !matches && <div className={classes.contTitle} >
                                    <Typography className={desktopClasses.title} gutterBottom>
                                        {t('columnMenu.settings.changePassword')}
                                    </Typography>
                                </div>
                }
                <div className={matches ? mobileClasses.contInputs : desktopClasses.contInputs}>
                    {
                        inputs.map((input,id) => <div key={`cp${id}`} style={{width: matches ? '100%' : 'auto'}} >
                                                <Typography className={matches ? mobileClasses.inputLabel : desktopClasses.inputLabel} gutterBottom>
                                                    {input.label}
                                                </Typography>
                                                <TextField 
                                                    fullWidth
                                                    type='password'
                                                    name={input.name}
                                                    value={input.value}
                                                    variant='outlined'
                                                    className={matches ? mobileClasses.textField : desktopClasses.textField}
                                                    style={{
                                                        marginBottom: id === (inputs.length - 1) ? '0px' : '35px'
                                                    }}
                                                    onChange={handleChange}
                                                />
                                            </div>)
                    }
                </div>
                {
                    responseMessage !== '' && <Box pl={3} pr={3} pt={3} mb={2}>
                                                    <Typography variant='subtitle1'>
                                                        { responseMessage }
                                                    </Typography>
                                                </Box>
                }
                <div className={matches ? mobileClasses.contButton : desktopClasses.contButton}>
                    <Button 
                        variant='outlined'
                        size='large'
                        type='submit'
                        fullWidth
                        className={matches ? mobileClasses.button : desktopClasses.button}
                    >
                        {t('buttons.save')}
                    </Button>
                </div>
            </form>
        </Grid>
    )
}

const mapDispatch = {
    ...FetchActions
}

export default connect(null,mapDispatch)(ClientChangePassword)