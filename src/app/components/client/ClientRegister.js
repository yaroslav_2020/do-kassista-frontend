import React , { useState , Fragment } from 'react'
import { connect } from 'react-redux'
import { Link , useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import * as LoginActions from '../../auth/store/actions/loginActions'
import * as FetchActions from 'app/store/actions/FetchActions'

import {
    Grid,
    useMediaQuery,
    FormControlLabel,
    Radio,
    RadioGroup,
    CircularProgress
} from '@material-ui/core'

import Formsy from 'formsy-react'
import { TextFieldFormsy } from '@fuse/core/formsy'

import { makeStyles , withStyles } from '@material-ui/core/styles'

import Logo from '../../assets/images/logo2.svg'
import GenericSnackbar from 'app/utils/GenericSnackbar'

const useStyles = makeStyles({
    container: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    loaderCont: {
        width: '100vw',
        height: '100vh',
        position: 'fixed',
        background: 'rgba(0,0,0,.1)',
        zIndex: '1000',
        justifyContent: 'center',
        boxSizing: 'border-box',
        alignItems: 'center',
        top: '0',
        left: '0'
    }
    
})

const desktopStyles = makeStyles({
    loginContainer: {
        width: '500px',
        boxShadow: '0px 5px 30px rgba(0,0,0,.09)',
        minHeight: '500px',
        borderRadius: '3px',
        background: '#fff',
        padding: '50px 32px',
        display: 'flex',
        flexDirection: 'column',
        '& a': {
            fontSize: '14px',
            fontWeight: '500',
            color: '#2F2F2F',
        },
        '& a:hover': {
            textDecoration: 'none'
        }
    },
    logo: {
        width: '250px',
        marginBottom: '50px'
    },
    title: {
        fontSize: '25px',
        fontWeight: 'bold',
        color: '#2F2F2F',
        marginBottom: '36px',
        textAlign: 'center'
    },
    label: {
        fontSize: '14px',
        fontWeight: '500',
        color: '#2F2F2F',
        marginBottom: '6px'
    },
    textField: {
        marginBottom: '20px',
        '& .MuiInputBase-root': {
            border: '1px solid #D1D1D1'
        },
        '& .MuiInputBase-root:hover': {
            border: '1px solid #D1D1D1'
        }
    },
    forgetLabel: {
        marginBottom: '20px',
        textAlign: 'right'
    },
    buttonBlue: {
        background: '#0097F6',
        color: '#fff',
        padding: '16px 0',
        fontSize: '17px',
        fontWeight: 'bold',
        borderRadius: '3px',
        marginBottom: '36px'
    },
    registerLabel: {
        textAlign: 'center'
    }
})

const mobileStyles = makeStyles({
    loginContainer: {
        width: '350px',
        boxShadow: '0px 5px 30px rgba(0,0,0,.09)',
        minHeight: '500px',
        borderRadius: '3px',
        padding: '50px 32px',
        display: 'flex',
        flexDirection: 'column',
        '& a': {
            fontSize: '14px',
            fontWeight: '500',
            color: '#2F2F2F',
        },
        '& a:hover': {
            textDecoration: 'none'
        }
    },
    logo: {
        width: '250px',
        marginBottom: '50px'
    },
    title: {
        fontSize: '25px',
        fontWeight: 'bold',
        color: '#2F2F2F',
        marginBottom: '36px',
        textAlign: 'center'
    },
    label: {
        fontSize: '14px',
        fontWeight: '500',
        color: '#2F2F2F',
        marginBottom: '6px'
    },
    textField: {
        marginBottom: '20px',
        '& .MuiInputBase-root': {
            border: '1px solid #D1D1D1'
        },
        '& .MuiInputBase-root:hover': {
            border: '1px solid #D1D1D1'
        }
    },
    forgetLabel: {
        marginBottom: '20px',
        textAlign: 'right'
    },
    buttonBlue: {
        background: '#0097F6',
        color: '#fff',
        padding: '16px 0',
        fontSize: '17px',
        fontWeight: 'bold',
        borderRadius: '3px',
        marginBottom: '36px'
    },
    registerLabel: {
        textAlign: 'center'
    }
})

const StyledRadio = withStyles({
    root: {
        color: '#cacaca',
        '&$checked': {
            color: '#0097F6',
        },
    },
    checked: {},
})((props) => <Radio color='default' {...props} />)

const ClientRegister = props => {

    const [fetching, setFetching] = useState(false);

    const [snackbar,setSnackbar] = useState({
        open: false,
        message: '', 
        severity: 'success',
    })

    const handleCloseSnackbar = ( value ) => setSnackbar( value );

    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const history = useHistory()

    const [isValidForm,setIsValidForm] = useState(false)

    const { t } = useTranslation()

    const matches = useMediaQuery('(max-width:600px)')

    const [loginData,setLoginData] = useState({
        email: '',
        password: '',
        name: '',
        confirm_password: '',
        bankAccount: '',
        bankName: '',
        phone_number: ''
    })

    const inputs = [
        {
            type: 'text',
            name: 'email',
            label: t('login.inputs.username'),
            value: loginData.email,
            icon: 'email',
        },
        {
            type: 'text',
            name: 'name',
            label: t('register.inputs.name'),
            value: loginData.name,
            icon: 'person_outline',
        },
        {
            type: 'text',
            name: 'phone_number',
            label: t('address.phoneNumber'),
            value: loginData.phone_number,
            icon: 'phone',
        },
        {
            type: 'password',
            name: 'password',
            label: t('login.inputs.password'),
            value: loginData.password,
            icon: 'vpn_key',
        },
        {
            type: 'password',
            name: 'confirm_password',
            label: t('register.inputs.confirmPassword'),
            value: loginData.confirm_password,
            icon: 'vpn_key',
        }
    ]    

    const IndependentOptions = [
        {
            type: 'text',
            name: 'bankName',
            label: t('register.inputs.bankName'),
            value: loginData.bankName,
            icon: 'account_balance',
        },
        {
            type: 'text',
            name: 'bankAccount',
            label: t('register.inputs.bankAccount'),
            value: loginData.bankAccount,
            icon: 'account_balance',
        },
    ]

    const options = [
        {
            value: 1,
            control: <StyledRadio value={1} color='primary' />,
            label: t('register.options.client'),
            labelPlacement: 'end',
        },
        {
            value: 3,
            control: <StyledRadio value={3} color='primary' />,
            label: t('register.options.independent'),
            labelPlacement: 'end'
        },
        {
            value: 4,
            control: <StyledRadio value={4} color='primary' />,
            label: t('register.options.employee'),
            labelPlacement: 'end'
        }
    ]

    const [selectedValue,setSelectedValue] = useState(options[0].value)

    const onChange = radio => {
        setSelectedValue(parseInt(radio.target.value))
    }

    const handleSubmit = async data => {
        setFetching(true);
        try {
            const response = await props.SignUp({
                ...data,
                user_type: selectedValue,
                register_mode: true
            })
    
            if(response.data)
                history.push('/offer')
            else if(response.error)
                setSnackbar({
                    open: true,
                    message: t(`Advices.${response.error}`), 
                    severity: 'warning',
                })
                setFetching(false);
        } catch (error) {
            console.log('Error in ClientRegister SignUp: ', error);
            setFetching(false);
        }
    }

    const disableButton = () => {
        setIsValidForm(false)
    }

    const enableButton = () => {
        setIsValidForm(true)
    }

    const handleChange = input => {
        setLoginData({
            ...loginData,
            [input.target.name]: input.target.value
        })
    }

    return (
        <Grid item xs={12} md={12} sm={12}>
            <GenericSnackbar 
                handleClose={ handleCloseSnackbar } 
                { ...snackbar }
            />
            {
                fetching &&
                <div className={classes.loaderCont} style={{display: props.fetching ? 'flex' : 'none',}}>
                    <CircularProgress />
                </div>
            }
            <div className={classes.container}>
                <img src={Logo} className={matches ? mobileClasses.logo : desktopClasses.logo} alt='logo' />
                <div style={{width: '500px'}}>
                    {
                        props.returnButton
                    }
                </div>
                <Formsy 
                    className={matches ? mobileClasses.loginContainer : desktopClasses.loginContainer} 
                    onValidSubmit={handleSubmit}
                    onValid={enableButton}
                    onInvalid={disableButton}
                >
                    <p className={matches ? mobileClasses.title : desktopClasses.title}>{t('general.register').toUpperCase()}</p>
                    <RadioGroup 
                        name='role' 
                        row
                        value={selectedValue}
                        onChange={onChange}
                        style={{marginBottom: '25px'}}
                    >
                        {
                            options.length
                                ? options.map(option => <FormControlLabel
                                                            key={options.indexOf(option)} 
                                                            {...option} 
                                                        />)
                                : <FormControlLabel/>
                        }
                    </RadioGroup>
                    {
                        inputs.map((input,index) => <Fragment key={`ri${index}`}>
                                                        <p className={matches ? mobileClasses.label : desktopClasses.label}>{input.label}</p>
                                                        <TextFieldFormsy
                                                            fullWidth
                                                            type={input.type}
                                                            variant='outlined'
                                                            name={input.name}
                                                            value={input.value}
                                                            onChange={handleChange}
                                                            className={matches ? mobileClasses.textField : desktopClasses.textField}
                                                        />
                                                    </Fragment>)
                    }
                    {
                        selectedValue === 3
                            ? IndependentOptions.map((input,index) => <Fragment key={`ri${index}`}>
                                                                        <p className={matches ? mobileClasses.label : desktopClasses.label}>{input.label}</p>
                                                                        <TextFieldFormsy
                                                                            fullWidth
                                                                            type={input.type}
                                                                            variant='outlined'
                                                                            name={input.name}
                                                                            value={input.value}
                                                                            onChange={handleChange}
                                                                            className={matches ? mobileClasses.textField : desktopClasses.textField}
                                                                        />
                                                                    </Fragment>)
                            : <div />
                    }
                    <button 
                        className={matches ? mobileClasses.buttonBlue : desktopClasses.buttonBlue}
                        disabled={!isValidForm}    
                    >{t('buttons.registerMe').toUpperCase()}</button>
                    <Link to='/offer/login' className={matches ? mobileClasses.registerLabel : desktopClasses.registerLabel}>{t('login.title')}</Link>
                </Formsy>
            </div>
        </Grid>
    )
}

const mapState = state => ({
    login: state.auth.login,
    user: state.auth.user
})

const mapDispatch = {
    ...LoginActions,
    ...FetchActions
}

export default connect(mapState,mapDispatch)(ClientRegister)