import React,{ useState , useEffect } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import * as PaymentActions from 'app/store/actions/PaymentActions'
import * as FetchActions from 'app/store/actions/FetchActions'


import GenericSnackbar from 'app/utils/GenericSnackbar'

import {
    Typography,
    Box,
    Grid,
    TextField,
    useMediaQuery,
    Button
} from '@material-ui/core'

import {
    CardNumberElement,
    CardExpiryElement,
    CardCvcElement,
    useElements,
    useStripe,
} from '@stripe/react-stripe-js'

import { makeStyles } from '@material-ui/core/styles'

import PaymentLogos from 'app/assets/images/payment-logos.png'
import { createSetupSecretIntent, updatePaymentMethod } from 'app/utils/PaymentIntent'
import { useHistory, useLocation } from 'react-router'
import { DatePicker, KeyboardDatePicker } from '@material-ui/pickers'
import moment from 'moment'

const useStyles = makeStyles({
    container: {
        borderRadius: '13px',
        background: '#fff',
        color: '#191919',
        boxShadow: '0px 5px 10px rgba(0,0,0,.12)'
    },
    cardInput: {
        border: '1px solid #DBDBDB',
        padding: '20px 25px',
        borderRadius: '9px',
        fontSize: '16px',
        marginBottom: '36px'
    },
    cardLabel: {
        fontSize: '20px',
        fontWeight: 'bold',
        color: '#272727',
        marginBottom: '12px'
    },
    textField: {
        '& .MuiInputBase-root fieldset': {
            border: '1px solid #DBDBDB',
            borderRadius: '9px',
            borderColor: '#DBDBDB'
        },
        '& .MuiInputBase-root fieldset:hover': {
            border: '1px solid #DBDBDB',
            borderRadius: '9px',
            borderColor: '#DBDBDB'
        },
        '& .MuiInputBase-root:focus fieldset': {
            border: '1px solid #DBDBDB',
            borderRadius: '9px',
            borderColor: '#DBDBDB'
        },
        '& .MuiInputBase-root:hover fieldset': {
            border: '1px solid #DBDBDB',
            borderRadius: '9px',
            borderColor: '#DBDBDB'
        },
        '& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline': {
            border: '1px solid #DBDBDB',
            borderRadius: '9px',
            borderColor: '#DBDBDB'
        }
    }
})

const desktopStyles = makeStyles({
    title: {
        fontSize: '25px',
        fontWeight: 'bold',
        color: '#272727'
    },
    inputLabel: {
        fontSize: '14px',
        fontWeight: 'bold',
        color: '#313131',
        marginBottom: '12px'
    },
    inputDiv: {
        width: '45%',
    },
    contInputs: {
        width: '100%',
        padding: '20px 74px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        flexWrap: 'wrap'
    },
    contTitle: {
        background: '#fff',
        // borderBottom: '1px solid #D8D8D8',
        padding: '30px 74px',
        borderRadius: '13px 13px 0px 0px',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        position: 'relative',
    },
    button: {
        background: '#0097F6',
        borderRadius: '6px',
        color: '#fff',
        outline: 'none',
        padding: '14px 45px',
        border: 'none',
        width: '300px',
        height: '75px',
        fontSize: '22px',
        fontWeight: 'bold',
        margin: '20px 0px 30px 0px',
        '&:hover': {
            background: '#0097F6',
            border: 'none'
        }
    },
    contButton: {
        width: '100%',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center'
    }
})

const mobileStyles = makeStyles({
    title: {
        fontSize: '20px',
        fontWeight: 'bold',
        color: '#272727'
    },
    inputLabel: {
        fontSize: '14px',
        fontWeight: 'bold',
        color: '#313131',
        marginBottom: '12px'
    },
    inputDiv: {
        width: '100%',
    },
    contInputs: {
        width: '100%',
        padding: '20px 17px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        flexWrap: 'wrap'
    },
    contTitle: {
        background: '#fff',
        padding: '30px 74px',
        borderRadius: '13px 13px 0px 0px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
    },
    button: {
        background: '#0097F6',
        borderRadius: '6px',
        color: '#fff',
        outline: 'none',
        padding: '18px 75px',
        border: 'none',
        width: '224px',
        height: '54px',
        fontSize: '15px',
        fontWeight: '500',
        margin: '20px 0px 30px 0px',
        '&:hover': {
            background: '#0097F6',
            border: 'none'
        }
    },
    contButton: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
})


const AddPaymentMethod = props => {
    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const matches = useMediaQuery('(max-width:600px)')

    const history = useHistory()

    const location = useLocation()

    const {paymentMethod, last4, exp_month, exp_year} = (location.state !== undefined) ? 
        location.state :
        {                                            
            paymentMethod: '',
            last4: '',
            exp_month: '',
            exp_year: ''
        };

    console.log("Data Retrieve from Location: ", paymentMethod, last4, exp_month, exp_year);

    const {t} = useTranslation()

    const stripe = useStripe()
    const elements = useElements()

    const [error,setError] = useState({
        cardNumber: '',
        expiresDate: '',
        cvc: '',
        generalError: ''
    })

    const [stripeFormValid, setStripeFormValid] = useState({
        cardName: '',
        cardNumber: false,
        cardExpiry: false,
        cardCvc: false
    })

    const [expYear, setExpYear] = useState(paymentMethod !== undefined ? moment(`${exp_month}-${exp_year}`, 'M/YYYY') : null)


    const [snackbar,setSnackbar] = useState({
        open: false,
        message: '', 
        severity: 'success',
    })

    const [setupIntent, setSetupIntent] = useState('')

    const handleCloseSnackbar = value => setSnackbar(value)

    useEffect(() => { 
        if(stripeFormValid.cardNumber && stripeFormValid.cardExpiry && stripeFormValid.cardCvc){
            props.fetching()
            createSetupSecretIntent({
                customer: props.clientEmail
            })            
            .then( result => {
                console.log('enter server response', result);
                
                setSetupIntent(result.data.setupIntent.client_secret)

                props.success()
            });
        } else {
            props.finish();
        }
    },[stripeFormValid])

    const handleCardNumberChange = input => {     
        setStripeFormValid( c => ({
            ...c,
            [input.elementType]: input.complete 
        }))        

        console.log('enter change: ', input);

        setError({
            ...error,
            cardNumber: input.error !== undefined ? t(`payment.stripe.error.${input.error.code}`) : ''
        })
    }

    const handleExpiresDateChange = input => {
        setStripeFormValid( c => ({
            ...c,
            [input.elementType]: input.complete
        }))     

        setError({
            ...error,
            expiresDate: input.error !== undefined ? t(`payment.stripe.error.${input.error.code}`) : ''
        })
    }

    const handleCvcChange = input => {
        setStripeFormValid( c => ({
            ...c,
            [input.elementType]: input.complete
        }))
        setError({
            ...error,
            cvc: input.error !== undefined ? t(`payment.stripe.error.${input.error.code}`) : ''
        })
    }

    const handleSubmit = async form => {
        form.preventDefault()
        props.fetching()
        
        if (!paymentMethod) {
            const cardNumber = elements.getElement(CardNumberElement)
            if (setupIntent) {
                stripe.confirmCardSetup(
                    setupIntent,  //SERVER RESPONSE FOR OPERATION
                    {
                        payment_method: {card: cardNumber}                        
                    }
                )
                .then(response => {
                    if(response.error){
                        setError({
                            ...error,
                            generalError: response.error.message
                        })
                        setSnackbar({
                            open: true,
                            message: response.error.message, 
                            severity: 'error',
                        })
                    }else{
                        if (response.setupIntent.status === 'succeeded') {
                            setError({
                                ...error,
                                generalError: ''
                            })
                            setSnackbar({
                                open: true,
                                message: t('payment.stripe.succeeded'), 
                                severity: 'success',
                            })
                            props.success()                        
                            setTimeout(() => {
                                history.push('/client/payment-methods')                            
                            }, 1000);
                        }
                    }
                })
                .catch(error => console.error(error))                    
            } else {
                props.error()
                console.log('enter no entra en confirm');
            }            
        } else {            
            updatePaymentMethod({
                paymentMethod,
                card: {
                    exp_month:  expYear.format('MM'),
                    exp_year:   expYear.format('YYYY')
                }
            })            
            .then( ({data}) => {
                if(data.error){
                    setError({
                        ...error,
                        generalError: data.error.message
                    })

                    setSnackbar({
                        open: true,
                        message: data.error.message, 
                        severity: 'error',
                    })
                }else{
                    setError({
                        ...error,
                        generalError: ''
                    })
                    setSnackbar({
                        open: true,
                        message: t('payment.stripe.updated'), 
                        severity: 'success',
                    })
                    props.success()                        
                    setTimeout(() => {
                        history.push('/client/payment-methods')                            
                    }, 1000);
                }                
            });
        }
    }


    return (
        <Grid item xs={12} md={props.md ? props.md : 8} sm={6}>
            <GenericSnackbar 
                handleClose={handleCloseSnackbar} 
                {...snackbar} 
            />
            <form onSubmit={handleSubmit} className={classes.container}>
                <div className={matches ? mobileClasses.contTitle : desktopClasses.contTitle}>
                    <Typography className={matches ? mobileClasses.title : desktopClasses.title} gutterBottom>
                        {
                            !paymentMethod
                            ?   t('general.addNewPaymentMethods')
                            :   t('general.updateNewPaymentMethods')                        
                        }
                    </Typography>
                </div>

                <div className={matches ? mobileClasses.contInputs : desktopClasses.contInputs} >
                    <Grid
                        container
                        direction='row'
                        justify='space-around'
                        alignItems='center'
                        spacing={2}
                    >

                        <Grid item xs={12} md={12} sm={12} >
                            <Typography variant='h5' align='center' style={{fontWeight: 'bold'}} gutterBottom >
                                {t('payment.title')}
                            </Typography>                    
                        </Grid>
                        <Grid item xs={12} md={12} sm={12} >
                            <p className={classes.cardLabel} >{t('payment.card')}</p>
                            <div className={classes.cardInput} >
                                {
                                    !paymentMethod
                                    ?   <CardNumberElement 
                                            options={{
                                                style: {
                                                    base: {
                                                        fontSize: '16px',
                                                        fontWeight: '400',
                                                        color: '#272727'
                                                    }
                                                },
                                            }}
        
                                            onChange={handleCardNumberChange}
                                        />
                                    :   <Typography variant='overline text' align='center' style={{fontWeight: 'bold'}} gutterBottom >
                                            {`****${last4}`}
                                        </Typography>       
                                }
                                <p>{error.cardNumber}</p>
                            </div>
                        </Grid>
                        <Grid item xs={12} md={6} sm={12} >
                            <p className={classes.cardLabel} >{t('payment.expires')}</p>
                            <div className={classes.cardInput} >

                                {
                                    !paymentMethod
                                    ?   <CardExpiryElement 
                                            options={{
                                                style: {
                                                    base: {
                                                        fontSize: '18px'
                                                    }
                                                },
                                            }}

                                            onChange={handleExpiresDateChange}
                                        />
                                    :   <DatePicker
                                            views={['month', 'year']}
                                            minDate={new Date()}
                                            format="MM/YYYY"
                                            label="Mes/Año"
                                            value={expYear}
                                            autoOk
                                            onChange={(newValue) => {
                                                setExpYear(moment(newValue))
                                                if (moment(newValue).isAfter(new Date())) {
                                                    setSetupIntent(true)                                                    
                                                } else {
                                                    setSetupIntent(false)                                                    
                                                }

                                            }}
                                        />
                                    // :   <KeyboardDatePicker
                                    //         views={['month', 'year']}
                                    //         minDate={new Date()}
                                    //         format="MM/YYYY"
                                    //         label="Mes/Año"
                                    //         value={expYear}
                                    //         autoOk
                                    //         onChange={(newValue) => {
                                    //             setExpYear(moment(newValue))
                                    //         }}
                                    //     />

                                }
                                <p>{error.expiresDate}</p>
                            </div>
                        </Grid>

                        <Grid item xs={12} md={6} sm={12} >
                            {
                                !paymentMethod &&
                                <>
                                    <p className={classes.cardLabel} >{t('payment.cvc')}</p>
                                    <div className={classes.cardInput} >
                                        <CardCvcElement 
                                            options={{
                                                style: {
                                                    base: {
                                                        fontSize: '18px'
                                                    }
                                                },
                                            }}

                                            onChange={handleCvcChange}
                                        />
                                        <p>{error.cvc}</p>
                                    </div>
                                </>                                
                            }
                        </Grid>
                        <Grid item xs={12} md={12} sm={12} >
                            <img src={PaymentLogos} />                        
                        </Grid>
                        <Grid item xs={12} md={6} sm={12} >
                            <Box mt={4}>
                                <Typography variant='body1' align='left' style={{color: 'red'}} >
                                    {error.generalError}
                                </Typography>
                            </Box>
                        </Grid>
                    </Grid>
                    <div className={matches ? mobileClasses.contButton : desktopClasses.contButton} >
                        <Button 
                            type='submit'
                            variant='contained'
                            size='large' 
                            color='primary'
                            fullWidth
                            className={matches ? mobileClasses.button : desktopClasses.button}
                            disabled={!setupIntent}
                        >
                            {t('buttons.save')}
                        </Button>
                    </div>
                </div>
            </form>                    

        </Grid>
    )
}

const mapState = state => ({
    paymentActive: state.Payment.active,
    cardHolderName: state.Offer.cardHolderName,    
    pricePerService: state.Offer.pricePerService,
    clientEmail: state.auth.user.data.email
})

const mapDispatch = {
    ...PaymentActions,
    ...FetchActions
}

export default connect(mapState,mapDispatch)(AddPaymentMethod)
