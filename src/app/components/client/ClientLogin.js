import React, { useState, Fragment } from 'react';
import { connect } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import { Grid, useMediaQuery } from '@material-ui/core';

import * as LoginActions from '../../auth/store/actions/loginActions';

import Formsy from 'formsy-react';
import { TextFieldFormsy } from '@fuse/core/formsy';
import FuseSplashScreen from '@fuse/core/FuseSplashScreen';

import { makeStyles } from '@material-ui/core/styles';

import Logo from '../../assets/images/logo2.svg';

const useStyles = makeStyles({
	container: {
		width: '100%',
		height: '100%',
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center'
	}
});

const desktopStyles = makeStyles({
	loginContainer: {
		width: '500px',
		boxShadow: '0px 5px 30px rgba(0,0,0,.09)',
		height: '500px',
		borderRadius: '3px',
		background: '#fff',
		padding: '50px 32px',
		display: 'flex',
		flexDirection: 'column',
		'& a': {
			fontSize: '14px',
			fontWeight: '500',
			color: '#2F2F2F'
		},
		'& a:hover': {
			textDecoration: 'none'
		}
	},
	logo: {
		width: '250px',
		marginBottom: '50px'
	},
	title: {
		fontSize: '25px',
		fontWeight: 'bold',
		color: '#2F2F2F',
		marginBottom: '36px',
		textAlign: 'center'
	},
	label: {
		fontSize: '14px',
		fontWeight: '500',
		color: '#2F2F2F',
		marginBottom: '6px'
	},
	textField: {
		marginBottom: '20px',
		'& .MuiInputBase-root': {
			border: '1px solid #D1D1D1'
		},
		'& .MuiInputBase-root:hover': {
			border: '1px solid #D1D1D1'
		}
	},
	forgetLabel: {
		marginBottom: '20px',
		textAlign: 'right'
	},
	buttonBlue: {
		background: '#0097F6',
		color: '#fff',
		padding: '16px 0',
		fontSize: '17px',
		fontWeight: 'bold',
		borderRadius: '3px',
		marginBottom: '36px'
	},
	registerLabel: {
		textAlign: 'center'
	}
});

const mobileStyles = makeStyles({
	loginContainer: {
		width: '350px',
		boxShadow: '0px 5px 30px rgba(0,0,0,.09)',
		height: '500px',
		borderRadius: '3px',
		padding: '50px 32px',
		display: 'flex',
		flexDirection: 'column',
		'& a': {
			fontSize: '14px',
			fontWeight: '500',
			color: '#2F2F2F'
		},
		'& a:hover': {
			textDecoration: 'none'
		}
	},
	logo: {
		width: '250px',
		marginBottom: '50px'
	},
	title: {
		fontSize: '25px',
		fontWeight: 'bold',
		color: '#2F2F2F',
		marginBottom: '36px',
		textAlign: 'center'
	},
	label: {
		fontSize: '14px',
		fontWeight: '500',
		color: '#2F2F2F',
		marginBottom: '6px'
	},
	textField: {
		marginBottom: '20px',
		'& .MuiInputBase-root': {
			border: '1px solid #D1D1D1'
		},
		'& .MuiInputBase-root:hover': {
			border: '1px solid #D1D1D1'
		}
	},
	forgetLabel: {
		marginBottom: '20px',
		textAlign: 'right'
	},
	buttonBlue: {
		background: '#0097F6',
		color: '#fff',
		padding: '16px 0',
		fontSize: '17px',
		fontWeight: 'bold',
		borderRadius: '3px',
		marginBottom: '36px'
	},
	registerLabel: {
		textAlign: 'center'
	}
});

const ClientLogin = props => {
	const classes = useStyles();
	const desktopClasses = desktopStyles();
	const mobileClasses = mobileStyles();

	const { t } = useTranslation();

	const history = useHistory();
	const matches = useMediaQuery('(max-width:600px)');

	const [loginData, setLoginData] = useState({
		email: '',
		password: ''
	});

	const [isValidForm, setIsValidForm] = useState(false);

	const handleChange = input => {
		setLoginData({
			...loginData,
			[input.target.name]: input.target.value
		});
	};

	const handleSubmit = async form => {
		props.Fetching();

		let response = await props.SignIn(loginData);

		console.log(response.data);

		if (response.data) {
			history.push('/offer');
		}
	};

	const disableButton = () => {
		setIsValidForm(false);
	};

	const enableButton = () => {
		setIsValidForm(true);
	};

	return (
		<Fragment>
			{' '}
			{!props.login.fetching ? (
				<Grid item xs={12} md={12} sm={12}>
					<div className={classes.container}>
						<img src={Logo} className={matches ? mobileClasses.logo : desktopClasses.logo} alt="logo" />
						<div style={{ width: '500px' }}> {props.returnButton} </div>{' '}
						<Formsy
							onValidSubmit={handleSubmit}
							onValid={enableButton}
							onInvalid={disableButton}
							className={matches ? mobileClasses.loginContainer : desktopClasses.loginContainer}
						>
							<p className={matches ? mobileClasses.title : desktopClasses.title}>
								{' '}
								{t('login.title').toUpperCase()}{' '}
							</p>{' '}
							<p className={matches ? mobileClasses.label : desktopClasses.label}>
								{' '}
								{t('general.email')} *{' '}
							</p>{' '}
							<TextFieldFormsy
								fullWidth
								variant="outlined"
								name="email"
								value={loginData.email}
								onChange={handleChange}
								className={matches ? mobileClasses.textField : desktopClasses.textField}
							/>{' '}
							<p className={matches ? mobileClasses.label : desktopClasses.label}>
								{' '}
								{t('login.inputs.password')} *{' '}
							</p>{' '}
							<TextFieldFormsy
								fullWidth
								type="password"
								variant="outlined"
								name="password"
								value={loginData.password}
								onChange={handleChange}
								className={matches ? mobileClasses.textField : desktopClasses.textField}
							/>{' '}
							<Link
								to="/offer/password/reset"
								className={matches ? mobileClasses.forgetLabel : desktopClasses.forgetLabel}
							>
								{' '}
								{t('general.forgetPassword')}{' '}
							</Link>{' '}
							<button
								className={matches ? mobileClasses.buttonBlue : desktopClasses.buttonBlue}
								disabled={!isValidForm}
							>
								{' '}
								{t('login.title').toUpperCase()}{' '}
							</button>{' '}
							<Link
								to="/offer/register"
								className={matches ? mobileClasses.registerLabel : desktopClasses.registerLabel}
							>
								{' '}
								{t('register.title')}{' '}
							</Link>{' '}
						</Formsy>{' '}
					</div>{' '}
				</Grid>
			) : (
				<FuseSplashScreen />
			)}{' '}
		</Fragment>
	);
};

const mapState = state => ({
	login: state.auth.login,
	user: state.auth.user
});

const mapDispatch = {
	...LoginActions
};

export default connect(mapState, mapDispatch)(ClientLogin);
