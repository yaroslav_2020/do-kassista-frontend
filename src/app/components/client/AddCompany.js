import React,{ useState , useEffect } from 'react'
import { connect } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import * as Address from '../../utils/Address' 
import * as Company from '../../utils/Company'
import * as CompanyActions from '../../store/actions/CompanyActions'
import * as FetchActions from '../../store/actions/FetchActions'

import {
    Typography,
    TextField,
    Button,
    Grid,
    MenuItem,
    FormControl,
    Select,
    useMediaQuery
} from '@material-ui/core'

import { makeStyles } from  '@material-ui/core/styles'

const useStyles = makeStyles({
    container: {
        borderRadius: '13px',
        background: '#fff',
        color: '#191919',
        boxShadow: '0px 5px 10px rgba(0,0,0,.12)'
    },
    textField: {
        borderRadius: '9px',
        marginBottom: '35px',
        '& fieldset': {
            border: '1px solid #DBDBDB'
        },
        '& .MuiInputBase-root:hover fieldset': {
            border: '1px solid #DBDBDB'
        },
        '& .MuiInputBase-root:focus fieldset': {
            border: '1px solid #DBDBDB'
        }
    },
    contRadioButtons: {
        width: '100%',
        marginBottom: '35px'
    },
})

const desktopStyles = makeStyles({
    title: {
        fontSize: '25px',
        fontWeight: 'bold',
        color: '#272727'
    },
    inputLabel: {
        fontSize: '14px',
        fontWeight: 'bold',
        color: '#313131',
        marginBottom: '12px'
    },
    inputDiv: {
        width: '45%',
    },
    contInputs: {
        width: '100%',
        padding: '20px 74px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        flexWrap: 'wrap'
    },
    contTitle: {
        background: '#fff',
        // borderBottom: '1px solid #D8D8D8',
        padding: '30px 74px',
        borderRadius: '13px 13px 0px 0px',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        position: 'relative',
    },
    button: {
        background: '#0097F6',
        borderRadius: '6px',
        color: '#fff',
        outline: 'none',
        padding: '14px 45px',
        border: 'none',
        width: '300px',
        height: '75px',
        fontSize: '22px',
        fontWeight: 'bold',
        margin: '20px 0px 30px 60px',
        '&:hover': {
            background: '#0097F6',
            border: 'none'
        }
    },
    contButton: {
        width: '100%',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center'
    }
})

const mobileStyles = makeStyles({
    title: {
        fontSize: '20px',
        fontWeight: 'bold',
        color: '#272727'
    },
    inputLabel: {
        fontSize: '14px',
        fontWeight: 'bold',
        color: '#313131',
        marginBottom: '12px'
    },
    inputDiv: {
        width: '100%',
    },
    contInputs: {
        width: '100%',
        padding: '20px 17px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        flexWrap: 'wrap'
    },
    contTitle: {
        background: '#fff',
        // borderBottom: '1px solid #D8D8D8',
        padding: '30px 74px',
        borderRadius: '13px 13px 0px 0px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
    },
    button: {
        background: '#0097F6',
        borderRadius: '6px',
        color: '#fff',
        outline: 'none',
        padding: '18px 75px',
        border: 'none',
        width: '224px',
        height: '54px',
        fontSize: '15px',
        fontWeight: '500',
        margin: '20px 0px 30px 0px',
        '&:hover': {
            background: '#0097F6',
            border: 'none'
        }
    },
    contButton: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
})

const AddCompany = props => {
    const history = useHistory()

    const {t} = useTranslation()

    const matches = useMediaQuery('(max-width:600px)')

    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const [addresses,setAddresses] = useState([])

    useEffect(() => {
        Address.getAddresses(props.user)
            .then(async res => {
                setAddresses([...res.filter(address => address.addressType === 'COMPANY_ADDRESS')])                
            })
            .catch(error => console.error(error))
    },[])

    const handleAddressChange = input => {
        props.saveCompanyData({
            address: input.target.value
        })
    }

    const inputs = [
        {
            label: t('address.fullName'),
            name: 'name',
            value: props.company.name,
            type: 'input'
        },
        {
            label: t('register.inputs.bankName'),
            name: 'bankName',
            value: props.company.bankName,
            type: 'input'
        },                
        {
            label: t('register.inputs.bankAccount'),
            name: 'bankAccount',
            value: props.company.bankAccount,
            type: 'input'
        },
        {
            label: t('company.swift'),
            name: 'swift',
            value: props.company.swift,
            type: 'input'
        },
        {
            label: t('company.cif'),
            name: 'cif',
            value: props.company.cif,
            type: 'input'
        },
        {
            label: t('address.phoneNumber'),
            name: 'regNumber',
            value: props.company.regNumber,
            type: 'input'
        },
    ]

    const handleChange = input => { 
        props.saveCompanyData({
            [input.target.name]: input.target.value
        })       
    }

    const handleSubmit = async form => {
        form.preventDefault()
        props.fetching()
        
        const response = props.company.iri === ''
                            ?   await Company.Create({
                                    user: props.user_iri,
                                    ...props.company,
                                    deleted: false,
                                })
                            :   await Company.Update({
                                    url: props.company.iri,
                                    user: props.user_iri,
                                    ...props.company,
                                    deleted: false,
                                })

        props.resetCompanyState()
                            
        if(props.setCompanyStep2){
            if(typeof props.setCompanyStep2 === 'function'){
                props.setCompanyStep2(response.data['@id'])
            }
        }

        if((response.status === 201 || response.status === 200) && !props.keep){
            props.success()
            history.push('/company/list')
        }else{
            props.error()   
        }            
    }

    return (
        <Grid item xs={12} md={props.md ? props.md : 8} sm={6}>
            <form onSubmit={handleSubmit} className={classes.container}>
                <div className={matches ? mobileClasses.contTitle : desktopClasses.contTitle}>
                    <Typography className={matches ? mobileClasses.title : desktopClasses.title} gutterBottom>
                        {`${t('general.add')} ${t('general.company')}`}
                    </Typography>
                </div>   
                <div className={matches ? mobileClasses.contInputs : desktopClasses.contInputs} >
                    {
                        inputs.length
                            ? inputs.map((input,id) => <div key={`acinp${id}`} className={matches ? mobileClasses.inputDiv : desktopClasses.inputDiv} >
                                                        <Typography className={matches ? mobileClasses.inputLabel : desktopClasses.inputLabel} gutterBottom >
                                                            { input.label }
                                                        </Typography>
                                                        <TextField
                                                            type='text'
                                                            name={input.name}
                                                            variant='outlined'
                                                            value={input.value}
                                                            className={classes.textField}
                                                            onChange={handleChange}
                                                            required
                                                            fullWidth                                                    
                                                        />
                                                    </div>)
                            : <div />
                    }                        
                    <div className={matches ? mobileClasses.inputDiv : desktopClasses.inputDiv} >
                        <Typography className={matches ? mobileClasses.inputLabel : desktopClasses.inputLabel} gutterBottom >
                            {t('general.address')}
                        </Typography>
                        <FormControl variant='outlined' fullWidth>                            
                            <Select
                                value={props.company.address}
                                onChange={handleAddressChange}
                                name='address'
                                className={classes.textField}
                                required
                                fullWidth
                            >
                                <MenuItem value=''>
                                    <em>None</em>
                                </MenuItem>
                                {
                                    props.addresses
                                        ? (props.addresses.length
                                            ?   props.addresses.map(address => <MenuItem key={props.addresses.indexOf(address)} value={address.value}>
                                                                                    { address.label }
                                                                                </MenuItem>)
                                            :   <MenuItem value='' />)
                                        : (addresses.length
                                            ?   addresses.map(address => <MenuItem key={addresses.indexOf(address)} value={address['@id']}>
                                                                            { address.fullName }
                                                                        </MenuItem>)
                                            :   <MenuItem value='' />)
                                }
                            </Select>
                        </FormControl>
                    </div>
                </div>
                <div className={matches ? mobileClasses.contButton : desktopClasses.contButton} >
                    <Button 
                        variant='outlined'
                        size='large' 
                        color='primary'
                        type='submit'
                        className={matches ? mobileClasses.button : desktopClasses.button}
                        fullWidth
                    >
                        {t('buttons.save')}
                    </Button>
                </div>
            </form>
        </Grid>
    )
}

const mapState = state => ({
    user: state.auth.user.data.iri,
    user_iri: state.auth.user.data.iri,
    company: state.Company
})

const mapDispatch = {
    ...CompanyActions,
    ...FetchActions
}

export default connect(mapState,mapDispatch)(AddCompany)