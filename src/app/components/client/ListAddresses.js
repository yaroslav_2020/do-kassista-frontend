import React,{ useState , useEffect } from 'react'
import { connect } from 'react-redux'
import { Link , useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import * as Address from '../../utils/Address'
import * as AddressActions from '../../store/actions/AddressActions'
import * as FetchActions from '../../store/actions/FetchActions'

import {
    Typography,
    Grid,
    Button,
    Accordion,
    AccordionSummary,
    AccordionDetails,
    useMediaQuery
} from '@material-ui/core'

import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

import { makeStyles } from '@material-ui/core/styles'

const urlIconCheck = 'data:image/svg+xml;charset=utf-8,%3Csvg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16"%3E%3Cpath fill-rule="evenodd" clip-rule="evenodd" d="M12 5c-.28 0-.53.11-.71.29L7 9.59l-2.29-2.3a1.003 1.003 0 00-1.42 1.42l3 3c.18.18.43.29.71.29s.53-.11.71-.29l5-5A1.003 1.003 0 0012 5z" fill="%23fff"/%3E%3C/svg%3E'

const useStyles = makeStyles((theme) => ({
    container: {
        borderRadius: '13px',
        background: '#fff',
        color: '#191919',
        // boxShadow: '0px 5px 10px rgba(0,0,0,.12)',
    },
    contTitle: {
        background: '#fff',
        // borderBottom: '1px solid #D8D8D8',
        padding: '30px 64px',
        borderRadius: '13px 13px 0px 0px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        position: 'relative',
    },
    card: {
        borderRadius: '13px',
        boxShadow: '0px 5px 10px rgba(0,0,0,.12)',
        marginTop: '32px'
    },
    cardHeader: {
        padding: '30px 64px',
        borderBottom: '1px solid #DBDBDB',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    cardTitleCont: {
        width: '50%',
    },
    cardActionButtonsCont: {
        width: '40%',
        display: 'flex',
    },
    cardContent: {
        padding: '25px 64px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        flexWrap: 'wrap'
    }
}))

const desktopStyles = makeStyles({
    title: {
        fontSize: '25px',
        fontWeight: 'bold',
        color: '#242424'
    },
    addButton: {
        borderRadius: '9px',
        background: '#0097F6',
        fontSize: '15px',
        fontWeight: '500',
        color: '#fff',
        padding: '15px',
        '&:hover': {
            background: '#0097F6',
        },
        '& a': {
            textDecoration: 'none',
            fontSize: '15px',
            fontWeight: '500',
            color: '#fff'
        },
        '& a:hover': {
            textDecoration: 'none',
            fontSize: '15px',
            fontWeight: '500',
            color: '#fff'
        }
    },
    cardTitle: {
        fontSize: '20px',
        fontWeight: 'bold',
        color: '#242424'
    },
    blueButton: {
        width: '115px',
        Height: '40px',
        border: '1px solid #0079F6',
        borderRadius: '9px',
        fontSize: '14px',
        fontWeight: '500',
        color: '#0097F6',
        marginRight: '15px',
        padding: '10px 24px'
    },
    blackButton: {
        width: '115px',
        Height: '40px',
        border: '1px solid #000',
        borderRadius: '9px',
        fontSize: '14px',
        fontWeight: '500',
        color: '#000',
        marginRight: '15px',
        padding: '10px 24px'
    },
    cardItem: {
        width: '50%',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginBottom: '25px'
    },
    cardItemLabel: {
        fontSize: '18px',
        fontWeight: '500',
        color: '#A8A8A8',
        minWidth: '160px',
        marginRight: '10px'
    },
    cardItemInfo: {
        fontSize: '18px',
        fontWeight: '500',
        color: '#272727'
    }
})

const mobileStyles = makeStyles({
    title: {
        fontSize: '20px',
        fontWeight: 'bold',
        color: '#272727'
    },
    addButton: {
        fontWeight: '400',
        fontSize: '20px',
        color: '#272727',
        background: '#fff',
        border: 'none',
        boxShadow: 'none',
        textDecoration: 'none',
        '&:hover': {
            textDecoration: 'none',
            fontSize: '20px',
            fontWeight: '400',
            color: '#272727'
        },
        '&:not([role=button])': {
            textDecoration: 'none',
            fontSize: '20px',
            fontWeight: '400',
            color: '#272727'
        }
    },
    plus: {
        fontSize: '20px',
        fontWeight: 'bold',
        color: '#272727'
    },
    container: {
        width: '100%',
    },
    contTitle: {
        width: '100%',
        background: '#fff',
        // borderBottom: '1px solid #D8D8D8',
        padding: '20px 30px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        position: 'relative',
    },
    accordion: {
        boxShadow: '0px 5px 10px rgba(0,0,0,.12)',
        width: '100%',
        borderRadius: '13px',
        marginTop: '25px',
        '&:before': {
            height: '0px'
        }
    },
    accordionSummary: {
        padding: '22px 35px',
        '&:before': {
            height: '0px'
        }
    },
    accordionDetails: {
        padding: '20px 0 0 0',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        flexWrap: 'wrap',
        borderTop: '1px solid #DBDBDB',
    },
    accordionTitle: {
        fontSize: '18px',
        fontWeight: '500',
        color: '#000'
    },
    blueButton: {
        width: '115px',
        Height: '40px',
        border: '1px solid #0079F6',
        borderRadius: '9px',
        fontSize: '14px',
        fontWeight: '500',
        color: '#0097F6',
        marginRight: '15px',
        padding: '10px 24px'
    },
    blackButton: {
        width: '115px',
        Height: '40px',
        border: '1px solid #000',
        borderRadius: '9px',
        fontSize: '14px',
        fontWeight: '500',
        color: '#000',
        marginRight: '15px',
        padding: '10px 24px'
    },
    accordionItem: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginBottom: '20px'
    },
    accordionItemLabel: {
        fontSize: '14px',
        fontWeight: '500',
        color: '#A8A8A8',
        minWidth: '150px',
        marginLeft: '20px',
        marginBottom: '3px'
    },
    accordionItemInfo: {
        fontSize: '16px',
        fontWeight: '400',
        marginLeft: '20px',
        color: '#242424'
    },
    accordionActionButtonsCont: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: '70px',
        padding: '15px 20px',
        borderTop: '1px solid #DBDBDB'
    },
})

const PersonalAddress = props => {

    const {t} = useTranslation()

    const history = useHistory()

    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const matches = useMediaQuery('(max-width:600px)')

    const [addresses,setAddresses] = useState([])

    useState(() => {
        props.fetching()
        Address.getAddresses(props.user)
            .then(res => {
                setAddresses([...res.filter(address => !address.deleted).map(address => ({
                    iri: address['@id'],
                    id: address.id,
                    title: address.fullName,
                    addressType: address.addressType,
                    info: [
                        {
                            title: t('general.address'),
                            body: address.street
                        },
                        {
                            title: t('address.floor'),
                            body: address.floor
                        },
                        {
                            title: t('address.phone'),
                            body: address.phoneNumber
                        },
                        {
                            title: t('address.postalCode'),
                            body: address.customPostCode === '' ? address.postalCode.code : address.customPostCode
                        },
                        {
                            title: t('address.nr'),
                            body: address.nr
                        },
                        {
                            title: t('address.country'),
                            body: address.country.name
                        },
                        {
                            title: t('address.county'),
                            body: address.county.name
                        },
                        {
                            title: t('address.city'),
                            body: address.city !== null ? address.city.name : '' 
                        },
                    ]
                }))])

                props.success()
            })
            .catch(error => {
                props.error()
                console.error(error)
            })
    },[])

    const [expanded,setExpanded] = useState('')

    const handleChange = panel => (event, newExpanded) => {
        setExpanded(newExpanded ? panel : false)
    }

    const switchAddressType = addressType => {
        switch(addressType){
            case 'SERVICE_ADDRESS':
                return t('OfferStep2.serviceAddress')
            case 'COMPANY_ADDRESS':
                return t('offerItem.companyAddress')
            case 'BILLING_ADDRESS':
                return t('OfferStep2.billingAddress')
            default:
                return ''
        }
    }

    const handleEditClick = async btn => {
        props.fetching()

        let res = await props.updateAddress(btn.target.name)

        if(res)
            history.push('/client/address/add')
    }

    const handleDeleteClick = async btn => {
        let address_id = btn.target.name

        props.fetching()
        let response  = await Address.SoftDelete(address_id)
        props.success()

        setAddresses(addresses.filter(address => address.id != address_id))
        setExpanded('')
    }

    return (
        <Grid item xs={12} md={8} sm={6}>
            {
                !matches
                    ?   <div className={classes.container}>
                            <div className={classes.contTitle}>
                                <Typography className={desktopClasses.title} gutterBottom>
                                    {t('general.myDirections')}
                                </Typography>
                                <Button
                                    variant='contained'
                                    className={desktopClasses.addButton}  
                                    onClick={() => history.push('/client/address/add')}
                                >
                                    <Link to='/client/address/add' >
                                        {t('general.addNewAddress')}
                                    </Link>
                                </Button>
                            </div>
                            {
                                addresses.map((address,id) => <div key={`address${id}`} className={classes.card}>
                                                                    <div className={classes.cardHeader}>
                                                                        <div className={classes.cardTitleCont}>
                                                                            <p className={desktopClasses.cardTitle} >{address.title}</p>
                                                                        </div>
                                                                        <div className={classes.cardActionButtonsCont}>
                                                                            <button 
                                                                                name={address.iri}
                                                                                onClick={handleEditClick}
                                                                                className={desktopClasses.blueButton} 
                                                                            >{t('general.edit')}</button>
                                                                            <button 
                                                                                className={desktopClasses.blackButton} 
                                                                                name={address.id}
                                                                                onClick={handleDeleteClick}
                                                                            >{t('general.delete')}</button>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.cardContent} >
                                                                        {
                                                                            address.info 
                                                                                ? address.info.map((info,id2) => <div key={`addres${id}sinfo${id2}`} className={desktopClasses.cardItem}>
                                                                                                                    <p className={desktopClasses.cardItemLabel} >{info.title.toUpperCase()}</p>
                                                                                                                    <p className={desktopClasses.cardItemInfo}>{info.body}</p>
                                                                                                                </div>)
                                                                                : <div/>
                                                                        }
                                                                    </div>
                                                                </div>)
                            }
                        </div>
                    :   <div className={mobileClasses.container} >
                            <div className={mobileClasses.contTitle}>
                                <Typography className={mobileClasses.title}>
                                    {t('general.myDirections')}
                                </Typography>
                                <Link to='/client/address/add' className={mobileClasses.addButton} >
                                    {t('general.add')} <span className={mobileClasses.plus}>+</span>
                                </Link>
                            </div>
                            {
                                addresses.map((address,id) => <Accordion key={`address${id}`} className={mobileClasses.accordion}>
                                                                    <AccordionSummary 
                                                                        expandIcon={<ExpandMoreIcon />}
                                                                        className={mobileClasses.accordionSummary}
                                                                    >
                                                                        <Typography className={mobileClasses.accordionTitle}>
                                                                            {address.title}
                                                                        </Typography>
                                                                    </AccordionSummary>
                                                                    <AccordionDetails className={mobileClasses.accordionDetails} >
                                                                        {
                                                                            address.info 
                                                                                ? address.info.map((info,id2) => <div key={`addres${id}sinfo${id2}`} className={mobileClasses.accordionItem}>
                                                                                                                    <p className={mobileClasses.accordionItemLabel} >{info.title.toUpperCase()}</p>
                                                                                                                    <p className={mobileClasses.accordionItemInfo}>{info.body}</p>
                                                                                                                </div>)
                                                                                : <div/>
                                                                        }
                                                                        <div className={mobileClasses.accordionActionButtonsCont}>
                                                                            <button 
                                                                                name={address.iri}
                                                                                onClick={handleEditClick}
                                                                                className={desktopClasses.blueButton} 
                                                                            >{t('general.edit')}</button>
                                                                            <button 
                                                                                className={desktopClasses.blackButton} 
                                                                                name={address.id}
                                                                                onClick={handleDeleteClick}
                                                                            >{t('general.delete')}</button>
                                                                        </div>
                                                                    </AccordionDetails>
                                                                </Accordion>)
                            }
                        </div>
            }
        </Grid>
    )
}

const mapState = state => ({
    user: state.auth.user.data.iri
})

const mapDispatch = {
    ...AddressActions,
    ...FetchActions
}

export default connect(mapState,mapDispatch)(PersonalAddress)