import React , { useState , useEffect } from 'react'
// import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import { validateTokenResetPassword, validatedResetPassword } from '../../utils/User'

import Header from "../../main/client_view_main/Header";
import Footer from "../../main/client_view_main/Footer";

import {
    Grid,
    useMediaQuery,
    TextField,
    CircularProgress
} from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles'
import GenericSnackbar from "../../utils/GenericSnackbar";

const useStyles = makeStyles({
    container: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    root: {
        flexGrow: 1,
        background: '#fff',
        position: 'relative'
    },
    loaderCont: {
        width: '100vw',
        height: '100vh',
        position: 'fixed',
        background: 'rgba(0,0,0,.1)',
        zIndex: '1000',
        display: 'flex',
        justifyContent: 'center',
        boxSizing: 'border-box',
        alignItems: 'center',
        top: '0',
        left: '0'
    },
})

const desktopStyles = makeStyles({
    resetContainer: {
        width: '900px',
        boxShadow: '0px 5px 30px rgba(0,0,0,.09)',
        height: '570px',
        // borderRadius: '3px',
        background: '#fff',
        padding: '50px 32px',
        display: 'flex',
        flexDirection: 'column',
        borderRadius: '13px',
        '& a': {
            fontSize: '14px',
            fontWeight: '500',
            color: '#2F2F2F',
        },
        '& a:hover': {
            textDecoration: 'none'
        }
    },
    mainCont: {
        padding: '0 8%'
    },
    title: {
        fontSize: '25px',
        fontWeight: 'bold',
        color: '#272727',
        marginBottom: '42px'
    },
    infoLabel: {
        width: '700px',
        fontSize: '16px',
        color: '#484848',
        fontWeight: '400',
        marginBottom: '42px',
        '& span': {
            color: '#0097F6',
            fontWeight: '500'
        }
    },
    inputLabel: {
        fontSize: '18px',
        fontWeight: '500',
        color: '#313131',
        marginBottom: '12px'
    },
    textField: {
        marginBottom: '32px',
        '& fieldset':{
            border: '1px solid #DBDBDB'
        },
        '& .MuiInputBase-root:hover fieldset': {
            border: '1px solid #DBDBDB'
        },
        '& .MuiInputBase-root:focus fieldset': {
            border: '1px solid #DBDBDB'
        }
    },
    button: {
        border: '1px solid #0097F6',
        color: '#0097F6',
        fontSize: '22px',
        fontWeight: 'bold',
        borderRadius: '9px',
        width: '250px',
        height: '80px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
})

const mobileStyles = makeStyles({
    resetContainer: {
        width: '350px',
        maxWidth: '100%',
        boxShadow: '0px 5px 30px rgba(0,0,0,.09)',
        minHeight: '310px',
        paddingBottom: '25px',
        // borderRadius: '3px',
        // padding: '50px 32px',
        display: 'flex',
        flexDirection: 'column',
        borderRadius: '13px',
        padding: '0 15px',
        '& a': {
            fontSize: '14px',
            fontWeight: '500',
            color: '#2F2F2F',
        },
        '& a:hover': {
            textDecoration: 'none'
        }
    },
    mainCont: {
        padding: '0 15px'
    },
    title: {
        fontSize: '25px',
        fontWeight: 'bold',
        color: '#272727',
        marginBottom: '20px',
        alignSelf: 'flex-start'
    },
    infoLabel: {
        width: '340px',
        maxWidth: '90%',
        marginTop: '50px',
        fontSize: '12px',
        color: '#484848',
        fontWeight: '400',
        marginBottom: '42px',
        '& span': {
            color: '#0097F6',
            fontWeight: '500'
        }
    },
    inputLabel: {
        fontSize: '14px',
        fontWeight: '500',
        color: '#313131',
        marginBottom: '12px'
    },
    textField: {
        marginBottom: '20px',
        '& fieldset':{
            border: '1px solid #DBDBDB'
        },
        '& .MuiInputBase-root:hover fieldset': {
            border: '1px solid #DBDBDB'
        },
        '& .MuiInputBase-root:focus fieldset': {
            border: '1px solid #DBDBDB'
        }
    },
    button: {
        border: '1px solid #0097F6',
        color: '#0097F6',
        fontSize: '14px',
        fontWeight: '500',
        borderRadius: '9px',
        width: '120px',
        height: '40px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    }
})

const ClientResetPassword2 = props => {

    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const [state,setState] = useState({
        password: '',
        confirmPassword: ''
    })

    const [snackbar,setSnackbar] = useState({
        open: false,
        message: '', 
        severity: 'success',
    })

    const handleCloseSnackbar = value => setSnackbar(value)

    const [token, setToken] = useState('')

    const [fetching,setFetching] = useState(false)

    const { t } = useTranslation()

    const matches = useMediaQuery('(max-width:600px)')

    const validateToken = async () => {
        let params = new URLSearchParams(window.location.search.slice(1,window.location.search.length));
        setFetching(true)


        
        let response = await validateTokenResetPassword(params.get('token'))
        setFetching(false)

        if (response.data.status) {
            if (response.data.status === 'ok') {
                setSnackbar({
                    open: true,
                    message: `${t('resetPasswordAdvices.validatedResetRequest')}`, 
                    severity: 'success',
                })
            }
        }else if (response.data.error) {
            setSnackbar({
                open: true,
                message: `${t('resetPasswordAdvices.user_not_found')}`, 
                severity: 'error',
            })
        }
        if(response.data.status === 'ok'){
            setToken(params.get('token'))
        }else{
            console.log('redirect')
        }
    }

    useEffect(() => {
        validateToken()
    },[])

    const handleSubmit = async () => {
        console.log(state)
    
        if (state.password === state.confirmPassword) {
            setFetching(true)
            let response = await validatedResetPassword({
                token,
                ...state
            })

            if(response.data.status){
                if(response.data.status === "ok")
                    setSnackbar({
                        open: true,
                        message: t('resetPasswordAdvices.resetPasswordCompleted'), 
                        severity: 'success',
                    })
            }else if(response.data.error){
                setSnackbar({
                    open: true,
                    message: t(`resetPasswordAdvices.${response.data.error}`), 
                    severity: 'error',
                })
            }            
            setFetching(false)
        } else {
            setSnackbar({
                open: true,
                message: `${t('resetPasswordAdvices.passwords_not_matched')}`, 
                severity: 'error',
            })
        }
    }

    const onChange = input => {
        setState({
            ...state,
            [input.target.name]: input.target.value
        })
    }

    return (
        <div className={classes.root}>
            <Header/>
            <div className={classes.loaderCont} style={{display: fetching ? 'flex' : 'none'}}>
                <CircularProgress />
            </div>
            <div className={matches ? mobileClasses.mainCont : desktopClasses.mainCont}>
                <Grid
                container
                spacing={2}
                >
                    <Grid item xs={12} md={12} sm={12}>
                        <GenericSnackbar 
                            handleClose={handleCloseSnackbar} 
                            {...snackbar} 
                        />
                        <div className={classes.container}>
                            {
                                matches && <p className={mobileClasses.title}>{t('general.resetPassword')}</p>
                            }
                            <div className={matches ? mobileClasses.resetContainer : desktopClasses.resetContainer} >
                                {
                                    !matches && <p className={desktopClasses.title}>{t('general.resetPassword')}</p>
                                }
                                {/* <p className={matches ? mobileClasses.infoLabel : desktopClasses.infoLabel} >{t('register.inputs.confirmPassword')}</p> */}
                                <p className={matches ? mobileClasses.inputLabel : desktopClasses.inputLabel}>{t('changePassword.newPassword')}</p>
                                <TextField 
                                    variant='outlined'
                                    name='password'
                                    value={state.password}
                                    type='password'
                                    onChange={onChange}
                                    className={matches ? mobileClasses.textField : desktopClasses.textField}
                                />
                                <p className={matches ? mobileClasses.inputLabel : desktopClasses.inputLabel}>{t('register.inputs.confirmPassword')}</p>
                                <TextField 
                                    variant='outlined'
                                    name='confirmPassword'
                                    value={state.confirmPassword}
                                    type='password'
                                    onChange={onChange}
                                    className={matches ? mobileClasses.textField : desktopClasses.textField}
                                />
                                <button className={matches ? mobileClasses.button : desktopClasses.button} onClick={handleSubmit} >
                                    {t('buttons.send')}
                                </button>
                            </div>
                        </div>
                    </Grid>
                </Grid>
            </div>
            <Footer/>
        </div>
        
    )
}

export default ClientResetPassword2