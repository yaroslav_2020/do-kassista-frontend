import React, { useState, useEffect } from 'react';
import { default as FusePageCarded } from '@fuse/core/FusePageCarded';
import { Link, useHistory } from 'react-router-dom';

import * as PostalCode from '../../utils/PostalCode';
import * as PostalCodeGroups from '../../utils/PostalCodeGroups';

import { useTranslation } from 'react-i18next';

import MessageSnackbar from '../utils/MessageSnackbar';

import { Button, Typography, TextField, Grid, Box, Select, MenuItem } from '@material-ui/core';

import { ArrowBack } from '@material-ui/icons';
import { truncate } from 'lodash';

const Create = props => {
	const [postalCode, setPostalCode] = useState({
		title: '',
		code: '',
		zoneName: '',
		address: '',
		postalCodeGroup: ''
	});

	const [fetching, setFetching] = useState(false);

	const { t, i18n } = useTranslation();

	const [postalCodeGroups, setPostalCodeGroups] = useState([]);

	useEffect(() => {
		setFetching(true);
		PostalCodeGroups.getPostalCodeGroups()
			.then(res => {
				let options = res.map(pcg => ({
					label: pcg.title,
					value: pcg['@id']
				}));

				setPostalCodeGroups(options);
				setFetching(false);
			})
			.catch(error => console.error(error));
	}, []);

	const info = [
		{
			name: 'title',
			value: postalCode.title,
			label: t('tableHeads.title')
		},
		{
			name: 'code',
			value: postalCode.code,
			label: t('tableHeads.code')
		},
		{
			name: 'zoneName',
			value: postalCode.zoneName,
			label: t('tableHeads.zoneName')
		},
		{
			name: 'address',
			value: postalCode.address,
			label: t('general.address')
		}
	];

	const handleChange = input => {
		setPostalCode({
			...postalCode,
			[input.target.name]: input.target.value
		});
	};

	const handleSubmit = async btn => {
		setFetching(true);

		let response = await PostalCode.createPostalCodes(postalCode);
		console.log(response);
		setFetching(false);
		setPostalCode({
			title: '',
			code: '',
			zoneName: '',
			address: '',
			postalCodeGroup: ''
		});
	};

	return (
		<FusePageCarded
			classes={{
				toolbar: 'p-0',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={
				<div className="flex flex-1 w-full items-center justify-between">
					<div className="flex flex-col items-start max-w-full">
						<div className="flex items-center max-w-full">
							<div className="flex flex-col min-w-0">
								<Link to="/postal_codes" style={{ textDecoration: 'none', marginBottom: '10px' }}>
									<ArrowBack /> {t('general.back')}
								</Link>
								<Typography variant="h4">{t('adminPanel.createPostalCode')}</Typography>
							</div>
						</div>
					</div>
					<div className="flex flex-row items-end max-w-full">{/* pagination */}</div>
				</div>
			}
			content={
				<Box p={4} mt={2} mb={2}>
					{fetching && <MessageSnackbar variant="info" message="Loading Data..." />}
					<Grid container direction="row" justify="flex-start" alignItems="flex-start" spacing={2}>
						{info.length ? (
							info.map(data => (
								<Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
									<Typography variant="subtitle1" style={{ fontWeight: 'bold' }}>
										{data.label}
									</Typography>
									<TextField
										type="text"
										name={data.name}
										value={data.value}
										variant="outlined"
										onChange={handleChange}
										fullWidth
									/>
								</Grid>
							))
						) : (
							<Grid />
						)}
						<Grid item xs={12} md={12} sm={12}>
							<Typography variant="subtitle1" style={{ fontWeight: 'bold' }}>
								{t('adminPanel.postalCodeGroups')}
							</Typography>
							<Select
								name="postalCodeGroup"
								value={postalCode.postalCodeGroup}
								variant="outlined"
								onChange={handleChange}
								fullWidth
							>
								<MenuItem value="">None</MenuItem>
								{postalCodeGroups.map((pcg, id) => (
									<MenuItem key={id} value={pcg.value}>
										{pcg.label}
									</MenuItem>
								))}
							</Select>
						</Grid>
						<Grid item xs={12} md={12} sm={12}>
							<Button variant="contained" color="primary" onClick={handleSubmit} fullWidth>
								{t('general.save')}
							</Button>
						</Grid>
					</Grid>
				</Box>
			}
			innerScroll
		></FusePageCarded>
	);
};

export default Create;
