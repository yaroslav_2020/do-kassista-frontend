import React, { useState, useEffect } from 'react';
import { default as FusePageCarded } from '@fuse/core/FusePageCarded';
import { Link, useHistory } from 'react-router-dom';

import * as PostalCode from '../../utils/PostalCode';
import * as PostalCodeGroups from '../../utils/PostalCodeGroups';

import { useTranslation } from 'react-i18next';

import MessageSnackbar from '../utils/MessageSnackbar';
import GenericPrompt from '../utils/GenericPrompt';

import {
	Button,
	Typography,
	// TextField,
	Grid,
	Box
} from '@material-ui/core';

import { ArrowBack } from '@material-ui/icons';

const Item = props => {
	const history = useHistory();

	const [fetching, setFetching] = useState(false);

	const { t } = useTranslation();

	const [dialog, setDialog] = useState({
		open: false,
		dataAction: -1
	});

	const [postalCode, setPostalCode] = useState({
		id: 0,
		title: '',
		code: '',
		allowOffer: '',
		activeStatus: '',
		zoneName: '',
		address: '',
		preferredPrice: '',
		currencyPreferredPrice: ''
	});

	useEffect(() => {
		if (props.match.params.id) {
			setFetching(true);

			PostalCode.getPostalCodeItem(props.match.params.id)
				.then(async res => {
					let postalCodeGroup = res.data.postalCodeGroup;

					if (postalCodeGroup !== null) {
						postalCodeGroup = await PostalCodeGroups.getPostalCodeGroupsId(
							parseInt(postalCodeGroup.split('/api/postal_code_groups/')[1])
						);
					}

					setPostalCode({
						...postalCode,
						id: res.data.id,
						title: res.data.title,
						code: res.data.code,
						allowOffer: res.data.allowOffer ? t('general.yes') : t('general.not'),
						activeStatus: res.data.activeStatus ? t('general.yes') : t('general.not'),
						zoneName: res.data.zoneName,
						address: res.data.address,
						preferredPrice: res.data.preferredPrice,
						currencyPreferredPrice: res.data.currencyPreferredPrice,
						postalCodeGroup: postalCodeGroup !== null ? postalCodeGroup.title : ''
					});

					setFetching(false);
				})
				.catch(error => console.error(error));
		}
	}, []);

	const info = [
		{
			name: 'title',
			value: postalCode.title,
			label: t('tableHeads.title')
		},
		{
			name: 'code',
			value: postalCode.code,
			label: t('tableHeads.code')
		},
		{
			name: 'allowOffer',
			value: postalCode.allowOffer,
			label: t('tableHeads.allowOffer')
		},
		{
			name: 'activeStatus',
			value: postalCode.activeStatus,
			label: t('tableHeads.activeStatus')
		},
		{
			name: 'zoneName',
			value: postalCode.zoneName,
			label: t('tableHeads.zoneName')
		},
		{
			name: 'address',
			value: postalCode.address,
			label: t('general.address')
		},
		{
			name: 'preferredPrice',
			value: postalCode.preferredPrice,
			label: t('tableHeads.preferedPrice')
		},
		{
			name: 'currencyPreferredPrice',
			value: postalCode.currencyPreferredPrice,
			label: t('tableHeads.currency')
		},
		{
			name: 'postalCodeGroup',
			value: postalCode.postalCodeGroup,
			label: t('adminPanel.postalCodeGroups')
		}
	];

	const openDialog = () =>
		setDialog({
			open: true,
			dataAction: props.match.params.id
		});

	const closeDialog = () =>
		setDialog({
			open: false,
			dataAction: -1
		});

	const handleDelete = postal_code_id => {
		closeDialog();
		setFetching(true);
		PostalCode.deletePostalCodes(postal_code_id)
			.then(res => {
				setFetching(false);
				history.push('/postal_codes');
			})
			.catch(error => console.error(error));
	};

	return (
		<FusePageCarded
			classes={{
				toolbar: 'p-0',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={
				<div className="flex flex-1 w-full items-center justify-between">
					<div className="flex flex-col items-start max-w-full">
						<div className="flex items-center max-w-full">
							<div className="flex flex-col min-w-0">
								<Link to="/postal_codes" style={{ textDecoration: 'none', marginBottom: '10px' }}>
									<ArrowBack /> {t('general.back')}
								</Link>
								<Typography variant="h4">{postalCode.title}</Typography>
							</div>
						</div>
					</div>
					<div className="flex flex-row items-end max-w-full">{/* pagination */}</div>
					<div className="flex flex-row items-end max-w-full">
						<Link to={`/postal_codes/update/${props.match.params.id}`} style={{ textDecoration: 'none' }}>
							<Button className="whitespace-no-wrap" variant="contained">
								{t('adminPanel.updatePostalCode')}
							</Button>
						</Link>
						<Button
							style={{ marginLeft: '10px' }}
							className="whitespace-no-wrap"
							color="secondary"
							variant="contained"
							onClick={openDialog}
						>
							{t('adminPanel.deletePostalCode')}
						</Button>
					</div>
				</div>
			}
			content={
				<Box p={4} mt={2} mb={2}>
					{fetching && <MessageSnackbar variant="info" message="Loading Data..." />}
					{!fetching && (
						<Grid container direction="row" justify="flex-start" alignItems="flex-start" spacing={2}>
							{info.length ? (
								info.map(data => (
									<Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
										<Typography variant="subtitle1" style={{ fontWeight: 'bold' }}>
											{data.label}
										</Typography>
										<Typography variant="body1">{data.value}</Typography>
									</Grid>
								))
							) : (
								<Grid />
							)}
						</Grid>
					)}
					<GenericPrompt
						title={t('adminPanel.deletePostalCode')}
						message={t('general.areYouSureToDoThis')}
						close={closeDialog}
						action={handleDelete}
						data={dialog}
					/>
				</Box>
			}
			innerScroll
		></FusePageCarded>
	);
};

export default Item;
