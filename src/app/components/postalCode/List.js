import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import GenericTable from '../utils/GenericTable';
import { default as FusePageCarded } from '@fuse/core/FusePageCarded';

import * as PostalCode from '../../utils/PostalCode';

import { useTranslation } from 'react-i18next';

import MessageSnackbar from '../utils/MessageSnackbar';
import GenericPrompt from '../utils/GenericPrompt';

import { Button, Typography } from '@material-ui/core';

const List = props => {
	const [postalCodes, setPostalCodes] = useState([]);

	const [fetching, setFetching] = useState(false);

	const [dialog, setDialog] = useState({
		open: false,
		dataAction: -1
	});

	const { t, i18n } = useTranslation();

	const tableHeaders = [
		'ID',
		t('tableHeads.title'),
		t('tableHeads.code'),
		t('tableHeads.activeStatus'),
		t('tableHeads.zoneName'),
		t('tableHeads.actions')
	];

	const openDialog = id =>
		setDialog({
			open: true,
			dataAction: id
		});

	const closeDialog = () =>
		setDialog({
			open: false,
			dataAction: -1
		});

	const handleDelete = postal_code_id => {
		setDialog({
			open: false,
			dataAction: -1
		});
		setFetching(true);
		console.log(postal_code_id);
		PostalCode.deletePostalCodes(postal_code_id)
			.then(res => {
				setFetching(false);
				getPostalCodesCollection();
			})
			.catch(error => console.error(error));
	};

	const getPostalCodesCollection = () => {
		setFetching(true);
		PostalCode.getPostalCodes().then(res => {
			let rows = [];

			res.map(data => {
				let row = [
					{
						link: true,
						path: `/postal_codes/item/${data.id}`,
						message: data.id
					},
					{
						link: false,
						message: data.title
					},
					{
						link: false,
						message: data.code
					},
					{
						link: false,
						message: data.activeStatus
					},
					{
						link: false,
						message: data.zoneName
					},
					{
						action: true,
						icons: [
							{
								icon: 'search',
								path: `/postal_codes/item/${data.id}`
							},
							{
								icon: 'edit',
								path: `/postal_codes/update/${data.id}`
							},
							{
								icon: 'delete',
								onClick: openDialog,
								id: data.id
							}
						]
					}
				];

				rows.push(row);
			});

			setPostalCodes(rows);
			setFetching(false);
		});
	};

	useEffect(() => {
		getPostalCodesCollection();
	}, []);

	return (
		<FusePageCarded
			classes={{
				toolbar: 'p-0',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={
				<div className="flex flex-1 w-full items-center justify-between">
					<div className="flex flex-col items-start max-w-full">
						<div className="flex items-center max-w-full">
							<div className="flex flex-col min-w-0">
								<Typography variant="h4">{t('adminPanel.postalCodes')}</Typography>
							</div>
						</div>
					</div>
					<div className="flex flex-row items-end max-w-full">{/* pagination */}</div>
					<div className="flex flex-row items-end max-w-full">
						<Link to="/postal_codes/create" style={{ textDecoration: 'none' }}>
							<Button className="whitespace-no-wrap" variant="contained">
								{t('adminPanel.createPostalCode')}
							</Button>
						</Link>
					</div>
				</div>
			}
			content={
				<div className="p-16 sm:p-24" style={{ maxHeight: '400px' }}>
					{fetching && <MessageSnackbar variant="info" message="Loading Data..." />}
					{!fetching && <GenericTable headers={tableHeaders} body={postalCodes} />}
					<GenericPrompt
						title={t('adminPanel.deletePostalCode')}
						message={t('general.areYouSureToDoThis')}
						close={closeDialog}
						action={handleDelete}
						data={dialog}
					/>
				</div>
			}
			innerScroll
		></FusePageCarded>
	);
};

export default List;
