import React , { useState, useEffect } from 'react'
import {default as FusePageCarded} from '@fuse/core/FusePageCarded'
import { Link , useHistory } from 'react-router-dom'

import * as PostalCode  from '../../utils/PostalCode'
import * as PostalCodeGroups from '../../utils/PostalCodeGroups'

import { useTranslation } from 'react-i18next'

import MessageSnackbar from '../utils/MessageSnackbar'
import GenericPrompt from '../utils/GenericPrompt'

import {
    Button,
    Typography,
    TextField,
    Grid,
    Box,
    Switch,
    Select,
    MenuItem
} from '@material-ui/core'

import {
    ArrowBack
} from '@material-ui/icons'

const Update = props => {

    const history = useHistory()

    const [fetching,setFetching] = useState(false)

    const {t,i18n} = useTranslation()

    const [postalCode,setPostalCode] = useState({
                                title: '',
                                code: '',
                                activeStatus: false,
                                zoneName: '',
                                address: '',
                                postalCodeGroup: ''
                            })

    const [dialog,setDialog] = useState({
        open: false,
        dataAction: -1,
        action: () => {}
    })
    
    const [postalCodeGroups,setPostalCodeGroups] = useState([])
    
    useEffect(() => {
        setFetching(true)
        PostalCodeGroups.getPostalCodeGroups()
            .then(res => {
                let options = res.map(pcg => ({
                    label: pcg.title,
                    value: pcg['@id']
                }))

                setPostalCodeGroups(options)
            })
            .catch(error => console.error(error))

        if(props.match.params.id)
            PostalCode.getPostalCodeItem(props.match.params.id)
                .then(res => {
                    setPostalCode({
                        ...postalCode,
                        title: res.data.title,
                        code: res.data.code,
                        activeStatus: res.data.activeStatus ? true : false,
                        zoneName: res.data.zoneName,
                        address: res.data.address ? res.data.address : '',
                        postalCodeGroup: res.data.postalCodeGroup !== null ? res.data.postalCodeGroup : ''
                    })
                    setFetching(false)
                })
                .catch(error => console.error(error))
        
    },[])

    const info = [
        {
            name: 'title',
            value: postalCode.title,
            label: t('tableHeads.title'),
            type: 'text'
        },
        {
            name: 'code',
            value: postalCode.code,
            label: t('tableHeads.code'),
            type: 'text'
        },
        {
            name: 'zoneName',
            value: postalCode.zoneName,
            label: t('tableHeads.zoneName'),
            type: 'text'
        },
        {
            name: 'address',
            value: postalCode.address,
            label: t('general.address'),
            type: 'text'
        },
        {
            name: 'postalCodeGroup',
            value: postalCode.postalCodeGroup,
            label: t('adminPanel.postalCodeGroups'),
            type: 'select',
            options: postalCodeGroups
        },
        {
            name: 'activeStatus',
            value: postalCode.activeStatus,
            label: t('tableHeads.activeStatus'),
            type: 'switch'
        },
    ]

    const openDialog = action => setDialog({
        open: true,
        dataAction: props.match.params.id,
        action
    })

    const closeDialog = () => setDialog({
        open: false,
        dataAction: -1,
        action: () => {}
    })

    const handleChange = input => {
        setPostalCode({
            ...postalCode,
            [input.target.name]: input.target.value
        })
    }

    const handleSubmit = async btn => {
        closeDialog()
        setFetching(true)
        let response = await PostalCode.updatePostalCodes(props.match.params.id,{
            ...postalCode,
            activeStatus: postalCode.activeStatus ? 1 : 0
        })
        setFetching(false)
    }

    const handleSwitchChange = () => {
        setPostalCode({
            ...postalCode,
            activeStatus: !postalCode.activeStatus
        })
    }

    const handleDelete = postal_code_id => {
        closeDialog()
        setFetching(true)
        PostalCode.deletePostalCodes(postal_code_id)
            .then(res => {
                setFetching(false)
                history.push('/postal_codes')
            })
            .catch(error => console.error(error))
            
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: 'p-0',
                header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
            }}
            header={
                <div className='flex flex-1 w-full items-center justify-between'>
                    <div className='flex flex-col items-start max-w-full'>
                        <div className='flex items-center max-w-full'>
                            <div className='flex flex-col min-w-0'>
                                <Link to='/postal_codes' style={{textDecoration: 'none',marginBottom: '10px'}}>
                                    <ArrowBack /> {t('general.back')}
                                </Link>
                                <Typography variant='h4'>
                                    {t('adminPanel.updatePostalCode')}
                                </Typography>
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        {/* pagination */}
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        <Link to='/postal_codes/create' style={{textDecoration: 'none'}}>
                            <Button className='whitespace-no-wrap' variant='contained'>
                                {t('adminPanel.createPostalCode')}
                            </Button>
                        </Link>
                        <Button 
                            style={{marginLeft: '10px'}} 
                            className='whitespace-no-wrap' 
                            color='secondary' 
                            variant='contained'
                            onClick={() => openDialog(handleDelete)}
                        >
                            {t('adminPanel.deletePostalCode')}
                        </Button>
                    </div>
                </div>
            }
            content={
                <Box p={4} mt={2} mb={2}>
                    {
                        fetching && <MessageSnackbar variant='info' message='Loading Data...' />
                    }
                    <Grid
                        container
                        direction='row'
                        justify='flex-start'
                        alignItems='flex-start'
                        spacing={2}
                    >
                        {
                            info.length
                                ?   info.map((data,id) => {
                                        if(data.type !== 'switch' && data.type !== 'select'){
                                            return <Grid key={id} item xs={12} md={12} sm={12}>
                                                        <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                            {data.label}
                                                        </Typography>
                                                        <TextField
                                                            type='text'
                                                            name={data.name}
                                                            value={data.value}
                                                            variant='outlined'
                                                            onChange={handleChange}
                                                            fullWidth
                                                        />
                                                    </Grid>
                                        }else if(data.type === 'select'){
                                            return <Grid key={id} item xs={12} md={12} sm={12}>
                                                        <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                            {data.label}
                                                        </Typography>
                                                        <Select
                                                            name={data.name}
                                                            value={data.value}
                                                            variant='outlined'
                                                            onChange={handleChange}
                                                            fullWidth
                                                        >
                                                            <MenuItem value=''>
                                                                None
                                                            </MenuItem>
                                                            {
                                                                data.options.map((option,id) => <MenuItem key={id} value={option.value}>{option.label}</MenuItem>)
                                                            }
                                                        </Select>
                                                    </Grid>

                                        }else{
                                            return <Grid key={id} item xs={12} md={12} sm={12}>
                                                        <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                            {data.label}
                                                        </Typography>
                                                        <Switch
                                                            checked={data.value}
                                                            onChange={handleSwitchChange}
                                                            name={data.name}
                                                        />
                                                    </Grid>
                                        }
                                    })
                                :   <Grid />
                        }
                        <Grid item xs={12} md={12} sm={12}>
                            <Button
                                variant='contained'
                                color='primary'
                                onClick={() => openDialog(handleSubmit)}
                                fullWidth
                            >
                                {t('general.save')}
                            </Button>
                        </Grid>
                    </Grid>
                    <GenericPrompt 
                        title='Action for Postal Code'
                        message={t('general.areYouSureToDoThis')}
                        close={closeDialog}
                        action={dialog.action}
                        data={dialog}
                    />
                </Box>
            }
            innerScroll
        >
        </FusePageCarded>
    )
}

export default Update