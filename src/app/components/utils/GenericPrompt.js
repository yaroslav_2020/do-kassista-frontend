import React from 'react'

import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    useMediaQuery
} from '@material-ui/core'

import { useTheme, withStyles } from '@material-ui/core/styles'
import { useTranslation } from 'react-i18next'
// import DeleteIcon from '@material-ui/icons/Delete';
import { grey, red } from '@material-ui/core/colors';

const DeleteButton = withStyles((theme) => ({
    root: {
      color: theme.palette.getContrastText(red[700]),
      backgroundColor: red[500],
      '&:hover': {
        backgroundColor: red[700],
      },
      marginLeft: '20px'
    },
  }))(Button);
  
  const CancelButton = withStyles((theme) => ({
    root: {
    //   color: theme.palette.getContrastText(grey[700]),
      backgroundColor: grey[50],
      '&:hover': {
        backgroundColor: grey[300],
      },
      marginLeft: '10px'
    },
  }))(Button);

const GenericPropmt = props => {
    const theme = useTheme()
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'))
    const {t} = useTranslation()

    return (
        <Dialog
            fullScreen={fullScreen}
            open={props.data.open}
            aria-labelledby='responsive-dialog-title'
        >
            <DialogTitle id='responsive-dialog-title'>{props.title}</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    {props.message}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <CancelButton onClick={props.close} variant="contained" disableElevation>
                  {t('general.cancel')}
                </CancelButton>
                <DeleteButton type="submit" variant="contained" onClick={() => props.action(props.data.dataAction)} autoFocus>
                  {t('buttons.confirm')}
                </DeleteButton>
            </DialogActions>
        </Dialog>
    )
}

export default GenericPropmt