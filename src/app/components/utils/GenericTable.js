import React from 'react'
import PropTypes from 'prop-types'
import GenericRow from './GenericRow'

import { makeStyles, useTheme } from '@material-ui/core/styles';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import LastPageIcon from '@material-ui/icons/LastPage';

import {
    Table,
    TableBody,
    TableHead,
    TableRow,
    TableCell,
    withStyles,
    Paper,
    TableContainer,
    TablePagination,
    IconButton,
    TableFooter

} from '@material-ui/core';

import {
    FirstPage,
    KeyboardArrowLeft,
    KeyboardArrowRight,

} from '@material-ui/icons';

const StyledTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white
    },
    body: {
        fontSize: 14
    }
}))(TableCell)

const StyledTableRow = withStyles(theme => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default
        }
    }
}))(TableRow)

const componentStyles = {
    root: {
        width: '100%',
        // overflowX: 'auto'
    },
    container: {
        maxHeight: 588,
    },
    table: {
        minWidth: 1024,
        width: '1005',
    },
    tableHeadSticky: {
        zIndex: 500,
        '& tr > th': {
            position: 'sticky',
            top: 25
        }
    },
}

const useStyles1 = makeStyles((theme) => ({
    root: {
        flexShrink: 0,
        marginLeft: theme.spacing(2.5),
    },
}))

const GenericTable = props => {
    return (
        <Paper className={props.classes.root}>
            <TableContainer  className={props.classes.container}>
                <Table stickyHeader aria-label="sticky table" className={props.classes.table}>
                    <TableHead>
                        <StyledTableRow>
                            {
                                props.headers.length ? 
                                    props.headers.map(head => <StyledTableCell key={head} align='left'>{head}</StyledTableCell>)
                                    : <StyledTableCell/>
                            }
                        </StyledTableRow>
                    </TableHead>
                    <TableBody>
                        {
                            // props.body.length ? props.body.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(row => <GenericRow key={props.body.indexOf(row)} body={[...row]} />) : <TableRow/>
                            props.body.length ? props.body.map(row => <GenericRow key={props.body.indexOf(row)} body={[...row]} />) : <TableRow/>
                        }   
                    </TableBody>
                </Table>
            </TableContainer>
        </Paper>
    )
}
export default withStyles(componentStyles)(GenericTable)