import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Pagination from '@material-ui/lab/Pagination';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));

export default function PaginationControlled({ count, onPageChange, handleUrlChange, page = 1 }) {
  const classes = useStyles();
  // const [page, setPage] = React.useState(currentPage);
  const handleChange = (event, value) => {
    // setPage(value);
    onPageChange(value);
    handleUrlChange( pag => ({
      ...pag,
      currentPage: pag.currentPage.slice(0, -1).concat(value),
    }));
  };

  return (
    <div className={classes.root}>
      <Typography>Page: {page}</Typography>
      <Pagination
        // count={10}
        count={count}
        page={page}
        onChange={handleChange}
        color="primary"
        shape="rounded"
        variant="outlined"
        showFirstButton
        showLastButton
      />
    </div>
  );
}