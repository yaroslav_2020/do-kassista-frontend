import React from 'react'
import { Link } from 'react-router-dom'

import {
    TableRow,
    TableCell,
    withStyles,
    Icon,
    IconButton
} from '@material-ui/core'

const StyledTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white
    },
    body: {
        fontSize: 14
    }
}))(TableCell)

const StyledTableRow = withStyles(theme => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default
        }
    }
}))(TableRow)

const componentStyles = {
    action: {
        padding: '5px',
        fontSize: '14px'
    },
}

const GenericRow = props => {
    return (
        <StyledTableRow key={props.key}>
            {
                props.body.length ? props.body.map(column => {
                    if(column.link)
                        return (
                            <StyledTableCell key={`c${props.key}${props.body.indexOf(column)}`} align='left'>
                                <Link to={column.path}>
                                    {column.message}
                                </Link>
                            </StyledTableCell>
                        )                    

                    if(column.action)
                            return (
                                <StyledTableCell key='6' align='left' className={props.classes.action}>
                                    {
                                        column.icons.length ? column.icons.map(icon => {
                                            if(icon.icon === '') {
                                                return ''                                        
                                            } else if(icon.icon !== 'delete'){
                                                return <Link key={`i${column.icons.indexOf(icon)}`} to={icon.path}>
                                                            <IconButton>
                                                                <Icon className='text-18' color='action'>{icon.icon}</Icon>
                                                            </IconButton>
                                                        </Link>
                                            }else{
                                                return  <IconButton key={`i${column.icons.indexOf(icon)}`} onClick={() => icon.onClick(icon.id)}>
                                                            <Icon className='text-18' color='action'>{icon.icon}</Icon>
                                                        </IconButton>
                                            }
                                        }):''
                                    }
                                </StyledTableCell>
                            )

                    return <StyledTableCell 
                                key={`c${props.key}${props.body.indexOf(column)}`} 
                                align='left'
                            >
                                {
                                    column.color 
                                        ?   <span
                                                style={{
                                                    background: column.color,
                                                    color: '#fff',
                                                    padding: '5px 20px',
                                                    borderRadius: '25px'
                                                }}
                                            >
                                                {column.message}
                                            </span>
                                        :   column.message
                                }   
                            </StyledTableCell>
                }): <td/>
            }
        </StyledTableRow>
    )
}

export default withStyles(componentStyles)(GenericRow)