import React , { useState } from 'react'
import {default as FusePageCarded} from '@fuse/core/FusePageCarded/index'
import { Link , useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import * as Faq from '../../utils/Faq'

import MessageSnackbar from '../utils/MessageSnackbar'

import {
    Button,
    Typography,
    TextField,
    Grid,
    Box,
    TextareaAutosize
} from '@material-ui/core'

import {
    ArrowBack
} from '@material-ui/icons'

const Create = props => {

    const [faq,setFaq] = useState({
                                question: '',
                                answer: '',
                            })

    const [fetching,setFetching] = useState(false)

    const {t} = useTranslation()

    const info = [
        {
            name: 'question',
            value: faq.question,
            label: t('general.question'),
            type: 'text',
        },
        {
            name: 'answer',
            value: faq.answer,
            label: t('general.answer'),
            type: 'textarea',
        }
    ]

    const handleChange = input => {
        setFaq({
            ...faq,
            [input.target.name]: input.target.value
        })
    }

    const handleClick = async btn => {
        setFetching(true)
        let response = await Faq.createFaq(faq)
        setFetching(false)
        setFaq({
            question: '',
            answer: '',
        })
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: 'p-0',
                header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
            }}
            header={
                <div className='flex flex-1 w-full items-center justify-between'>
                    <div className='flex flex-col items-start max-w-full'>
                        <div className='flex items-center max-w-full'>
                            <div className='flex flex-col min-w-0'>
                                <Link to='/faq' style={{textDecoration: 'none',marginBottom: '10px'}}>
                                    <ArrowBack /> {t('general.back')}
                                </Link>
                                <Typography variant='h4'>
                                    {t('adminPanel.createFaq')}
                                </Typography>
                            </div>
                        </div>
                    </div>
                </div>
            }
            content={
                <Box p={4} mt={2} mb={2}>
                    <Box mb={2}>
                        {
                            fetching && <MessageSnackbar variant='info' message='Loading Data...' />
                        }
                    </Box>
                    <Grid
                        container
                        direction='row'
                        justify='flex-start'
                        alignItems='flex-start'
                        spacing={2}
                    >
                        {
                            info.length
                                ?   info.map(data => {
                                        if(data.type === 'textarea'){
                                            return  <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                        <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                            {data.label}
                                                        </Typography>
                                                        <TextareaAutosize 
                                                            rowsMin={5}
                                                            margin='normal'
                                                            value={data.value}
                                                            onChange={handleChange}
                                                            inputlabelprops={{
                                                                shrink: true,
                                                            }}
                                                            style={{
                                                                width: '100%'
                                                            }}
                                                            name={data.name}
                                                        />
                                                    </Grid>
                                        }else{
                                            return  <Grid key={info.indexOf(data)} item xs={12} md={12} sm={12}>
                                                        <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                            {data.label}
                                                        </Typography>
                                                        <TextField
                                                            type={data.type}
                                                            name={data.name}
                                                            value={data.value}
                                                            variant='outlined'
                                                            onChange={handleChange}
                                                            fullWidth
                                                        />
                                                    </Grid>
                                        }
                                    })
                                :   <Grid />
                        }
                        <Grid item xs={12} md={12} sm={12}>
                            <Button
                                variant='contained'
                                color='primary'
                                onClick={handleClick}
                                fullWidth
                            >
                                {t('general.save')}
                            </Button>
                        </Grid>
                    </Grid>
                </Box>
            }
            innerScroll
        >
        </FusePageCarded>
    )
}

export default Create