import React , { useState , useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import GenericTable from '../utils/GenericTable'
import {default as FusePageCarded} from '@fuse/core/FusePageCarded/index'

import MessageSnackbar from '../utils/MessageSnackbar'

import * as Faq from '../../utils/Faq'

import {
    Button,
    Typography,
} from '@material-ui/core'

const List = props => {

    const {t} = useTranslation()

    const [fetching,setFetching] = useState(false)

    const [faq,setFaq] = useState([])

    useEffect(() => {
        setFetching(true)
        Faq.getFaq()
            .then(res => {
                let rows = []

                res.map(data => {
                    let row = [
                        {
                            link: true,
                            path: `/faq/${data.id}`,
                            message: data.id
                        },
                        {
                            link: false,
                            message: data.question
                        },
                        {
                            action: true,
                            icons: [
                                {
                                    icon: 'search',
                                    path: `/faq/${data.id}`
                                },
                                {
                                    icon: 'edit',
                                    path: `/faq/update/${data.id}`
                                },
                                {
                                    icon: 'delete',
                                    path: '/login'
                                }
                            ]
                        }
                    ]

                    rows.push(row)
                })

                setFaq(rows)
                setFetching(false)
            })
    },[])

    const tableHeaders = [
        'ID',
        t('general.question'),
        t('tableHeads.actions'),
    ]

    return (
        <FusePageCarded
            classes={{
                toolbar: 'p-0',
                header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
            }}
            header={
                <div className='flex flex-1 w-full items-center justify-between'>
                    <div className='flex flex-col items-start max-w-full'>
                        <div className='flex items-center max-w-full'>
                            <div className='flex flex-col min-w-0'>
                                <Typography variant='h4'>
                                    {t('general.faq')}
                                </Typography>
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        {/* pagination */}
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        <Link to='/faq/create' style={{textDecoration: 'none'}}>
                            <Button className='whitespace-no-wrap' variant='contained'>
                                {t('adminPanel.createFaq')}
                            </Button>
                        </Link>
                    </div>
                </div>
            }
            content={
                <div className='p-16 sm:p-24' style={{maxHeight: '400px'}}>
                    {
                        fetching && <MessageSnackbar variant='info' message='Loading Data...' />
                    }
                    {
                        !fetching && <GenericTable headers={tableHeaders} body={faq} /> 
                    }                   
                </div>
            }
            innerScroll
        >
        </FusePageCarded>
    )
}

export default List