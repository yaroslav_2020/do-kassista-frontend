import React , { useState , useEffect } from 'react'
import {default as FusePageCarded} from '@fuse/core/FusePageCarded'
import { Link , useParams } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import moment from 'moment'

import * as Offer from '../../utils/Offer'
import { getOfferFullSchedule } from 'app/utils/OfferFullSchedule';
import OfferStatusAdmin from 'app/components/offer/OfferStatusAdmin';

import MessageSnackbar from '../utils/MessageSnackbar'
import GenericPrompt from '../utils/GenericPrompt'

import {
    Button,
    Typography,
    Grid,
    Box,
    // Divider,
    Accordion,
    AccordionSummary,
    AccordionDetails
} from '@material-ui/core'

import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

import { makeStyles } from '@material-ui/core/styles'

import {
    ArrowBack
} from '@material-ui/icons'

const useStyles = makeStyles(theme => ({
    accordionSummary: {
        backgroundColor: theme.palette.primary.main,
        color: '#fff',
        fontWeight: 'bold',
        borderTop: '1px solid #fff'
    }
}))

const Update = () => {


    const classes = useStyles()
    const {t} = useTranslation()
    
    const { id } = useParams();

    const [dialog,setDialog] = useState({
        open: false,
        dataAction: -1
    })
    
    const [offer,setOffer] = useState({
        id: 0,
        createdAt: new Date(),
        startFromDate: '',
        endDate: '',
        client: {
            name: '',
            email: '',
            phoneNumber: '',
            bankName: '',
            bankAccount: ''
        },
        comercial: null,
        offerBuilding: {
            roomNumber: 0,
            bathRoomNumber: 0,
            kitchenNumber: 0,
            livingNumber: 0
        },
        periodPoints: [],
        extraServices: [],
        hours: 0,
        pricePerDay: 0,
        pricePerService: 0,        
        serviceAddress: '',
        billingAddress: '',
        company: null,
        vouchers: []
    })

    const [fetching,setFetching] = useState(false)

    const [serviceSchedule, setServiceSchedule] = useState([]);
    
    const period_detail = value => t(`Step1Frequency.period.${value}`)
    
    useEffect(() => {
        setFetching(true);
        (async () => {
            try {
                const { data: offerFetch } = await Offer.offerDetails( id );
                
                let extraServices = {}
                let hours = 0
                
                offerFetch.periodPoints.map( pp => {                    
                    if ( hours === 0 ) { hours = pp.durationInMinutes / 60 }
                    if ( pp.pointServices ) {
                        pp.pointServices.map( ps => {
                                extraServices = {
                                    ...extraServices,
                                    [`service_${ps.id}`]: {
                                        ...extraServices[`service_${ps.id}`],
                                        service: ps.extraService ? ps.extraService.name : '',
                                        days: extraServices[`service_${ps.id}`] ? [...extraServices[`service_${ps.id}`].days,pp.day] : [pp.day]
                                    }
                                }
                        })
                    }

                })

                setOffer({
                    id: offerFetch.id,
                    createdAt: new Date(offerFetch.timestapableCreated),
                    startFromDate: offerFetch.startFromDate,
                    endDate: offerFetch.endDate,
                    client: {
                        name: offerFetch.client[0].name,
                        email: offerFetch.client[0].email,
                        phoneNumber: offerFetch.client[0].phoneNumber,
                        bankName: offerFetch.client[0].bankName,
                        bankAccount: offerFetch.client[0].bankAccount
                    },
                    comercial: offerFetch.comercial,
                    offerBuilding: {
                        roomNumber: offerFetch.offerBuilding.roomNumber,
                        bathRoomNumber: offerFetch.offerBuilding.bathRoomNumber,
                        kitchenNumber: offerFetch.offerBuilding.kitchenNumber,
                        livingNumber: offerFetch.offerBuilding.livingNumber
                    },
                    period: period_detail(offerFetch.period),
                    periodPoints: offerFetch.periodPoints,
                    extraServices: Object.values(extraServices),
                    serviceAddress: `${offerFetch.serviceAddress.country.name}, ${offerFetch.serviceAddress.county.name}, ${offerFetch.serviceAddress.city ? offerFetch.serviceAddress.city.name : ''} 
                                    ${offerFetch.serviceAddress.street}, ${t('address.floor')}: ${offerFetch.serviceAddress.floor}, ${t('address.phoneNumber')}: ${offerFetch.serviceAddress.phoneNumber}`,
                    hours,
                    pricePerDay: offerFetch.pricePerDay,
                    pricePerService: offerFetch.pricePerService,                    
                    company: offerFetch.company,
                    vouchers: [...offerFetch.vouchers]
                });

            } catch (error) {
                console.log( 'Error en offerFetch request: ', error );
                // setFetching(false)
            }

        })();

    }, [ id ]);

    useEffect(() => {
        if( offer.id !== 0 )
        {
            const startFromDate = moment( offer.startFromDate ).format( 'YYYY-MM-DDThh:mm:ssZ' );
            const endDate = moment( offer.endDate ).format( 'YYYY-MM-DDThh:mm:ssZ' );
            const startHour = offer.periodPoints[0].startHour;
            const endHour = offer.periodPoints[0].endHour;
            const days = Object.values( offer.periodPoints ).map( ({ day }) => day );

            (async () => {
                try {
                    const data = await getOfferFullSchedule(offer.id)                
                    setServiceSchedule( data['hydra:member'] );
                    setFetching(false)
                } catch (error) {
                    console.log( error );
                }
            })();    
        }        

    }, [ offer ]);

    const openDialog = () => setDialog({
        open: true,
        dataAction: id
    })

    const closeDialog = () => setDialog({
        open: false,
        dataAction: -1
    })

    const handleDelete = offer_id => {
        closeDialog()
        // setFetching(true)
        // Offer.deleteOffer(offer_id)
        //     .then(res => {
        //         setFetching(false)
        //         history.push('/offers')
        //     })
        //     .catch(error => console.error(error))
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: 'p-0',
                header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
            }}
            header={
                <div className='flex flex-1 w-full items-center justify-between'>
                    <div className='flex flex-col items-start max-w-full'>
                        <div className='flex items-center max-w-full'>
                            <div className='flex flex-col min-w-0'>
                                <Link to='/offers' style={{textDecoration: 'none',marginBottom: '10px'}}>
                                    <ArrowBack /> {t('general.back')}
                                </Link>
                                <Typography variant='h4'>
                                    { `Actualizar ${t('general.order')} #${offer.id}` }
                                </Typography>
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        {/* pagination */}
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        <Button 
                            style={{marginLeft: '10px'}} 
                            className='whitespace-no-wrap' 
                            color='secondary' 
                            variant='contained'
                            onClick={openDialog}
                        >
                            {t('adminPanel.deleteOffer')}
                        </Button>
                    </div>
                </div>
            }
            content={
                <Box p={2} mb={2}>
                    {
                        fetching && <MessageSnackbar variant='info' message='Loading Data...' />
                    }
                    {
                        !fetching && 
                        <>
                            <Box p={1} mb={1}>
                                <Grid 
                                    container
                                    direction='row'
                                    justify='space-around'
                                    alignItems='center'
                                    spacing={3}
                                >
                                    <Grid item xs={12} align='right'>
                                        <Typography variant='h6' gutterBottom>
                                            {offer.createdAt.toLocaleDateString()}
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={12}>
                                        <Typography variant='h6' gutterBottom>
                                            {t('general.service')}
                                        </Typography>
                                    </Grid>                                
                                </Grid>
                            </Box>

                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon style={{color: '#fff'}} />}
                                    className={classes.accordionSummary}
                                >
                                    <Typography>{t('general.clientInformation')}</Typography>
                                </AccordionSummary>                                
                                <AccordionDetails
                                    style={{
                                        display: 'flex',
                                        flexDirection: 'column'
                                    }}
                                >
                                    <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                        {t('register.inputs.name')}
                                    </Typography>
                                    <Typography variant='body1'>
                                        {offer.client.name}
                                    </Typography>
                                    <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                        {t('address.phoneNumber')}
                                    </Typography>
                                    <Typography variant='body1'>
                                        {offer.client.phoneNumber}
                                    </Typography>
                                    <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                        {t('OfferStep2.email')}
                                    </Typography>
                                    <Typography variant='body1'>
                                        {offer.client.email}
                                    </Typography>                                                
                                    {
                                        ( !!offer.client.bankName ) && 
                                        <>
                                            <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                {t('register.inputs.bankName')}
                                            </Typography>
                                            <Typography variant='body1'>
                                                {offer.client.bankName}
                                            </Typography>
                                        </>
                                    }
                                    {
                                        ( !!offer.client.bankAccount ) &&
                                        <>
                                            <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>
                                                {t('register.inputs.bankAccount')}
                                            </Typography>
                                            <Typography variant='body1'>
                                                {offer.client.bankAccount}
                                            </Typography>
                                        </>
                                    }
                                </AccordionDetails>
                            </Accordion>
                            
                            {
                                //ASK
                                ( !!offer.comercial ) &&
                                <Accordion>
                                    <AccordionSummary
                                        expandIcon={<ExpandMoreIcon style={{color: '#fff'}} />}
                                        className={classes.accordionSummary}
                                    >
                                        <Typography>comercial Details</Typography>
                                    </AccordionSummary>
                                    <AccordionDetails>
                                        Aqui va la informacion del comercio
                                    </AccordionDetails>
                                </Accordion>
                            }

                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon style={{color: '#fff'}} />}
                                    className={classes.accordionSummary}
                                >
                                    <Typography>{t('offerItem.cleaningType')}</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Grid 
                                        container
                                        direction='row'
                                        justify='space-around'
                                        alignItems='center'
                                        spacing={3}
                                    >
                                        <Grid item xs={12}>
                                            <Typography variant='body1' gutterBottom>
                                                {`${offer.offerBuilding.roomNumber} ${t('offerBuilding.room')}`}<br/>
                                                {`${offer.offerBuilding.bathRoomNumber} ${t('offerBuilding.bathroom')}`}<br/>
                                                {`${offer.offerBuilding.kitchenNumber} ${t('offerBuilding.kitchen')}`}<br/>
                                                {`${offer.offerBuilding.livingNumber} ${t('offerBuilding.living')}`}<br/>
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </AccordionDetails>
                            </Accordion>

                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon style={{color: '#fff'}} />}
                                    className={classes.accordionSummary}
                                >
                                    <Typography>{t('offerItem.serviceFrequency')}</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Grid 
                                        container
                                        direction='row'
                                        justify='space-around'
                                        alignItems='center'
                                        spacing={6}
                                    >
                                        <Grid item xs={12}>
                                            <Typography variant='body1' gutterBottom>
                                                { offer.period }
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </AccordionDetails>
                            </Accordion>

                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon style={{color: '#fff'}} />}
                                    className={classes.accordionSummary}
                                >
                                    <Typography>{t('offerItem.serviceDay')}</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Grid 
                                        container
                                        direction='row'
                                        justify='space-around'
                                        alignItems='center'
                                        spacing={3}
                                    >
                                        <Grid item xs={12} md={3} />
                                        <Grid item xs={12} md={3}>
                                            <Typography variant='subtitle1' gutterBottom>
                                                {t('Schedule.startHour')}
                                            </Typography>
                                        </Grid>   
                                        <Grid item xs={12} md={3}>
                                            <Typography variant='subtitle1' gutterBottom>
                                                {t('Schedule.endHour')}
                                            </Typography>
                                        </Grid>                           
                                        <Grid item xs={12} md={3}>
                                            <Typography variant='subtitle1' gutterBottom>
                                                {t('offerItem.serviceHours')}
                                            </Typography>
                                        </Grid>    
                                        {
                                            ( !!offer.periodPoints.length ) &&
                                            offer.periodPoints.map( pp => 
                                                <>
                                                    <Grid item xs={12} md={3}>
                                                        <Typography variant='body1' gutterBottom>
                                                            { t(`Schedule.days.${pp.day.toLowerCase()}`) }
                                                        </Typography>
                                                    </Grid>
                                                    <Grid item xs={12} md={3}>
                                                        <Typography variant='body1' gutterBottom>
                                                            { pp.startHour }
                                                        </Typography>
                                                    </Grid>
                                                    <Grid item xs={12} md={3}>
                                                        <Typography variant='body1' gutterBottom>
                                                            { pp.endHour }
                                                        </Typography>
                                                    </Grid>
                                                    <Grid item xs={12} md={3}>
                                                        <Typography variant='body1' gutterBottom>
                                                            { `${pp.durationInMinutes / 60} ${t('Schedule.hours')}` }
                                                        </Typography>
                                                    </Grid>
                                                </>
                                            )                                
                                        }
                                    </Grid>
                                </AccordionDetails>
                            </Accordion>

                            <Accordion >
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon style={{color: '#fff'}} />}
                                    className={classes.accordionSummary}
                                >
                                    <Typography>{t('general.serviceDaysList')}</Typography>
                                </AccordionSummary>                                
                                <AccordionDetails>
                                    <Grid
                                        container
                                        direction='row'
                                        justify='space-around'
                                        alignItems='center'
                                        spacing={3}
                                    >
                                        <Grid item md={4} sm={12} />
                                        <Grid item md={2} sm={12} >
                                            <Typography variant='subtitle1' gutterBottom>
                                                { t( 'general.clientStatus' ) }
                                            </Typography>
                                        </Grid>
                                        <Grid item md={2} sm={12} >
                                            <Typography variant='subtitle1' gutterBottom>
                                                { t( 'general.workerStatus' ) }
                                            </Typography>
                                        </Grid>                            
                                        <Grid item md={4} sm={12} >
                                            <Typography variant='subtitle1' gutterBottom>
                                                { t( 'general.payment' ) }
                                            </Typography>
                                        </Grid>      
                                        {
                                            Object.values( serviceSchedule ).map( ({ dateTimeStart, id, clientOfferStatus, workerOfferStatus, paymentStatus }) => (
                                                <OfferStatusAdmin dateTimeStart={dateTimeStart} id={id} clientOfferStatus={clientOfferStatus} workerOfferStatus={workerOfferStatus} paymentStatus={paymentStatus} update={true} />                            
                                            ))
                                        }
                                    </Grid>                                        
                                </AccordionDetails>
                            </Accordion>

                            {
                                //ASK
                                ( !!offer.extraServices.length ) &&
                                offer.extraServices.map(service =>
                                    <Accordion key={ offer.extraServices.indexOf( service ) }>
                                        <AccordionSummary
                                            expandIcon={<ExpandMoreIcon style={{color: '#fff'}} />}
                                            className={classes.accordionSummary}
                                        >
                                            <Typography>{service.service}</Typography>
                                        </AccordionSummary>
                                        <AccordionDetails>
                                            <Grid 
                                                container
                                                direction='row'
                                                justify='space-around'
                                                alignItems='center'
                                                spacing={3}
                                            >
                                                <Grid item xs={12}>
                                                    {
                                                        ( !!service.days.length ) &&
                                                        service.days.map( day =>
                                                            <Typography key={service.days.indexOf(day)} variant='body1' gutterBottom>
                                                                { t(`Schedule.days.${day.toLowerCase()}`) }
                                                            </Typography>
                                                        )                                                    
                                                    }
                                                </Grid>
                                            </Grid>
                                        </AccordionDetails>
                                    </Accordion>
                                )
                            }

                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon style={{color: '#fff'}} />}
                                    className={classes.accordionSummary}
                                >
                                    <Typography>{t('general.totalDuration')}</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Grid 
                                        container
                                        direction='row'
                                        justify='space-around'
                                        alignItems='center'
                                        spacing={3}
                                    >
                                        <Grid item xs={12} md={3}>
                                            <Typography variant='subtitle1' gutterBottom>
                                                {t('offerItem.serviceTotalHours')}
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={12} md={9}>
                                            <Typography variant='body1' gutterBottom>
                                                { `${ offer.hours * serviceSchedule.length } ${t('Schedule.hours')}` }
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={12} md={3}>
                                            <Typography variant='subtitle1' gutterBottom>
                                                {t('offerItem.serviceTotalDays')}
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={12} md={9}>
                                            <Typography variant='body1' gutterBottom>
                                                { `${ serviceSchedule.length } ${t('general.days')}` }
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </AccordionDetails>
                            </Accordion>                            

                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon style={{color: '#fff'}} />}
                                    className={classes.accordionSummary}
                                >
                                    <Typography>{t('OfferStep2.serviceAddress')}</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Grid 
                                        container
                                        direction='row'
                                        justify='space-around'
                                        alignItems='center'
                                        spacing={3}
                                    >
                                        <Grid item xs={12}>
                                            <Typography variant='body1' gutterBottom>
                                                { offer.serviceAddress }
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </AccordionDetails>
                            </Accordion>

                            {
                                //ASK
                                ( !!offer.company ) &&
                                <Accordion>
                                    <AccordionSummary
                                        expandIcon={<ExpandMoreIcon style={{color: '#fff'}} />}
                                        className={classes.accordionSummary}
                                    >
                                        <Typography>{t('offerItem.companyData')}</Typography>
                                    </AccordionSummary>
                                    <AccordionDetails>
                                        <Grid 
                                            container
                                            direction='row'
                                            justify='space-around'
                                            alignItems='center'
                                            spacing={3}
                                        >
                                            <Grid item xs={12} md={4}>
                                                <Typography variant='subtitle2' gutterBottom>
                                                    {t('address.fullName')}
                                                </Typography>
                                                <Typography variant='body1' gutterBottom>
                                                    {offer.company.name}
                                                </Typography>                                                
                                            </Grid>
                                            <Grid item xs={12} md={4}>
                                                <Typography variant='subtitle2' gutterBottom>
                                                    {t('register.inputs.bankName')}
                                                </Typography>
                                                <Typography variant='body1' gutterBottom>
                                                    {offer.company.bankName}
                                                </Typography>                                                
                                            </Grid>
                                            <Grid item xs={12} md={4}>
                                                <Typography variant='subtitle2' gutterBottom>
                                                    {t('register.inputs.bankAccount')}
                                                </Typography>
                                                <Typography variant='body1' gutterBottom>
                                                    {offer.company.bankAccount}
                                                </Typography>                                                
                                            </Grid>
                                            <Grid item xs={12} md={4}>
                                                <Typography variant='subtitle2' gutterBottom>
                                                    {t('address.phoneNumber')}
                                                </Typography>
                                                <Typography variant='body1' gutterBottom>
                                                    {offer.company.regNumber}
                                                </Typography>                                                
                                            </Grid>
                                            <Grid item xs={12} md={4}>
                                                <Typography variant='subtitle2' gutterBottom>
                                                    {t('company.swift')}
                                                </Typography>
                                                <Typography variant='body1' gutterBottom>
                                                    {offer.company.swift}
                                                </Typography>                                                
                                            </Grid>
                                            <Grid item xs={12} md={4}>
                                                <Typography variant='subtitle2' gutterBottom>
                                                    {t('company.cif')}
                                                </Typography>
                                                <Typography variant='body1' gutterBottom>
                                                    {offer.company.cif}
                                                </Typography>                                                
                                            </Grid>
                                            <Grid item xs={12} md={12}>
                                                <Typography variant='subtitle2' gutterBottom>
                                                    {t('offerItem.companyAddress')}
                                                </Typography>
                                                <Typography variant='body1' gutterBottom>
                                                    {`${offer.company.address.country.name}, ${offer.company.address.county.name}, ${offer.company.address.city ? offer.company.address.city.name : ''} 
                                                        ${offer.company.address.street}, ${t('address.floor')}: ${offer.company.address.floor}, ${t('address.phoneNumber')}: ${offer.company.address.phoneNumber}`}
                                                </Typography>                                                
                                            </Grid>
                                        </Grid>
                                    </AccordionDetails>
                                </Accordion>
                            }

                            {
                                //ASK
                                ( !!offer.billingAddress ) &&
                                <Accordion>
                                    <AccordionSummary
                                        expandIcon={<ExpandMoreIcon style={{color: '#fff'}} />}
                                        className={classes.accordionSummary}
                                    >
                                        <Typography>{t('OfferStep2.billingAddress')}</Typography>
                                    </AccordionSummary>
                                    <AccordionDetails>
                                        <Grid 
                                            container
                                            direction='row'
                                            justify='space-around'
                                            alignItems='center'
                                            spacing={3}
                                        >
                                            <Grid item xs={12} md={12} sm={12}>
                                                <Typography variant='body1' gutterBottom>
                                                    {offer.billingAddress}
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                    </AccordionDetails>
                                </Accordion>
                            }

                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon style={{color: '#fff'}} />}
                                    className={classes.accordionSummary}
                                >
                                    <Typography>{t('general.total')}</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Grid 
                                        container
                                        direction='row'
                                        justify='space-around'
                                        alignItems='center'
                                        spacing={3}
                                    >
                                    {
                                        (offer.vouchers.length > 0) &&
                                        <>                                        
                                            <Grid item xs={12} md={3}>
                                                <Typography variant='h7' gutterBottom>
                                                    {t('general.subTotal')}
                                                </Typography>
                                            </Grid>
                                            <Grid item xs={12} md={9}>
                                                <Typography variant='subtitle1' gutterBottom>
                                                    {  
                                                        (offer.vouchers[0].type === 'percent') ?
                                                        (offer.pricePerService * 100)  / (100 - offer.vouchers[0].discount) :
                                                        (offer.pricePerService + offer.vouchers[0].discount)
                                                    } &euro;
                                                </Typography>
                                            </Grid>

                                            <Grid item xs={12} md={3}>
                                                <Typography variant='h7' gutterBottom>
                                                    {t('voucher.code')}
                                                </Typography>
                                            </Grid>
                                            <Grid item xs={12} md={9}>
                                                <Typography variant='subtitle2' gutterBottom>
                                                    { offer.vouchers[0].code }
                                                </Typography>
                                            </Grid>

                                            <Grid item xs={12} md={3}>
                                                <Typography variant='h7' gutterBottom>
                                                    {t('general.vouchersValue')}
                                                </Typography>
                                            </Grid>
                                            <Grid item xs={12} md={9}>
                                                <Typography variant='subtitle1' gutterBottom>
                                                    - {
                                                        (offer.vouchers[0].type === 'percent') ?
                                                        ((offer.pricePerService)  / (100 - offer.vouchers[0].discount)) * offer.vouchers[0].discount :
                                                        (offer.vouchers[0].discount)
                                                        
                                                    } &euro;
                                                </Typography>
                                            </Grid>
                                        </>
                                    }
                                        <Grid item xs={12} md={3}>
                                            <Typography variant='subtitle2' gutterBottom>
                                                { t('general.totalPerDay') }
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={12} md={9}>
                                            <Typography variant='subtitle1' gutterBottom>
                                                { offer.pricePerDay } &euro;
                                            </Typography>
                                        </Grid>

                                        <Grid item xs={12} md={3}>
                                            <Typography variant='subtitle2' gutterBottom>
                                                { t('general.totalPerService') }
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={12} md={9}>
                                            <Typography variant='subtitle1' gutterBottom>
                                                { offer.pricePerService } &euro;
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </AccordionDetails>
                            </Accordion>

                            <GenericPrompt 
                                title='Delete Offer'
                                message='Are you sure?'
                                close={closeDialog}
                                action={handleDelete}
                                data={dialog}
                            />
                        </>
                    } 
                </Box>
            }
            innerScroll
        >
        </FusePageCarded>
    )
}

export default Update;