import React , { useState , useEffect } from 'react'
import { Link } from 'react-router-dom'
import GenericTable from '../utils/GenericTable'
import {default as FusePageCarded} from '@fuse/core/FusePageCarded'

import * as Offer from '../../utils/Offer'
import formatOffer from './helpers/formatOffer'

import MessageSnackbar from '../utils/MessageSnackbar'
import GenericPrompt from '../utils/GenericPrompt'

import { useTranslation } from 'react-i18next'

import {
    Button,
    Typography,
} from '@material-ui/core'
import PaginationControlled from '../utils/PaginationControlled'

const tableHeaders = [
    'ID',
    'Client',
    'Start Date',
    'Status',
    'Price',
    // 'Deleted',
    'State',
    'Actions'
]

const List = () => {

    const [offers,setOffers] = useState({
        data: [],
        lastPage: '',
        currentPage: '',
    })
    
    const [fetching,setFetching] = useState(true)
    
    const [dialog,setDialog] = useState({ open: false, dataAction: -1 })
    
    const openDialog = (id) => setDialog({ open: true, dataAction: id })
    
    const closeDialog = () => setDialog({ open: false, dataAction: -1 })
    
    const {t} = useTranslation()

    const [page, setPage] = useState(1);

    const getOffers = async() => {
        try {
            const offerCollectionFetch = (!!offers.currentPage) ? await Offer.getOffersCollection(offers.currentPage) : await Offer.getOffersCollection();
            const rows = offerCollectionFetch['hydra:member'].map( offer => formatOffer(offer, openDialog) )
            setOffers({
                data: rows,
                lastPage: (!!offerCollectionFetch['hydra:view']) ? offerCollectionFetch['hydra:view']['hydra:last'] : '',
                currentPage: (!!offerCollectionFetch['hydra:view']) ? offerCollectionFetch['hydra:view']['@id'] : '',
            })
            setFetching( false );
        } catch (error) {
            console.log( 'offerCollectionFetch error: ', error );
            setFetching( false );        
        }
    }

    useEffect(() => {
        setFetching( true );
        getOffers();    
    }, [ page ])

    const handleDelete = (offer_id) => {
        setDialog({ open: false, dataAction: -1 });
        setFetching(true)
        Offer.deleteOffer(offer_id)
            .then((res) => {
                getOffers();
                setFetching(false)
            })
            .catch(error => console.error(error))
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: 'p-0',
                header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
            }}
            header={
                <div className='flex flex-1 w-full items-center justify-between'>
                    <div className='flex flex-col items-start max-w-full'>
                        <div className='flex items-center max-w-full'>
                            <div className='flex flex-col min-w-0'>
                                <Typography variant='h4'>
                                    Offers
                                </Typography>
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        {/* pagination */}
                    </div>
                    <div className='flex flex-row items-end max-w-full'>
                        <Link to='/offers/create' style={{textDecoration: 'none'}}>
                            <Button className='whitespace-no-wrap' variant='contained'>
                                CREATE Offer
                            </Button>
                        </Link>
                    </div>
                </div>
            }
            content={
                <div className='p-16 sm:p-24' style={{maxHeight: '400px'}}>
                    {
                        fetching && <MessageSnackbar variant='info' message='Loading Data...' />
                    }
                    {
                        !fetching &&
                        <>
                            <GenericTable headers={tableHeaders} body={offers.data} />
                            <PaginationControlled
                                count={ offers.lastPage.slice(-1) }
                                page={page}
                                onPageChange={setPage}
                                handleUrlChange={setOffers}
                            />  
                        </>
                    }    
                    <GenericPrompt 
                        title={t('adminPanel.deleteOffer')}
                        message={t('general.areYouSureToDoThis')}
                        close={closeDialog}
                        action={handleDelete}
                        data={dialog}
                    />               
                </div>
            }
            innerScroll
        >
        </FusePageCarded>
    )
}

export default List