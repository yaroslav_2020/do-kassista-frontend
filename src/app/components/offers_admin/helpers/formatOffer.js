import moment from 'moment';

const formatOffer = ( {id, client, startFromDate, status, pricePerService, deleted}, openDialog ) => [
    {
        link: true,
        path: '/login',
        message: id
    },
    {
        link: false,
        message: client[0].name
    },
    {
        link: false,
        message: moment( startFromDate ).format('DD-MM-YYYY')
    },
    {
        link: false,
        message: status,
        color: 'orange'
    },
    {
        link: false,
        message: pricePerService
    },
    {
        link: false,
        message: deleted ? 'Eliminado' : 'Activo',
        color: deleted ? 'red' : 'green'
    },
    {
        action: true,
        icons: [
            {
                icon: 'search',
                path: `/offers/${id}`
            },
            {
                icon: 'edit',
                path: `/offers/update/${id}`
            },
            {
                icon: 'delete',
                onClick: openDialog,
                id: id
            }
        ]
    }
];

export default formatOffer;