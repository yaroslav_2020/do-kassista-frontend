import moment from 'moment';

const formatPayment = ( {id, status, date, amount, paymentUrl, currency, description, lastTransaction, cardToken, errorMessage} ) => [
    {
        link: false,
        // path: '/login',
        message: id
    },
    {
        link: false,
        message: status
    },
    {
        link: false,
        message: moment( date ).format('DD-MM-YYYY')
    },
    {
        link: false,
        message: amount,
        // color: 'orange'
    },
    {
        link: false,
        message: currency
    },    
    {
        link: false,
        message: description
    },   
    {
        link: false,
        message: lastTransaction
    }, 
    {
        link: false,
        message: cardToken
    },
    {
        link: false,
        message: paymentUrl
    }, 
    {
        link: false,
        message: errorMessage
    },   

    // {
    //     link: false,
    //     message: deleted ? 'Eliminado' : 'Activo',
    //     color: deleted ? 'red' : 'green'
    // },
];

export default formatPayment;