import React,{ useState , useEffect} from 'react'
import AppBarOptions from '@trd/components/utils/AppBarOptions'
import { useTranslation } from 'react-i18next'
import { connect } from 'react-redux'
import {default as FusePageCarded} from '@fuse/core/FusePageCarded'
import { Link , useHistory } from 'react-router-dom'

import * as OfferActions from '../../store/actions/OfferActions'

import OfferStep1 from '@trd/components/Offer/OfferStep1'
import OfferStep2 from '@trd/components/Offer/OfferStep2'
import OfferStep3 from '@trd/components/Offer/OfferStep3'

import {
    Grid,
    Typography,
    Box
} from '@material-ui/core'

import {
    ArrowBack, TrafficOutlined
} from '@material-ui/icons'

import AssignmentIcon from '@material-ui/icons/Assignment'
import HouseIcon from '@material-ui/icons/House'
import PaymentIcon from '@material-ui/icons/Payment'

const OfferDetails = props => {

    const {t,i18n} = useTranslation()

    useEffect(() => {
        props.getExtraServices()
    },[])

    const options = [
        {
            icon: <AssignmentIcon />,
            label: t('OfferDetails.step1')
        },
        {
            icon: <HouseIcon />,
            label: t('OfferDetails.step2')
        },
        {
            icon: <PaymentIcon />,
            label: t('OfferDetails.step3')
        },
    ]

    const [tabValue,setTabValue] = useState(0)

    const handleChange = (event,value) => {
        setTabValue(value)
    }

    const TabPanel = props => {
        const { children, value, index, ...other } = props

        return (
            <Typography
                component="div"
                role="tabpanel"
                hidden={value !== index}
                id={`simple-tabpanel-${index}`}
                aria-labelledby={`simple-tab-${index}`}
                {...other}
            >
                <Box p={3}>{children}</Box>
            </Typography>
        )
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: 'p-0',
                header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
            }}
            header={
                <div className='flex flex-1 w-full items-center justify-between'>
                    <div className='flex flex-col items-start max-w-full'>
                        <div className='flex items-center max-w-full'>
                            <div className='flex flex-col min-w-0'>
                                <Link to='/offers' style={{textDecoration: 'none',marginBottom: '10px'}}>
                                    <ArrowBack /> BACK
                                </Link>
                                <Typography variant='h4'>
                                    Create Offers
                                </Typography>
                            </div>
                        </div>
                    </div>
                </div>
            }
            content={
                <Grid item xs={12} md={12} sm={6}>
                    <AppBarOptions options={options}
                                onChange={handleChange} 
                                value={tabValue} />
                    <TabPanel value={tabValue} index={0}>
                        <OfferStep1 setTabPanel={setTabValue} />
                    </TabPanel>
                    <TabPanel value={tabValue} index={1}>
                        <OfferStep2 setTabPanel={setTabValue} admin={true} />
                    </TabPanel>
                    <TabPanel value={tabValue} index={2}>
                        <OfferStep3 admin={true} />
                    </TabPanel>                
                </Grid>
            }
            innerScroll
        >
        </FusePageCarded>
    )
}

const mapDispatch = {
    ...OfferActions
}

export default connect(null,mapDispatch)(OfferDetails)