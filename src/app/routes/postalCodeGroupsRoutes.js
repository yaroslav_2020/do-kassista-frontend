import React from 'react'
import { Route } from 'react-router-dom'
import List from '../components/postalCodeGroup/List.js'
import Item from '../components/postalCodeGroup/Item'
import Create from '../components/postalCodeGroup/Create'
import Update from '../components/postalCodeGroup/Update.js'

export default [
    <Route path='/postal_code_groups/create' exact component={Create} key='Create' />,
    <Route path='/postal_code_groups/update/:id' exact component={Update} key='Create' />,
    <Route path='/postal_code_groups/:id' exact component={Item} key='Item' />,
    <Route path='/postal_code_groups/' exact strict component={List} key='List' />,
]