import React from 'react'
import { Route } from 'react-router-dom'
import List from '../components/voucher/List'
import Item from '../components/voucher/Item'
import Create from '../components/voucher/Create'
import Update from '../components/voucher/Update'

export default [
    <Route path='/vouchers/create' exact component={Create} key='Create' />,
    <Route path='/vouchers/update/:id' exact component={Update} key='Update' />,
    <Route path='/vouchers/:id' exact component={Item} key='Item' />,
    <Route path='/vouchers/' exact strict component={List} key='List' />,
]