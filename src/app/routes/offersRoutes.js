import React from 'react'
import { Route } from 'react-router-dom'
import Item from 'app/components/offers_admin/Item'
import Create from 'app/components/offers_admin/Create'
import Update from 'app/components/offers_admin/Update'
import List from 'app/components/offers_admin/List'

export default [
    <Route path='/offers/create' exact component={Create} key='Create' />,
    <Route path='/offers/update/:id' exact component={Update} key='Update' />,
    <Route path='/offers/:id' exact component={Item} key='Item' />,
    <Route path='/offers/' exact strict component={List} key='List' />,
]
// export default [
//     <Route path='/offers/' exact strict component={List} key='List' />,
//     <Route path='/offers/create' exact component={Create} key='Create' />,
//     <Route path='/offers/:id' exact component={Item} key='Item' />,
//     <Route path='/offers/update/:id' exact component={Update} key='Update' />,
// ]