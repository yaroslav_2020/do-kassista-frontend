import React from 'react'
import { Route } from 'react-router-dom'
import List from '../components/postalCode/List.js'
import Item from '../components/postalCode/Item'
import Create from '../components/postalCode/Create'
import Update from '../components/postalCode/Update'

export default [
    <Route path='/postal_codes/create' exact component={Create} key='Create' />,
    <Route path='/postal_codes/update/:id' exact component={Update} key='Create' />,
    <Route path='/postal_codes/:id' exact component={Item} key='Item' />,
    <Route path='/postal_codes/' exact strict component={List} key='List' />,
]