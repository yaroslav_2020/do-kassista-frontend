import React from 'react'
import { Route } from 'react-router-dom'
import List from '../components/extraService/List'
import Item from '../components/extraService/Item'
import Create from '../components/extraService/Create'
import Update from '../components/extraService/Update'

export default [
    <Route path='/extra_services/create' exact component={Create} key='Create' />,
    <Route path='/extra_services/update/:id' exact component={Update} key='Update' />,
    <Route path='/extra_services/:id' exact component={Item} key='Item' />,
    <Route path='/extra_services/' exact strict component={List} key='List' />,
]