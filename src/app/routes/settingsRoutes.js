import React from 'react'
import { Route } from 'react-router-dom'
import List from '../components/settings/List.js'
import Item from '../components/settings/Item'
import Create from '../components/settings/Create'
import Update from '../components/settings/Update'

export default [
    <Route path='/settings/create' exact component={Create} key='Create' />,
    <Route path='/settings/update/:id' exact component={Update} key='Update' />,
    <Route path='/settings/:id' exact component={Item} key='Item' />,
    <Route path='/settings/' exact strict component={List} key='List' />,
]