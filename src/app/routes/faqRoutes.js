import React from 'react'
import { Route } from 'react-router-dom'
import List from '../components/faq/List'
import Item from '../components/faq/Item'
import Create from '../components/faq/Create'
import Update from '../components/faq/Update'

export default [
    <Route path='/faq/create' exact component={Create} key='Create' />,
    <Route path='/faq/update/:id' exact component={Update} key='Update' />,
    <Route path='/faq/:id' exact component={Item} key='Item' />,
    <Route path='/faq/' exact strict component={List} key='List' />,
]