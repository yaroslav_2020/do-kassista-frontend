import React from 'react'
import { Route } from 'react-router-dom'
import List from '../components/user/List'
import Item from '../components/user/Item'
import Create from '../components/user/Create'
import Update from '../components/user/Update'

export default [
    <Route path='/users/create' exact component={Create} key='Create' />,
    <Route path='/users/update/:id' exact component={Update} key='Update' />,
    <Route path='/users/:id' exact component={Item} key='Item' />,
    <Route path='/users/' exact strict component={List} key='List' />,
]