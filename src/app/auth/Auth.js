import React, { Component, Fragment } from 'react'
import jwtService from 'app/services/jwtService';
import auth0Service from 'app/services/auth0Service';
import FuseSplashScreen from '@fuse/core/FuseSplashScreen';
import { connect } from 'react-redux';
import * as userSliceActions from './store/userSlice';

class Auth extends Component {

    state = {
        waitAuthCheck: true
    }

    componentDidMount(){
        return Promise.all([
            this.jwtCheck()
        ]).then(() => {
            this.setState({waitAuthCheck: false})
        })
    }

    jwtCheck = () => new Promise(resolve => {

        jwtService.on('onAutoLogin', () => {
            /**
             * Sign in and retrieve user data from Api
             */
            jwtService.signInWithToken()
                .then(user => {
                    this.props.setUserData(user);
                    resolve();
                })
                .catch(error => {
                    resolve();
                })
        });

        jwtService.on('onAutoLogout', (message) => {
            if (message)
				console.log(message)

            this.props.logout();

            resolve();
        });

        jwtService.on('onNoAccessToken', () => {

            resolve();
        });

        jwtService.init();

        return Promise.resolve();
    })

    auth0Check = () => new Promise(resolve => {
        auth0Service.init(
            success => {
                if (!success)
					resolve()
            }
        );

        if(auth0Service.isAuthenticated()){
            /**
             * Retrieve user data from Auth0
             */
            auth0Service.getUserData().then(tokenData => {
                this.props.setUserDataAuth0(tokenData);
                resolve();
            })
        }else{
            resolve();
        }

        return Promise.resolve();
    })

    render(){
        return this.state.waitAuthCheck ? <FuseSplashScreen/> : <React.Fragment children={this.props.children}/>;
    }
}

const mapDispatch = {
	...userSliceActions
}

export default connect(null,mapDispatch)(Auth)