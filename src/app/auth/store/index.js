import { combineReducers } from '@reduxjs/toolkit';
import {default as login} from './reducers/loginReducer'
import user from './userSlice';

const authReducers = combineReducers({
	user,
	login
});

export default authReducers;
