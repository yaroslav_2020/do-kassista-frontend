import { LoginTypes } from '../types/loginTypes';
import jwtService from 'app/services/jwtService';
import { setUserData } from '../userSlice';
import { showMessage } from 'app/store/fuse/messageSlice';
import { ENTRYPOINT } from 'config/entrypoint';
import axios from 'axios';

export const Fetching = () => dispatch => {
    dispatch({
        type: LoginTypes.FETCHING
    });
};

export const SignIn = loginData => dispatch => {
    const { email, password } = loginData;
    console.log(email, password);
    //User SignIn
    return jwtService
        .signInWithEmailAndPassword(email, password)
        .then(user => {
            //set user data in redux state
            dispatch(setUserData(user));
            //update login state
            dispatch({
                type: LoginTypes.SUCCESS
            });

            return user;
        })
        .catch(error => {
            dispatch(
                showMessage({
                    message: error,
                    autoHideDuration: 3000,
                    anchorOrigin: {
                        vertical: 'top',
                        horizontal: 'center'
                    },
                    variant: 'error'
                })
            );
            //notify if an error occurred
            dispatch({
                type: LoginTypes.ERROR,
                payload: error
            });

            return error;
        });
};

export const SignUp = data => async dispatch => {
    console.log(JSON.stringify(data));
    try {
        const response = await axios({
            method: 'POST',
            url: `${ENTRYPOINT}/openapi/security/register/`,
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(data)
        });

        let status = response.status;

        if (response.status === 'ok') {
            return jwtService
                .signInWithEmailAndPassword(data.email, data.password)
                .then(user => {
                    //set user data in redux state
                    dispatch(setUserData(user));
                    //update login state
                    dispatch({
                        type: LoginTypes.SUCCESS
                    });

                    return user;
                })
                .catch(error => {
                    dispatch(
                        showMessage({
                            message: error,
                            autoHideDuration: 3000,
                            anchorOrigin: {
                                vertical: 'top',
                                horizontal: 'center'
                            },
                            variant: 'error'
                        })
                    );
                    //notify if an error occurred
                    dispatch({
                        type: LoginTypes.ERROR,
                        payload: error
                    });
                    return error;
                });
        } else {
            return response.data;
        }
    } catch (err) {
        console.log(err);
        // console.log(status)
    }
};