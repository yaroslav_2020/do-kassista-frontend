import {LoginTypes} from '../types/loginTypes'

const InitialState = {
    success: false,
    error : '',
    fetching: false,
}

export default (state = InitialState, action) => {
    switch(action.type){
        case LoginTypes.SUCCESS:
            return {
                ...InitialState,
                success: true,
                fetching: false
            }
        case LoginTypes.ERROR:
            return {
                ...InitialState,
                error: action.payload,
                fetching: false
            }
        case LoginTypes.FETCHING:
            return {
                ...InitialState,
                fetching: true
            }
        default: 
            return state
    }
}