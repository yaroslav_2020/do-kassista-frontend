import {default as userRoles} from './store/types/UserRoles'

const {
    // ROLE_USER,
    // ROLE_COMERCIAL,
    // ROLE_ADMIN,
    // ROLE_SUPER_ADMIN
    ROLE_USER,
    ROLE_CLIENT,
    ROLE_INDEPENDENT_WORKER,
    ROLE_EMPLOYEE_WORKER,
    ROLE_ADMIN,
} = userRoles

const authRoles = {
    admin    : [ROLE_ADMIN],
    staff    : [ROLE_EMPLOYEE_WORKER, ROLE_INDEPENDENT_WORKER],
    user     : [ROLE_CLIENT, ROLE_USER],
    onlyGuest: [],

    // admin    : ['admin'],
    // staff    : ['admin', 'staff'],
    // user     : ['admin', 'staff', 'user'],

    // ROLE_SUPER_ADMIN: [ROLE_SUPER_ADMIN],
    // ROLE_ADMIN: [ROLE_ADMIN, ROLE_SUPER_ADMIN],
    // ROLE_COMMERCIAL: [ROLE_COMERCIAL, ROLE_ADMIN, ROLE_SUPER_ADMIN],
    // ROLE_USER: [ROLE_USER, ROLE_COMERCIAL, ROLE_ADMIN, ROLE_SUPER_ADMIN]
};

export default authRoles;
