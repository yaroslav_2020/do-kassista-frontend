import { CompanyTypes } from '../types/CompanyTypes'

const InitialState = {
    iri: '',
    name: '',
    bankName: '',
    bankAccount: '',
    swift: '',
    cif: '',
    regNumber: '',
    address: '',
}

export default (state = InitialState,action) => {
    switch(action.type){
        case CompanyTypes.SET_COMPANY_TO_UPDATE:
            return {
                ...state,
                ...action.payload
            }
        case CompanyTypes.SET_COMPANY_FIELD:
            return {
                ...state,
                ...action.payload
            }
        default:
            return InitialState
    }
}