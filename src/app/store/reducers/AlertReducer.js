import { AlertTypes } from '../types/AlertTypes'

const InitialState = {
    show: false
}

export default (state = InitialState,action) => {
    switch(action.type){
        case AlertTypes.SHOW_ALERT:
            return {
                show: true
            }
        case AlertTypes.HIDE_ALERT:
            return {
                show: false
            }
        default:
            return InitialState
    }
}