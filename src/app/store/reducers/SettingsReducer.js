import { SettingsTypes } from '../types/SettingsTypes'

const InitialState = {
    standardPrice: 0,
    room_cleaning_duration_time: 0,
    bathroom_cleaning_duration_time: 0,
    kitchen_cleaning_duration_time: 0,
    living_cleaning_duration_time: 0,
    min_hours: 0,
    max_hours: 0,
    max_price_per_hour: 0,
    min_price_per_hour: 0
}

export default (state = InitialState, action) => {
    switch(action.type){
        case SettingsTypes.GET_GENERAL_SETTINGS:
            return {
                ...state,
                ...action.payload
            }
        default:
            return state
    }
}