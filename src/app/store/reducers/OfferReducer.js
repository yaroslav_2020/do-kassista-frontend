import { OfferTypes } from '../types/OfferTypes'

const InitialState = {
  success: false,
  error : '',
  fetching: false,

  /* DATA FROM STEP 1 */
  startFromDate: '',
  endDate: '',
  startHour: '',
  endHour: '',
  hoursNeeded: 0,
  durationInHours: 0,
  period: 'several_times_a_week',
  otherDetails: '',
  pricePerDay: 0,
  pricePerService: 0,
  pricePerHour: 1,
  pet: 'PET_OTHER',
  otherPet: '',
  cardHolderName: '',
  cleanningTools: 'CLEANNING_TOOLS_YES',
  offerBuilding: {
      roomNumber: 0,
      bathRoomNumber: 0,
      kitchenNumber: 0,
      livingNumber: 0,
      accessType: 'i_am_at_home',
      accessDescription: ''
  },
  periodPoints: {
    monday: {
      checked: false,
      day: 'MONDAY',
      startHour: '',
      endHour: '',
      durationInMinutes: 180,
      label: 'Lunes',
    },
    tuesday: {
      checked: false,
      day: 'TUESDAY',
      startHour: '',
      endHour: '',
      durationInMinutes: 180,
      label: 'Martes',
    },
    wednesday: {
      checked: false,
      day: 'WEDNESDAY',
      startHour: '',
      endHour: '',
      durationInMinutes: 180,
      label: 'Miercoles',
    },
    thursday: {
      checked: false,
      day: 'THURSDAY',
      startHour: '',
      endHour: '',
      durationInMinutes: 180,
      label: 'Jueves',
    },
    friday: {
      checked: false,
      day: 'FRIDAY',
      startHour: '',
      endHour: '',
      durationInMinutes: 180,
      label: 'Viernes',
    },
    saturday: {
      checked: false,
      day: 'SATURDAY',
      startHour: '',
      endHour: '',
      durationInMinutes: 180,
      label: 'Sabado',
    },
    sunday: {
      checked: false,
      day: 'SUNDAY',
      startHour: '',
      endHour: '',
      durationInMinutes: 180,
      label: 'Domingo',
    },
  },
  offerFullSchedule: {},
  pointServices: {},
  voucherApplied: {},
  vouchers: [],   
  /* END STEP 1 */

  /* DATA FROM STEP 2 */
  personalData: {
    name: '',
    phoneNumber: '',
    email: '',
    confirmEmail: ''
  },
  client: '',
  company: '',
  serviceAddress: '',
  billingAddress: '',
  billingType: 'PERSONAL_BILLING',
  privacyPolicy: false,
  /* END STEP 2 */

  /* DATA FROM STEP 3 */
  payment: {
    option: 'stripe',
    name: '',
    card: '',
    expiresIn: {
      month: '',
      year: ''
    },
    securityCode: ''
  }
  /* END STEP 3 */
}

export default (state = InitialState, action) => {
    let newState
  
    switch(action.type){
        case OfferTypes.SAVE_STEP_1:
          newState = {
            ...state,
            ...action.payload
          }
          return newState
        case OfferTypes.GET_EXTRA_SERVICES:
          newState = {
            ...state,
            pointServices: {
              ...action.payload
            }
          }
          return newState
        case OfferTypes.SAVE_PERIOD_POINT:
          newState = {
            ...state,
            periodPoints: {
              ...state.periodPoints,
              ...action.payload
            }
          }
          return newState
        case OfferTypes.SAVE_POINT_SERVICE:
          newState = {
            ...state,
            pointServices: {
              ...state.pointServices,
              ...action.payload
            }            
          }
          return newState
        case OfferTypes.SAVE_EXTRA_INFO:
          newState = {
            ...state,
            offerBuilding: {
              ...state.offerBuilding,
              ...action.payload.offerBuilding
            },
            otherDetails: action.payload.otherDetails            
          }
          return newState
        case OfferTypes.SAVE_STEP_2_PERSONAL_DATA:
          newState = {
            ...state,
            personalData: {
              ...state.personalData,
              ...action.payload
            }
          }
          return newState
        case OfferTypes.SAVE_STEP_2:
          newState = {
            ...state,
            ...action.payload
          }
          return newState
        case OfferTypes.SAVE_STEP_3:
          newState = {
            ...state,
            ...action.payload
          }
          return newState
        case OfferTypes.RESTART:
          return InitialState     
        default: 
          return state
    }
}