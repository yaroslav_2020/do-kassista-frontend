import { FetchTypes } from '../types/FetchTypes'

const InitialState = {
    fetching: false,
    error: false,
    success: false
}

export default (state = InitialState,action) => {
    switch(action.type){
        case FetchTypes.FETCHING:
            return {
                ...state,
                fetching: true,
                error: false,
                success: false
            }
        case FetchTypes.SUCCESS:
            return {
                ...state,
                fetching: false,
                error: false,
                success: true
            }
        case FetchTypes.ERROR:
            return {
                ...state,
                fetching: false,
                error: true,
                success: false
            }
        default:
            return InitialState
    }
}