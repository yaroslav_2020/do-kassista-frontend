import { AddressTypes } from '../types/AddressTypes'

const InitialState = {
    iri: '',
    addressType: 'SERVICE_ADDRESS',
    fullName: '',
    nr: 0,
    phoneNumber: '',
    street: '',
    floor: '',
    postalCode: '',
    customPostCode: '',
    country: '',
    county: '',
    city: ''
}

export default (state = InitialState,action) => {
    let newState

    switch(action.type){
        case AddressTypes.SET_ADDRESS_TO_UPDATE:
            return {
                ...state,
                ...action.payload
            }
        case AddressTypes.SET_ADDRESS_FIELD:
            return {
                ...state,
                ...action.payload
            }
        default:
            return InitialState
    }
}