import { PaymentTypes } from '../types/PaymentTypes'

const InitialState = {
    active: false,
}

export default (state = InitialState, action) => {
    switch(action.type){
        case PaymentTypes.PAY: 
            return {
                active: true
            }
        case PaymentTypes.FINISH: 
            return {
                active: false
            }
        default:
            return state
    }
}