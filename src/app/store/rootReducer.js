import { combineReducers } from '@reduxjs/toolkit';
import auth from 'app/auth/store';
import fuse from './fuse';

import { default as Offer } from './reducers/OfferReducer'
import { default as Address } from './reducers/AddressReducer'
import { default as Company } from './reducers/CompanyReducer'
import { default as Fetch } from './reducers/FetchReducer'
import { default as Payment } from './reducers/PaymentReducer'
import { default as Settings } from './reducers/SettingsReducer'
import { default as Alert } from './reducers/AlertReducer'

const createReducer = asyncReducers =>
	combineReducers({
		auth,
		fuse,
		Offer,
		Address,
		Company,
		Fetch,
		Payment,
		Settings,
		Alert,
		...asyncReducers
	});

export default createReducer;
