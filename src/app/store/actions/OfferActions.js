import { OfferTypes } from '../types/OfferTypes'
import axios from 'axios'
import { ENTRYPOINT } from 'config/entrypoint'

export const SaveStep1 = Step1Data => dispatch => {
    dispatch({
        type: OfferTypes.SAVE_STEP_1,
        payload: Step1Data
    })
}

export const getExtraServices = () => async dispatch => {
    try{
        
        let response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}/api/extra_services`
        })

        let extraServices = response.data['hydra:member']

        let services = await Promise.all(extraServices.filter(service => service.activeStatus).map(service => [
            `service_${service.id}`,
            {
                id: service.id,
                iri: service['@id'],
                name: service.name,
                checked: false,
                days: [],
                hours: 3,
                icon: service.icon,
                value: service.addedValue
            }
        ]))

        dispatch({
            type: OfferTypes.GET_EXTRA_SERVICES,
            payload: Object.fromEntries(services)
        })        

    }catch(error){
        console.error(error)
    }
}

export const SavePeriodPoint = PeriodPoint => dispatch => {
    dispatch({
        type: OfferTypes.SAVE_PERIOD_POINT,
        payload: PeriodPoint
    })
}

export const SavePointService = PointService => dispatch => {
    dispatch({
        type: OfferTypes.SAVE_POINT_SERVICE,
        payload: PointService
    })
}

export const SaveExtraInfo = ExtraInfo => dispatch => {
    dispatch({
        type: OfferTypes.SAVE_EXTRA_INFO,
        payload: ExtraInfo
    })
}

export const SaveStep2PersonalData = Step2PersonalData => dispatch => {
    dispatch({
        type: OfferTypes.SAVE_STEP_2_PERSONAL_DATA,
        payload: Step2PersonalData
    })
}

export const SaveStep2Data = Step2Data => dispatch => {
    dispatch({
        type: OfferTypes.SAVE_STEP_2,
        payload: Step2Data
    })
}

export const SaveStep3 = Step3Data => dispatch => {
    dispatch({
        type: OfferTypes.SAVE_STEP_3,
        payload: Step3Data
    })
}

export const RestartOfferForm = () => dispatch => {
    dispatch({
        type: OfferTypes.RESTART,
    })
}