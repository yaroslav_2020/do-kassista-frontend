import { FetchTypes } from '../types/FetchTypes'

export const fetching = () => dispatch => {
    dispatch({
        type: FetchTypes.FETCHING,
    })
}

export const error = () => dispatch => {
    dispatch({
        type: FetchTypes.ERROR,
    })
}

export const success = () => dispatch => {
    dispatch({
        type: FetchTypes.SUCCESS,
    })
}