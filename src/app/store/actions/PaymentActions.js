import { PaymentTypes } from '../types/PaymentTypes'

export const pay = () => dispatch => {
    dispatch({
        type: PaymentTypes.PAY,
    })
}

export const finish = () => dispatch => {
    dispatch({
        type: PaymentTypes.FINISH,
    })
}