import { AlertTypes } from '../types/AlertTypes'

export const alertShow = () => dispatch => {
    dispatch({
        type: AlertTypes.SHOW_ALERT,
    })
}

export const alertHide = () => dispatch => {
    dispatch({
        type: AlertTypes.HIDE_ALERT,
    })
}