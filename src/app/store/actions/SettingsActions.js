import { SettingsTypes } from '../types/SettingsTypes'
import axios from 'axios'
import { ENTRYPOINT } from 'config/entrypoint'
import { OfferTypes } from '../types/OfferTypes'

export const getGeneralSettings = () => async dispatch => {
    
    try{
        const token = localStorage.getItem('jwt_access_token')

        let response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}/api/settings`,
            headers: {
                'Content-Type': 'application/json',
            }
        })

        let settings = response.data['hydra:member']

        let data = settings.filter(setting => 
                                        (   
                                            setting.name === 'room_cleaning_duration_time' ||
                                            setting.name === 'bathroom_cleaning_duration_time' ||                                       
                                            setting.name === 'kitchen_cleaning_duration_time' ||                                       
                                            setting.name === 'living_cleaning_duration_time' ||
                                            setting.name === 'max_hours' ||
                                            setting.name === 'min_hours' ||
                                            setting.name === 'max_price_per_hour' ||
                                            setting.name === 'min_price_per_hour' ||
                                            setting.name === 'price_per_cleanning_tools' ||                                    
                                            setting.name === 'start_labor_schedule' ||                                    
                                            setting.name === 'end_labor_schedule' ||                                    
                                            setting.name === 'hours_buffer'                                    
                                        )
                                    )
        
        data = data.map(setting => {
            if(
                setting.name === 'start_labor_schedule' ||
                setting.name === 'end_labor_schedule'
            ){
                return [setting.name,setting.value]
            }

            return [setting.name,parseFloat(setting.value)]
        })

        data = Object.fromEntries(data)

        if(!data.room_cleaning_duration_time)
            data.room_cleaning_duration_time = 0
        
        if(!data.bathroom_cleaning_duration_time)
            data.bathroom_cleaning_duration_time = 0

        if(!data.kitchen_cleaning_duration_time)
            data.kitchen_cleaning_duration_time = 0

        if(!data.living_cleaning_duration_time)
            data.living_cleaning_duration_time = 0
        
        if(!data.max_hours)
            data.max_hours = 0
        
        if(!data.min_hours)
            data.min_hours = 0

        if(!data.max_price_per_hour)
            data.max_price_per_hour = 0

        if(!data.min_price_per_hour)
            data.min_price_per_hour = 0
        
        if(!data.price_per_cleanning_tools)
            data.price_per_cleanning_tools = 0
        
        if(!data.start_labor_schedule)
            data.start_labor_schedule = 0
        
        if(!data.end_labor_schedule)
            data.end_labor_schedule = 0

        if(!data.hours_buffer)
            data.hours_buffer = 0

        dispatch({
            type: OfferTypes.SAVE_STEP_1,
            payload: {
                hoursNeeded: data.min_hours
            }
        })

        dispatch({
            type: SettingsTypes.GET_GENERAL_SETTINGS,
            payload: data
        })
    }catch(error){
        console.error(error)
    }
}