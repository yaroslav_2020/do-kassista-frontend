import { CompanyTypes } from '../types/CompanyTypes'
import axios from 'axios'
import { ENTRYPOINT } from 'config/entrypoint'

export const resetCompanyState = () => dispatch => {
    dispatch({
        type: CompanyTypes.RESET_COMPANY_INFO,
        payload: {
            iri: '',
            name: '',
            bankName: '',
            bankAccount: '',
            swift: '',
            cif: '',
            regNumber: '',
            address: '',
        }
    })
}

export const updateCompany = company_iri => async dispatch => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}${company_iri}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        })

        if(response.status === 200){
            dispatch({
                type: CompanyTypes.SET_COMPANY_TO_UPDATE,
                payload: {
                    iri: response.data['@id'],
                    name: response.data.name,
                    bankName: response.data.bankName,
                    bankAccount: response.data.bankAccount,
                    swift: response.data.swift,
                    cif: response.data.cif,
                    regNumber: response.data.regNumber,
                    address: response.data.address ? response.data.address['@id'] : '',
                }
            })

            return 1
        }
        
        return 0
    }catch(error){
        console.error(error)
    }
    
}

export const saveCompanyData = data => async dispatch => {
    dispatch({
        type: CompanyTypes.SET_COMPANY_FIELD,
        payload: data
    })    
}