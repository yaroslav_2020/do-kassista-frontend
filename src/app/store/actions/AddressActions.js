import { AddressTypes } from '../types/AddressTypes'
import axios from 'axios'
import { ENTRYPOINT } from 'config/entrypoint'

export const resetAddressState = () => dispatch => {
    dispatch({
        type: AddressTypes.RESET_ADDRESS_INFO,
        payload: {
            iri: '',
            addressType: 'SERVICE_ADDRESS',
            fullName: '',
            nr: 0,
            phoneNumber: '',
            street: '',
            floor: '',
            postalCode: '',
            customPostCode: '',
            country: '',
            county: '',
            city: ''
        }
    })
}

export const updateAddress = address_iri => async dispatch => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}${address_iri}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        })

        if(response.status === 200){
            dispatch({
                type: AddressTypes.SET_ADDRESS_TO_UPDATE,
                payload: {
                    iri: address_iri,
                    addressType: response.data.addressType,
                    fullName: response.data.fullName,
                    nr: response.data.nr,
                    phoneNumber: response.data.phoneNumber,
                    street: response.data.street,
                    floor: response.data.floor,
                    postalCode: response.data.postalCode !== null ? response.data.postalCode['@id'] : '',
                    customPostCode: response.data.customPostCode !== null ? response.data.customPostCode : '',
                    country: response.data.country['@id'],
                    county: response.data.county['@id'],
                    city: response.data.city !== null ? response.data.city['@id'] : ''
                }
            })

            return 1
        }
        
        return 0
    }catch(error){
        console.error(error)
    }    
}

export const saveAddressData = data => async dispatch => {
    dispatch({
        type: AddressTypes.SET_ADDRESS_FIELD,
        payload: data
    })   
}