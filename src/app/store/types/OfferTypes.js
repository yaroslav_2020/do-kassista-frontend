export const OfferTypes = {
    FETCHING: 'FETCHING',
    SUCCESS: 'SUCCESS',
    ERROR: 'ERROR',
    GET_EXTRA_SERVICES: 'GET_EXTRA_SERVICES',
    SAVE_STEP_1: 'SAVE_STEP_1',
    SAVE_PERIOD_POINT: 'SAVE_PERIOD_POINT',
    SAVE_POINT_SERVICE: 'SAVE_POINT_SERVICE',
    SAVE_EXTRA_INFO: 'SAVE_EXTRA_INFO',
    SAVE_STEP_2_PERSONAL_DATA: 'SAVE_STEP_2_PERSONAL_DATA',
    SAVE_STEP_2: 'SAVE_STEP_2',
    SAVE_STEP_3: 'SAVE_STEP_3',
    RESTART: 'RESTART',
}