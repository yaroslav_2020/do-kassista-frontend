import axios from 'axios'
import { ENTRYPOINT } from '../../config/entrypoint'

export const getExtraServices = async () => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}/api/extra_services`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        })

        return response.data['hydra:member']
    }catch(error){
        console.error(error)
    }
}

export const createExtraService = async data => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'POST',
            url: `${ENTRYPOINT}/api/extra_services`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `bearer ${token}`
            },
            data: JSON.stringify(data)
        })

        return response
    }catch(error){
        console.error(error)
    }
}

export const getExtraServiceItem = async extra_service_id => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}/api/extra_services/${extra_service_id}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        })

        return response
    }catch(error){
        console.error(error)
    }
}

export const updateExtraService = async (extra_service_id,data) => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'PUT',
            url: `${ENTRYPOINT}/api/extra_services/${extra_service_id}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `bearer ${token}`
            },
            data: JSON.stringify(data)
        })

        return response
    }catch(error){
        console.error(error)
    }
}

export const deleteExtraService = async extra_service_id => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'DELETE',
            url: `${ENTRYPOINT}/api/extra_services/${extra_service_id}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `bearer ${token}`
            }
        })

        return response
    }catch(error){
        console.error(error)
    }
}