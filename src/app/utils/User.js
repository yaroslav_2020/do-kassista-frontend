import axios from 'axios'
import { ENTRYPOINT } from '../../config/entrypoint'

export const getUserInfo = async user_iri => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}${user_iri}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        })
        
        return response.data
    }catch(error){
        console.error(error)
    }
}

export const Update = async data => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'PUT',
            url: `${ENTRYPOINT}${data.user_iri}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            data: JSON.stringify(data)
        })
        
        return response
    }catch(error){
        console.error(error)
    }
}

export const changePassword = async data => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'POST',
            url: `${ENTRYPOINT}/api/security/password/change/`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            data: JSON.stringify(data)
        })
        
        return response
    }catch(error){
        console.error(error)
    }
}

// export function fetch(id, options = {}) {
//     if ('undefined' === typeof options.headers) {
//       options.headers = new Headers();
//     }
  
//     if (null === options.headers.get('Accept')) {
//       options.headers.set('Accept', 'application/ld+json');
//     }
  
//     if ('undefined' !== options.body &&
//       !(options.body instanceof FormData) &&
//       null === options.headers.get('Content-Type')
//     ) {
//       options.headers.set('Content-Type', 'application/ld+json');
//     }

//     const accessToken = localStorage.getItem('jwt_access_token');
//     // const accessToken = jwtService.getAccessToken();
//     if (accessToken !== null && accessToken.length) {
//       options.headers.set('Authorization', 'Bearer ' + accessToken);
//     }
  
//     return global.fetch(new URL(id, ENTRYPOINT), options).then(response => {
//       if (response.ok) return response;
  
//       return response.json().then(json => {
//         const error = json['hydra:description'] || response.statusText;
//         if (!json.violations) throw Error(error);
  
//         let errors = { _error: error };
//         json.violations.map(
//           violation => (errors[violation.propertyPath] = violation.message)
//         );
  
//         // throw new SubmissionError(errors);
//       });
//     });
//   }

export const getWorkerOffers = async user_id => {
    try{
        // fetch(`${ENTRYPOINT}/api/offers?assignedWorker.id=${user_id}`)
        // .then(response =>
        //   response
        //     .json()
        //     .then(retrieved => ({ retrieved }))
        // )
        // .then(({ retrieved }) => {
        //   /*retrieved = normalize(retrieved);*/
        // })
        // .catch(e => {

        // });
        const token = localStorage.getItem('jwt_access_token')
        const response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}/api/offers?assignedWorker.id=${user_id}&deleted=false`,
            headers: {
                'Content-Type': 'application/ld+json',
                'Authorization': `Bearer ${token}`
            }
        })

        return response.data
    }catch(error){
        console.error(error)
    }
}

export const getOffers = async user_id => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'GET',
            // url: `${ENTRYPOINT}/api/users/offers/${user_id}`,
            url: `${ENTRYPOINT}/api/offers?client.id=${user_id}&deleted=false`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
        
        return response
    }catch(error){
        console.error(error)
    }
}

export const getUserId = async user_id => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}/api/users/${user_id}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        })
        
        return response.data
    }catch(error){
        console.error(error)
    }
}

export const deleteUserId = async user_id => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'DELETE',
            url: `${ENTRYPOINT}/api/users/${user_id}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        })
        
        return response.data
    }catch(error){
        console.error(error)
    }
}

export const createUser = async data => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'POST',
            url: `${ENTRYPOINT}/openapi/security/register/`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            data: JSON.stringify(data)
        })
        
        return response.data
    }catch(error){
        console.error(error)
    }
}

export const resetPassword = async data => {
    try{
        const response = await axios({
            method: 'POST',
            url: `${ENTRYPOINT}/openapi/security/password/reset/request/`,
            headers: {
                'Content-Type': 'application/json',
            },
            data: JSON.stringify(data)
        })
        
        return response
    }catch(error){
        console.error(error)
    }
}

export const validateTokenResetPassword = async data => {
    try {
        const response = await axios({
            method:'GET',
            url: `${ENTRYPOINT}/openapi/security/password/reset/authenticate/?token=${data}`,
            headers: {
                'Content-Type': 'application/json',
            }
        })
        
        return response
    } catch (error) {
        
    }
}

export const validatedResetPassword = async data => {
    try {
        const response = await axios({
            method:'POST',
            url: `${ENTRYPOINT}/openapi/security/password/reset/?token=${data.token}`,
            headers: {
                'Content-Type': 'application/json',
            },
            data: JSON.stringify({
                password: data.password,
                confirmation_password: data.confirmPassword,
            })
        })

        console.log('Aquiiiiiii',response)

        return response
    } catch (error) {
        
    }
}