import axios from 'axios'
import { ENTRYPOINT } from '../../config/entrypoint'

export const getUserScheduleCollection = async(url = '/api/user_schedules') => {    
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'GET',
            url: `${ ENTRYPOINT }${ url }`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
        return response.data
    }catch(error){
        console.error(error)
    }
}

export const createUserSchedule = (data) => {
        const token = localStorage.getItem('jwt_access_token')

        const request = axios({
            method: 'POST',
            url: `${ENTRYPOINT}/api/user_schedules`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            data: JSON.stringify(data)
        })
        .then((data) => data)
        .catch((data) => data)

        return request
}

export const updateDaySchedule = (id, data) => {
        const token = localStorage.getItem('jwt_access_token')

        const response = axios({
            method: 'PUT',
            url: `${ENTRYPOINT}/api/user_schedules/${id}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            data: JSON.stringify(data)
        })
        .then((data) => data)
        .catch((data) => data)
        
        return response
}

export const deleteUserSchedule = async id => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'DELETE',
            url: `${ENTRYPOINT}/api/user_schedules/${id}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            data: JSON.stringify({
                deleted: true
            })
        })
        
        return response
    }catch(error){
        console.error(error)
    }
}

export const deleteUserScheduleHours = async url => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'DELETE',
            url: `${ENTRYPOINT}${url}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            data: JSON.stringify({
                deleted: true
            })
        })
        
        return response
    }catch(error){
        console.error(error)
    }
}