import axios from 'axios'
import { ENTRYPOINT } from '../../config/entrypoint'

export const getVouchers = async () => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}/api/vouchers`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `bearer ${token}`
            }
        })

        return response.data['hydra:member']
    }catch(error){
        console.error(error)
    }
}

export const createVoucher = async data => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'POST',
            url: `${ENTRYPOINT}/api/vouchers/collection/new`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `bearer ${token}`
            },
            data: JSON.stringify(data)
        })

        return response
    }catch(error){
        console.error(error)
    }
}

export const getVoucherItem = async voucher_id => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}/api/vouchers/${voucher_id}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `bearer ${token}`
            }
        })

        return response
    }catch(error){
        console.error(error)
    }
}

export const updateVoucher = async (voucher_id, data) => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'PUT',
            url: `${ENTRYPOINT}/api/vouchers/${voucher_id}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `bearer ${token}`
            },
            data: JSON.stringify(data)
        })

        return response
    }catch(error){
        console.error(error)
    }
}

// export const updateVoucher = async (voucher_id,data) => {
//     try{
//         const token = localStorage.getItem('jwt_access_token')

//         const response = await axios({
//             method: 'PUT',
//             url: `${ENTRYPOINT}/api/vouchers/${voucher_id}`,
//             headers: {
//                 'Content-Type': 'application/json',
//                 'Authorization': `bearer ${token}`
//             },
//             data: JSON.stringify(data)
//         })

//         return response
//     }catch(error){
//         console.error(error)
//     }
// }

export const deleteVoucher = async voucher_id => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'DELETE',
            url: `${ENTRYPOINT}/api/vouchers/${voucher_id}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `bearer ${token}`
            }
        })

        return response
    }catch(error){
        console.error(error)
    }
}