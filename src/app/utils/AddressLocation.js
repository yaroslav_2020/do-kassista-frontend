import axios from 'axios'
import { ENTRYPOINT } from '../../config/entrypoint'

export const getCountries = async () => {

    try{
        const token = localStorage.getItem('jwt_access_token')

        const countries = await axios.get(`${ENTRYPOINT}/api/countries`,{
            headers: {
                'Authorization': `bearer ${token}`
            }
        })

        return countries.data['hydra:member']
    }catch(error){
        console.error(error)
    }
}

export const getCounties = async country => {

    try{
        const token = localStorage.getItem('jwt_access_token')

        const counties = await axios.get(`${ENTRYPOINT}${country}`,{
            headers: {
                'Authorization': `bearer ${token}`
            },
        })

        return counties.data.counties
    }catch(error){
        console.error(error)
    }
}

export const getCities = async county => {

    try{
        const token = localStorage.getItem('jwt_access_token')

        const cities = await axios.get(`${ENTRYPOINT}${county}`,{
            headers: {
                'Authorization': `bearer ${token}`
            },
        })

        return cities.data.cities
    }catch(error){
        console.error(error)
    }
}