import axios from 'axios'
import { ENTRYPOINT } from '../../config/entrypoint'

export const getUsers = async (url = '/api/users') => {
    try{
        const token = localStorage.getItem('jwt_access_token')
        const response = await axios({
            method: 'GET',
            url: `${ ENTRYPOINT }${ url }`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        })
        // return response.data['hydra:member']
        return response.data
    }catch(error){
        console.error(error)
    }
}

export const getUsersByRole = async (role = 'ROLE_CLIENT') => {
    try{
        const token = localStorage.getItem('jwt_access_token')
        const response = await axios({
            method: 'GET',
            url: `${ ENTRYPOINT }/api/users?roles=${ role }&pagination=false`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        })
        return response.data['hydra:member']
        // return response.data
    }catch(error){
        console.error(error)
    }
}