import axios from 'axios';
import { ENTRYPOINT } from '../../config/entrypoint';

export const getPostalCodeGroups = async () => {
	try {
		const token = localStorage.getItem('jwt_access_token');

		const response = await axios({
			method: 'GET',
			url: `${ENTRYPOINT}/api/postal_code_groups`,
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			}
		});

		return response.data['hydra:member'];
	} catch (error) {
		console.error(error);
	}
};

export const createPostalCodeGroups = async data => {
	try {
		const token = localStorage.getItem('jwt_access_token');

		const response = await axios({
			method: 'POST',
			url: `${ENTRYPOINT}/api/postal_code_groups`,
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			},
			data: JSON.stringify(data)
		});

		return response;
	} catch (error) {
		console.error(error);
	}
};

export const getPostalCodeGroupsId = async postal_code_group_id => {
	try {
		const token = localStorage.getItem('jwt_access_token');

		const response = await axios({
			method: 'GET',
			url: `${ENTRYPOINT}/api/postal_code_groups/${postal_code_group_id}`,
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			}
		});

		return response.data;
	} catch (error) {
		console.error(error);
	}
};

export const updatePostalCodeGroups = async (postal_code_group_id, data) => {
	try {
		const token = localStorage.getItem('jwt_access_token');

		const response = await axios({
			method: 'PUT',
			url: `${ENTRYPOINT}/api/postal_code_groups/${postal_code_group_id}`,
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			},
			data: JSON.stringify(data)
		});

		return response;
	} catch (error) {
		console.error(error);
	}
};

export const deletePostalCodeGroups = async postal_code_group_id => {
	try {
		const token = localStorage.getItem('jwt_access_token');

		const response = await axios({
			method: 'DELETE',
			url: `${ENTRYPOINT}/api/postal_code_groups/${postal_code_group_id}`,
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			}
		});

		return response;
	} catch (error) {
		console.error(error);
	}
};
