import axios from 'axios';
import { ENTRYPOINT } from '../../config/entrypoint';

export const getPostalCodes = async () => {
	try {
		const token = localStorage.getItem('jwt_access_token');

		const response = await axios({
			method: 'GET',
			url: `${ENTRYPOINT}/api/postal_codes`,
			headers: {
				'Content-Type': 'application/json',
				Authorization: `bearer ${token}`
			}
		});

		return response.data['hydra:member'];
	} catch (error) {
		console.error(error);
	}
};

export const createPostalCodes = async data => {
	console.log(JSON.stringify(data));
	try {
		const token = localStorage.getItem('jwt_access_token');
		console.log(token);
		const response = await axios({
			method: 'POST',
			url: `${ENTRYPOINT}/api/postal_code/add/`,
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			},
			data: JSON.stringify(data)
		});

		console.log(response);

		return response;
	} catch (error) {
		console.error(error);
	}
};

export const getPostalCodeItem = async postal_code_id => {
	try {
		const token = localStorage.getItem('jwt_access_token');

		const response = await axios({
			method: 'GET',
			url: `${ENTRYPOINT}/api/postal_code/item/${postal_code_id}`,
			headers: {
				'Content-Type': 'application/json',
				Authorization: `bearer ${token}`
			}
		});

		return response;
	} catch (error) {
		console.error(error);
	}
};

export const updatePostalCodes = async (postal_code_id, data) => {
	try {
		const token = localStorage.getItem('jwt_access_token');

		const response = await axios({
			method: 'PUT',
			url: `${ENTRYPOINT}/api/postal_codes/update/${postal_code_id}`,
			headers: {
				'Content-Type': 'application/json',
				Authorization: `bearer ${token}`
			},
			data: JSON.stringify(data)
		});

		return response;
	} catch (error) {
		console.error(error);
	}
};

export const deletePostalCodes = async postal_code_id => {
	try {
		const token = localStorage.getItem('jwt_access_token');
		console.log(postal_code_id);
		const response = await axios({
			method: 'DELETE',
			url: `${ENTRYPOINT}/api/postal_code/remove/${postal_code_id}/`,
			headers: {
				'Content-Type': 'application/json',
				Authorization: `bearer ${token}`
			}
		});

		return response;
	} catch (error) {
		console.error(error);
	}
};
