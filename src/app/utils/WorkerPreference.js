import axios from 'axios'
import { ENTRYPOINT } from '../../config/entrypoint'

export const getUserPreference = async(user_id) => {    
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'GET',
            url: `${ ENTRYPOINT }/api/user_preferences?user=${user_id}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
        return response.data
    }catch(error){
        console.error(error)
    }
}

export const createUserPreference = (data) => {
    const token = localStorage.getItem('jwt_access_token')

    const request = axios({
        method: 'POST',
        url: `${ENTRYPOINT}/api/user_preferences`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        data: JSON.stringify(data)
    })
    .then((data) => data)
    .catch((data) => data)

    return request
}

export const updateUserPreference = (id, data) => {
    const token = localStorage.getItem('jwt_access_token')

    const response = axios({
        method: 'PUT',
        url: `${ENTRYPOINT}/api/users/${id}/preference`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        data: JSON.stringify(data)
    })
    .then((data) => data)
    .catch((data) => data)
    
    return response
}