import axios from 'axios'
import { ENTRYPOINT } from '../../config/entrypoint'

export const getCompanies = async user_iri => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}${user_iri}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        })

        return response.data.company
    }catch(error){
        console.error(error)
    }
}

export const Create = async data => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'POST',
            url: `${ENTRYPOINT}/api/companies`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            data: JSON.stringify(data)
        })
        
        return response
    }catch(error){
        console.error(error)
    }
}

export const Update = async data => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'PUT',
            url: `${ENTRYPOINT}${data.url}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            data: JSON.stringify(data)
        })

        return response

    }catch(error){
        console.error(error)
    }
}

export const SoftDelete = async company_id => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'PUT',
            url: `${ENTRYPOINT}/api/companies/delete/${company_id}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            data: JSON.stringify({
                deleted: true
            })
        })

        return response

    }catch(error){
        console.error(error)
    }
}

export const getCompanyInformation = async company_iri => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}${company_iri}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        })

        if(response.status === 200)   
            return response.data        
        
        return 0
    }catch(error){
        console.error(error)
    }
    
}
