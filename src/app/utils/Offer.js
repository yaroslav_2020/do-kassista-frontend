import axios from 'axios'
import { ENTRYPOINT } from '../../config/entrypoint'

export const getOffersCollection = async(url = '/api/offers') => {    
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'GET',
            url: `${ ENTRYPOINT }${ url }`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
        return response.data
    }catch(error){
        console.error(error)
    }
}

export const createOffer = async (data) => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'POST',
            url: `${ENTRYPOINT}/api/client/offers/new`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            data: JSON.stringify(data)
        })

        return response;
        
    }catch(error){
        console.error(error)
    }
}

export const offerDetails = async offer_id => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}/api/offers/${offer_id}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
        
        return response
    }catch(error){
        console.error(error)
    }
}

export const deleteOffer = async offer_id => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'PUT',
            url: `${ENTRYPOINT}/api/offers/delete/${offer_id}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            data: JSON.stringify({
                deleted: true
            })
        })
        
        return response
    }catch(error){
        console.error(error)
    }
}

// export const updateOffer = async offer_id => {
//     try{
//         const token = localStorage.getItem('jwt_access_token')

//         const response = await axios({
//             method: 'PUT',
//             url: `${ENTRYPOINT}/api/offers/${offer_id}`,
//             headers: {
//                 'Content-Type': 'application/json',
//                 'Authorization': `Bearer ${token}`
//             },
//             data: JSON.stringify({
//                 deleted: true
//             })
//         })
        
//         return response
//     }catch(error){
//         console.error(error)
//     }
// }