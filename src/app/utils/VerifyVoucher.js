import axios from 'axios'
import { ENTRYPOINT } from '../../config/entrypoint'

export const VoucherVerify = async code => {
    try{

        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'POST',
            url: `${ENTRYPOINT}/openapi/voucher/verify`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `bearer ${token}`
            },
            data: {
                code
            }
        })

        return response.data
    }catch(error){
        console.error(error)
    }
}