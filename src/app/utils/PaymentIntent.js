import axios from 'axios'
import { ENTRYPOINT } from '../../config/entrypoint'

export const createSetupSecretIntent = async (data) => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'POST',
            url: `${ENTRYPOINT}/payment/stripe/setup_secret_intent`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            data: JSON.stringify(data)
        })

        return response;
        
    }catch(error){
        console.error(error)
    }
}

export const listPaymentMethod = async (data) => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'POST',
            url: `${ENTRYPOINT}/payment/stripe/payment_method_list`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            data: JSON.stringify(data)
        })

        return response;
        
    }catch(error){
        console.error(error)
    }
}

export const updatePaymentMethod = async (data) => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'POST',
            url: `${ENTRYPOINT}/payment/stripe/update_payment_method`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            data: JSON.stringify(data)
        })

        return response;
        
    }catch(error){
        console.error(error)
    }
}

export const setDefaultPaymentMethod = async (data) => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'POST',
            url: `${ENTRYPOINT}/payment/stripe/default_payment_method`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            data: JSON.stringify(data)
        })

        return response;
        
    }catch(error){
        console.error(error)
    }
}


export const detachPaymentMethod = async (data) => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'POST',
            url: `${ENTRYPOINT}/payment/stripe/detach_payment_method`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            data: JSON.stringify(data)
        })

        return response;
        
    }catch(error){
        console.error(error)
    }
}