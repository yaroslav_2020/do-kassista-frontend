import axios from 'axios'
import { ENTRYPOINT } from '../../config/entrypoint'

export const getFaqs = async () => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}/api/faqs`
        })

        return response.data['hydra:member']
    }catch(error){
        console.error(error)
    }
}