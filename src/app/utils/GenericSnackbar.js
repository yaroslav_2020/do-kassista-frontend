import React from 'react'

import Alert from '@material-ui/lab/Alert'
import Snackbar from '@material-ui/core/Snackbar'

import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
    root: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: '10px',
        left: '0'
    }
})

const GenericSnackbar = props => {

    const classes = useStyles()

    return (
        <div className={classes.root}>
            <Snackbar 
                anchorOrigin={{ 
                    vertical: 'top', 
                    horizontal: 'center' 
                }} 
                open={props.open} 
                autoHideDuration={6000} 
                onClose={() => props.handleClose(false)}
            >
                <Alert onClose={() => props.handleClose(false)} severity={props.severity}>
                    {props.message}
                </Alert>
            </Snackbar>      
        </div>
    )
}

export default GenericSnackbar