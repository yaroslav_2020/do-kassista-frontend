import axios from 'axios'
import { ENTRYPOINT } from '../../config/entrypoint'

export const getAddresses = async user_iri => {
    try{
        const token = localStorage.getItem('jwt_access_token')
        const response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}${user_iri}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        })

        return response.data.addresses
    }catch(error){
        console.error(error)
    }
}

export const Save = async data => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'POST',
            url: `${ENTRYPOINT}/api/addresses`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            data: JSON.stringify(data)
        })

        return response

    }catch(error){
        console.error(error)
    }
}

export const Update = async data => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'PUT',
            url: `${ENTRYPOINT}${data.url}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            data: JSON.stringify(data)
        })

        return response

    }catch(error){
        console.error(error)
    }
}

export const SoftDelete = async address_id => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'PUT',
            url: `${ENTRYPOINT}/api/addresses/delete/${address_id}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            data: JSON.stringify({
                deleted: true
            })
        })

        return response

    }catch(error){
        console.error(error)
    }
}

export const getPostalCodes = async () => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}/api/postal_codes`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `bearer ${token}`
            }
        })

        return response.data['hydra:member']
    }catch(error){
        console.error(error)
    }
}

export const getAddressInformation = async address_iri => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}${address_iri}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        })

        if(response.status === 200)
            return response.data
                
        return 0
    }catch(error){
        console.error(error)
    }    
}