import React from 'react'
import { connect } from 'react-redux'

import { makeStyles } from '@material-ui/core/styles'

import UserIcon from '../assets/images/icons/user-icon.svg'

import * as AlertActions from '../store/actions/AlertActions'

const useStyles = makeStyles({
    container: {
        width: '100vw',
        height: '100vh',
        position: 'fixed',
        background: 'rgba(0,0,0,.1)',
        zIndex: '1000',
        display: 'flex',
        justifyContent: 'center',
        boxSizing: 'border-box',
        alignItems: 'center',
        top: '0',
        left: '0'
    },
    alertCont: {
        width: '700px',
        maxWidth: '90%',
        padding: '70px 15px',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        background: '#fff',
        borderRadius: '13px',
        minHeight: '600px'
    },
    text: {
        fontSize: '17px',
        fontWeight: '400',
        color: '#434343',
        marginBottom: '36px'
    },
    image: {
        width: '120px',
        heigth: '120px',
        borderRadius: '100%',
        marginBottom: '13px',
    },
    contButton: {
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
        marginTop: '60px',
        flexWrap: 'wrap',
        width: '80%'
    },
    button: {
        width: '200px',
        userSelect: 'none',
        height: '67px',
        marginBottom: '25px',
        border: '1px solid #0097F6',
        color: '#0097F6',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: '9px',
        fontWeight: 'bold',
        fontSize: '22px',
        transition: 'all 300ms ease',
        '&:hover': {
            color: '#fff',
            background: '#0097F6'
        }
    }
})

const AlertPopup = props => {
    const classes = useStyles()

    return (
        <div 
            className={classes.container}
        >
            <div className={classes.alertCont}>
                {
                    props.text && <p className={classes.text} >{props.text}</p>
                }
                {
                    props.image && <img src={props.image} className={classes.image} />
                }
                <div className={classes.contButton} className={classes.contButton}>
                    <button className={classes.button} onClick={props.confirmAction} >Aceptar</button>
                    <button className={classes.button} onClick={props.alertHide} >Cancelar</button>
                </div>
            </div>
        </div>
    )
}

const mapState = state => ({
    show: state.Alert.show
})

const mapDispatch = {
    ...AlertActions
}

export default connect(mapState,mapDispatch)(AlertPopup)