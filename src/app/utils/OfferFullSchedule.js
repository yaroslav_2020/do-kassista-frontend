import axios from 'axios'
import { ENTRYPOINT } from '../../config/entrypoint'

export const calculateOfferFullSchedule = async data => {
    try{
        const response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}/offer_full_schedule/calculate?data=${JSON.stringify(data)}`,
            headers: {
                'Content-Type': 'application/json',
            }, 
        })

        return response.data

    }catch(error){
        console.error(error)
    }
}

export const getOfferFullSchedule = async id => {
    try{
        const response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}/api/offer_full_schedules?offer=${id}&order%5BdateTimeStart%5D=asc`,
            headers: {
                'Content-Type': 'application/json',
            }, 
        })

        return response.data

    }catch(error){
        console.error(error)
    }
}

export const getPaymentOfferFullSchedule = async id => {
    try{
        const response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}/api/payments?offer=${id}`,
            headers: {
                'Content-Type': 'application/json',
            }, 
        })

        return response.data

    }catch(error){
        console.error(error)
    }
}

export const updateOfferFullSchedule = async (id, data) => {
    try{
        const response = await axios({
            method: 'PUT',
            url: `${ENTRYPOINT}/api/offer_full_schedules/${id}`,
            headers: {
                'Content-Type': 'application/json',
            },
            data: JSON.stringify(data)
        })

        return response

    }catch(error){
        console.error(error)
    }
}