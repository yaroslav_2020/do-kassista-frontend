import axios from 'axios'
import { JsonWebTokenError } from 'jsonwebtoken'
import { ENTRYPOINT } from '../../config/entrypoint'

export const getFaq = async () => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}/api/faqs`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `bearer ${token}`
            }
        })

        return response.data['hydra:member']
    }catch(error){
        console.error(error)
    }
}

export const getFaqItem = async faq_id => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}/api/faqs/${faq_id}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `bearer ${token}`
            }
        })

        return response.data
    }catch(error){
        console.error(error)
    }
}

export const updateFaq = async (faq_id,data) => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'PUT',
            url: `${ENTRYPOINT}/api/faqs/${faq_id}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `bearer ${token}`
            },
            data: JSON.stringify(data)
        })

        return response.data
    }catch(error){
        console.error(error)
    }
}

export const createFaq = async data => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'POST',
            url: `${ENTRYPOINT}/api/faqs`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `bearer ${token}`
            },
            data: JSON.stringify(data)
        })

        return response
    }catch(error){
        console.error(error)
    }
}