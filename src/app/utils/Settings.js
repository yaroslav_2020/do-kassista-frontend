import axios from 'axios'
import { ENTRYPOINT } from '../../config/entrypoint'

export const getSettings = async (url = '/api/settings') => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'GET',
            url: `${ ENTRYPOINT }${ url }`
        })

        // return response.data['hydra:member']
        return response.data;
    }catch(error){
        console.error(error)
    }
}

export const getSettingItem = async setting_id => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'GET',
            url: `${ENTRYPOINT}/api/settings/${setting_id}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `bearer ${token}`
            }
        })

        return response
    }catch(error){
        console.error(error)
    }
}

export const createSettings = async data => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'POST',
            url: `${ENTRYPOINT}/api/settings-new`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `bearer ${token}`
            },
            data: JSON.stringify(data)
        })

        return response
    }catch(error){
        console.error(error)
    }
}

export const updateSettings = async (setting_id,data) => {
    try{
        const token = localStorage.getItem('jwt_access_token')

        const response = await axios({
            method: 'PUT',
            url: `${ENTRYPOINT}/api/settings/${setting_id}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `bearer ${token}`
            },
            data: JSON.stringify(data)
        })

        return response
    }catch(error){
        console.error(error)
    }
}