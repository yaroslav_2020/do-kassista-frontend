import { authRoles } from 'app/auth';
import i18next from 'i18next';

const navigationConfig = [
	{
		id: 'applications',
		title: 'Applications',
		translate: 'APPLICATIONS',
		auth: authRoles.admin,
		type: 'group',
		icon: 'apps',
		children: [
			{
				id: 'home',
				title: 'Home',
				translate: 'adminPanel.home',
				type: 'item',
				icon: 'home',
				url: '/admin'
			},
			{
				id: 'users-table',
				title: 'Users',
				translate: 'adminPanel.users',
				type: 'item',
				icon: 'group',
				url: '/users'
			},
			{
				id: 'offers-table',
				title: 'Offers',
				translate: 'adminPanel.offers',
				type: 'item',
				icon: 'list_alt',
				url: '/offers'
			},
			{
				id: 'postal-code-table',
				title: 'Postal Codes',
				translate: 'adminPanel.postalCodes',
				type: 'item',
				icon: 'location_on',
				url: '/postal_codes'
			},
			{
				id: 'postal-code-groups-table',
				title: 'Postal Code Groups',
				translate: 'adminPanel.postalCodeGroups',
				type: 'item',
				icon: 'location_on',
				url: '/postal_code_groups'
			},
			{
				id: 'extra-service-table',
				title: 'Extra Services',
				translate: 'adminPanel.extraServices',
				type: 'item',
				icon: 'room_service',
				url: '/extra_services'
			},
			{
				id: 'voucher-table',
				title: 'Vouchers',
				translate: 'adminPanel.vouchers',
				type: 'item',
				icon: 'card_giftcard',
				url: '/vouchers'
			},
			{
				id: 'settings-table',
				title: 'Settings',
				translate: 'general.settings',
				type: 'item',
				icon: 'settings',
				url: '/settings'
			},
			{
				id: 'question',
				title: 'FAQ',
				translate: 'general.faq',
				type: 'item',
				icon: 'question_answer',
				url: '/faq'
			}
		]
	}
];

export default navigationConfig;
