import React from 'react'
import { Redirect } from 'react-router-dom'
import FuseUtils from '@fuse/utils'

import LoginConfig from '../main/auth/login/LoginConfig'
import AdminConfig from '../main/auth/admin/AdminConfig'
import OfferConfig from '../main/client_view_main/offer/OfferConfig'

import { default as userRoutes } from '../routes/userRoutes'
import { default as offersRoutes } from '../routes/offersRoutes'
import { default as postalCodeRoutes } from '../routes/postalCodeRoutes'
import { default as postalCodeGroupsRoutes } from '../routes/postalCodeGroupsRoutes'
import { default as extraServiceRoutes } from '../routes/extraServiceRoutes'
import { default as voucherRoutes } from '../routes/voucherRoutes'
import { default as settingsRoutes } from '../routes/settingsRoutes'
import { default as faqRoutes } from '../routes/faqRoutes'
import { authRoles } from 'app/auth'


let routeConfigs = [
	LoginConfig,
    AdminConfig,
    OfferConfig
]

const erpRoutes = [
    ...userRoutes,
    ...offersRoutes,
    ...postalCodeRoutes,
    ...postalCodeGroupsRoutes,
    ...extraServiceRoutes,
    ...voucherRoutes,
    ...settingsRoutes,
    ...faqRoutes
]

erpRoutes.map(route => {
    let newRoute = {
        settings: {
            layout: {
                config: {
                    navbar: {
                        display: true
                    },
                    toolbar: {
                        display: true
                    },
                    footer: {
                        display: false
                    },
                    leftSidePanel: {
                        display: false
                    },
                    rightSidePanel: {
                        display: false
                    }
                }
            }
        },
        redirect_url_not_authorized: '/login',
        auth: authRoles.admin,
        routes: [
            {
                path: route.props.path,
                component: route.props.component,
                options: {
                    redirectUrl: '/login'
                }
            }
        ]
    }

    routeConfigs = [...routeConfigs,newRoute]
})

const routes = [
    ...FuseUtils.generateRoutesFromConfigs(routeConfigs),
    {
        path    : '/',
        // exact   : true,
        // auth    : null,
        component: () => <Redirect to='/offer'/>
    },
    // {
    //     path     : '/offer',
    //     exact    : true,
    //     auth: null,
    //     component: () => <Redirect to='/offer/login'/>
    // },
    // {
    //     component: () => <Redirect to="/pages/errors/error-404"/>
    // }
]

export default routes
