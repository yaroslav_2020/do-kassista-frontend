import React,{ useState, useEffect } from 'react'
import { darken } from '@material-ui/core/styles/colorManipulator'
import { makeStyles } from '@material-ui/styles'
import clsx from 'clsx'
import Formsy from 'formsy-react'
import { connect } from 'react-redux'
import FuseAnimate from '@fuse/core/FuseAnimate'
import { Button, Card, CardContent, Typography, InputAdornment, Icon } from '@material-ui/core'
import { TextFieldFormsy } from '@fuse/core/formsy'
import * as loginActions from '../../../auth/store/actions/loginActions'
import { useHistory } from 'react-router-dom'
import FuseSplashScreen from '@fuse/core/FuseSplashScreen'
import { default as userRoles } from '../../../auth/store/types/UserRoles'

const useStyles = makeStyles(theme => ({
    root: {
        background: `radial-gradient(${darken(theme.palette.primary.dark,0.5)} 0%, ${theme.palette.primary.dark} 80%)`,
        color: theme.palette.primary.contrastText
    }
})) 

const Login = props => {

    const history = useHistory()

    const classes = useStyles()

    const [isValidForm,setIsValidForm] = useState(false)
    const [loginData,setLoginData] = useState({
                                        email: '',
                                        password: ''
                                    })


    useEffect(() => {},[props.login.fetching])

    useEffect(() => {
        if(props.user.role.length){
            const hasAccessToAdmin = props.user.role.indexOf(userRoles.ROLE_COMERCIAL) > -1
                                 || props.user.role.indexOf(userRoles.ROLE_ADMIN) > -1
                                 || props.user.role.indexOf(userRoles.ROLE_SUPER_ADMIN) > -1
            if(hasAccessToAdmin){
                history.push('/admin')
            }else{
               console.log('No eres admin')
               localStorage.removeItem('jwt_access_token')
            }
        }
    },[props.user])

    const handleSubmit = (form) => {
        props.Fetching()
        props.SignIn(loginData)
    }

    const disableButton = () => {
        setIsValidForm(false)
    }

    const enableButton = () => {
        setIsValidForm(true)
    }

    const handleChange = input => {
        setLoginData({
            ...loginData,
            [input.target.name]: input.target.value
        })
    }

    const inputs = [
        {
            key: 1,
            className: 'mb-16',
            type: 'text',
            name: 'email',
            label: 'Username/Email',
            validations: {
                minLength: 4
            },
            validationErrors: {
                minLength: 'Min character length is 4'
            },
            InputProps: {
                endAdornment: <InputAdornment position='end'><Icon className='text-20' color='action'>email</Icon></InputAdornment>
            },
            variant: 'outlined',
            required: true,
            value: loginData.email,
            onChange: handleChange
        },
        {
            key: 2,
            className: 'mb-16',
            type: 'password',
            name: 'password',
            label: 'Password',
            validations: {
                minLength: 4
            },
            validationErrors: {
                minLength: 'Min character length is 4'
            },
            InputProps: {
                endAdornment: <InputAdornment position='end'><Icon className='text-20' color='action'>vpn_key</Icon></InputAdornment>
            },
            variant: 'outlined',
            required: true,
            value: loginData.password,
            onChange: handleChange
        }
    ]

    const handleRenderComponent = () => {
        if(!props.login.fetching){
            /**
             * if is not fetching show the login form
             */
            return (
                <FuseAnimate animatio='transition.expandIn'>
                    <Card className='w-full max-w-384'>
                        <CardContent className='flex flex-col items-center justify-center p-32'>
                            <Typography variant='h6' className='mt-16 mb-32'>
                                LOGIN
                            </Typography>
                            <div className='w-full'>
                                <Formsy
                                    onValidSubmit={handleSubmit}
                                    onValid={enableButton}
                                    onInvalid={disableButton}
                                    className='flex flex-col justify-center w-full'
                                    autoComplete='off'
                                >
                                    {                                        
                                        inputs.length > 0 ? inputs.map(input => <TextFieldFormsy {...input} />) : <div></div>
                                    }
                                    <Button
                                        type='submit'
                                        variant='contained'
                                        color='primary'
                                        className='w-full mx-auto mt-16 normal-case'
                                        aria-label='LOG IN'
                                        disabled={!isValidForm}
                                        value='legacy'
                                    >
                                        Login
                                    </Button>
                                </Formsy>
                            </div>
                        </CardContent>
                    </Card>
                </FuseAnimate>      
            )
        }else{
            /**
             * if is fetching show the spinner
             */
            return <FuseSplashScreen/>
        }
    }

    return(
        <div className={clsx(classes.root, 'flex flex-col flex-auto flex-shrink-0 items-center justify-center p-32')}>
            <div className='flex flex-col items-center justify-center w-full'>        
                {
                    handleRenderComponent()
                }
            </div>
        </div>
    )
}

const mapState = state => ({
    login: state.auth.login,
    user: state.auth.user
})

const mapDispatch = {
    ...loginActions
}

export default connect(mapState,mapDispatch)(Login)