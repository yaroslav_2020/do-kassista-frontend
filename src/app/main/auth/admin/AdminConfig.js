import { authRoles } from '../../../auth'
import Admin from './Admin'

const AdminConfig = {
	settings: {
		layout: {
			config: {
				navbar: {
					display: true
				},
				toolbar: {
					display: true
				},
				footer: {
					display: false
				},
				leftSidePanel: {
					display: false
				},
				rightSidePanel: {
					display: false
				}
			}
		}
    },
    auth: authRoles.admin,
	routes: [
		{
			path: '/admin',
            component: Admin,
            options: {
                redirectUrl: '/login'
            }
		}
	]
};

export default AdminConfig;
