import React , { useEffect } from 'react'
import { Switch , Route , Link , useHistory } from 'react-router-dom'
import { connect } from 'react-redux'
import {
    CircularProgress
} from '@material-ui/core'

import Header from  '../Header'
import Footer from  '../Footer'

import OfferDetails from '@trd/components/Offer/OfferDetails'
import FaqColumn from '@trd/components/FaqColumn/FaqColumn'
import List from '../../../components/offer/List'
import OfferInfo from '../../../components/offer/OfferInfo'
import ListAddresses from '../../../components/client/ListAddresses'
import ListPaymentMethods from 'app/components/client/ListPaymentMethods'
import AddAddress from '../../../components/client/AddAddress'
import AddCompany from '../../../components/client/AddCompany'
import ListCompany from '../../../components/client/ListCompany'
import ClientInfo from '../../../components/client/ClientInfo'
import ClientChangePassword from '../../../components/client/ClientChangePassword'

import UserLoggedInMenu  from '@trd/components/FaqColumn/partials/UserLoggedInMenu'
import FaqSection from '@trd/components/FaqColumn/partials/FaqSection'
import FaqOfferDetails from '@trd/components/FaqColumn/partials/FaqOfferDetails'

import ClientLogin from '../../../components/client/ClientLogin'
import ClientRegister from '../../../components/client/ClientRegister'
import ClientResetPassword from '../../../components/client/ClientResetPassword'

import WorkerOfferList from 'app/components/worker/OfferList'
import WorkerReceivePayments from '../../../components/worker/ReceivePayments'
import WorkerSchedule from 'app/components/worker/WorkerSchedule'

import ReturnIcon from '../../../assets/images/icons/return-icon.svg'

import {
    Grid,
    useMediaQuery
} from '@material-ui/core'

import { makeStyles } from '@material-ui/styles'
import AddPaymentMethod from 'app/components/client/AddPaymentMethod'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        background: '#fff',
        position: 'relative'
    },
    trdSideRightTitle: {
        fontSize: 22,
    },
    trdSideRightMb: {
        marginBottom: 25,
    },
    loaderCont: {
        width: '100vw',
        height: '100vh',
        position: 'fixed',
        background: 'rgba(0,0,0,.1)',
        zIndex: '1000',
        display: 'flex',
        justifyContent: 'center',
        boxSizing: 'border-box',
        alignItems: 'center',
        top: '0',
        left: '0'
    },
    container: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        padding: '0 15px',
        width: '100%'
    },
    rootContainer:{
        maxWidth: '1170px',
        margin: 'auto',
    }
}))

const desktopStyles = makeStyles({
    mainCont: {
        padding: '0 8%'
    },
})

const mobileStyles = makeStyles({
    mainCont: {
        padding: '0 15px'
    },
    returnButton: {
        marginLeft: '25px',
        marginBottom: '30px',
        width: '100%',
        '& a': {
            color: '#000',
            fontSize: '24px',
            fontWeight: '500',
            display: 'flex',
            alignItems: 'center',
        },
        '& a:hover': {
            textDecoration: 'none',
        }
    },
    returnIcon: {
        marginRight: '20px'
    }
})

const Offer = props => {
    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const matches = useMediaQuery('(max-width:600px)')

    const history = useHistory()

    useEffect(() => {
        if(history.location.pathname !== '/offer' && 
            history.location.pathname !== '/offer/login' &&
            history.location.pathname !== '/offer/register' &&
            history.location.pathname !== '/offer/password/reset' &&
            props.user === ''){

                history.push('/offer')
            }
    },[history.location.pathname])

    useEffect(() => {
        if (props.role === 'ROLE_EMPLOYEE_WORKER' ||
            props.role === 'ROLE_INDEPENDENT_WORKER') {   
                switch (history.location.pathname) {
                    case '/offer':
                    case '/client/payment-methods':
                    case '/client/payment-methods/add':
                        history.push('/client/info')                            
                        break;            
                    default:
                        break;
                }
        }

        if(props.role === 'ROLE_CLIENT') {
            switch (history.location.pathname) {
                case '/offer/worker/list':
                case '/offer/worker/payments':
                case '/worker/schedule':
                    history.push('/client/info')                            
                    break;            
                default:
                    break;
            }
        }

    }, ([history.location.pathname]))

    return (
        <div className={classes.root}>
            <Header />
            <div className={classes.loaderCont} style={{display: props.fetching ? 'flex' : 'none'}}>
                <CircularProgress />
            </div>
            <div className={matches ? mobileClasses.mainCont : desktopClasses.mainCont} >
                <Grid
                    container
                    spacing={2}
                    className={classes.rootContainer}
                >
                    <Switch>
                        <Route path='/pages/auth/forgot-password'>
                            <h1>Resetting password</h1>
                        </Route>
                        {
                            matches && <Route path='/offer/myAccount' exact>
                                            <div className={classes.container} >
                                                <UserLoggedInMenu />
                                            </div>
                                        </Route>
                        }
                        <Route path='/offer/login' exact>
                            <ClientLogin 
                                returnButton={<div className={mobileClasses.returnButton}>
                                                    <Link to='/offer'><img src={ReturnIcon} className={mobileClasses.returnIcon} /> Regresar</Link>
                                                </div>}
                            />
                        </Route>
                        <Route path='/offer/register' exact>
                            <ClientRegister 
                                returnButton={
                                    <div className={mobileClasses.returnButton}>
                                        <Link to='/offer/login'><img src={ReturnIcon} className={mobileClasses.returnIcon} />Regresar</Link>
                                    </div>
                                }
                            />
                        </Route>
                        <Route path='/offer/password/reset' exact>
                            <ClientResetPassword 
                                returnButton={<div className={mobileClasses.returnButton}>
                                                    <Link to='/offer/login'><img src={ReturnIcon} className={mobileClasses.returnIcon} />Regresar</Link>
                                                </div>}
                            />
                        </Route>
                        <Route path='/offer' exact>
                            <OfferDetails />
                            <FaqColumn>
                                <FaqOfferDetails />
                                <FaqSection />
                            </FaqColumn>
                        </Route>
                        <Route path='/offer/item/:id' exact>
                            <div className={mobileClasses.returnButton}>
                                <Link to={matches ? '/offer/myAccount' : '/offer' }><img src={ReturnIcon} className={mobileClasses.returnIcon} /> Regresar</Link>
                            </div>
                            {
                                !matches && <FaqColumn>
                                                <UserLoggedInMenu />
                                            </FaqColumn>  
                            }
                            <OfferInfo />
                        </Route>
                        <Route path='/offer/list' exact >
                            <div className={mobileClasses.returnButton}>
                                <Link to={matches ? '/offer/myAccount' : '/offer' }><img src={ReturnIcon} className={mobileClasses.returnIcon} /> Regresar</Link>
                            </div>
                            {
                                !matches && <FaqColumn>
                                                <UserLoggedInMenu />
                                            </FaqColumn>  
                            }
                            <List />
                        </Route>
                        <Route path='/offer/worker/list' exact >
                            <div className={mobileClasses.returnButton}>
                                <Link to='/client/info'><img src={ReturnIcon} className={mobileClasses.returnIcon} /> Regresar</Link>
                            </div>
                            {
                                !matches && <FaqColumn>
                                                <UserLoggedInMenu />
                                            </FaqColumn>  
                            }
                            <WorkerOfferList />
                        </Route>
                        <Route path='/worker/schedule' exact>
                            <div className={mobileClasses.returnButton}>
                                <Link to={matches ? '/offer/myAccount' : '/client/info' }><img src={ReturnIcon} className={mobileClasses.returnIcon} /> Regresar</Link>
                            </div>
                            {
                                !matches && <FaqColumn>
                                                <UserLoggedInMenu />
                                            </FaqColumn>  
                            }
                            <WorkerSchedule />
                        </Route>
                        <Route path='/offer/worker/payments' exact >
                            <div className={mobileClasses.returnButton}>
                                <Link to={matches ? '/offer/myAccount' : '/offer' }><img src={ReturnIcon} className={mobileClasses.returnIcon} /> Regresar</Link>
                            </div>
                            {
                                !matches && <FaqColumn>
                                                <UserLoggedInMenu />
                                            </FaqColumn>  
                            }
                            <WorkerReceivePayments />
                        </Route>
                        <Route path='/client/address' exact >
                            <div className={mobileClasses.returnButton}>
                                <Link to={matches ? '/offer/myAccount' : '/offer' }><img src={ReturnIcon} className={mobileClasses.returnIcon} /> Regresar</Link>
                            </div>
                            {
                                !matches && <FaqColumn>
                                                <UserLoggedInMenu />
                                            </FaqColumn>  
                            }
                            <ListAddresses />
                        </Route>
                        <Route path='/client/address/add' exact >
                            <div className={mobileClasses.returnButton}>
                                <Link to={matches ? '/offer/myAccount' : '/offer' }><img src={ReturnIcon} className={mobileClasses.returnIcon} /> Regresar</Link>
                            </div>
                            {
                                !matches && <FaqColumn>
                                                <UserLoggedInMenu />
                                            </FaqColumn>  
                            }
                            <AddAddress /> 
                        </Route>
                        <Route path='/client/payment-methods' exact>
                            <div className={mobileClasses.returnButton}>
                                <Link to={matches ? '/offer/myAccount' : '/client/info' }><img src={ReturnIcon} className={mobileClasses.returnIcon} /> Regresar</Link>
                            </div>
                            {
                                !matches && <FaqColumn>
                                                <UserLoggedInMenu />
                                            </FaqColumn>  
                            }
                            <ListPaymentMethods />
                        </Route>
                        <Route path='/client/payment-methods/add' exact >
                            <div className={mobileClasses.returnButton}>
                                <Link to={matches ? '/offer/myAccount' : '/client/payment-methods' }><img src={ReturnIcon} className={mobileClasses.returnIcon} /> Regresar</Link>
                            </div>
                            {
                                !matches && <FaqColumn>
                                                <UserLoggedInMenu />
                                            </FaqColumn>  
                            }
                            <AddPaymentMethod /> 
                        </Route>                        

                        <Route path='/company/add' exact >
                            <div className={mobileClasses.returnButton}>
                                <Link to={matches ? '/offer/myAccount' : '/offer' }><img src={ReturnIcon} className={mobileClasses.returnIcon} /> Regresar</Link>
                            </div>
                            {
                                !matches && <FaqColumn>
                                                <UserLoggedInMenu />
                                            </FaqColumn>  
                            }
                            <AddCompany />
                        </Route>
                        <Route path='/company/list' exact>
                            <div className={mobileClasses.returnButton}>
                                <Link to={matches ? '/offer/myAccount' : '/offer' }><img src={ReturnIcon} className={mobileClasses.returnIcon} /> Regresar</Link>
                            </div>
                            {
                                !matches && <FaqColumn>
                                                <UserLoggedInMenu />
                                            </FaqColumn>  
                            }
                            <ListCompany />
                        </Route>
                        <Route path='/client/info' exact >
                            {   
                                !(
                                    props.role === 'ROLE_EMPLOYEE_WORKER' ||
                                    props.role === 'ROLE_INDEPENDENT_WORKER'
                                ) &&
                                <div className={mobileClasses.returnButton}>
                                    <Link to={matches ? '/offer/myAccount' : '/offer' }><img src={ReturnIcon} className={mobileClasses.returnIcon} /> Regresar</Link>
                                </div>
                            }
                            {
                                !matches && <FaqColumn>
                                                <UserLoggedInMenu />
                                            </FaqColumn>  
                            }
                            <ClientInfo />
                        </Route>
                        <Route path='/client/change/password' exact >
                            {   
                                
                                <div className={mobileClasses.returnButton}>
                                    <Link to={
                                        (props.role === 'ROLE_EMPLOYEE_WORKER' ||
                                        props.role === 'ROLE_INDEPENDENT_WORKER') ?
                                        '/client/info' :
                                        '/offer' 
                                    }>
                                        <img src={ReturnIcon} className={mobileClasses.returnIcon} />
                                        Regresar
                                    </Link>
                                </div>
                            }
                            {
                                !matches && <FaqColumn>
                                                <UserLoggedInMenu />
                                            </FaqColumn>  
                            }
                            <ClientChangePassword />
                        </Route>                    
                    </Switch>                    
                </Grid>
            </div>
            <Footer />
        </div>
    )    
}

const mapState = state => ({
    fetching: state.Fetch.fetching,
    user: state.auth.user.data.iri,
    role: state.auth.user.role[0]
})

export default connect(mapState,{})(Offer)