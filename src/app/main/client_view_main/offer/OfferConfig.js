import Offer from './Offer'
import ResetPassword from '../../../components/client/clientResetPasswordStep2'
//import { authRoles } from '../../../auth'


const OfferConfig = {
    settings: {
        layout: {
            config: {
                navbar: {
                    display: false
                },
                toolbar: {
                    display: false
                },
                footer: {
                    display: false
                },
                leftSidePanel: {
                    display: false
                },
                rightSidePanel: {
                    display: false
                }
            }
        }
    },
    routes: [
        {
        //     path: '/',
        //     component: Offer,
        // },
        // {
            path: '/offer',
            component: Offer        
        },
        {
            path: '/worker',
            component: Offer
        },
        {
            path: '/client',
            component: Offer
        },
        {
            path: '/company',
            component: Offer
        },
        {
            path: '/pages/auth/forgot-password',
            component: ResetPassword
        }
    ]
}

export default OfferConfig