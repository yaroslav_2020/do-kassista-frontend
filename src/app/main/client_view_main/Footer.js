import React from 'react'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import {
    useMediaQuery
} from '@material-ui/core'

import Facebook from '../../assets/images/icons/facebook.svg'
import Instagram from '../../assets/images/icons/instagram.svg'
import Twitter from '../../assets/images/icons/twitter.svg'
// import FooterImage from '../../assets/images/footer-image.svg'
// import PaymentMaestro from '../../assets/images/payment-maestro.svg'
// import PaymentMasterCard from '../../assets/images/payment-mastercard.svg'
// import PaymentPaypal from '../../assets/images/payment-paypal.svg'
// import PaymentVisa from '../../assets/images/payment-visa.svg'
// import PaymentVisaElectron from '../../assets/images/payment-visaelectron.svg'

import { makeStyles } from '@material-ui/core/styles'

import Logo from '../../assets/images/logo2.svg'

const useStyles = makeStyles({
    footer: {
        width: '100%',
        minHeight: '520px',
        paddingTop: '50px',
        paddingBottom: '50px',
        borderTop: '1px solid #707070',
        marginTop: '100px',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
        position: 'relative',
        overflow: 'hidden'
    },
    imgCont: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: '50px'
    },
    actionsCont: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    img: {
        top: '48%',
        left: '-317px',
        width: '500px',
        height: '350px',
        position: 'absolute',
    },
    paymentCont: {
        marginTop: '25px',
        display: 'flex',
        '&>img': {
            margin: '0 4.5px'
        }
    }
})

const desktopStyles = makeStyles({
    container: {
        width: '90%',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
    },
    footerMenuCont: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        '&>p': {
            fontSize: '18px',
            fontWeight: 'bold',
            marginBottom: '32px',
            color: '#212121',
        },
        '&>a':{
            fontSize: '19px',
            color: '#848484',
            fontWeight: '400',
            marginBottom: '15px',
            textDecoration: 'none'
        },
        '&>a:hover': {
            textDecoration: 'none'
        }
    }
})

const mobileStyles = makeStyles({
    container: {
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        flexDirection: 'column'
    },
    footerMenuCont: {
        display: 'flex',
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        '&>p': {
            fontSize: '18px',
            fontWeight: 'bold',
            marginBottom: '32px',
            color: '#212121',
        },
        '&>a':{
            fontSize: '18px',
            color: '#848484',
            fontWeight: '400',
            marginBottom: '15px',
            textDecoration: 'none'
        },
        '&>a:last-child':{
            marginBottom: '32px',
        },
        '&>a:hover': {
            textDecoration: 'none'
        }
    }
})

const Footer = props => {
    
    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const { t } = useTranslation()

    const matches = useMediaQuery('(max-width:600px)')

    const info = [
        {
            title: t('footer.menu'),
            options: [
                {
                    label: t('footer.connect'),
                    url: '/offer',
                    link: true
                },
                {
                    label: t('footer.pro'),
                    url: 'https://kassista.com/pro'   
                },
                {
                    label: t('footer.faq'),
                    url: '/faq',
                    link: true   
                },
                {
                    label: t('footer.blog'),
                    url: 'https://kassista.com/blog'   
                },
                {
                    label: t('footer.contact'),
                    url: 'https://kassista.com/contacto'   
                }
            ]
        },
        {
            title: t('footer.information'),
            options: [
                {
                    label: t('footer.useConditions'),
                    url: 'https://www.kassista.com/#'   
                },
                {
                    label: t('footer.privacyPolicy'),
                    url: 'https://www.kassista.com/#'   
                },
                {
                    label: t('footer.cookiesPolicy'),
                    url: 'https://www.kassista.com/politica-de-cookies/'   
                },
            ]
        },
        {
            title: t('footer.professionals'),
            options: [
                {
                    label: t('footer.beProfessional'),
                    url: 'https://www.kassista.com/empleo/'   
                },
            ]
        },
    ]

    return (
        <footer className={classes.footer}>
            <div className={classes.imgCont} >
                <img src={Logo} />
            </div>
            <div className={matches ? mobileClasses.container : desktopClasses.container}>
                {
                    info.map((data,id) => <div key={`fm${id}`} className={matches ? mobileClasses.footerMenuCont : desktopClasses.footerMenuCont} >
                                            <p>{data.title}</p>
                                            {
                                                data.options.map((option,id2) => {
                                                    if(option.link)
                                                        return <Link key={`fm${id}o${id2}`} to={option.url}>{option.label}</Link>

                                                    return <a key={`fm${id}o${id2}`} href={option.url}>{option.label}</a>
                                                })
                                            }
                                        </div>)
                }
                <div className={matches ? mobileClasses.footerMenuCont : desktopClasses.footerMenuCont} >
                    <p>{t('footer.followUs')}</p>
                    <div className={classes.actionsCont}>
                        <a href='https://www.kassista.com/#'><img src={Facebook} alt='Facebook' /></a>
                        <a href='https://www.kassista.com/#'><img src={Twitter} alt='Twitter' style={{margin: '0 12px'}} /></a>
                        <a href='https://www.kassista.com/#'><img src={Instagram} alt='Instagram' /></a>
                    </div>
                </div>
            </div>
            {/* <div className={classes.paymentCont} >
                <img src={PaymentVisa} alt='payment' />
                <img src={PaymentVisaElectron} alt='payment' />
                <img src={PaymentMasterCard} alt='payment' />
                <img src={PaymentMaestro} alt='payment' />
                <img src={PaymentPaypal} alt='payment' />
            </div> */}
        </footer>
    )
}

export default Footer