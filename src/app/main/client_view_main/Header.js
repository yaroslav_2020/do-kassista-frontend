import React , { useState , useEffect , Fragment } from 'react'
import { connect , useDispatch } from 'react-redux'
import { Link , useHistory } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import { useTranslation } from 'react-i18next'

import { logoutUser } from '../../auth/store/userSlice'

import {
    Drawer,
    useMediaQuery
} from '@material-ui/core'

import Logo from '../../assets/images/logo2.svg'
import UserHeader from '../../assets/images/icons/user-header.svg'
import MenuConnect from '../../assets/images/icons/menu-connect.svg'
import MenuPro from '../../assets/images/icons/menu-pro.svg'
import MenuFaq from '../../assets/images/icons/menu-faq.svg'
import MenuBlog from '../../assets/images/icons/menu-blog.svg'
import MenuContact from '../../assets/images/icons/menu-contact.svg'
import MenuLogout from '../../assets/images/icons/menu-logout.svg'
import MenuHamburger from '../../assets/images/icons/menu-hamburger.svg'

import UserIcon from 'app/assets/images/icons/user-icon.svg'
import UserIcon2 from 'app/assets/images/icons/user-header.svg'
import RequestsIcon from 'app/assets/images/icons/requests.svg'
import LocationIcon from 'app/assets/images/icons/location.svg'
import PadlockIcon from 'app/assets/images/icons/padlock.svg'
import Schedule from 'app/assets/images/icons/calendar.svg'
import LogoutIcon from 'app/assets/images/icons/logout.svg'
import WorkerPayments from 'app/assets/images/icons/menu-worker-payments.svg'

// const useStyles = makeStyles({
    
// })

const desktopStyles = makeStyles({
    header: {
        marginBottom: '45px',
        boxShadow: '0px 3px 6px rgba(0,0,0,.05)',
        minHeight: '115px',
        padding: '32px 100px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    logo: {
        width: '250px',
        height: '50px'
    },
    userButton: {
        marginLeft: '20px',
        borderRadius: '50px',
        boxShadow: '0px 3px 10px rgba(0,0,0,.10)',
        padding: '14px 25px',
        display: 'flex',
        fontSize: '14px',
        fontWeight: '500',
        color: '#2C2A2A',
        justifyContent: 'center',
        alignItems: 'center',
        '&>img': {
            marginRight: '15px'
        }
    },
    options: {
        display: 'flex',
        alignItems: 'center',
        '&>a': {
            fontSize: '14px',
            fontWeight: '500',
            color: '#2C2A2A',
            margin: '0 20px',
            textDecoration: 'none',
        },
        '&>a:hover': {
            textDecoration: 'none',
        }
    },
})

const mobileStyles = makeStyles({
    list: {
        '& .MuiPaper-root':{
            width: '250px',
        },
        '& .MuiPaper-root a': {
            fontSize: '18px',
            fontWeight: '500',
            color: '#2C2A2A',
            display: 'flex',
            marginTop: '30px',
            alignItems: 'center'
        },
        '& .MuiPaper-root a:nth-child(2)': {
            marginTop: '60px'
        },
        '& .MuiPaper-root a:hover': {
            textDecoration: 'none'
        },
        '& .MuiPaper-root a img': {
            marginLeft: '25px',
            marginRight: '8px',
        }
    },
    logo: {
        width: '170px',
        height: '30px'
    },
    userButton: {
        width: '180px',
        marginLeft: '30px',
        marginTop: '30px',
        borderRadius: '50px',
        boxShadow: '0px 3px 10px rgba(0,0,0,.10)',
        padding: '14px 25px',
        display: 'flex',
        fontSize: '14px',
        fontWeight: '500',
        color: '#2C2A2A',
        justifyContent: 'center',
        alignItems: 'center',
        '&>img': {
            marginRight: '15px'
        }
    },
    fullList: {
        width: 'auto',
    },
    header: {
        marginBottom: '30px',
        boxShadow: '0px 3px 6px rgba(0,0,0,.05)',
        minHeight: '80px',
        padding: '20px 36px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    options: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        '&>a': {
            fontSize: '14px',
            fontWeight: '500',
            color: '#2C2A2A',
            margin: '0 20px',
            textDecoration: 'none',
        },
        '&>a:hover': {
            textDecoration: 'none',
        }
    },
    hamburger: {
        width: '14px',
        height: '14px'
    }
})

const Header = props => {
    
    // const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const dispatch = useDispatch()

    const {t} = useTranslation()

    const matches = useMediaQuery('(max-width:600px)')

    // const [link,setLink] = useState(props.user.data.iri === '' ? '/offer/login' : (matches ? '/offer/myAccount' : '/client/info'))
    const [link,setLink] = useState(props.user.data.iri === '' ? '/offer/login' : '/client/info')

    const [open,setOpen] = useState(false)

    useEffect(() => {
        setLink(props.user.data.iri === '' ? '/offer/login' : '/client/info')
    },[props.user.data.iri,matches])

    const history = useHistory()
    
    return (
        <div className={matches ? mobileClasses.header : desktopClasses.header} id='headerToScroll'>
            <Link to='/offer'><img src={Logo} className={matches ? mobileClasses.logo : desktopClasses.logo} alt='kassista' /></Link>
            <div className={matches ? mobileClasses.options : desktopClasses.options}>
                {
                    //  DESKTOP NAVBAR OPTIONS
                    !matches && <Fragment>
                                    <Link to='/offer'>Connect</Link>
                                        <a href='https://kassista.com/pro'>Pro</a>
                                        <a href='https://kassista.com/blog'>Blog</a>
                                        <a href='https://kassista.com/contacto'>Contacto</a>
                                        <a href='https://kassista.com/faq'>FAQ</a>
                                        <button 
                                            className={desktopClasses.userButton}
                                            onClick={() => history.push(link)}
                                        >
                                            <img src={UserHeader} alt='user' />
                                            {
                                                props.user.data.iri !== ''
                                                    ?   props.user.data.displayName
                                                    :   t('login.title')
                                            }
                                        </button>
                                </Fragment>
                }
                {
                    //  MOBILE NAVBAR OPTIONS
                    matches && <Fragment>
                                    <button onClick={() => setOpen(true)}>
                                        <img src={MenuHamburger} className={mobileClasses.hamburger} alt='menu' />
                                    </button>
                                    <Drawer anchor='right' open={open} className={mobileClasses.list} onClose={() => setOpen(false)} >
                                        <button 
                                            className={mobileClasses.userButton}
                                            onClick={() => {
                                                setOpen(false)
                                                history.push(link)
                                            }}
                                        >
                                            <img src={UserHeader} alt='user' />
                                            {
                                                props.user.data.iri !== ''
                                                    ?   props.user.data.displayName
                                                    :   t('login.title')
                                            }
                                        </button>
                                        {
                                            props.role === 'ROLE_CLIENT' ?
                                            <>
                                                <Link to='/offer' onClick={() => setOpen(false)} ><img src={MenuConnect} alt='menu-option'/>Connect</Link>
                                                <Link to='/client/info' onClick={() => setOpen(false)} ><img src={UserIcon} alt='menu-option'/>{t('columnMenu.settings.personalInformation')}</Link>
                                                <Link to='/offer/list' onClick={() => setOpen(false)} ><img src={RequestsIcon} alt='menu-option'/>{t('columnMenu.myRequests')}</Link>
                                                <Link to='/client/address' onClick={() => setOpen(false)} ><img src={LocationIcon} alt='menu-option'/>{t('columnMenu.myAddresses')}</Link>
                                                <Link to='/client/payment-methods' onClick={() => setOpen(false)} ><img src={WorkerPayments} alt='menu-option'/>{t('columnMenu.myPaymentMethods')}</Link>
                                                <Link to='/client/change/password' onClick={() => setOpen(false)} ><img src={PadlockIcon} alt='menu-option'/>{t('columnMenu.settings.changePassword')}</Link>                                    
                                            </> : props.role === 'ROLE_INDEPENDENT_WORKER' || props.role === 'ROLE_EMPLOYEE_WORKER' ?
                                            <>
                                                <Link to='/client/info' onClick={() => setOpen(false)} ><img src={UserIcon} alt='menu-option'/>{t('columnMenu.settings.personalInformation')}</Link>
                                                <Link to='/offer/worker/list' onClick={() => setOpen(false)} ><img src={RequestsIcon} alt='menu-option'/>{t('columnMenu.myRequests')}</Link>
                                                <Link to='/worker/schedule' onClick={() => setOpen(false)} ><img src={Schedule} alt='menu-option'/>{t('columnMenu.settings.workerSchedule')}</Link>
                                                <Link to='/client/change/password' onClick={() => setOpen(false)} ><img src={PadlockIcon} alt='menu-option'/>{t('columnMenu.settings.changePassword')}</Link>
                                            </> :
                                                <>
                                                    <a href='https://kassista.com/pro'><img src={MenuPro} alt='menu-option'/>Pro</a>
                                                    <a href='https://kassista.com/blog'><img src={MenuBlog} alt='menu-option'/>Blog</a>
                                                </>                                         
                                        }
                                        <a href='https://kassista.com/faq'><img src={MenuFaq} alt='menu-option'/>FAQ</a>
                                        <a href='https://kassista.com/contacto'><img src={MenuContact} alt='menu-option'/>Contacto</a>
                                        <Link onClick={() => dispatch(logoutUser())} ><img src={LogoutIcon} alt='menu-option'/>{t('buttons.logout')}</Link>
                                    </Drawer>
                                </Fragment>
                }
            </div>
        </div>
    )
}

const mapState = state => ({
    user: state.auth.user,
    role: state.auth.user.role[0]
})

export default connect(mapState,{})(Header)
