import React,{ useEffect } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import { default as userRoles } from 'app/auth/store/types/UserRoles'

import NotLoggedIn from './partials/NotLoggedIn'
import UserLoggedInMenu  from './partials/UserLoggedInMenu'
import FaqSection from './partials/FaqSection'
import FaqOfferDetails from './partials/FaqOfferDetails'

import {
    Grid,
} from '@material-ui/core'

const FaqColumn = props => {

    useEffect(() => {
        if(props.user.role.length){
            const useOtherLogin = props.user.role.indexOf(userRoles.ROLE_COMERCIAL) > -1
                                 || props.user.role.indexOf(userRoles.ROLE_ADMIN) > -1
                                 || props.user.role.indexOf(userRoles.ROLE_SUPER_ADMIN) > -1
            // if(useOtherLogin){
            //     localStorage.removeItem('jwt_access_token')                
            // }
        }
    },[props.user])

    return (
        <Grid item xs={12} md={4} sm={6}>
            {/* {
                props.user.data.displayName === ''
                    ? <NotLoggedIn />
                    : <UserLoggedInMenu />
            }
            <FaqOfferDetails />
            <FaqSection /> */}
            {
                props.children
            }
        </Grid>
    ) 
}

const mapState = state => ({
    user: state.auth.user
})

export default connect(mapState,{})(FaqColumn)