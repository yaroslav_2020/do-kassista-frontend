import React,{ useState } from 'react'
import { useTranslation } from 'react-i18next'

import LoginModal from '../../utils/LoginModal'
import RegisterModal from '../../utils/RegisterModal'

import {
    Box,
    Card,
    CardContent,
    Grid,
    Button
} from '@material-ui/core'

const NotLoggedIn = props => {
    const {t} = useTranslation()

    const [openLogin, setOpenLogin] = useState(false)
    const [openRegister, setOpenRegister] = useState(false)    

    const handleOpenLogin = btn => {
        setOpenLogin(true)
    }

    const handleCloseLogin = btn => {
        setOpenLogin(false)
    }

    const handleOpenRegister = btn => {
        setOpenRegister(true)
    }

    const handleCloseRegister = btn => {
        setOpenRegister(false)
    }

    return (
        <Box mt={2} mb={2} mr={2}>
            <Card>
                <CardContent>
                    <LoginModal open={openLogin} setOpen={setOpenLogin} handleClose={handleCloseLogin} />
                    <RegisterModal open={openRegister} setOpen={setOpenRegister} handleClose={handleCloseRegister} />
                    <Grid
                        container
                        direction='row'
                        justify='space-around'
                        alignItems='center'
                        spacing={0}
                    >
                        <Grid item align='center' xs={12} md={6} sm={12} >
                            <Button variant='outlined' onClick={handleOpenLogin} size='large' color='primary' >
                                {t('buttons.login')}
                            </Button>
                        </Grid>
                        <Grid item align='center' xs={12} md={6} sm={12} >
                            <Button variant='outlined' onClick={handleOpenRegister} size='large' color='primary' >
                                {t('buttons.register')}
                            </Button>
                        </Grid>
                    </Grid>
                </CardContent>
            </Card>
        </Box>
    )
}

export default NotLoggedIn