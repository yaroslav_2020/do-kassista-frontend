import React,{ useState } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import * as CompanyActions from 'app/store/actions/CompanyActions'
import * as AddressActions from 'app/store/actions/AddressActions'

import AccordionMenu from '../../utils/AccordionMenu'

import {
    Typography,
    useMediaQuery
} from '@material-ui/core'

import UserImg from 'app/assets/images/icons/user-header.svg'

import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
    container: {
        borderRadius: '13px',
        background: '#fff',
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        boxShadow: '0px 5px 10px rgba(0,0,0,.08)'
    },
    section: {
        borderBottom: '1px solid #EBEBEB',
        width: '100%',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexWrap: 'wrap'
    },
    titleCont: {
        borderBottom: '1px solid #EBEBEB',
        width: '100%',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        padding: '30px 25px',
        flexWrap: 'nowrap !important'
    },
    img: {
        width: '50px',
        borderRadius: '100%',
        marginRight: '15px',
    },
    accordionCont: {
        width: '100%',
        padding: '20px 0px'
    }
})

const desktopStyles = makeStyles({
    title: {
        fontSize: '18px',
        fontWeight: 'bold',
        color: '#242424',
        textAlign: 'center',
    },

})

const mobileStyles = makeStyles({
    title: {
        fontSize: '21px',
        fontWeight: 'bold',
        color: '#242424',
        textAlign: 'center',
    },
})

const UserLoggedInMenu = props => {

    const {t} = useTranslation()

    const [expanded,setExpanded] = useState('panel1')

    const matches = useMediaQuery('(max-width:600px)')

    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const menu = [
        {
            name: 'panel1',
            title: t('columnMenu.settings.personalInformation'),
            icon: 'person',
            link: '/client/info',
        },
        {
            name: 'panel2',
            title: t('columnMenu.myRequests'),
            icon: 'requests',
            link: '/offer/list'
        },
        {
            name: 'panel3',
            title: t('columnMenu.myAddresses'),
            icon: 'location',
            link: '/client/address'
        },
        {
            name: 'panel4',
            title: t('columnMenu.myPaymentMethods'),
            icon: 'payments',
            link: '/client/payment-methods',
        },
        {
            name: 'panel5',
            title: t('columnMenu.settings.changePassword'),
            icon: 'padlock',
            link: '/client/change/password'
        },
        {
            name: 'panel6',
            title: t('buttons.logout'),
            icon: 'logout',
            logOut: true
        },
    ]

    const workerMenu = [
        {
            name: 'panel1',
            title: t('columnMenu.settings.personalInformation'),
            icon: 'person',
            link: '/client/info',
        },
        {
            name: 'panel2',
            title: t('columnMenu.myRequests'),
            icon: 'requests',
            link: '/offer/worker/list'
        },
        {
            name: 'panel2',
            title: t('columnMenu.settings.workerSchedule'),
            icon: 'schedule',
            link: '/worker/schedule'
        },        
        {
            name: 'panel4',
            title: t('columnMenu.settings.changePassword'),
            icon: 'padlock',
            link: '/client/change/password'
        },
        {
            name: 'panel4',
            title: t('buttons.logout'),
            icon: 'logout',
            logOut: true
        },
    ]

    const handleChange = panel => (event, newExpanded) => {
        setExpanded(newExpanded ? panel : false);
    }

    return (
        <div className={classes.container} >            
            <div className={classes.titleCont} >
                <img 
                    src={UserImg} 
                    className={classes.img}
                    alt='user'
                />
                <Typography className={matches ? mobileClasses.title : desktopClasses.title} align='left'>
                    {`${t('general.welcome')} ${props.user}`}
                </Typography>
            </div>
            <div className={classes.accordionCont} >
                {   
                    props.role !== 'ROLE_INDEPENDENT_WORKER' && props.role !== 'ROLE_EMPLOYEE_WORKER'
                        ?   menu.map(m => <AccordionMenu 
                                                key={menu.indexOf(m)}
                                                {...m}
                                            />)
                        :   workerMenu.map(m => <AccordionMenu 
                                                    key={menu.indexOf(m)}
                                                    {...m}
                                                />)
                }
            </div>
        </div>
    )
}

const mapState = state => ({
    user: state.auth.user.data.displayName,
    role: state.auth.user.role[0]
})

const mapDispatch = {
    ...CompanyActions,
    ...AddressActions
}

export default connect(mapState,mapDispatch)(UserLoggedInMenu)