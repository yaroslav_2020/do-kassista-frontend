import React,{ useState , useEffect , Fragment } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'
import moment from 'moment'

import { VoucherVerify } from 'app/utils/VerifyVoucher'
import * as FetchActions from 'app/store/actions/FetchActions'
import * as OfferActions from 'app/store/actions/OfferActions'

import GenericSnackbar from 'app/utils/GenericSnackbar'
import HighlightOffIcon from '@material-ui/icons/HighlightOff';

import {
    Box,
    Typography,
    // Grid,
    TextField,
    Button,
    IconButton
} from '@material-ui/core'



// import LocationOnIcon from '@material-ui/icons/LocationOn'
// import DateRangeIcon from '@material-ui/icons/DateRange'
// import QueryBuilderIcon from '@material-ui/icons/QueryBuilder'

import LocationIcon from 'app/assets/images/icons/location.svg'
import HomeIcon from 'app/assets/images/icons/home.svg'
import ClockIcon from 'app/assets/images/icons/clock.svg'
import CalendarIcon from 'app/assets/images/icons/calendar.svg'
import FaqIcon from '../../utils/FaqIcon'

import { makeStyles } from '@material-ui/core/styles'
import { Alert } from '@material-ui/lab'

const useStyles = makeStyles({
    container: {
        borderRadius: '13px',
        background: '#fff',
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        boxShadow: '0px 5px 10px rgba(0,0,0,.08)'
    },
    section: {
        padding: '30px 0',
        borderBottom: '1px solid #EBEBEB',
        width: '100%'
    },
    infoSection: {
        padding: '30px 25px',
        borderBottom: '1px solid #EBEBEB',
        width: '100%',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexWrap: 'wrap'
    },
    voucherAlert: {
        paddingTop: '20px',
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap',
        textAlign: 'center'

    },
    icon: {
        color: '#000',
        marginRight: '15px',
        alignSelf: 'flex-start',
    },
    totalsCont:{
        width: '100%'
    },
    totalLabel: {
        fontSize: '18px',
        color: '#242424',
        fontWeight: '400'
    },
    frecuencyLabelsCont: {
        width: '90%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent:'flex-start', 
        alignItems: 'flex-start'
    },
    priceCont: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '12px',
        marginBottom: '18px'
    },
    voucherCont: {
        width: '100%',
        marginTop: '20px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
})

// const desktopStyles = makeStyles({

// })

const mobileStyles = makeStyles({
    title: {
        fontSize: '21px',
        fontWeight: 'bold',
        color: '#242424',
        textAlign: 'center'
    },
    infoText: {
        fontSize: '15px',
        color: '#242424',
    },
    totalText: {
        fontSize: '16px',
        fontWeight: '400',
        color: '#242424',
        textAlign: 'center'
    },
    totalLabelCont: {
        width: '100%',
        marginTop: '20px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    totalServiceLabelCont: {
        width: '100%',
        marginTop: '12px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    totalLabel: {
        fontSize: '18px',
        color: '#242424',
        fontWeight: 'bold'
    },
    totalServiceLabel: {
        fontSize: '14px',
        color: '#242424',
        fontWeight: '400'
    },
    totalSmallText: {
        fontSize: '13px',
        fontWeight: '400',
        textAlign: 'center',
        color: '#242424',
        marginBottom: '20px'
    },
    spanText: {
        fontSize: '13px',
        fontWeight: 'bold',
        color: '#0097F6',
    },
    textField: {
        width: '60%',
        fontSize: '14px',
        '& fieldset': {            
            border: '1px solid #EBEBEB',
            borderRadius: '4px 0 0 4px',
        },
        '& .MuiInputBase-root:hover fieldset': {
            border: '1px solid #EBEBEB',
        },
        '& .MuiInputBase-root:focus fieldset': {
            border: '1px solid #EBEBEB',
        }
    },
    voucherButton: {
        height: "55px",
        boxShadow: "none",
        borderRadius: "0 4px 4px 0",
        background: "#0097F6",
        color: "#fff",
        padding: '12px',
        fontSize: "16px",
        minWidth: '150px',
        width: '40%',
        '&:hover': {
            boxShadow: 'none',
            background: '#0097F6'
        },
    },
    priceButton: {
        border: "1px solid #0097F6",
        width: "45px",
        height: "45px",
        borderRadius: "5px",
        color: "#000",
        fontSize: "27px",
        fontWeight: "bold",
    },
    priceLabel: {
        width: "90px",
        height: "45px",
        border: "1px solid #0097F6",
        borderRadius: "5px",
        margin: "0 7px",
        color: "#000",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        fontWeight: "bold",
        fontSize: "17px",
        padding: "0 10px",
    },
    spanDays: {
        fontWeight: 'bold'
    }

})

const FaqOfferDetails = props => {

    const {t} = useTranslation()

    const [snackbar,setSnackbar] = useState({
        open: false,
        message: '', 
        severity: 'success',
    })

    const handleCloseSnackbar = value => setSnackbar(value)

    const classes = useStyles()
    const mobileClasses = mobileStyles()

    const [voucher,setVoucher] = useState({
        code: '',
        response: '',
        type: '',
        discount: 0
    })

    const [showVoucherDescriptions, setShowVoucherDescriptions] = useState(false)

    const [totalDays, setTotalDays] = useState(0)

    const [voucherApplied, setVoucherApplied] = useState({
        code: '',
        subtotal: 0,
        discountApplied: 0
    })

    const days = [
        'sunday',
        'monday',
        'tuesday',
        'wednesday',
        'thursday',
        'friday',
        'saturday'
    ]

    useEffect(() => {
        props.SaveStep1({
            pricePerHour: Math.round((parseFloat(props.minPricePerHour) + parseFloat(props.maxPricePerHour) * 0.25))
        })
    },[props.minPricePerHour])

    useEffect(() => {
        let pricePerDay = 0

        let subtotal = 0
        let totalPerService = 0
        let totalDays = 0
        
        let discountApplied = 0

        if(
            (props.offer.offerBuilding.roomNumber > 0 ||
            props.offer.offerBuilding.bathRoomNumber > 0 ||
            props.offer.offerBuilding.kitchenNumber > 0 ||
            props.offer.offerBuilding.livingNumber > 0) &&
            ((props.offer.period === 'several_times_a_week' &&
            Object.values(props.offer.periodPoints).filter(day => day.checked).length > 0) ||
            props.offer.period !== 'several_times_a_week')
        ){
            let date1 = moment(props.offer.startFromDate)
            let date2 = moment(props.offer.endDate)
    
            date2.add(1,'days')
    
            let daysCount = {
                sunday: 0,
                monday: 0,
                tuesday: 0,
                wednesday: 0,
                thursday: 0,
                friday: 0,
                saturday: 0
            }
    
            for (var now = date1; now.isBefore(date2); now.add(1, 'days')) {
                daysCount[days[now.day()]]++
            }
    
            pricePerDay = Math.round(props.offer.hoursNeeded * props.multiplicatorPrice * 100)/100
    
            Object.values(props.offer.pointServices).map(service => {
                if(service.checked){
                    pricePerDay += service.value
                }
            })
    
            if(props.offer.cleanningTools === 'CLEANNING_TOOLS_YES')
                pricePerDay += props.settings.price_per_cleanning_tools
    
            Object.entries(props.offer.periodPoints).map((day) => {
                if(day[1].checked){
                    subtotal += Math.round(pricePerDay * daysCount[day[0]] * 100)/100
                    totalDays += daysCount[day[0]]                    
                }
            })             

            setTotalDays(totalDays)

            if (    voucher.discount > 0 && 
                    pricePerDay > 0
            ) {
                if (voucher.type === 'fixed') {
                    discountApplied = voucher.discount

                    if ( (subtotal - discountApplied) < 0 ) {
                        totalPerService = 0;
                    } else {
                        totalPerService = subtotal - discountApplied;
                    }                    
                } else if (voucher.type === 'percent') {
                    discountApplied = subtotal*(voucher.discount / 100)

                    totalPerService = subtotal - discountApplied
                }

                setVoucherApplied({
                    ...voucherApplied,
                    subtotal,
                    discountApplied
                })
                pricePerDay = totalPerService / totalDays

            }
            
            if(subtotal === 0)
                pricePerDay = 0
        }
        props.SaveStep1({
            pricePerDay,
            pricePerService: totalPerService || subtotal,            
        })        
    },[
        props.offer.offerBuilding,
        props.offer.period,
        props.offer.periodPoints,
        props.offer.startFromDate,
        props.offer.endDate,
        props.offer.hoursNeeded,
        props.offer.pointServices,
        props.offer.cleanningTools,
        props.multiplicatorPrice,
        voucher.discount
    ])

    const period_detail = () => {
        return t(`Step1Frequency.period.${props.offer.period}`)
    }

    const handleChange = input => {
        setVoucher({
            ...voucher,
            [input.target.name]: input.target.value
        })
    }

    const deleteVoucher = (e) => {
        e.preventDefault();

        props.SaveStep1({
            voucherApplied: {},
            vouchers: []
        })

        setShowVoucherDescriptions(false)
        
        setVoucherApplied({
            code: '',
            subtotal: 0,
            discountApplied: 0
        })

        setVoucher({
            code: '',
            response: '',
            type: '',
            discount: 0
        })        

    }

    const handleClick = async btn => {
        if(voucher.code !== '' && voucherApplied.code === ''){
            const verify = await VoucherVerify(voucher.code)            
                        
            props.fetching()                         
            if(!verify.result){
                props.success()

                if(!verify.VoucherVerify) {
                    setSnackbar({
                        open: true,
                        message: t('voucher.apiResponse.notAvailable'), 
                        severity: 'warning',
                    })
    
                    props.success()
    
                } else if(verify.UserLoggedIn){
                    if(verify.UserOwner && verify.VoucherVerify.readyToBeUsed){
                        setVoucher({
                            ...voucher,
                            response: t('voucher.apiResponse.available'),
                            discount: verify.VoucherVerify.discount,
                            type: verify.VoucherVerify.type
                        })

                        props.SaveStep1({
                            voucherApplied: verify.VoucherVerify,
                            vouchers: [`/api/vouchers/${verify.VoucherVerify.id}`]
                        })

                        setShowVoucherDescriptions(true)

                        setSnackbar({
                            open: true,
                            message: t('voucher.apiResponse.available'), 
                            severity: 'success',
                        })
                    }
                        
                    if(!verify.UserOwner && verify.VoucherVerify.readyToBeUsed){
                        setVoucher({
                            ...voucher,
                            response: t('voucher.apiResponse.available'),
                            discount: verify.VoucherVerify.discount,
                            type: verify.VoucherVerify.type
                        }) 

                        props.SaveStep1({
                            voucherApplied: verify.VoucherVerify,
                            vouchers: [`/api/vouchers/${verify.VoucherVerify.id}`]
                        })

                        setShowVoucherDescriptions(true)

                        setSnackbar({
                            open: true,
                            message: t('voucher.apiResponse.available'), 
                            severity: 'success',
                        })
                    }

                    setVoucherApplied((v) => ({
                        ...v,
                        code: voucher.code               
                    }))
                }else{

                    if((!verify.VoucherVerify.user) && verify.VoucherVerify.readyToBeUsed){

                        setVoucher({
                            ...voucher,
                            response: t('voucher.apiResponse.available'),
                            discount: verify.VoucherVerify.discount,
                            type: verify.VoucherVerify.type
                        }) 

                        props.SaveStep1({
                            voucherApplied: verify.VoucherVerify,
                            vouchers: [`/api/vouchers/${verify.VoucherVerify.id}`]
                        })

                        setShowVoucherDescriptions(true)

                        setSnackbar({
                            open: true,
                            message: t('voucher.apiResponse.available'), 
                            severity: 'success',
                        })

                        setVoucherApplied((v) => ({
                            ...v,
                            code: voucher.code               
                        }))

                    }else{                        
                        setVoucher({
                            ...voucher,
                            response: t('voucher.apiResponse.notAvailable')
                        })
                        setSnackbar({
                            open: true,
                            message: t('voucher.apiResponse.notAvailable'), 
                            severity: 'warning',
                        })
                    }                    
                }  

            }else{
                props.success()
                setVoucher({
                    ...voucher,
                    response: t(`voucher.apiResponse.${verify.translation_response}`)
                })
                setSnackbar({
                    open: true,
                    message: t(`voucher.apiResponse.${verify.translation_response}`), 
                    severity: (verify.translation_response === 'notFound') ? 'error' : 'warning',
                })
            }
            
        }else if( voucher.code === '') {
            setSnackbar({
                open: true,
                message: t('voucher.apiResponse.isEmpty'), 
                severity: 'warning',
            })            
        }else if ((voucherApplied.code === voucher.code) && voucher.code !== '') {
            setSnackbar({
                open: true,
                message: t('voucher.apiResponse.alreadyApplied'), 
                severity: 'warning',
            })
        }else{
            setSnackbar({
                open: true,
                message: t('voucher.apiResponse.notFound'), 
                severity: 'error',
            })
        }
    }

    const handleMultiplicator = value => {
        let newMultiplicator = Math.round((props.multiplicatorPrice + value) * 100)/100
        if(newMultiplicator >= props.minPricePerHour && newMultiplicator <= props.maxPricePerHour){
            props.SaveStep1({
                pricePerHour: Math.round((props.multiplicatorPrice + value) * 100)/100
            })
        }
    }

    return (
        <Box mb={2}>
            <GenericSnackbar 
                handleClose={handleCloseSnackbar} 
                {...snackbar} 
            />
            <div className={classes.container} >     
                <div className={classes.section}>
                    <Typography variant='h5' className={mobileClasses.title} gutterBottom>
                        {t('columnOfferDetails.title')}
                    </Typography>
                </div>    
                <div className={classes.infoSection} style={{borderBottom: '0'}}>
                    <img src={LocationIcon} className={classes.icon} alt='location' />
                    <Typography variant='body1' className={mobileClasses.infoText} gutterBottom>
                        20390 - Madrid
                    </Typography>   
                </div>   
                <div className={classes.infoSection}>
                    <img src={HomeIcon} className={classes.icon} alt='location' />
                    <div>
                        <Typography variant='body1' className={mobileClasses.infoText} gutterBottom>
                                    { period_detail() }
                        </Typography>
                        <Typography variant='body1' className={mobileClasses.infoText} gutterBottom>
                            { `${props.offer.offerBuilding.roomNumber} ${t('offerBuilding.room')}` }
                        </Typography>
                        <Typography variant='body1' className={mobileClasses.infoText} gutterBottom>
                            { `${props.offer.offerBuilding.bathRoomNumber} ${t('offerBuilding.bathroom')}` }
                        </Typography>
                        <Typography variant='body1' className={mobileClasses.infoText} gutterBottom>
                            { `${props.offer.offerBuilding.kitchenNumber} ${t('offerBuilding.kitchen')}` }
                        </Typography>
                        <Typography variant='body1' className={mobileClasses.infoText} gutterBottom>
                            { `${props.offer.offerBuilding.livingNumber} ${t('offerBuilding.living')}` }
                        </Typography>
                    </div> 
                </div>     
                <div className={classes.infoSection} style={{borderBottom: '0', flexWrap: 'nowrap'}}>
                    <img src={ClockIcon} className={classes.icon} alt='location' />
                    <div className={classes.frecuencyLabelsCont}>
                        <Typography variant='subtitle1' align='left' className={mobileClasses.title} gutterBottom>
                            { `${t('general.frequency')}:` }
                        </Typography>
                        <Typography variant='subtitle1' className={mobileClasses.infoText} gutterBottom>
                            { `${period_detail()}` }
                    </Typography>
                    </div>
                </div>           
                <div className={classes.infoSection} >
                    {
                        Object.values(props.offer.periodPoints).filter(pp => pp.checked).map((pp,id) => <div key={`fqodpp${id}`} style={{width: '100%'}} >
                                                                                                            <table 
                                                                                                                style={{minWidth: '100%', marginTop: '15px'}}
                                                                                                                className={mobileClasses.infoText}
                                                                                                            >
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td colSpan={3}><span className={mobileClasses.spanDays}>{t(`Schedule.days.${pp.day.toLowerCase()}`)}</span></td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td>{`${t('columnOfferDetails.start')}: ${props.offer.startHour}`}</td>
                                                                                                                        <td>{`${t('columnOfferDetails.hours')}: ${props.offer.hoursNeeded}`}</td>
                                                                                                                        <td>{`${t('columnOfferDetails.end')}: ${props.offer.endHour}`}</td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </div>)
                    }
                </div>
                {
                    Object.values(props.offer.pointServices).length
                        ? Object.values(props.offer.pointServices).map(
                            (service,id) => service.checked && <div key={`fqods${id}`} className={classes.infoSection}>
                                                                    <FaqIcon className={ classes.icon } color='primary' fontSize="large" icon={service.icon}/>
                                                                    <Typography variant='body1' className={mobileClasses.infoText} gutterBottom>
                                                                        { service.name } <br />
                                                                        {
                                                                            service.days.map(day => <Fragment key={service.days.indexOf(day)} >{t(`Schedule.days.${day.toLowerCase()}`)} <br/></Fragment>)
                                                                        }
                                                                    </Typography> 
                                                                </div>)
                        : <div />
                }

                <div className={classes.infoSection} >
                    <img src={CalendarIcon} className={classes.icon} alt='location' />
                    <Typography variant='body1' className={mobileClasses.infoText} gutterBottom>
                        { `${props.hoursNeeded} ${t('columnOfferDetails.hours')} / ${ totalDays } ${t('general.days')}` }
                    </Typography>
                </div>
                <div className={classes.infoSection} style={{borderBottom: 'none'}} >
                    <div>
                        <p className={mobileClasses.totalText}>{t('faqSection.choosePricePerHour')}</p>
                        <div className={classes.priceCont}>
                            <button className={mobileClasses.priceButton} onClick={() => handleMultiplicator(-0.5)}>-</button>
                            <p className={mobileClasses.priceLabel}>{props.multiplicatorPrice} &euro;</p>
                            <button className={mobileClasses.priceButton} onClick={() => handleMultiplicator(0.5)}>+</button>
                        </div>
                        <p className={mobileClasses.totalSmallText}><span className={mobileClasses.spanText}>{t('Advices.probability100')}</span> {t('Advices.acceptOfferPrice')}</p>
                    </div>
                    <div className={classes.totalsCont} >                        
                        {
                            showVoucherDescriptions &&
                            <>
                                <div className={mobileClasses.totalServiceLabelCont}>
                                    <p className={mobileClasses.totalServiceLabel}>{t('general.subTotal')}</p>
                                    <p className={mobileClasses.totalServiceLabel}>{voucherApplied.subtotal} &euro;</p>
                                </div>

                                <div className={mobileClasses.totalServiceLabelCont}>
                                    <p className={mobileClasses.totalServiceLabel}>{t('general.vouchersValue')}</p>
                                    <p className={mobileClasses.totalServiceLabel}>- {voucherApplied.discountApplied} &euro;</p>
                                </div>              
                                
                                <div className={mobileClasses.totalServiceLabelCont}>
                                    <p className={mobileClasses.totalServiceLabel}>{t('general.disableVoucher')}</p>
                                    <IconButton edge="end" aria-label="delete">
                                        <HighlightOffIcon color="error" onClick={ deleteVoucher } />
                                    </IconButton>                                                
                                </div>  

                            </>
                        }

                        <div className={mobileClasses.totalLabelCont}>
                            <p className={mobileClasses.totalLabel}>{t('general.totalPerDay')}</p>
                            <p className={mobileClasses.totalLabel}>{props.offer.pricePerDay} &euro;</p>
                        </div>

                        <div className={mobileClasses.totalServiceLabelCont}>
                            <p className={mobileClasses.totalServiceLabel}>{t('general.totalPerService')}</p>
                            <p className={mobileClasses.totalServiceLabel}>{props.offer.pricePerService} &euro;</p>
                        </div>
                    </div>
                    <div className={classes.voucherCont} >
                        <TextField 
                            type='text'
                            label={t('voucher.code')}
                            variant='outlined'
                            name='code'
                            value={voucher.code}
                            onChange={handleChange}
                            className={mobileClasses.textField}
                            inputProps={{
                                style: {
                                    padding: '19px 8px',
                                }
                            }}
                            disabled={showVoucherDescriptions}
                        />
                        <Button
                            variant='contained'
                            onClick={handleClick}
                            className={mobileClasses.voucherButton}
                            disabled={showVoucherDescriptions}
                        >
                            {t('buttons.useVoucher')}
                        </Button> 
                    </div>

                    {
                        snackbar.open &&
                        <div className={classes.voucherAlert}>
                            <Alert 
                                severity={snackbar.severity}
                                anchorOrigin={'top', 'center'}
                            >   
                                    {snackbar.message}
                            </Alert>
                        </div>

                    }
                </div>
            </div>
        </Box>
    )
}

const mapState = state => ({
    offer: state.Offer,
    multiplicatorPrice: state.Offer.pricePerHour,   
    hoursNeeded: state.Offer.hoursNeeded,
    minPricePerHour: state.Settings.min_price_per_hour, 
    maxPricePerHour: state.Settings.max_price_per_hour, 
    settings: state.Settings
})

const mapDispatch = {
    ...FetchActions,
    ...OfferActions
}

export default connect(mapState,mapDispatch)(FaqOfferDetails)