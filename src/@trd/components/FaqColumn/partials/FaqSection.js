import React,{ useState,useEffect } from 'react'
import { useTranslation } from 'react-i18next'

import FaqAccordion from '../../utils/FaqAccordion'

import { getFaqs } from 'app/utils/FaqActions'

import {
    Box,
    Typography,
    useMediaQuery
} from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles'

import QuestionMarkIcon from 'app/assets/images/icons/question-mark.svg'

const useStyles = makeStyles({
    container: {
        borderRadius: '13px',
        background: '#fff',
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',        
        boxShadow: '0px 5px 10px rgba(0,0,0,.08)',
    },
    section: {
        borderBottom: '1px solid #EBEBEB',
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexWrap: 'wrap',
    },
})

const desktopStyles = makeStyles({
    title: {
        fontSize: '18px',
        fontWeight: 'bold',
        color: '#242424',
        textAlign: 'center',
    },

})

const mobileStyles = makeStyles({
    title: {
        fontSize: '21px',
        fontWeight: 'bold',
        color: '#242424',
        textAlign: 'center',
    },
})

const FaqSection = props => {
    const {t} = useTranslation()

    const [expanded,setExpanded] = useState('')
    const [faqs,setFaqs] = useState([])

    const matches = useMediaQuery('(max-width:600px)')

    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    useEffect(() => {
        getFaqs()
            .then(res => {
                let faq = res.map(f => ({
                    question: f.question,
                    id: f.id,
                    answer: f.answer
                }))
                setFaqs([...faq])
            })
            .catch(error => console.error(error))
    },[])

    const handleChange = panel => (event, newExpanded) => {
        if(panel === expanded){
            setExpanded('')
        }else{
            setExpanded(panel)
        }
    }

    return (
        <Box mb={2}>
            <div className={classes.container}>
                <div className={classes.section} style={{padding: '30px 25px'}}> 
                    <Typography className={matches ? mobileClasses.title : desktopClasses.title} align='center'>
                        {t('faqSection.title')}
                    </Typography>
                    <img src={QuestionMarkIcon} alt='questionMark' />
                </div>
                <div  className={classes.section} style={{paddingBottom: '25px'}}>
                    {
                        faqs.length
                            ? faqs.map(faq => <FaqAccordion 
                                                    key={faqs.indexOf(faq)}                        
                                                    question={faq.question}
                                                    answer={faq.answer}
                                                    expanded={expanded}
                                                    handleChange={handleChange}
                                                    name={`panel${faqs.indexOf(faq)}`}
                                                    borderBottom={faqs.indexOf(faq) < (faqs.length - 1)}
                                                />)
                            : <div />
                    }
                </div>                
            </div>
        </Box>
    )
}

export default FaqSection