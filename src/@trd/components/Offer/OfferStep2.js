import React, { Fragment, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';
// import { Link } from 'react-router-dom'
import clsx from 'clsx';

import * as OfferActions from 'app/store/actions/OfferActions';

import Step2AddAddress from './partials/Step2AddAddress';
// import Step2AddCompany from './partials/Step2AddCompany'
import * as Address from 'app/utils/Address';
import * as Company from 'app/utils/Company';
import * as User from 'app/utils/User';
import * as UserAdminPanel from 'app/utils/UserAdminPanel';

// import NotLoggedIn from '../FaqColumn/partials/NotLoggedIn'

import LoginStep2 from './partials/LoginStep2';

import GenericSnackbar from 'app/utils/GenericSnackbar';

import {
	Box,
	Card,
	CardContent,
	Grid,
	Typography,
	Button,
	FormControl,
	Select,
	MenuItem,
	// RadioGroup,
	// FormControlLabel,
	Radio,
	useMediaQuery,
	Checkbox
} from '@material-ui/core';

import { makeStyles, withStyles } from '@material-ui/core/styles';

import checkboxIcon from 'app/assets/images/icons/checkbox-icon.svg';
// import { divide } from 'lodash'

const useStyles = makeStyles(theme => ({
	checkRoot: {
		'&:hover': {
			backgroundColor: 'transparent'
		}
	},
	icon: {
		borderRadius: 3,
		width: 16,
		height: 16,
		boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
		backgroundColor: '#fff',
		backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
		'$root.Mui-focusVisible &': {
			outline: '2px auto rgba(19,124,189,.6)',
			outlineOffset: 2
		},
		'input:hover ~ &': {
			backgroundColor: '#ebf1f5'
		},
		'input:disabled ~ &': {
			boxShadow: 'none',
			background: 'rgba(206,217,224,.5)'
		}
	},
	checkedIcon: {
		backgroundColor: '#0097F6',
		backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
		'&:before': {
			display: 'block',
			width: 16,
			height: 16,
			backgroundImage: `url('${checkboxIcon}')`,
			backgroundRepeat: 'no-repeat',
			backgroundPosition: 'center',
			content: '""'
		},
		'input:hover ~ &': {
			backgroundColor: '#0097F6'
		}
	},
	root: {
		width: '100%'
	},
	textField: {
		width: '100%'
	},
	container: {
		width: '100%',
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center'
	},
	privacyCont: {
		width: '100%',
		display: 'flex',
		justifyContent: 'flex-start',
		alignItems: 'center',
		margin: '35px 0'
	},
	privacyLabel: {
		fontSize: '15px',
		fontWeight: '500',
		color: '#272727'
	},
	button: {
		width: '300px',
		height: '75px',
		margin: theme.spacing(0),
		color: '#fff',
		fontSize: 22,
		background: '#0097F6',
		'&:hover': {
			background: '#0097F6'
		}
	}
}));

const desktopStyles = makeStyles({
	title: {
		fontSize: '24px',
		fontWeight: 'bold',
		color: '#191919'
	},
	label: {
		fontSize: '20px',
		fontWeight: 'bold',
		color: '#191919'
	},
	contLinks: {
		display: 'flex',
		justifyContent: 'space-around',
		alignItems: 'center',
		'& a': {
			fontSize: '24px',
			fontWeight: '500',
			border: '1px solid #0097F6',
			color: '#0097F6',
			borderRadius: '9px',
			padding: '17px 25px',
			display: 'flex',
			justifyContent: 'center',
			alignItems: 'center'
		},
		'& a:hover': {
			textDecoration: 'none'
		}
	}
});

const mobileStyles = makeStyles({
	title: {
		fontSize: '18px',
		fontWeight: 'bold',
		color: '#191919'
	},
	label: {
		fontSize: '16px',
		fontWeight: 'normal',
		color: '#191919'
	},
	contLinks: {
		display: 'flex',
		justifyContent: 'space-around',
		alignItems: 'center',
		flexDirection: 'column',
		'& a': {
			fontSize: '16px',
			fontWeight: '500',
			borderRadius: '9px',
			border: '1px solid #0097F6',
			color: '#0097F6',
			padding: '17px 25px',
			display: 'flex',
			justifyContent: 'center',
			alignItems: 'center'
		},
		'& a:hover': {
			textDecoration: 'none'
		}
	}
});

// const StyledRadio = withStyles({
//     root: {
//         color: '#cacaca',
//         '&$checked': {
//             color: '#0097F6',
//         },
//     },
//     checked: {},
// })((props) => <Radio color='default' {...props} />)

const OfferStep2 = props => {
	const { t } = useTranslation();

	const classes = useStyles();
	const desktopClasses = desktopStyles();
	const mobileClasses = mobileStyles();

	const matches = useMediaQuery('(max-width:600px)');

	const [serviceAddresses, setServiceAddresses] = useState([]);
	const [billingAddresses, setBillingAddresses] = useState([]);
	const [billingType, setBillingType] = useState(props.billingType);
	const [companies, setCompanies] = useState([]);

	const [users, setUsers] = useState(0);

	// const [snackbar,setSnackbar] = useState({
	//     open: false,
	//     message: '',
	//     severity: 'success',
	// })

	const handleCloseSnackbar = value => props.setSnackbar(value);

	const getAddresses = async () => {
		let response = await Address.getAddresses(props.user);

		if (response !== undefined) {
			setServiceAddresses([
				...response
					.filter(address => address.addressType === 'SERVICE_ADDRESS' && !address.deleted)
					.map(address => ({
						type: address.addressType,
						label: address.fullName,
						value: address['@id']
					}))
			]);

			setBillingAddresses([
				...response
					.filter(address => address.addressType === 'BILLING_ADDRESS' && !address.deleted)
					.map(address => ({
						type: address.addressType,
						label: address.fullName,
						value: address['@id']
					}))
			]);
		}
	};

	const getCompanies = async () => {
		let response = await Company.getCompanies(props.user);

		if (response !== undefined)
			setCompanies([
				...response
					.filter(company => !company.deleted)
					.map(company => ({
						label: company.name,
						value: company['@id']
					}))
			]);
	};

	const getUserInfo = async () => {
		let response = await User.getUserInfo(props.user);

		if (response !== undefined)
			props.SaveStep2PersonalData({
				name: response.name,
				email: response.email
			});
	};

	useEffect(() => {
		if (props.user !== '') {
			getAddresses();
			getCompanies();
			getUserInfo();
		}

		if (props.admin)
			UserAdminPanel.getUsers()
				.then(res => {
					setUsers([
						...res
							.filter(user => user.roles[0] === 'ROLE_CLIENT')
							.map(user => ({
								value: user['@id'],
								label: user.name
							}))
					]);
				})
				.catch(error => console.error(error));
	}, []);

	const handleClientChange = input => {
		props.SaveStep2Data({
			client: input.target.value
		});
	};

	const handleSelectChange = async input => {
		props.SaveStep2Data({
			[input.target.name]: input.target.value
		});
	};

	const handleBillingTypeChange = input => {
		props.SaveStep2Data({
			company: '',
			billingAddress: '',
			billingType: input.target.value
		});

		setBillingType(input.target.value);
	};

	const addServiceAddress = async data => {
		await getAddresses();
		props.SaveStep2Data(data);
	};

	const addBillingAddress = async data => {
		await getAddresses();
		props.SaveStep2Data(data);
	};

	const addCompany = async data => {
		await getCompanies();
		props.SaveStep2Data(data);
	};

	const handleNextPanel = () => {
		if (props.serviceAddress !== '' && props.privacyPolicy) {
			const header = document.querySelector('#headerToScroll');
			header.scrollIntoView();
			props.setTabPanel(2);
		} else {
			props.setSnackbar({
				open: true,
				message: t('Advices.incompleteForm'),
				severity: 'warning'
			});
		}
	};

	const handlePrivacyPolicy = () => {
		props.SaveStep1({
			privacyPolicy: !props.privacyPolicy
		});
	};

	return (
		<Fragment>
			<GenericSnackbar handleClose={handleCloseSnackbar} {...props.snackbar} />
			{props.user !== '' ? (
				<Fragment>
					{/* <Box mb={2}>
                                <Grid
                                    container
                                    direction='row'
                                    justify='space-between'
                                    alignItems='center'
                                    spacing={2}
                                >
                                    <Grid className={matches ? mobileClasses.title : desktopClasses.title} item xs={12} md={12} sm={12}>
                                        <Typography variant='h5' align='center' style={{fontWeight: 'bold'}} gutterBottom>
                                            {t('OfferStep2.title')}
                                        </Typography>       
                                        <Typography className={matches ? mobileClasses.label : desktopClasses.label} align='center' gutterBottom>
                                            {t('OfferStep2.message')}
                                        </Typography>                
                                    </Grid>                                
                                    <Grid className={matches ? mobileClasses.title : desktopClasses.title} item xs={12} md={6} sm={12}>
                                        <Typography align='center' style={{fontWeight: 'bold'}} gutterBottom>
                                            {t('OfferStep2.name')}:
                                        </Typography>  
                                        <Typography className={matches ? mobileClasses.label : desktopClasses.label} align='center' gutterBottom>
                                            { props.personalData.name }
                                        </Typography>   
                                    </Grid> 
                                    <Grid className={matches ? mobileClasses.title : desktopClasses.title} item xs={12} md={6} sm={12}>
                                        <Typography align='center' style={{fontWeight: 'bold'}} gutterBottom>
                                            {t('OfferStep2.email')}:
                                        </Typography> 
                                        <Typography className={matches ? mobileClasses.label : desktopClasses.label} align='center' gutterBottom>
                                            { props.personalData.email }
                                        </Typography>   
                                    </Grid>                                
                                </Grid>
                            </Box> */}
					{!!props.admin && (
						<Box mt={2} mb={2}>
							<Card>
								<CardContent>
									<Grid item align="center" xs={12} md={12} sm={12}>
										<Box mb={2}>
											<Typography variant="h5" gutterBottom>
												{t('adminPanel.users')}
											</Typography>
										</Box>
									</Grid>
									<Grid item xs={12} md={12} sm={12}>
										<Typography variant="subtitle1" gutterBottom>
											{t('adminPanel.users')}
										</Typography>
										<FormControl variant="outlined" fullWidth>
											<Select
												value={props.client}
												name="users"
												required
												onChange={handleClientChange}
												fullWidth
											>
												<MenuItem value="">
													<em>None</em>
												</MenuItem>
												{users.length ? (
													users.map(option => {
														return (
															<MenuItem key={users.indexOf(option)} value={option.value}>
																{option.label}
															</MenuItem>
														);
													})
												) : (
													<MenuItem value="" />
												)}
											</Select>
										</FormControl>
									</Grid>
								</CardContent>
							</Card>
						</Box>
					)}
					<Box mt={2} mb={2}>
						<Grid item align="center" xs={12} md={12} sm={12}>
							<Box mb={2}>
								<Typography
									className={matches ? mobileClasses.title : desktopClasses.title}
									gutterBottom
								>
									{t('OfferStep2.serviceAddress')}
								</Typography>
							</Box>
						</Grid>
						<Grid item xs={12} md={12} sm={12}>
							<Typography className={matches ? mobileClasses.label : desktopClasses.label} gutterBottom>
								{t('OfferStep2.serviceAddress')}
							</Typography>
							<FormControl variant="outlined" fullWidth>
								<Select
									value={props.serviceAddress}
									name="serviceAddress"
									required
									onChange={handleSelectChange}
									fullWidth
								>
									<MenuItem value="">
										<em>None</em>
									</MenuItem>
									{serviceAddresses.length ? (
										serviceAddresses.map(option => (
											<MenuItem key={serviceAddresses.indexOf(option)} value={option.value}>
												{option.label}
											</MenuItem>
										))
									) : (
										<MenuItem value="" />
									)}
								</Select>
							</FormControl>
						</Grid>
					</Box>
					<Box mt={2} mb={2}>
						{!props.serviceAddress && (
							<Step2AddAddress
								md={12}
								setServiceAddress={addServiceAddress}
								addressType="SERVICE_ADDRESS"
								keep={true}
							/>
						)}
					</Box>
					{/* <Box mt={2} mb={2}>
                                <Grid
                                    container
                                    direction='row'
                                    justify='center'
                                    alignItems='center'
                                    spacing={2}
                                >
                                    <Grid item align='center' xs={12} md={12} sm={12}>
                                        <Typography className={matches ? mobileClasses.title : desktopClasses.title} gutterBottom >
                                            {t('OfferStep2.billingType.title')}
                                        </Typography>
                                        <RadioGroup 
                                            aria-label='position' 
                                            name='billingType' 
                                            row
                                            value={billingType}
                                            onChange={handleBillingTypeChange}
                                        >
                                            <FormControlLabel
                                                value='PERSONAL_BILLING'
                                                control={<StyledRadio />}
                                                label={t('OfferStep2.billingType.options.personal')}
                                                labelPlacement= 'start'
                                            />
                                            <FormControlLabel
                                                value='COMPANY_BILLING'
                                                control={<StyledRadio />}
                                                label={t('general.company')}
                                                labelPlacement= 'start'
                                            />
                                        </RadioGroup>
                                    </Grid>
                                </Grid>
                            </Box> */}
					{/* <Box mt={2} mb={2}>
                                <Grid
                                    container
                                    direction='row'
                                    justify='center'
                                    alignItems='center'
                                    spacing={2}
                                >
                                    <Grid item align='center' xs={12} md={12} sm={12}>
                                        <Typography className={matches ? mobileClasses.title : desktopClasses.title} gutterBottom >
                                            {t('OfferStep2.billingAddress')}
                                        </Typography>                                            
                                    </Grid>
                                    {
                                        billingType === 'COMPANY_BILLING' && <Grid item xs={12} md={12} sm={12}>
                                                                                <Typography className={matches ? mobileClasses.label : desktopClasses.label} variant='body1' gutterBottom >
                                                                                    {t('general.company')}
                                                                                </Typography>    
                                                                                <FormControl variant='outlined' fullWidth>                            
                                                                                    <Select
                                                                                        value={props.company}
                                                                                        name='company'
                                                                                        required
                                                                                        onChange={handleSelectChange}
                                                                                        fullWidth
                                                                                    >
                                                                                        <MenuItem value=''>
                                                                                            <em>None</em>
                                                                                        </MenuItem>
                                                                                        {
                                                                                            companies.length
                                                                                            ? companies.map(option => <MenuItem 
                                                                                                                            key={companies.indexOf(option)} 
                                                                                                                            value={option.value}
                                                                                                                        >
                                                                                                                            {option.label}
                                                                                                                        </MenuItem>)
                                                                                            : <MenuItem value='' />
                                                                                        }                                
                                                                                    </Select>
                                                                                </FormControl>
                                                                            </Grid>
                                    }
                                    <Grid item xs={12} md={12} sm={12}>
                                        <Typography className={matches ? mobileClasses.label : desktopClasses.label} gutterBottom >
                                            {t('OfferStep2.billingAddress')}
                                        </Typography>  
                                        <FormControl variant='outlined' fullWidth>                            
                                            <Select
                                                value={props.billingAddress}
                                                name='billingAddress'
                                                required
                                                onChange={handleSelectChange}
                                                fullWidth
                                            >
                                                <MenuItem value=''>
                                                    <em>None</em>
                                                </MenuItem>
                                                {
                                                    billingAddresses.length
                                                    ? billingAddresses.map(option => <MenuItem 
                                                                                                key={billingAddresses.indexOf(option)} 
                                                                                                value={option.value}
                                                                                            >
                                                                                                {option.label}
                                                                                            </MenuItem>)
                                                    : <MenuItem value='' />
                                                }                                
                                            </Select>
                                        </FormControl>
                                    </Grid>                                        
                                </Grid>
                            </Box> */}
					{/* <Box mt={2} mb={2}>
                            {
                                billingType === 'COMPANY_BILLING' && props.company === '' &&  <Step2AddCompany md={12} 
                                                                                                setBillingAddress={addBillingAddress} 
                                                                                                setCompanyStep2={addCompany} 
                                                                                                keep={true} 
                                                                                            />
                            }
                            </Box> */}
					{/* <Box mt={2} mb={2}>
                            {
                                props.billingAddress === '' && <Step2AddAddress 
                                                                    md={12} 
                                                                    setBillingAddress={addBillingAddress} 
                                                                    addressType='BILLING_ADDRESS'
                                                                    keep={true} 
                                                                />
                            }
                            </Box>                        */}
					{!!props.serviceAddress && (
						<div className={classes.privacyCont}>
							<Checkbox
								className={classes.checkRoot}
								checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
								icon={<span className={classes.icon} />}
								checked={props.privacyPolicy}
								onChange={handlePrivacyPolicy}
							/>
							<span className={classes.privacyLabel}>{t('general.privacyPolicy')}</span>
						</div>
					)}
					{!!props.serviceAddress && (
						<div className={classes.container}>
							<Button
								variant="contained"
								color="secondary"
								className={classes.button}
								onClick={handleNextPanel}
							>
								{t('buttons.next')}
							</Button>
						</div>
					)}
				</Fragment>
			) : (
				<>
					{/* <Link to='/offer/login' >{t('login.title')}</Link>                        
                        <Link to='/offer/register' >{t('register.title')}</Link>    */}
					<LoginStep2 />
				</>
			)}
		</Fragment>
	);
};

const mapState = state => ({
	user: state.auth.user.data.iri,
	client: state.Offer.client,
	personalData: state.Offer.personalData,
	serviceAddress: state.Offer.serviceAddress,
	billingAddress: state.Offer.billingAddress,
	billingType: state.Offer.billingType,
	company: state.Offer.company,
	privacyPolicy: state.Offer.privacyPolicy
});

const mapDispatch = {
	...OfferActions
};

export default connect(mapState, mapDispatch)(OfferStep2);
