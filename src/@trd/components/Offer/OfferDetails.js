import React,{ useState , useEffect} from 'react';
import AppBarOptions from '../utils/AppBarOptions';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';

import * as OfferActions from 'app/store/actions/OfferActions'
import * as SettingsActions from 'app/store/actions/SettingsActions'

import OfferStep1 from './OfferStep1'
import OfferStep2 from './OfferStep2'
import OfferStep3 from './OfferStep3'

import {
    Grid,
    Typography,
    Box,
} from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles'

import Tab1Active from 'app/assets/images/icons/tab1active.svg'
import Tab1Inactive from 'app/assets/images/icons/tab1inactive.svg'
import Tab2Active from 'app/assets/images/icons/tab2active.svg'
import Tab2Inactive from 'app/assets/images/icons/tab2inactive.svg'
import Tab3Active from 'app/assets/images/icons/tab3active.svg'
import Tab3Inactive from 'app/assets/images/icons/tab3inactive.svg'

const useStyles = makeStyles({
    container: {
        borderRadius: '0 0 13px 13px',
        background: '#fff',
        color: '#191919',
        boxShadow: '0px 5px 10px rgba(0,0,0,.12)'
    },
    tabPanel: {
        '& .MuiBox-root': {
            padding: '24px 0 24px 0'
        }
    }
})

const OfferDetails = props => {

    const {t} = useTranslation()

    const classes = useStyles()

    useEffect(() => {
        props.getExtraServices()
        props.getGeneralSettings()
    },[])

    const [tabValue,setTabValue] = useState(0)

    const [snackbar,setSnackbar] = useState({
        open: false,
        message: '', 
        severity: 'success',
    })

    const options = [
        {
            active: Tab1Active,
            inactive: Tab1Inactive,
            label: t('OfferDetails.step1')
        },
        {
            active: Tab2Active,
            inactive: Tab2Inactive,
            label: t('OfferDetails.step2')
        },
        {
            active: Tab3Active,
            inactive: Tab3Inactive,
            label: t('OfferDetails.step3')
        },
    ];

    const handleChange = value => {
        setTabValue(value)
    }

    const TabPanel = ({ children, value, index, ...other }) => {

        return (
            <Typography
                component="div"
                role="tabpanel"
                hidden={value !== index}
                id={`simple-tabpanel-${index}`}
                aria-labelledby={`simple-tab-${index}`}
                {...other}
            >
                <Box p={3}>{children}</Box>
            </Typography>
        )
    }

    return <Grid item xs={12} md={8} sm={6}>
                <AppBarOptions 
                    options={options}
                    onClick={handleChange}
                    value={tabValue}                   
                />
                <div className={classes.container}>
                    <TabPanel value={tabValue} index={0} className={classes.tabPanel}>
                        <OfferStep1 setSnackbar={setSnackbar} snackbar={snackbar} setTabPanel={setTabValue} className={classes.tabPanel} />
                    </TabPanel>
                    <TabPanel value={tabValue} index={1}>
                        <OfferStep2 setSnackbar={setSnackbar} snackbar={snackbar} setTabPanel={setTabValue} />
                    </TabPanel>
                    <TabPanel value={tabValue} index={2} >
                        <OfferStep3 setSnackbar={setSnackbar} snackbar={snackbar} setTabPanel={setTabValue} />
                    </TabPanel>      
                </div>        
            </Grid>
}

const mapDispatch = {
    ...OfferActions,
    ...SettingsActions
}

export default connect(null,mapDispatch)(OfferDetails)