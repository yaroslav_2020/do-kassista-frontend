import React,{ useState , useEffect , Fragment } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'
import moment from 'moment'

import * as OfferActions from 'app/store/actions/OfferActions'
import * as PaymentActions from 'app/store/actions/PaymentActions'
import * as FetchActions from 'app/store/actions/FetchActions'

import AccordionInfo from '../utils/AccordionInfo'
import PaymentData from '../utils/PaymentData'

import * as Address from 'app/utils/Address'
import * as Company from 'app/utils/Company'
import * as Offer from 'app/utils/Offer'

import {
    Box,
    Button,
    CircularProgress
} from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles'

import GenericSnackbar from 'app/utils/GenericSnackbar'
import { updateVoucher } from 'app/utils/Voucher'

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
    },
    container: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        width: '300px',
        height: '75px',
        margin: theme.spacing(0),
        color: '#fff',
        fontSize: 22,
        background: '#0097F6',
        '&:hover': {
            background: '#0097F6',
        }
    },
    loaderCont: {
        width: '100vw',
        height: '100vh',
        position: 'fixed',
        background: 'rgba(0,0,0,.1)',
        zIndex: '1000',
        justifyContent: 'center',
        boxSizing: 'border-box',
        alignItems: 'center',
        top: '0',
        left: '0'
    }
})) 

const OfferStep3 = props => {
    const {t} = useTranslation()

    const classes = useStyles()

    const [expanded,setExpanded] = useState('panel1')
    const [serviceAddress,setServiceAddress] = useState('')
    const [billingAddress,setBillingAddress] = useState('')
    const [company,setCompany] = useState('')

    const [fetching,setFetching] = useState(false)

    const [paymentToken,setPaymentToken] = useState(undefined)

    const [completed, setCompleted] = useState(false)

    const [reserveButtonEnabled,setReserveButtonEnabled] = useState(false)

    const handleCloseSnackbar = value => props.setSnackbar(value)

    const days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

    let periodPoints = {
        ...props.offer.periodPoints,
    }

    days.map( (day) => {
        periodPoints[day] = {
            ...periodPoints[day],
            startHour: props.startHour,
            endHour: props.endHour
        }
    })

    useEffect(() => {

        let validateForm1 = ((props.offer.offerBuilding.roomNumber > 0 ||
            props.offer.offerBuilding.bathRoomNumber > 0 ||
            props.offer.offerBuilding.kitchenNumber > 0 ||
            props.offer.offerBuilding.livingNumber > 0) &&
            ((props.offer.period === 'several_times_a_week' &&
            Object.values(props.offer.periodPoints).filter(day => day.checked).length > 0) ||
            props.offer.period !== 'several_times_a_week') &&
            props.offer.pricePerService > 0 &&
            props.offer.pricePerDay > 0)

        let validateForm2 = (props.offer.serviceAddress !== '' && props.offer.privacyPolicy)

        setReserveButtonEnabled( validateForm1 && validateForm2 && props.paymentActive )

    },[
        props.offer.offerBuilding,
        props.offer.period,
        props.offer.periodPoints,
        props.offer.startFromDate,
        props.offer.endDate,
        props.hoursNeeded,
        props.offer.pointServices,
        props.offer.cleanningTools,
        props.offer.serviceAddress,
        props.offer.privacyPolicy,
        props.offer.cardHolderName,
        props.paymentActive
    ])

    useEffect(() => {
        if(paymentToken !== undefined && fetching && completed){
            const header = document.querySelector('#headerToScroll')
            try{                

                const pointServices = Object.values(props.offer.pointServices).filter(service => service.checked)
                    .map(service => ({
                        extraService: service.iri,
                        hours: service.hours
                    }))                         

                periodPoints = Object.values(periodPoints).filter(periodPoint => periodPoint.checked)
    
                periodPoints = periodPoints.map(periodPoint => ({
                    ...periodPoint,
                    durationInMinutes: parseFloat(periodPoint.durationInMinutes),
                    pointServices: pointServices
                }))                

                let startDate = new Date(props.offer.startFromDate)
                let endDate = new Date(props.offer.endDate)

                let data = {
                    startFromDate: moment(startDate).utc(),
                    endDate: moment(endDate).utc(),
                    client: [props.user],
                    period: props.offer.period,
                    cardHolderName: props.offer.cardHolderName,
                    pricePerDay: parseFloat(props.offer.pricePerDay),
                    pricePerService: parseFloat(props.offer.pricePerService),
                    otherDetails: props.offer.otherDetails,
                    pet: props.offer.pet,
                    otherPet: props.offer.otherPet,
                    activePaymentMethod: paymentToken,
                    cleanningTools: props.offer.cleanningTools === 'CLEANNING_TOOLS_YES' ? true : false,
                    offerBuilding: {
                        roomNumber: parseInt(props.offer.offerBuilding.roomNumber),
                        bathRoomNumber: parseInt(props.offer.offerBuilding.bathRoomNumber),
                        kitchenNumber: parseInt(props.offer.offerBuilding.kitchenNumber),
                        livingNumber: parseInt(props.offer.offerBuilding.livingNumber),
                        accessType: props.offer.offerBuilding.accessType,
                        accessDescription: props.offer.offerBuilding.accessDescription,
                    },
                    periodPoints: [...periodPoints],
                    company: props.offer.company !== '' ? props.offer.company : null,
                    offerBilling: props.offer.billingAddress !== '' ? {
                        billingType: props.offer.billingType,
                        address: props.offer.billingAddress
                    } : null,
                    payments: [],
                    serviceAddress: props.offer.serviceAddress,
                    vouchers: props.offer.vouchers,
                }

                if(props.offer.company == '')
                    delete data.company
    

                //42424242424242

                if( data ) {
                    Offer.createOffer(data) 
                        .then((response) => {
                            if (response) {                                
                                if(response.status === 201) {
                                    props.setSnackbar({
                                        open: true,
                                        message: t('Advices.offerCreated'), 
                                        severity: 'success',
                                    })                                            
                                } else {
                                    props.setSnackbar({
                                        open: true,
                                        message: t('Advices.somethingWrong'), 
                                        severity: 'error',
                                    })
                                }

                                if(props.offer.vouchers.length === 0 )
                                    setFetching(false)
                            } else {
                                window.location.reload()
                            }
                        })                   
                        .finally(() => {
                            if(props.offer.vouchers.length === 0 )
                                window.location.reload()
                        })

                    if(props.offer.vouchers.length > 0 && props.snackbar.severity === 'success' ) {
                        updateVoucher( props.offer.voucherApplied.id, {
                            usageCount: (props.offer.voucherApplied.usageCount + 1)
                        }).finally(() => {                                            
                            window.location.reload()
                        })
                    }    
                        
                }
                props.RestartOfferForm()
                header.scrollIntoView()                
                // (() => history.push('/offer'))();
                // props.setTabPanel(0)
                // (() => {
                //     return <Redirect to="/offer" />
                // })();
            }catch(error){
                props.error()
                header.scrollIntoView()
                setFetching(false)
                props.setSnackbar({
                    open: true,
                    message: t('Advices.somethingWrong'), 
                    severity: 'error',
                })
            }
        }else{
            if(!props.paymentActive)
                setFetching(false)
        }
    },[paymentToken,props.paymentActive, completed])

    useEffect(() => {
        if(props.serviceAddress !== ''){
            Address.getAddressInformation(props.serviceAddress)
                .then(res => {
                    let address = `${res.country.name}, ${res.county.name}, ${res.city ? res.city.name : ''} 
                                    ${res.street}, ${t('address.floor')}: ${res.floor}, ${t('address.phoneNumber')}: ${res.phoneNumber}`
                    setServiceAddress(address)
                })
                .catch(error => console.error(error))
        }else{
            setServiceAddress('')
        }
            

        if(props.billingAddress !== ''){
            Address.getAddressInformation(props.billingAddress)
                .then(res => {
                    let address = `${res.country.name}, ${res.county.name}, ${res.city ? res.city.name : ''} 
                                    ${res.street}, ${t('address.floor')}: ${res.floor}, ${t('address.phoneNumber')}: ${res.phoneNumber}`
                    setBillingAddress(address)
                })
                .catch(error => console.error(error))
        }else{
            setBillingAddress('')
        }

        if(props.company !== ''){
            Company.getCompanyInformation(props.company)
                .then(res => {
                    let company = `${res.name}, ${t('register.inputs.bankName')}: ${res.bankName}, 
                                    ${t('register.inputs.bankAccount')}: ${res.bankAccount}, 
                                    ${t('company.swift')}: ${res.swift}, ${t('company.cif')}: ${res.cif}, 
                                    ${t('address.phoneNumber')}: ${res.regNumber}` 

                                    
                    if(props.billingAddress === ''){
                        let billingAddress = `${res.address.country.name}, ${res.address.county.name}, ${res.address.city ? res.address.city.name : ''} 
                                                ${res.address.street}, ${t('address.floor')}: ${res.address.floor}, ${t('address.phoneNumber')}: ${res.address.phoneNumber}`
                    
                        setBillingAddress(billingAddress)
                    }                       

                    setCompany(company)
                })
                .catch(error => console.error(error))
        }else{
            setCompany('')
        }            

    },[props.serviceAddress,props.billingAddress,props.company])

    const period_detail = () => {
        return t(`Step1Frequency.period.${props.offer.period}`)
    }

    const pet_detail = () => {
        switch(props.offer.pet){
            case 'PET_NOT':
                return t('Step1Pets.options.not')      
            case 'PET_CAT':
                return t('Step1Pets.options.cat')  
            case 'PET_DOG':
                return t('Step1Pets.options.dog')  
            case 'PET_OTHER':
                return t('Step1Pets.options.other') 
            default:
                return ''
        }
    }

    const data = [
        {
            title: t('OfferStep2.serviceAddress'),
            name: 'panel1',
            info: [
                {
                    key: t('OfferStep2.serviceAddress'),
                    value: serviceAddress
                },
            ]
        },
        // {
        //     title: t('OfferStep3.billingInformation'),
        //     name: 'panel3',
        //     info: [
        //         {
        //             key: 'Company',
        //             value: company
        //         },
        //         {
        //             key: `${t('address.fullName')} - ${t('OfferStep3.billingInformation')}`,
        //             value: billingAddress
        //         },
        //     ]
        // },
        {
            title: t('OfferStep3.personalDetails'),
            name: 'panel4',
            info: [
                {
                    key: t('OfferStep2.name'),
                    value: props.personalData.name
                },
                {
                    key: t('OfferStep2.email'),
                    value: props.personalData.email
                },
            ]
        },
        {
            title: t('OfferStep3.offerDetails'),
            name: 'panel5',
            info: [
                {
                    key: t('offerBuilding.room'),
                    value: props.offer.offerBuilding.roomNumber
                },
                {
                    key: t('offerBuilding.bathroom'),
                    value: props.offer.offerBuilding.bathRoomNumber
                },
                {
                    key: t('offerBuilding.kitchen'),
                    value: props.offer.offerBuilding.kitchenNumber
                },
                {
                    key: t('offerBuilding.living'),
                    value: props.offer.offerBuilding.livingNumber
                },
                {
                    key: t('Step1Frequency.startDateTitle'),
                    value: moment(props.offer.startFromDate).format("DD-MM-YYYY")
                },
                {
                    key: t('Step1Frequency.endDateTitle'),
                    value: moment(props.offer.endDate).format("DD-MM-YYYY")
                },
                {
                    key: t('general.frequency'),
                    value: period_detail()
                },
                ...Object.values(props.periodPoints).filter(pp => pp.checked).map(pp => ({
                    key: t(`Schedule.days.${ pp.day.toLowerCase() }`),
                    value: [
                        `${t('Schedule.startHour')}: ${props.startHour}`,
                        `${t('Schedule.endHour')}: ${props.endHour}`,
                        `${t('Schedule.hours')}: ${parseInt(pp.durationInMinutes) / 60}`
                    ]
                })),

                ...Object.values( props.offerFullSchedule ).map( ({ startHour }) => 
                    ({
                        key: `${ t( 'Step1Frequency.serviceDay' ) }: ${ t( `Schedule.days.${ moment( startHour.date ).format('dddd').toLowerCase() }`) }`,
                        value: `${moment(startHour.date).format("DD-MM-YYYY")}`
                    })
                ),
                ...Object.values(props.pointServices).filter(ps => ps.checked).map(ps => ({
                    key: ps.name,
                    // value: [
                    //     `${ps.hours} ${t('Schedule.hours')}`,
                    //     ...ps.days
                    // ]
                })),
                {
                    key: t('general.pets'),
                    value: pet_detail()
                },
                {
                    key: t('OfferStep3.accessType'),
                    value: props.offer.offerBuilding.accessType === 'i_am_at_home' ? t('Step1ExtraInfo.accessOptions.i_am_at_home') : t('Step1ExtraInfo.accessOptions.specs_in_description')
                },
                {
                    key: t('OfferStep3.accessDescription'),
                    value: props.offer.offerBuilding.accessDescription
                },
                {
                    key: t('OfferStep3.otherDetails'),
                    value: props.offer.otherDetails
                }
            ]
        }
    ]

    const handleSubmit = form => {
        form.preventDefault()
        setFetching(true)

        let data = Object.fromEntries(new FormData(form.target))

        let payload = {
            payment: {
                option:  data.paymentOption,
                name: data.name,
                card: data.card,
                expiresIn: {
                    month: data.month,
                    year: data.year
                },
                securityCode: data.security
            }
        }
        props.SaveStep3(payload)
        props.pay()
        setCompleted(true);
    }

    return (
        <Fragment>
            <GenericSnackbar 
                handleClose={handleCloseSnackbar} 
                {...props.snackbar} 
            />
            {
                fetching && 
                <div className={classes.loaderCont} style={{display: props.fetching ? 'flex' : 'none',}}>
                    <CircularProgress />
                </div>
            }
            <form onSubmit={handleSubmit}>                
                <Box mb={2}>
                    {
                        data.length
                            ? data.map(props => <AccordionInfo 
                                                    key={data.indexOf(props)} 
                                                    expanded={expanded} 
                                                    setExpanded={setExpanded} 
                                                    {...props} />)
                            : <div/>
                    }
                </Box>
                <Box mt={4} mb={2}>
                    <PaymentData setPaymentToken={setPaymentToken} setFetching={setFetching} />
                </Box>
                <div className={classes.container}>
                    <Button 
                        type='submit' 
                        variant='contained' 
                        color='secondary' 
                        className={classes.button}
                        disabled={!reserveButtonEnabled}
                    >
                        {t('general.reserve')}
                    </Button>
                </div>
            </form>
        </Fragment>
    )
}

const mapState = state => ({
    offer: state.Offer,
    company: state.Offer.company,
    personalData: state.Offer.personalData,
    serviceAddress: state.Offer.serviceAddress,
    billingAddress: state.Offer.billingAddress,
    pointServices: state.Offer.pointServices,
    periodPoints: state.Offer.periodPoints,
    startHour: state.Offer.startHour,
    endHour: state.Offer.endHour,
    offerFullSchedule: state.Offer.offerFullSchedule,
    paymentActive: state.Payment.active,
    user: state.auth.user.data.iri    
    
})

const mapDispatch = {
    ...OfferActions,
    ...PaymentActions,
    ...FetchActions
}

export default connect(mapState,mapDispatch)(OfferStep3)