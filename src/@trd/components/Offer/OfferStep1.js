import React from 'react';

import GenericSnackbar from 'app/utils/GenericSnackbar';
import FormStep1 from './partials/FormStep1';

const OfferStep1 = ({ setSnackbar, snackbar, setTabPanel }) => {

    const handleCloseSnackbar = ( value ) => setSnackbar( value );

    return (
        <>
            <GenericSnackbar 
                handleClose={ handleCloseSnackbar } 
                { ...snackbar } 
            />
            <FormStep1
                setSnackbar={ setSnackbar }
                setTabPanel={ setTabPanel }
            />
        </>
    );
}

export default OfferStep1;