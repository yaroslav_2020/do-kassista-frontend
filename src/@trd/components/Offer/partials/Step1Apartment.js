import React from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import * as OfferActions from 'app/store/actions/OfferActions'

import {
    Typography,
    Grid,
    useMediaQuery
} from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles'
import CustomTextField from './CustomTextField'

const useDesktopStyles = makeStyles({
    container: {
        width: '100%',
        padding: '0 100px',
        marginBottom: '32px'
    },
    contTitle: {
        marginBottom: '30px',
        width: '100%',
        textAlign: 'center'
    },
    title: {
        fontSize: '22px',
        fontWeight: 'bold',
        color: '#191919'
    },
    textFieldContainer: {
        maxWidth: '230px'
    },
    textField: {
        color: '#313131',
        outline: 'none',
        '&>div>fieldset': {
            borderColor: '#DBDBDB',
            borderWidth: 1,
        }
    }
});

const useMobileStyles = makeStyles({
    container: {
        width: '100%',
        padding: '0 15px',
        marginBottom: '32px'
    },
    contTitle: {
        marginBottom: '25px',
        width: '100%',
        textAlign: 'center'
    },
    title: {
        fontSize: '18px',
        fontWeight: 'bold',
        color: '#191919'
    },
});

const Step1Apartment = ({ offerBuilding, roomCleaningDurationTime, bathroomCleaningDurationTime, kitchenCleaningDurationTime, livingCleaningDurationTime}) => {

    const {t} = useTranslation()
    
    const desktopClasses = useDesktopStyles()
    const mobileClasses = useMobileStyles()

    const matches = useMediaQuery('(max-width:600px)')

    const inputs = [
        {
            label: t('offerBuilding.room'),
            id: 'habitacion',
            name: 'roomNumber',
            value: offerBuilding.roomNumber,
            multiplicatorTime: roomCleaningDurationTime
        },
        {
            label: t('offerBuilding.bathroom'),
            id: 'banno',
            name: 'bathRoomNumber', 
            value: offerBuilding.bathRoomNumber,
            multiplicatorTime: bathroomCleaningDurationTime
        },
        {   
            label: t('offerBuilding.kitchen'),
            id: 'cocina',
            name: 'kitchenNumber', 
            value: offerBuilding.kitchenNumber,
            multiplicatorTime: kitchenCleaningDurationTime
        },
        {
            label: t('offerBuilding.living'),
            id: 'salon',
            name: 'livingNumber',
            value: offerBuilding.livingNumber,
            multiplicatorTime:  livingCleaningDurationTime
        }
    ];

    return (
        <div className={ matches ? mobileClasses.container : desktopClasses.container }>        

            <div className={ matches ? mobileClasses.contTitle : desktopClasses.contTitle }>
                
                <Typography variant='h5' className={ matches ? mobileClasses.title : desktopClasses.title } align='center' gutterBottom > 
                    { t('Step1Apartment.title') }
                </Typography>

            </div>

            <Grid container spacing={2} direction='row' justify='center' alignItems='center'>
                <Grid item xs={12} md={3} sm={12}>

                </Grid>
                <Grid item xs={12} md={6} sm={12}>
                    <Grid
                        container
                        direction='row'
                        justify='center'
                        alignItems='center'
                        spacing={2}
                    >
                        {
                            inputs.map( (input,id) => (
                                <Grid 
                                    key={`obtf${id}`} 
                                    align='center' 
                                    item 
                                    xs={6} 
                                    md={6} 
                                    sm={6} 
                                    className={desktopClasses.textFieldContainer}
                                >
                                    <CustomTextField input={ input } />
                                </Grid>
                                )
                            )
                        }
                    </Grid>
                </Grid>
                <Grid item xs={12} md={3} sm={12}>
                </Grid>                
            </Grid>
        </div>
    );  
}

const mapState = state => ({
    offerBuilding: state.Offer.offerBuilding,
    roomCleaningDurationTime: state.Settings.room_cleaning_duration_time,
    bathroomCleaningDurationTime: state.Settings.bathroom_cleaning_duration_time,
    kitchenCleaningDurationTime: state.Settings.kitchen_cleaning_duration_time,
    livingCleaningDurationTime: state.Settings.living_cleaning_duration_time,
})

const mapDispatch = {
    ...OfferActions
}

export default connect(mapState,mapDispatch)(Step1Apartment)