import React , { useState } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'
import moment from 'moment'

import {
    useMediaQuery
} from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles'

import * as OfferActions from 'app/store/actions/OfferActions'

// import QuestionMarkIcon from 'app/assets/images/icons/question-mark.svg'
import InfoIcon from 'app/assets/images/icons/info.svg'


import GenericSnackbar from 'app/utils/GenericSnackbar'

const useStyles = makeStyles({
    hoursCont: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    img: {
        marginRight: '10px'
    },
    title: {
        fontSize: '18px',
        fontWeight: 'bold',
        color: '#272727',
        marginBottom: '10px'
    },
    subTitle: {
        fontSize: '15px',
        fontWeight: '400',
        color: '#272727',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '18px'
    },
    button: {
        width: "50px",
        height: "50px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        background: "#0396F2",
        color: "#fff",
        fontSize: "39px",
        fontWeight: "400",
        userSelect: 'none',
        cursor: 'pointer'
    },
    leftButton: {
        borderRadius: "9px 0 0 9px",
    },
    rightButton: {
        borderRadius: "0 9px 9px 0",
    },
    hoursCont: {  
        width: '100%',      
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
    hoursInput: {
        width: "250px",
        maxWidth: '80%',
        height: "50px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        borderTop: "1px solid #0396F2",
        borderBottom: "1px solid #0396F2",
        fontSize: "16px",
        fontWeight: "400",
        color: "#313131",
    }
})

const desktopStyles = makeStyles({
    container: {
        width: '100%',
        marginTop: '60px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        marginBottom: '70px',
        padding: '0 100px'
    },
})

const mobileStyles = makeStyles({
    container: {
        width: '100%',
        marginTop: '35px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        marginBottom: '35x',
    },
})

const Step1HoursNeeded = props => {
    const {t} = useTranslation()

    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const matches = useMediaQuery('(max-width:600px)')

    const [snackbar,setSnackbar] = useState({
        open: false,
        message: '', 
        severity: 'success',
    })

    const handleCloseSnackbar = value => setSnackbar(value)

    const handleHours = value => {

        let newValue = props.hoursNeeded;

        let periodPoints = Object.values({ ...props.periodPoints });

        if (( props.hoursNeeded + value ) >= props.min_hours && ( props.hoursNeeded + value ) <= props.max_hours ) {
            
            let endServiceHour = moment(`${moment().format('YYYY-MM-DD')} ${props.startHour}:00`,'YYYY-MM-DD HH:mm:ss')
            
            let endLaborHour = moment(`${moment().format('YYYY-MM-DD')} ${props.endLaborHour}:00`,'YYYY-MM-DD HH:mm:ss')

            endServiceHour.add(moment.duration(props.hoursNeeded + value,'hours')) 

            if(endServiceHour < endLaborHour){
                newValue += value

                if(value > 0){
                    if((newValue - Math.floor(newValue)) >= 0.6)
                        newValue = Math.ceil(newValue)
                }else{
                    if(newValue > 0){
                        if((newValue - Math.floor(newValue)) >= 0.6 && (newValue - Math.floor(newValue)) <= 0.9)
                            newValue = Math.floor(newValue) + 0.5
                    }else{
                        newValue = 0
                    }            
                }
            }else{
                setSnackbar({
                    open: true,
                    message: t('serviceTime.notSaveWithLabor'), 
                    severity: 'warning',
                })
            }           
        }else{
            setSnackbar({
                open: true,
                message: t('serviceTime.hoursAmount'), 
                severity: 'warning',
            })
        } 
        newValue= Math.round( newValue * 10 ) / 10;

        props.SaveStep1({
            hoursNeeded: newValue,
        })
        
        // Set the time for the periodPoints.
        periodPoints = periodPoints.map( periodPoint =>
            [ periodPoint.day.toLowerCase(), {
                ...periodPoint,
                endHour: moment( props.startHour, 'HH::mm' ).add( (newValue * 60), 'minutes' ).format('HH:mm'),
                durationInMinutes: ( newValue * 60 )
            }]
        );

        periodPoints = Object.fromEntries( periodPoints );
        
        props.SavePeriodPoint( periodPoints );
    }

    const handleAdvice = () => {
        if(props.hoursNeeded < 4){
            return '0 - 49'
        }else if(props.hoursNeeded >= 4 && props.hoursNeeded < 5){
            return '50 - 79'
        }else if(props.hoursNeeded >= 5 && props.hoursNeeded < 6){
            return '80 - 99'
        }else if(props.hoursNeeded >= 6 && props.hoursNeeded < 7){
            return '100 - 125'
        }else if(props.hoursNeeded >= 7 && props.hoursNeeded < 8){
            return '126 - 149'
        }else if(props.hoursNeeded >= 8){
            return '150'
        }
    }

    return (
        <div className={matches ? mobileClasses.container : desktopClasses.container}>
            <GenericSnackbar 
                handleClose={handleCloseSnackbar} 
                {...snackbar} 
            />
            <p className={classes.title} >
                {t('ExtraServiceSchedule.howManyHours')}
            </p>
            <div className={classes.hoursCont} >
                <p className={`${classes.button} ${classes.leftButton}`} onClick={() => handleHours(-0.5)}>-</p>

                <p className={classes.hoursInput} >{`${props.hoursNeeded} ${t('Schedule.hours')}`}</p>

                <p className={`${classes.button} ${classes.rightButton}`} onClick={() => handleHours(0.5)}>+</p>
            </div>
            <p className={classes.subTitle} >
                <img className={ classes.img } src={ InfoIcon } alt='info' /> {`${t('Advices.adviceFor')} ${handleAdvice()} m`}<sup>2</sup>
                 
            </p>
        </div>
    )
}

const mapState = state => ({
    hoursNeeded: state.Offer.hoursNeeded,
    min_hours: state.Settings.min_hours,
    max_hours: state.Settings.max_hours,
    startHour: state.Offer.startHour,
    endLaborHour: state.Settings.end_labor_schedule,
    periodPoints: state.Offer.periodPoints
})

const mapDispatch = {
    ...OfferActions
}

export default connect(mapState,mapDispatch)(Step1HoursNeeded)