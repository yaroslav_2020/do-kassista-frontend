import React , { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'

import LoginWizard from '../../utils/LoginWizard'
import RegisterWizard from '../../utils/RegisterWizard'


import {
    Tabs,
    Tab
} from '@material-ui/core'

const TabPanel = (props) => {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
            children

        )}
      </div>
    );
  }

const useStyles = makeStyles(theme => ({
    container: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        padding: '20px 0'
    },
    tabContainer: {
        display: 'flex',
        flexGrow: 1,
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    loginContainer: {
        width: '60%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap',
        borderRadius: '16px',
        background: '#fff',
        padding: '45px 10px 25px 10px',
        boxShadow: '0px 5px 20px rgba(0,0,0,.06)',
        marginTop: '30px',
        '& a': {
            fontSize: '14px',
            fontWeight: '500',
            color: '#2F2F2F',
        },
        '& a:hover': {
            textDecoration: 'none'
        }
    },
}))

const desktopStyles = makeStyles(theme => ({
    activeTab: {
        fontWeight: 'bold',
        fontSize: '22px',
        paddingBottom: '5px',
        color: '#272727',
        borderBottom: '1px solid #0097F6',
        '&:first-child': {
            marginRight: '10px',
        }
    },
    inactiveTab: {
        fontWeight: 'bold',
        fontSize: '22px',
        paddingBottom: '5px',
        color: '#BEBEBE',
        '&:first-child': {
            marginRight: '10px',
        }
    } 

}))

const mobileStyles = makeStyles(theme => ({

}))

const LoginStep2 = props => {

    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue)
    };

    return (
        <div className={classes.container} >
            <Tabs value={value} onChange={handleChange} >
                <Tab label="Iniciar sesion"  />
                <Tab label="Registrarse"  />
            </Tabs>

            <TabPanel className={value === 0 ? classes.loginContainer : ''} value={value} index={0} >
                <LoginWizard />
            </TabPanel>
            <TabPanel className={value === 1 ? classes.loginContainer : ''} value={value} index={1} >
                <RegisterWizard />
            </TabPanel>
        </div>
    )
}

export default LoginStep2