import React,{ useState , useEffect } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import * as Company from 'app/utils/Company'
import * as PostalCode from 'app/utils/PostalCode'
import * as CompanyActions from 'app/store/actions/CompanyActions'

import * as Address from 'app/utils/Address'
import * as AddressLocation from 'app/utils/AddressLocation'
import * as AddressActions from 'app/store/actions/AddressActions'
import * as FetchActions from 'app/store/actions/FetchActions'

import {
    Typography,
    TextField,
    Button,
    Grid,
    MenuItem,
    FormControl,
    Select,
    useMediaQuery
} from '@material-ui/core'

import { makeStyles } from  '@material-ui/core/styles'

const useStyles = makeStyles({
    container: {
        marginTop: '15px',
        borderRadius: '13px',
        background: '#fff',
        color: '#191919',
        // boxShadow: '0px 5px 10px rgba(0,0,0,.12)'
    },
    textField: {
        borderRadius: '9px',
        marginBottom: '35px',
        '& fieldset': {
            border: '1px solid #DBDBDB'
        },
        '& .MuiInputBase-root:hover fieldset': {
            border: '1px solid #DBDBDB'
        },
        '& .MuiInputBase-root:focus fieldset': {
            border: '1px solid #DBDBDB'
        }
    },
})

const desktopStyles = makeStyles({
    title: {
        fontSize: '25px',
        fontWeight: 'bold',
        color: '#272727'
    },
    inputLabel: {
        fontSize: '14px',
        fontWeight: 'bold',
        color: '#313131',
        marginBottom: '12px'
    },
    inputDiv: {
        width: '45%',
    },
    contInputs: {
        width: '100%',
        padding: '20px 60px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        flexWrap: 'wrap'
    },
    contTitle: {
        background: '#fff',
        // borderBottom: '1px solid #D8D8D8',
        padding: '30px 74px',
        borderRadius: '13px 13px 0px 0px',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        position: 'relative',
    },
    button: {
        background: '#0097F6',
        borderRadius: '6px',
        color: '#fff',
        outline: 'none',
        padding: '14px 45px',
        border: 'none',
        width: '300px',
        height: '75px',
        fontSize: '22px',
        fontWeight: 'bold',
        margin: '20px 0px 30px 60px',
        '&:hover': {
            background: '#0097F6',
            border: 'none'
        }
    },
    contButton: {
        width: '100%',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center'
    }
})

const mobileStyles = makeStyles({
    title: {
        fontSize: '20px',
        fontWeight: 'bold',
        color: '#272727'
    },
    inputLabel: {
        fontSize: '14px',
        fontWeight: 'bold',
        color: '#313131',
        marginBottom: '12px'
    },
    inputDiv: {
        width: '100%',
    },
    contInputs: {
        width: '100%',
        padding: '20px 17px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        flexWrap: 'wrap'
    },
    contTitle: {
        background: '#fff',
        // borderBottom: '1px solid #D8D8D8',
        padding: '30px 60px',
        borderRadius: '13px 13px 0px 0px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
    },
    button: {
        background: '#0097F6',
        borderRadius: '6px',
        color: '#fff',
        outline: 'none',
        padding: '18px 75px',
        border: 'none',
        width: '224px',
        height: '54px',
        fontSize: '15px',
        fontWeight: '500',
        margin: '20px 0px 30px 0px',
        '&:hover': {
            background: '#0097F6',
            border: 'none'
        }
    },
    contButton: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
})

const Step2AddCompany = props => {

    const {t} = useTranslation()

    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const matches = useMediaQuery('(max-width:600px)')

    const [postalCodes,setPostalCodes] = useState([])

    const [countries,setCountries] = useState([])

    const [counties,setCounties] = useState([])

    const [cities,setCities] = useState([])

    const [companyAddress,setCompanyAddress] = useState({
                                addressType: 'COMPANY_ADDRESS',
                                fullName: '',
                                nr: '',
                                phoneNumber: '',
                                street: '',
                                floor: '',
                                country: '',
                                county: '',
                                city: '',
                                postalCode: '',
                                customPostCode: ''
                            })

    const [company,setCompany] = useState({
                                    name: '',
                                    bankName: '',
                                    bankAccount: '',
                                    swift: '',
                                    cif: '',
                                    regNumber: '',
                                })

    const changeCountry = async value => {
        setCompanyAddress({
            ...companyAddress,
            country: value,
            county: '',
            city: ''
        })

        setCities([])
        setCounties([])
        
        if(value !== ''){            
            let response = await AddressLocation.getCounties(value)
            setCounties([...response])
        }
    }

    const changeCounty = async value => {
        setCompanyAddress({
            ...companyAddress,
            county: value,
            city: ''
        })

        setCities([])

        if(value !== ''){
            let response = await AddressLocation.getCities(value)

            setCities([...response])
        }
    }

    const changeCity = value => {
        setCompanyAddress({
            ...companyAddress,
            city: value
        })
    }

    const getPostalCodes = () => {
        PostalCode.getPostalCodes()
                .then(res => setPostalCodes([...res.filter(pc => pc.activeStatus).map(pc => ({
                    value: pc['@id'],
                    title: pc.title
                }))]))
                .catch(error => console.error(error))
    }

    const getCountries = () => {
        AddressLocation.getCountries()
                .then(async res => {
                    await setCountries([...res])

                    if(props.address.country !== ''){
                        await changeCountry(props.address.country)

                        if(props.address.county !== ''){
                            await changeCounty(props.address.county)

                            if(props.address.city !== '')
                                await changeCity(props.address.city)
                        }                        
                    }
                        
                })
                .catch(error => console.error(error))
    }

    useEffect(() => {
        if(props.user !== ''){
            getPostalCodes()
            getCountries()
        }
    },[])

    const companyInputs = [
        {
            label: t('address.fullName'),
            name: 'name',
            value: company.name,
            type: 'input'
        },
        {
            label: t('register.inputs.bankName'),
            name: 'bankName',
            value: company.bankName,
            type: 'input'
        },                
        {
            label: t('register.inputs.bankAccount'),
            name: 'bankAccount',
            value: company.bankAccount,
            type: 'input'
        },
        {
            label: t('company.swift'),
            name: 'swift',
            value: company.swift,
            type: 'input'
        },
        {
            label: t('company.cif'),
            name: 'cif',
            value: company.cif,
            type: 'input'
        },
        {
            label: t('address.phoneNumber'),
            name: 'regNumber',
            value: company.regNumber,
            type: 'input'
        },
    ]

    const addressInputs = [
        {
            label: t('address.fullName'),
            name: 'fullName',
            value: companyAddress.fullName,
            type: 'text'
        },
        {
            label: t('address.nr'),
            name: 'nr',
            value: companyAddress.nr,
            type: 'number'
        },                
        {
            label: t('address.phoneNumber'),
            name: 'phoneNumber',
            value: companyAddress.phoneNumber,
            type: 'text'
        },
        {
            label: t('address.street'),
            name: 'street',
            value: companyAddress.street,
            type: 'text'
        },
        {
            label: t('address.floor'),
            name: 'floor',
            value: companyAddress.floor,
            type: 'text'
        },
    ]

    const handleChange = input => { 
        setCompany({
            ...company,
            [input.target.name]: input.target.value
        })     
    }

    const handleChangeAddress = input => {
        if(input.target.name === 'nr' && input.target.value === '')
            input.target.value = 0

        if(input.target.name === 'nr' && parseInt(input.target.value) > 0 && input.target.value[0] === '0')
            input.target.value = input.target.value.substr(1,input.target.value.length)


        setCompanyAddress({
            ...companyAddress,
            [input.target.name]: input.target.name === 'nr' ? parseInt(input.target.value) : input.target.value
        })
    }    

    const handleCountryChange = input => changeCountry(input.target.value)

    const handleCountyChange = input => changeCounty(input.target.value)

    const handleCityChange = input => changeCity(input.target.value)

    const saveCompanyAddress = async () => {
        let data = {...companyAddress}

        if(data.postalCode === '')delete data.postalCode
        if(data.customPostCode === '')delete data.customPostCode
        if(data.city === '')delete data.city

        let response = await Address.Save({
                            ...data,
                            user: props.user,
                            deleted: false,
                        })         

        return response.data['@id']
    }

    const saveCompany = async address => {
        const response = await Company.Create({
                            user: props.user,
                            ...company,
                            address,
                            deleted: false,
                        })
                    
        if(props.setCompanyStep2){
            if(typeof props.setCompanyStep2 === 'function'){
                props.setCompanyStep2({
                    company: response.data['@id']
                })
            }
        }
    }

    const handleSubmit = async form => {
        form.preventDefault()
        props.fetching()
        let address = await saveCompanyAddress()
        await saveCompany(address)
        props.success()
    }

    return (
        <Grid item xs={12} md={props.md ? props.md : 8} sm={6}>
            <form onSubmit={handleSubmit} className={classes.container}>
                <div className={matches ? mobileClasses.contTitle : desktopClasses.contTitle}>
                    <Typography className={matches ? mobileClasses.title : desktopClasses.title} gutterBottom>
                        {`${t('general.add')} ${t('general.company')}`}
                    </Typography>
                </div>
                <div className={matches ? mobileClasses.contInputs : desktopClasses.contInputs} >
                    {
                        companyInputs.length
                            ? companyInputs.map(input => <div key={companyInputs.indexOf(input)} className={matches ? mobileClasses.inputDiv : desktopClasses.inputDiv} >
                                                            <Typography className={matches ? mobileClasses.inputLabel : desktopClasses.inputLabel} gutterBottom >
                                                                { input.label }
                                                            </Typography>
                                                            <TextField
                                                                type='text'
                                                                name={input.name}
                                                                variant='outlined'
                                                                value={input.value}
                                                                className={classes.textField}
                                                                onChange={handleChange}
                                                                required
                                                                fullWidth                                                    
                                                            />
                                                        </div>)
                            : <div />
                    }

                    {
                        addressInputs.length
                            ? addressInputs.map(input => <div className={matches ? mobileClasses.inputDiv : desktopClasses.inputDiv} key={addressInputs.indexOf(input)} >
                                                            <Typography className={matches ? mobileClasses.inputLabel : desktopClasses.inputLabel} gutterBottom >
                                                                { input.label }
                                                            </Typography>
                                                            <TextField
                                                                type={input.type}
                                                                name={input.name}
                                                                variant='outlined'
                                                                value={input.value}
                                                                className={classes.textField}
                                                                onChange={handleChangeAddress}
                                                                required
                                                                fullWidth                                                    
                                                            />
                                                        </div>)
                            : <div />
                    }

                    <div className={matches ? mobileClasses.inputDiv : desktopClasses.inputDiv} >
                        <Typography className={matches ? mobileClasses.inputLabel : desktopClasses.inputLabel} gutterBottom >
                            {t('address.postalCode')}
                        </Typography>
                        <FormControl variant='outlined' fullWidth>                            
                            <Select
                                value={companyAddress.postalCode}
                                name='postalCode'
                                className={classes.textField}
                                disabled={companyAddress.postalCode === '' && companyAddress.customPostCode.length > 0}
                                required={!(companyAddress.postalCode === '' && companyAddress.customPostCode.length > 0)}
                                onChange={handleChangeAddress}
                                fullWidth
                            >
                                <MenuItem value=''>
                                    <em>{t('Step1Pets.options.other')}</em>
                                </MenuItem>
                                {
                                    postalCodes.length
                                    ? postalCodes.map(option => <MenuItem key={postalCodes.indexOf(option)} value={option.value}>{option.title}</MenuItem>)
                                    : <MenuItem value='' />
                                }                                
                            </Select>
                        </FormControl>
                    </div>
                    <div className={matches ? mobileClasses.inputDiv : desktopClasses.inputDiv} >
                        <Typography className={matches ? mobileClasses.inputLabel : desktopClasses.inputLabel} gutterBottom >
                            {t('address.customPostalCode')}
                        </Typography>
                        <TextField
                            type='text'
                            name='customPostCode'
                            variant='outlined'
                            className={classes.textField}
                            value={companyAddress.customPostCode}
                            onChange={handleChangeAddress}
                            disabled={companyAddress.postalCode !== ''}
                            required={!(companyAddress.postalCode !== '')}
                            fullWidth                                                    
                        />
                    </div>
                    <div className={matches ? mobileClasses.inputDiv : desktopClasses.inputDiv} >
                        <Typography className={matches ? mobileClasses.inputLabel : desktopClasses.inputLabel} gutterBottom >
                            {t('address.country')}
                        </Typography>
                        <FormControl variant='outlined' fullWidth>                            
                            <Select
                                value={companyAddress.country}
                                name='country'
                                className={classes.textField}
                                required
                                onChange={handleCountryChange}
                                fullWidth
                            >
                                <MenuItem value=''>
                                    <em>None</em>
                                </MenuItem>
                                {
                                    countries.length
                                    ? countries.map(option => <MenuItem key={countries.indexOf(option)} value={`/api/countries/${option.id}`}>{option.name}</MenuItem>)
                                    : <MenuItem value='' />
                                }                                
                            </Select>
                        </FormControl>
                    </div>
                    <div className={matches ? mobileClasses.inputDiv : desktopClasses.inputDiv} >
                        <Typography className={matches ? mobileClasses.inputLabel : desktopClasses.inputLabel} gutterBottom >
                            {t('address.county')}
                        </Typography>
                        <FormControl variant='outlined' fullWidth>                            
                            <Select
                                value={companyAddress.county}
                                name='county'
                                required
                                onChange={handleCountyChange}
                                className={classes.textField}
                                fullWidth
                            >
                                <MenuItem value=''>
                                    <em>None</em>
                                </MenuItem>
                                {
                                    counties.length
                                    ? counties.map(option => <MenuItem key={counties.indexOf(option)} value={`/api/counties/${option.id}`}>{option.name}</MenuItem>)
                                    : <MenuItem value='' />
                                }                                
                            </Select>
                        </FormControl>
                    </div>
                    <div className={matches ? mobileClasses.inputDiv : desktopClasses.inputDiv} >
                        <Typography className={matches ? mobileClasses.inputLabel : desktopClasses.inputLabel} gutterBottom >
                            {t('address.city')}
                        </Typography>
                        <FormControl variant='outlined' fullWidth>                            
                            <Select
                                value={companyAddress.city}
                                name='city'
                                required={cities.leength > 0}
                                className={classes.textField}
                                onChange={handleCityChange}
                                fullWidth
                            >
                                <MenuItem value=''>
                                    <em>None</em>
                                </MenuItem>
                                {
                                    cities.length
                                    ? cities.map(option => <MenuItem key={cities.indexOf(option)} value={`/api/cities/${option.id}`}>{option.name}</MenuItem>)
                                    : <MenuItem value='' />
                                }                                
                            </Select>
                        </FormControl>
                    </div>
                </div>
                <div className={matches ? mobileClasses.contButton : desktopClasses.contButton} >
                    <Button 
                        variant='outlined'
                        size='large' 
                        color='primary'
                        type='submit'
                        fullWidth
                        className={matches ? mobileClasses.button : desktopClasses.button}
                    >
                        {t('buttons.add')}
                    </Button>
                </div>
            </form>
        </Grid>
    )
}

const mapState = state => ({
    user: state.auth.user.data.iri,
    company: state.Company,
    address: state.Address
})

const mapDispatch = {
    ...CompanyActions,
    ...AddressActions,
    ...FetchActions
}

export default connect(mapState,mapDispatch)(Step2AddCompany)