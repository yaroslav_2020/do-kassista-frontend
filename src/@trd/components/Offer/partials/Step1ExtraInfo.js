import React from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import * as OfferActions from 'app/store/actions/OfferActions'

import {
    Radio,
    RadioGroup,
    FormControl,
    FormControlLabel,
    Typography,
    TextareaAutosize,
    useMediaQuery
} from '@material-ui/core'

import { makeStyles , withStyles } from '@material-ui/core/styles'

const StyledRadio = withStyles({
    root: {
        color: '#cacaca',
        '&$checked': {
            color: '#0097F6',
        },
    },
    checked: {},
})((props) => <Radio color='default' {...props} />)

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
    },
    formControl: {
        width: '100%',
        minWidth: 150
    },
    textarea: {
        width: '100%',
        border: '1px solid #cacaca',
        padding: '10px',
        borderRadius: '15px',
        outline: 'none',
        marginTop: '20px'
    }
}))

const desktopStyles = makeStyles({
    container: {
        width: '100%',
        borderBottom: '1px solid #CCC',
        padding: '30px 60px'
    }
})

const mobileStyles = makeStyles({
    container: {
        width: '100%',
        borderBottom: '1px solid #CCC',
        padding: '30px 15px'
    },
    title: {
        fontSize: '18px',
        fontWeight: 'bold',
        color: '#191919',
        textAlign: 'center',
        marginBottom: '17px'
    }
})

const Step1ExtraInfo = props => {

    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const matches = useMediaQuery('(max-width:600px)')

    const {t} = useTranslation()

    const handleRadioChange = radio => {
        props.SaveExtraInfo({
            offerBuilding: {
                accessType: radio.target.value
            }
        })
    }

    const handleChange = input => {
        if(input.target.name === 'accessDescription'){
            props.SaveExtraInfo({
                offerBuilding: {
                    accessDescription: input.target.value
                }
            })
        }else{
            props.SaveStep1({
                otherDetails: input.target.value
            })
        }
    }

    return (
        <>
            <div className={matches ? mobileClasses.container : desktopClasses.container} >
                <Typography variant='subtitle1' align='left' className={mobileClasses.title} gutterBottom>
                    {t('Step1ExtraInfo.access')}
                </Typography>
                <FormControl className={classes.formControl}>
                    <RadioGroup
                        name='accessType'
                        value={props.accessType}
                        onChange={handleRadioChange}
                    >
                        <FormControlLabel value='i_am_at_home' control={<StyledRadio />} label={t('Step1ExtraInfo.accessOptions.i_am_at_home')} />
                        <FormControlLabel value='specs_in_description' control={<StyledRadio />} label={t('Step1ExtraInfo.accessOptions.specs_in_description')} />
                    </RadioGroup>
                </FormControl>
                {
                    props.accessType === 'specs_in_description' && <TextareaAutosize 
                                                                        rowsMin={5}
                                                                        placeholder={t('Step1ExtraInfo.indication')}
                                                                        className={classes.textarea}
                                                                        margin='normal'
                                                                        inputlabelprops={{
                                                                            shrink: true,
                                                                        }}
                                                                        value={props.accessDescription}
                                                                        onChange={handleChange}
                                                                        name='accessDescription' 
                                                                    />
                }            
            </div>
            <div className={matches ? mobileClasses.container : desktopClasses.container} style={{borderBottom: 'none'}} >
                <Typography variant='subtitle1' align='left' className={mobileClasses.title} style={{fontWeight: 'bold'}} gutterBottom>
                    {t('Step1ExtraInfo.otherInformation')}
                </Typography>
                <TextareaAutosize 
                    rowsMin={5}
                    className={classes.textarea}
                    placeholder={t('Step1ExtraInfo.indication')}
                    margin='normal'
                    value={props.otherDetails}
                    onChange={handleChange}
                    inputlabelprops={{
                        shrink: true,
                    }}
                    name='otherDetails'
                />
            </div>
        </>
    )
}

const mapState = state => ({
    accessType: state.Offer.offerBuilding.accessType,
    accessDescription: state.Offer.offerBuilding.accessDescription,
    otherDetails: state.Offer.otherDetails
})

const mapDispatch = {
    ...OfferActions
}

export default connect(mapState,mapDispatch)(Step1ExtraInfo)