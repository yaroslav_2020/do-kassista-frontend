import React from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import * as OfferActions from 'app/store/actions/OfferActions'

import {
    RadioGroup,
    Radio,
    FormControlLabel,
    useMediaQuery
} from '@material-ui/core'

import { makeStyles , withStyles } from '@material-ui/core'

const StyledRadio = withStyles({
    root: {
        color: '#cacaca',
        '&$checked': {
            color: '#0097F6',
        },
    },
    checked: {},
})((props) => <Radio color='default' {...props} />)

const useStyles = makeStyles({
    title: {
        fontSize: '18px',
        fontWeight: 'bold',
        color: '#191919',
        marginBottom: '25px'
    },
    label: {
        fontSize: '16px',
        fontWeight: '500',
        color: '#191919'
    }
})

const desktopStyles = makeStyles({
    container: {
        width: '100%',
        borderBottom: '1px solid #CCC',
        padding: '30px 60px'
    }
})

const mobileStyles = makeStyles({
    container: {
        width: '100%',
        borderBottom: '1px solid #CCC',
        padding: '30px 15px'
    }
})

const Step1CleanningTools = props => {

    const {t} = useTranslation()

    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const matches = useMediaQuery('(max-width:600px)')

    const handleRadioChange = input => {
        props.SaveStep1({
            cleanningTools: input.target.value
        })
    }

    return (
        <div className={matches ? mobileClasses.container : desktopClasses.container} >
            <p className={classes.title}>{t('cleanningTools.title')}</p>
            <RadioGroup
                name='accessType'
                value={props.cleanningTools}
                onChange={handleRadioChange}
            >
                <div style={{
                    display: 'flex',
                    justifyContent: 'flex-start',
                    alignItems: 'center'
                }}>
                    <FormControlLabel value='CLEANNING_TOOLS_YES' className={classes.label} control={<StyledRadio />} label={t('general.yes')} />
                    <span className={classes.label}>({`${props.price_per_cleanning_tools}`} &euro;)</span>
                </div>
                <FormControlLabel value='CLEANNING_TOOLS_NOT' className={classes.label} control={<StyledRadio />} label={t('general.Not')} />
            </RadioGroup>
        </div>
    )
}

const mapState = state => ({
    cleanningTools: state.Offer.cleanningTools,
    price_per_cleanning_tools: state.Settings.price_per_cleanning_tools
})

const mapDispatch = {
    ...OfferActions
}

export default connect(mapState,mapDispatch)(Step1CleanningTools)

