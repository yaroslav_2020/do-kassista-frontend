import React,{ useState , useEffect } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import moment from 'moment'

import * as OfferActions from 'app/store/actions/OfferActions'
import { calculateOfferFullSchedule } from 'app/utils/OfferFullSchedule'

import Step1DateDetails from './Step1DateDetails'
import Step1HoursNeeded from './Step1HoursNeeded'
import CardRadioButtom from '@trd/components/utils/CardRadioButtom'
import ScheduleFormColumn from '@trd/components/utils/ScheduleFormColumn'

import {
    Typography, 
    RadioGroup,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
    firstContainer: {
        width: '100%',
        marginBottom: '50px'
    },
    radioContainer: {
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexWrap: 'wrap'
    },
    optionsContainer: {
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
        flexWrap: 'wrap'

    },
    adviceCont: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
})

const desktopStyles = makeStyles({
    secondContainer:{
        width: '100%',
        padding: '0 50px'
    },
    title: {
        fontSize: '22px',
        fontWeight: 'bold',
        color: '#191919'
    },
    contTitle: {
        marginBottom: '50px'
    },
    scheduleContainer: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap',
        borderRadius: '16px',
        background: '#fff',
        padding: '45px 10px 25px 10px',
        boxShadow: '0px 5px 20px rgba(0,0,0,.06)'
    },
    advice: {
        fontSize: '14px',
        color: '#191919'
    }
})

const mobileStyles = makeStyles({
    secondContainer:{
        width: '100%',
        padding: '0 15px'
    },
    title: {
        fontSize: '18px',
        fontWeight: 'bold',
        color: '#191919'
    },
    contTitle: {
        marginBottom: '25px',
        marginTop: '35px'
    },
    scheduleContainer: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap',
        background: '#fff',
        boxShadow: '0px 3px 6px rgba(0,0,0,.16)',
        borderRadius: '16px',
        padding: '20px'
    },
    chooseDays: {
        width: '100%',
        marginBottom: '20px',
        '& p': {
            fontSize: '16px',
            color: '#191919'
        }
    }
})

const days = [
    'SUNDAY',
    'MONDAY',
    'TUESDAY',
    'WEDNESDAY',
    'THURSDAY',
    'FRIDAY',
    'SATURDAY'
]

const Step1Frequency = props => {

    const {t} = useTranslation();

    const matches = useMediaQuery('(max-width:600px)');

    const classes = useStyles();
    const desktopClasses = desktopStyles();
    const mobileClasses = mobileStyles();

    const [disableDays,setDisableDays] = useState(false);

    const [schedule,setSchedule] = useState([]);


    const radioOptions = [
        {
            value: 'only_once',
            icon: 'info',
            label: t('Step1Frequency.period.only_once')
        },
        {
            value: 'several_times_a_week',
            icon: 'info',
            label: t('Step1Frequency.period.several_times_a_week')
        },
        {
            value: 'each_1_week',
            icon: 'info',
            label: t('Step1Frequency.period.each_1_week'),
            price: '10'
        },
        {
            value: 'each_2_week',
            icon: 'info',
            label: t('Step1Frequency.period.each_2_week'),
            price: '20'
        },
        {
            value: 'each_4_week',
            icon: 'info',
            label: t('Step1Frequency.period.each_4_week'),
            price: '80'
        },       
    ]

    const handleStartAndEndDate = () => {
        const startDate = new Date(props.startFromDate);
        const endDate = new Date(props.endDate);
        (async () => {
            try {
                const response = await calculateOfferFullSchedule({
                    startFromDate: moment(startDate).format('YYYY-MM-DDThh:mm:ssZ'),
                    endDate: moment(endDate).format('YYYY-MM-DDThh:mm:ssZ'),
                    startHour: props.startHour,
                    endHour: props.endHour,
                    days: Object.values(props.periodPoints).filter(periodPoint => periodPoint.checked).map(periodPoint => periodPoint.day)
                });
                setSchedule([...response]);
            } catch (error) {
                console.log('An error in calculating OfferFullSchedule has occurred: ', error);
            }
        })();
    }

    useEffect(() => {
        if(props.startFromDate && props.endDate && props.endHour && props.startHour && (props.endHour !== props.startHour))
            handleStartAndEndDate()
    },[props.startFromDate,props.endDate,props.startHour,props.endHour,props.periodPoints])

    const eneableSpecificDay = () => {
        let day = days[new Date(props.startFromDate).getDay()]

        let periodPoints = Object.values({...props.periodPoints})

        periodPoints = periodPoints.map(periodPoint =>
            [ periodPoint.day.toLowerCase(),{
                ...periodPoint,
                checked: periodPoint.day === day,
            }]
        );

        periodPoints = Object.fromEntries(periodPoints)

        props.SavePeriodPoint(periodPoints)
        setDisableDays(true)
    }

    const resetSchedule = () => {
        let periodPoints = Object.values(props.periodPoints)

        periodPoints = periodPoints.map( periodPoint =>
            [ periodPoint.day.toLowerCase(), {
                ...periodPoint,
                checked: false,
            }]
        );

        periodPoints = Object.fromEntries(periodPoints)
                    
        props.SavePeriodPoint(periodPoints)
        setDisableDays(false)
    }   

    const onChange = value => {
        props.SaveStep1({
            period: value
        })

        if(value !== 'several_times_a_week'){
            eneableSpecificDay()
        }else{
            resetSchedule()
        }
    }

    const showDay = day => {

        let date = new Date(day)
        
        date = moment(date)

        return date.format('DD-MM-YYYY')
    }

    return (
        <>
            <div className={classes.firstContainer}>

                <div className={matches ? mobileClasses.contTitle : desktopClasses.contTitle}>
                    <Typography variant='h5' className={matches ? mobileClasses.title : desktopClasses.title} align='center' gutterBottom>
                        {t('Step1Frequency.title')}
                    </Typography>
                </div>

                <RadioGroup
                    aria-label='position' 
                    name='period' 
                    value={ props.period }
                >
                    <div className={ classes.radioContainer }>
                        {
                            radioOptions.map( option =>
                                <CardRadioButtom 
                                    key={ option.value }
                                    n={ radioOptions.slice(0,2).length }
                                    onClick={ () => onChange(option.value) }
                                    { ...option }
                                />
                            )
                        } 
                    </div>
                </RadioGroup>

            </div>

            <Step1DateDetails />

            <Step1HoursNeeded />

            <div className={matches ? mobileClasses.secondContainer : desktopClasses.secondContainer}>

                <div className={matches ? mobileClasses.scheduleContainer : desktopClasses.scheduleContainer}>
                    {
                        matches && 
                        <div className={mobileClasses.chooseDays} >
                            <p>{ t('general.choose_days') }</p>
                        </div>
                    }
                    {
                        <div style={{width: '100%', margin: '10px 0 10px 25px'}}>
                            {
                                schedule[0] &&
                                <p><strong>{t('general.startDay')}:</strong> {showDay(schedule[0].startHour.date)} / {t( `Schedule.days.${ moment( schedule[0].startHour.date ).format('dddd').toLowerCase() }`)}</p>
                            }
                            {
                                schedule[schedule.length - 1] &&
                                <p><strong>{t('general.endDay')}:</strong> {showDay(schedule[schedule.length - 1].startHour.date)}</p>
                            }
                            
                        </div>
                    }
                    {
                        Object.values( props.periodPoints ).length &&
                        Object.values( props.periodPoints ).map( day => 
                            <ScheduleFormColumn 
                                n={ 7 }
                                canChange={ true }
                                textAlign={ matches ? 'center' : 'left' }
                                key={ Object.values(props.periodPoints).indexOf(day) }
                                day={ day.day.toLowerCase() }
                                disabled={ disableDays }
                            />
                        )
                    }
                </div>
            </div>
        </>
    )
}

const mapState = state => ({
    period: state.Offer.period,
    startFromDate: state.Offer.startFromDate,
    endDate: state.Offer.endDate,
    startHour: state.Offer.startHour,
    endHour: state.Offer.endHour,
    periodPoints: state.Offer.periodPoints,
    hoursNeeded: state.Offer.hoursNeeded
})

const mapDispatch = {
    ...OfferActions
}

export default connect(mapState,mapDispatch)(Step1Frequency)