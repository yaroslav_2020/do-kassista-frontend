import React, { useState , useEffect } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'
import GenericSnackbar from 'app/utils/GenericSnackbar'
import DatePicker from '../../utils/DatePicker'
import moment from 'moment'

import * as OfferActions from 'app/store/actions/OfferActions'

import {
    Typography,
    useMediaQuery,
} from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles'

const desktopStyles = makeStyles({
    title: {
        fontSize: '18px',
        fontWeight: 'bold',
        color: '#191919',
        marginTop: '15px'
    },
    container: {
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
        flexWrap: 'wrap',
        padding: '0 100px'
    },
    textField: {
        width: '220px',
        maxWidth: '100%'
    },
    textFieldCont: {
        minHeight: '125px',
        width: '30%',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'column'
    }
})

const mobileStyles = makeStyles({
    title: {
        fontSize: '18px',
        fontWeight: 'bold',
        color: '#191919',
        marginTop: '25px',
        marginBottom: '13px' 
    },
    container: {
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
        flexWrap: 'wrap',
        padding: '0 15px'
    },
    textFieldCont: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    }
})

const handleMinForEndDate = dateString => {
    let date = new Date(dateString);

    date.setDate( date.getDate() + 1 );

    return date;
}

const initMinPicker = increment => {
    let date = new Date()

    date.setDate( date.getDate() + increment )

    return date
}

const days = [
    'SUNDAY',
    'MONDAY',
    'TUESDAY',
    'WEDNESDAY',
    'THURSDAY',
    'FRIDAY',
    'SATURDAY'
];

const Step1DateDetails = props => {

    useEffect(() => {
        let date = new Date()
        let date2 = new Date()

        date.setDate(date.getDate() + 1)
        date2.setDate(date2.getDate() + 2)

        if(props.startFromDate === '')
            props.SaveStep1({
                'startFromDate': date.toISOString()
            })

        if(props.endDate === '')
            props.SaveStep1({
                'endDate': date2.toISOString()
            })

    }, []);    

    const {t} = useTranslation();
    
    const desktopClasses = desktopStyles();
    const mobileClasses = mobileStyles();

    const matches = useMediaQuery('(max-width:600px)');

    const [snackbar,setSnackbar] = useState({
        open: false,
        message: '', 
        severity: 'success',
    });

    const [disabledDatePicker, setDisabledDatePicker] = useState( true );

    const handleStartHourValue = ( hour = props.startHour ) => {

        let date = moment(`${moment().format('YYYY-MM-DD')} 8:00:00`,'YYYY-MM-DDTHH:mm:ss[Z]');

        // let date = moment( `${moment().format('YYYY-MM-DDTHH:mm')}`,'YYYY-MM-DDTHH:mm:ss[Z]' );

        if (hour !== '' ) { date = moment( `${moment().format('YYYY-MM-DD')} ${hour}:00`,'YYYY-MM-DDTHH:mm:ss[Z]' ) }
        
        if (props.startHour === '' ) { props.SaveStep1({ startHour: date.format('HH:mm') }) }

        return date;
    };    

    const handleCloseSnackbar = value => setSnackbar(value);

    const verifyStartHourChange = ( pickerName, pickerValue ) => {
        let startLaborHour = moment(`${moment().format('YYYY-MM-DD')} ${props.startLaborHour}:00`,'YYYY-MM-DDTHH:mm:ss[Z]');
        let endLaborHour = moment(`${moment().format('YYYY-MM-DD')} ${props.endLaborHour}:00`,'YYYY-MM-DDTHH:mm:ss[Z]');
        let timeSelected = moment( `${moment().format('YYYY-MM-DD')} ${ pickerValue.getHours()}:${ pickerValue.getMinutes() }`, 'YYYY-MM-DDTHH:mm:ss[Z]');
        let periodPoints = Object.values({ ...props.periodPoints });

        if( timeSelected.valueOf() >= startLaborHour.valueOf() && timeSelected.add(props.hoursNeeded, 'hours').valueOf() <= endLaborHour.valueOf() ){
            const date = handleStartHourValue(`${ pickerValue.getHours() }:${ pickerValue.getMinutes() }`);
            props.SaveStep1({
                [ pickerName ]: date.format('HH:mm'),
            })
            // Set the time for the periodPoints.
            periodPoints = periodPoints.map( periodPoint =>
                [ periodPoint.day.toLowerCase(), {
                    ...periodPoint,
                    startHour: date.format('HH:mm'),
                    endHour: moment( date ).add( periodPoint.durationInMinutes, 'minutes' ).format('HH:mm'),
                }]
            );
            periodPoints = Object.fromEntries( periodPoints );
            props.SavePeriodPoint( periodPoints );
        }else{
            setSnackbar({
                open: true,
                message: t('serviceTime.notSaveWithLabor'), 
                severity: 'warning',
            })
        }
    }; 

    const eneableSpecificDay = ( date, isResetOff = true, pastDay = false ) => {

        const day = days[ new Date( date ).getDay() ];

        const startDay = days[ new Date( props.startFromDate ).getDay() ];            
        
        let periodPoints = Object.values({ ...props.periodPoints });

        periodPoints = periodPoints.map( periodPoint =>
            [ periodPoint.day.toLowerCase(), {
                ...periodPoint,
                checked: ( periodPoint.day === day ) || ( pastDay && ( periodPoint.day === startDay ) ) || ( isResetOff &&  (  periodPoint.checked && !(  periodPoint.day === pastDay ) ) ),
                startHour: props.startHour,
                endHour: props.endHour
            }]
        );

        periodPoints = Object.fromEntries( periodPoints );

        props.SavePeriodPoint( periodPoints );
    };

    const handlePicker = ( pickerName, pickerValue ) => {

        if ( pickerName === 'startHour' ) { verifyStartHourChange( pickerName, pickerValue ) }

        else if ( pickerName === 'startFromDate' ) {

            let endDate = new Date( pickerValue.valueOf() );

            endDate.setDate( endDate.getDate() + 1 );

            props.SaveStep1({
                [ pickerName ]: pickerValue.toISOString(),
                endDate: endDate.toISOString()
            });

            eneableSpecificDay( pickerValue.toISOString(), false );

            setDisabledDatePicker( false );

        } else if ( pickerName === 'endDate' ) {

            const pastDay = days[ new Date( props.endDate ).getDay() ];
            
            props.SaveStep1({ [ pickerName ]: pickerValue.toISOString() });
            
            if ( props.endDate !== pickerValue.toISOString() ) { eneableSpecificDay( pickerValue.toISOString(), true, pastDay ) }                

            else { eneableSpecificDay( pickerValue.toISOString() ) }            
        }

    };

    return (    
        <div className={matches ? mobileClasses.container : desktopClasses.container}>            
            <GenericSnackbar 
                handleClose={ handleCloseSnackbar } 
                {...snackbar} 
            />

            <div className={matches ? mobileClasses.textFieldCont : desktopClasses.textFieldCont} >
                <Typography variant='h5' className={matches ? mobileClasses.title : desktopClasses.title} align='center' gutterBottom>
                    {t(props.period !== 'only_once' ? 'Step1Frequency.startDateTitle' : 'Step1Frequency.serviceDay')}
                </Typography>

                <DatePicker 
                    type='date'
                    name='startFromDate'
                    handlePicker={ handlePicker }
                    value={props.startFromDate !== '' ? new Date(props.startFromDate) : initMinPicker(1)}
                    minDate={initMinPicker(1)}
                    className={desktopClasses.textField}
                />
            </div>

            {
                props.period !== 'only_once' &&
                <div className={matches ? mobileClasses.textFieldCont : desktopClasses.textFieldCont}>
                    <Typography variant='h5' className={matches ? mobileClasses.title : desktopClasses.title} align='center' gutterBottom>
                        {t('Step1Frequency.endDateTitle')}
                    </Typography>

                    <DatePicker 
                        type='date'
                        disabled= { disabledDatePicker }
                        name='endDate'
                        handlePicker={ handlePicker }
                        value={props.endDate !== '' ? new Date(props.endDate) : initMinPicker(2)}
                        minDate={props.startFromDate !== '' ? handleMinForEndDate( props.startFromDate ) : initMinPicker(2)}
                        className={ desktopClasses.textField }
                    />
                </div>
            }

            <div className={matches ? mobileClasses.textFieldCont : desktopClasses.textFieldCont}>
                <Typography variant='h5' className={matches ? mobileClasses.title : desktopClasses.title} align='center' gutterBottom>
                    {t('Step1Frequency.startHourTitle')}
                </Typography>

                <DatePicker 
                    type='time'
                    name='startHour'
                    handlePicker={ handlePicker }
                    value={ handleStartHourValue() }
                    className={ desktopClasses.textField }
                />
            </div>

        </div>
    )
}

const mapState = state => ({
    startFromDate: state.Offer.startFromDate,
    endDate: state.Offer.endDate,
    startHour: state.Offer.startHour,
    endHour: state.Offer.endHour,
    startLaborHour: state.Settings.start_labor_schedule,
    endLaborHour: state.Settings.end_labor_schedule,
    hoursNeeded: state.Offer.hoursNeeded,
    period: state.Offer.period,
    periodPoints: state.Offer.periodPoints
})

const mapDispatch = {
    ...OfferActions
}

export default connect(mapState,mapDispatch)(Step1DateDetails)