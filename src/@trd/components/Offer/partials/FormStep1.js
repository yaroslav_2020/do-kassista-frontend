import React from 'react';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';
import moment from 'moment';

import { calculateOfferFullSchedule } from 'app/utils/OfferFullSchedule';
import Step1Apartment from './Step1Apartment';
import Step1CleanningTools from './Step1CleanningTools';
import Step1ExtraInfo from './Step1ExtraInfo';
import Step1Frequency from './Step1Frequency';
import Step1Pets from './Step1Pets';
import Step1Service from './Step1Service';
import { Button, makeStyles } from '@material-ui/core';

import * as OfferActions from 'app/store/actions/OfferActions';

const useStyles = makeStyles(theme => ({
	root: {
		width: '100%'
	},
	container: {
		width: '100%',
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center'
	},
	textField: {
		width: '100%'
	},
	formControl: {
		width: '100%',
		minWidth: 150
	},
	button: {
		width: '300px',
		height: '75px',
		margin: theme.spacing(0),
		color: '#fff',
		fontSize: 22,
		background: '#0097F6',
		'&:hover': {
			background: '#0097F6'
		}
	}
}));

const FormStep1 = ({ setSnackbar, setTabPanel, offer, SaveStep1 }) => {
	const classes = useStyles();

	const { t } = useTranslation();

	const handleSubmit = async form => {
		form.preventDefault();
		let header = document.querySelector('#headerToScroll');
		header.scrollIntoView();

		if (offer.startFromDate !== '' && offer.endDate !== '' && offer.endHour !== '' && offer.startHour !== '') {
			const startFromDate = moment(offer.startFromDate).format('YYYY-MM-DDThh:mm:ssZ');
			const endDate = moment(offer.endDate).format('YYYY-MM-DDThh:mm:ssZ');
			const startHour = offer.startHour;
			const endHour = offer.endHour;
			const days = Object.values(offer.periodPoints)
				.filter(days => days.checked)
				.map(({ day }) => day);

			const serviceSchedule = await calculateOfferFullSchedule({
				startFromDate,
				endDate,
				startHour,
				endHour,
				days
			});

			SaveStep1({ offerFullSchedule: { ...serviceSchedule } });

			if (
				(offer.period === 'only_once' && serviceSchedule.length === 1) ||
				(offer.period !== 'only_once' && serviceSchedule.length >= 2)
			) {
				if (
					(offer.offerBuilding.roomNumber > 0 ||
						offer.offerBuilding.bathRoomNumber > 0 ||
						offer.offerBuilding.kitchenNumber > 0 ||
						offer.offerBuilding.livingNumber > 0) &&
					((offer.period === 'several_times_a_week' &&
						Object.values(offer.periodPoints).filter(day => day.checked).length > 0) ||
						offer.period !== 'several_times_a_week') &&
					offer.pricePerDay > 0 &&
					offer.pricePerService > 0
				) {
					setTabPanel(1);
				} else {
					setSnackbar({
						open: true,
						message: t('Advices.incompleteForm'),
						severity: 'warning'
					});
				}
			} else {
				setSnackbar({
					open: true,
					message: t('Advices.incompleteForm'),
					severity: 'warning'
				});
			}
		} else {
			setSnackbar({
				open: true,
				message: t('Advices.incompleteForm'),
				severity: 'warning'
			});
		}
	};

	return (
		<form onSubmit={handleSubmit}>
			<div className="main-class">
				<Step1Apartment />
				<Step1Service />
				<Step1Frequency />
				<Step1Pets />
				<Step1CleanningTools />
				<Step1ExtraInfo />
			</div>

			<div className={classes.container}>
				<Button type="submit" variant="contained" color="secondary" className={classes.button}>
					{t('buttons.next')}
				</Button>
			</div>
		</form>
	);
};

const mapState = state => ({
	offer: state.Offer
});

const mapDispatch = {
	...OfferActions
};

export default connect(mapState, mapDispatch)(FormStep1);
