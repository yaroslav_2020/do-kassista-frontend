import React from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import * as OfferActions from 'app/store/actions/OfferActions'

import DogImg from 'app/assets/images/dog.png'
import CatImg from 'app/assets/images/cat.png'
import NotImg from 'app/assets/images/not.svg'

import {
    Typography,
    useMediaQuery,
    TextareaAutosize
} from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
    contTextarea: {
        width: '100%'
    },
    optionCont: {
        display: 'flex',
        marginBottom: '40px',
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap'        
    }
})

const desktopStyles = makeStyles({
    container: {
        width: '100%',
        marginTop: '70px',
        borderBottom: '1px solid #CCC',
        padding: '0 60px 50px 60px'
    },
    title: {
        fontWeight: 'bold',
        fontSize: '22px',
        color: '#191919'
    },
    option: {
        width: '85px',
        height: '85px',
        borderRadius: '50%',
        cursor: 'pointer',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        boxShadow: '0px 5px 10px rgba(0,0,0,0.16)',
        margin: '20px 8px'
    },

})

const mobileStyles = makeStyles({
    container: {
        width: '100%',
        marginTop: '50px',
        borderBottom: '1px solid #CCC',
        padding: '0 15px 30px 15px'
    },
    title: {
        fontWeight: 'bold',
        fontSize: '18px',
        color: '#191919'
    },
    label: {
        fontSize: '18px',
        fontWeight: 'bold',
        marginBottom: '20px',
        color: '#191919'
    },
    option: {
        width: '70px',
        height: '70px',
        borderRadius: '50%',
        cursor: 'pointer',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        boxShadow: '0px 5px 10px rgba(0,0,0,0.16)',
        margin: '20px 8px'
    },
})

const Step1Pets = props => {

    const {t} = useTranslation()

    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const matches = useMediaQuery('(max-width:600px)')

    const options = [
        {
            value: 'PET_NOT',
            img: NotImg
        },
        {
            value: 'PET_CAT',
            img: CatImg
        },
        {
            value: 'PET_DOG',
            img: DogImg
        },
        {
            value: 'PET_OTHER'
        }
    ]

    const onChange = value => {
        props.SaveStep1({
            pet: value
        })        
    }

    const handleChange = input => {
        props.SaveStep1({
            otherPet: input.target.value
        })
    }

    return (
        <div className={matches ? mobileClasses.container : desktopClasses.container} >
            <Typography variant='h5' align='center' className={matches ? mobileClasses.title : desktopClasses.title} gutterBottom>
                {t('Step1Pets.title')}
            </Typography>
            <div className={classes.optionCont}>
                {
                    options.map((option,id) => {
                        if(option.img){
                            return <div 
                                        key={id} 
                                        className={matches ? mobileClasses.option : desktopClasses.option} 
                                        onClick={() => onChange(option.value)}
                                        style={{
                                            background: props.pet === option.value ? '#52BAF6' : '#fff'
                                        }}
                                    >
                                        <img src={option.img} className={mobileClasses.content} alt={ t( 'general.pets' ) } />
                                    </div>
                        }else{
                            return <div 
                                        key={id} 
                                        className={matches ? mobileClasses.option : desktopClasses.option}  
                                        onClick={() => onChange(option.value)}
                                        style={{
                                            background: props.pet === option.value ? '#52BAF6' : '#fff',
                                            color: props.pet !== option.value ? '#52BAF6' : '#fff'
                                        }}
                                    >
                                        {t('Step1Pets.options.other')}
                                    </div>
                        }
                    })
                }               
            </div>
            {
                props.pet === 'PET_OTHER' && <div className={classes.contTextarea}>
                                                <p className={mobileClasses.label} >{t('general.whatPetDoYouHave')}</p>
                                                <TextareaAutosize 
                                                    rowsMin={5}
                                                    style={{
                                                        width: '100%',
                                                        border: '1px solid #cacaca',
                                                        padding: '10px',
                                                        borderRadius: '15px',
                                                        outline: 'none',
                                                        marginTop: '10px'
                                                    }}
                                                    placeholder={t('Step1ExtraInfo.indication')}
                                                    margin='normal'
                                                    // value={props.otherDetails}
                                                    onChange={handleChange}
                                                    inputlabelprops={{
                                                        shrink: true,
                                                    }}
                                                    value={props.otherPet}
                                                    name='otherPet'
                                                />
                                                </div>
            } 
        </div>
    )
}

const mapState = state => ({
    pet: state.Offer.pet,
    otherPet: state.Offer.otherPet
})

const mapDispatch = {
    ...OfferActions
}

export default connect(mapState,mapDispatch)(Step1Pets)