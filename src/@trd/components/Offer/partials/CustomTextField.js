import React from 'react';
import { connect } from 'react-redux';
import { TextField, withStyles } from '@material-ui/core';

import * as OfferActions from 'app/store/actions/OfferActions';


const CustomField = withStyles({
    root: {
        '& input + fieldset': {
            borderColor: '#DBDBDB',
            borderWidth: 1,
            borderRadius: '9px'
        },
        '& .Mui-focused .MuiOutlinedInput-notchedOutline': {
            borderColor: '#DBDBDB !important',
            borderWidth: 1,
            borderRadius: '9px'
        },
        '& .MuiOutlinedInput-root:hover fieldset': {
            borderColor: '#DBDBDB !important',
            borderWidth: 1,
        }
    },
    textField: {
        color: '#313131',
        outline: 'none',
        '&>div>fieldset': {
            borderColor: '#DBDBDB',
            borderWidth: 1,
        }
    },

})(TextField);

const CustomTextField = ({ offerBuilding, periodPoints, roomCleaningDurationTime, bathroomCleaningDurationTime, kitchenCleaningDurationTime, livingCleaningDurationTime, min_hours, input, SaveStep1 }) => {

    
    const handleChange = ({ name: key, multiplicatorTime: multiplicator }, { target: { value } }) => {
                
        const hours = {
            roomNumber: offerBuilding.roomNumber * roomCleaningDurationTime,
            bathRoomNumber: offerBuilding.bathRoomNumber * bathroomCleaningDurationTime,
            kitchenNumber: offerBuilding.kitchenNumber * kitchenCleaningDurationTime,
            livingNumber: offerBuilding.livingNumber * livingCleaningDurationTime,
        };
        
        hours[key] = value * multiplicator;

        const duration = min_hours + hours.roomNumber + hours.bathRoomNumber + hours.kitchenNumber + hours.livingNumber;
        
        SaveStep1({
            offerBuilding: {
                ...offerBuilding,
                [ key ]: Number( value )
            },
            hoursNeeded: duration,
            periodPoints: {
                monday: {   
                    ...periodPoints.monday,
                    durationInMinutes: ( duration * 60 )
                },
                tuesday: {
                    ...periodPoints.tuesday,
                    durationInMinutes: ( duration * 60 )
                },
                wednesday: {
                    ...periodPoints.wednesday,
                    durationInMinutes: ( duration * 60 )
                },
                thursday: {
                    ...periodPoints.thursday,
                    durationInMinutes: ( duration * 60 )
                },
                friday: {
                    ...periodPoints.friday,
                    durationInMinutes: ( duration * 60 )
                },
                saturday: {
                    ...periodPoints.saturday,
                    durationInMinutes: ( duration * 60 )
                },
                sunday: {
                    ...periodPoints.sunday,
                    durationInMinutes: ( duration * 60 )
                },
            }
        })       
    }

    return (
        <div>
            <CustomField 
                required
                inputProps={{
                    min: 0
                }}
                className={ CustomField.textField }
                variant='outlined'
                type='number'
                align='left' 
                value={input.value}
                label={input.label}
                fullWidth
                onChange={ (e) => handleChange( input, e ) }
            />
        </div>
    )
}

const mapState = state => ({
    offerBuilding: state.Offer.offerBuilding,
    periodPoints: state.Offer.periodPoints,
    roomCleaningDurationTime: state.Settings.room_cleaning_duration_time,
    bathroomCleaningDurationTime: state.Settings.bathroom_cleaning_duration_time,
    kitchenCleaningDurationTime: state.Settings.kitchen_cleaning_duration_time,
    livingCleaningDurationTime: state.Settings.living_cleaning_duration_time,
    min_hours: state.Settings.min_hours,
})

const mapDispatch = {
    ...OfferActions
}

export default connect(mapState,mapDispatch)(CustomTextField);
