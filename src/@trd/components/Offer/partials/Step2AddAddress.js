import React,{ useState,useEffect } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import * as Address from 'app/utils/Address'
import * as PostalCode from 'app/utils/PostalCode'
import * as AddressLocation from 'app/utils/AddressLocation'
import * as AddressActions from 'app/store/actions/AddressActions'
import * as FetchActions from 'app/store/actions/FetchActions'

import {
    Typography,
    TextField,
    Button,
    Grid,
    MenuItem,
    FormControl,
    Select,
    useMediaQuery
} from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
    container: {
        marginTop: '15px',
        borderRadius: '13px',
        background: '#fff',
        color: '#191919',
        // boxShadow: '0px 5px 10px rgba(0,0,0,.12)'
    },
    textField: {
        borderRadius: '9px',
        marginBottom: '35px',
        '& fieldset': {
            border: '1px solid #DBDBDB'
        },
        '& .MuiInputBase-root:hover fieldset': {
            border: '1px solid #DBDBDB'
        },
        '& .MuiInputBase-root:focus fieldset': {
            border: '1px solid #DBDBDB'
        }
    },
})

const desktopStyles = makeStyles({
    title: {
        fontSize: '25px',
        fontWeight: 'bold',
        color: '#272727',
        textAlign: 'center'
    },
    inputLabel: {
        fontSize: '14px',
        fontWeight: 'bold',
        color: '#313131',
        marginBottom: '12px'
    },
    inputDiv: {
        width: '45%',
    },
    contInputs: {
        width: '100%',
        padding: '20px 40px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        flexWrap: 'wrap'
    },
    contTitle: {
        width: '100%',
        display: 'felx',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        background: '#fff',
        borderRadius: '13px 13px 0px 0px',
        position: 'relative',
        marginTop: '70px'
    },
    button: {
        background: '#0097F6',
        borderRadius: '6px',
        color: '#fff',
        outline: 'none',
        padding: '14px 40px',
        border: 'none',
        width: '300px',
        height: '75px',
        fontSize: '22px',
        fontWeight: 'bold',
        margin: '20px 0px 30px 60px',
        '&:hover': {
            background: '#0097F6',
            border: 'none'
        }
    },
    contButton: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
})

const mobileStyles = makeStyles({
    title: {
        fontSize: '20px',
        fontWeight: 'bold',
        color: '#272727',
        textAlign: 'center'
    },
    inputLabel: {
        fontSize: '14px',
        fontWeight: 'bold',
        color: '#313131',
        marginBottom: '12px'
    },
    inputDiv: {
        width: '100%',
    },
    contInputs: {
        width: '100%',
        padding: '20px 20px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        flexWrap: 'wrap'
    },
    contTitle: {
        background: '#fff',
        // borderBottom: '1px solid #D8D8D8',
        display: 'felx',
        justifyContent: 'center',
        alignItems: 'center',
        padding: '30px 20px',
        borderRadius: '13px 13px 0px 0px',
        position: 'relative',
        marginTop: '30px'
    },
    button: {
        background: '#0097F6',
        borderRadius: '6px',
        color: '#fff',
        outline: 'none',
        padding: '18px 20px',
        border: 'none',
        width: '224px',
        height: '54px',
        fontSize: '15px',
        fontWeight: '500',
        margin: '20px 0px 30px 0px',
        '&:hover': {
            background: '#0097F6',
            border: 'none'
        }
    },
    contButton: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
})

const Step2AddAddress = props => {

    const {t} = useTranslation()

    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const matches = useMediaQuery('(max-width:600px)')

    const [postalCodes,setPostalCodes] = useState([])

    const [countries,setCountries] = useState([])

    const [counties,setCounties] = useState([])

    const [cities,setCities] = useState([])

    const [state,setState] = useState({
        addressType: props.addressType,
        fullName: '',
        nr: '',
        phoneNumber: '',
        street: '',
        floor: '',
        country: '',
        county: '',
        city: '',
        postalCode: '',
        customPostCode: ''
    })

    const inputs = [
        {
            label: t('address.fullName'),
            name: 'fullName',
            value: state.fullName,
            type: 'text'
        },
        {
            label: t('address.nr'),
            name: 'nr',
            value: state.nr,
            type: 'number'
        },                
        {
            label: t('address.phoneNumber'),
            name: 'phoneNumber',
            value: state.phoneNumber,
            type: 'text'
        },
        {
            label: t('address.street'),
            name: 'street',
            value: state.street,
            type: 'text'
        },
        {
            label: t('address.floor'),
            name: 'floor',
            value: state.floor,
            type: 'text'
        },
    ]

    const changeCountry = async value => {
        setState({
            ...state,
            country: value,
            county: '',
            city: ''
        })

        setCities([])
        setCounties([])
        
        if(value !== ''){            
            let response = await AddressLocation.getCounties(value)
            setCounties([...response])
        }
    }

    const changeCounty = async value => {
        setState({
            ...state,
            county: value,
            city: ''
        })

        setCities([])

        if(value !== ''){
            let response = await AddressLocation.getCities(value)

            setCities([...response])
        }
    }

    const changeCity = value => {
        setState({
            ...state,
            city: value
        })
    }

    const getPostalCodes = () => {
        PostalCode.getPostalCodes()
                .then(res => setPostalCodes([...res.filter(pc => pc.activeStatus).map(pc => ({
                    value: pc['@id'],
                    title: pc.title
                }))]))
                .catch(error => console.error(error))
    }

    const getCountries = () => {
        AddressLocation.getCountries()
                .then(async res => {
                    setCountries([...res])                       
                })
                .catch(error => console.error(error))
    }

    useEffect(() => {
        if(props.user !== ''){
            getPostalCodes()
            getCountries()
        }
    },[])

    const handleChange = input => {
        if(input.target.name === 'nr' && input.target.value === '')
            input.target.value = 0

        if(input.target.name === 'nr' && parseInt(input.target.value) > 0 && input.target.value[0] === '0')
            input.target.value = input.target.value.substr(1,input.target.value.length)


        setState({
            ...state,
            [input.target.name]: input.target.name === 'nr' ? parseInt(input.target.value) : input.target.value
        })
    }    

    const handleCountryChange = input => changeCountry(input.target.value)

    const handleCountyChange = input => changeCounty(input.target.value)

    const handleCityChange = input => changeCity(input.target.value)

    const handleSubmit = async form => {
        form.preventDefault()

        const header = document.querySelector('#headerToScroll')
        header.scrollIntoView()
        
        props.fetching()

        let data = {...state}

        if(data.postalCode === '')delete data.postalCode
        if(data.customPostCode === '')delete data.customPostCode
        if(data.city === '')delete data.city

        let response = await Address.Save({
                            ...data,
                            user: props.user,
                            deleted: false,
                        })
        
        props.resetAddressState()

        if(props.setServiceAddress){
            if(typeof props.setServiceAddress === 'function'){
                props.setServiceAddress({
                    serviceAddress: response.data['@id']
                })
            }
        }

        if(props.setBillingAddress){
            if(typeof props.setBillingAddress === 'function'){
                props.setBillingAddress({
                    billingAddress: response.data['@id']
                })
            }
        }


        if((response.status === 201 || response.status === 200) && !props.keep){
            props.success()
        }else{
            props.error()
        }
    }

    return (
        <Grid item xs={12} md={props.md ? props.md : 8} sm={6}>                
            <form onSubmit={handleSubmit} className={classes.container}>
                <div className={matches ? mobileClasses.contTitle : desktopClasses.contTitle}>
                    <Typography className={matches ? mobileClasses.title : desktopClasses.title} gutterBottom>
                        {`${t('general.add')} ${t('general.address')}`}
                    </Typography>
                </div>
                <div className={matches ? mobileClasses.contInputs : desktopClasses.contInputs} >

                    {
                        inputs.length
                            ? inputs.map(input =>  <div className={matches ? mobileClasses.inputDiv : desktopClasses.inputDiv} key={inputs.indexOf(input)} >
                                                        <Typography className={matches ? mobileClasses.inputLabel : desktopClasses.inputLabel} gutterBottom >
                                                            { input.label }
                                                        </Typography>
                                                        <TextField
                                                            type={input.type}
                                                            name={input.name}
                                                            variant='outlined'
                                                            value={input.value}
                                                            className={classes.textField}
                                                            onChange={handleChange}
                                                            required
                                                            fullWidth                                                    
                                                        />
                                                    </div>)
                            : <div />
                    }
                    <div className={matches ? mobileClasses.inputDiv : desktopClasses.inputDiv} >
                        <Typography className={matches ? mobileClasses.inputLabel : desktopClasses.inputLabel} gutterBottom >
                            {t('address.postalCode')}
                        </Typography>
                        <FormControl variant='outlined' fullWidth>                            
                            <Select
                                value={state.postalCode}
                                name='postalCode'
                                className={classes.textField}
                                disabled={state.postalCode === '' && state.customPostCode.length > 0}
                                required={!(state.postalCode === '' && state.customPostCode.length > 0)}
                                onChange={handleChange}
                                fullWidth
                            >
                                <MenuItem value=''>
                                    <em>Otro</em>
                                </MenuItem>
                                {
                                    postalCodes.length
                                    ? postalCodes.map(option => <MenuItem key={postalCodes.indexOf(option)} value={option.value}>{option.title}</MenuItem>)
                                    : <MenuItem value='' />
                                }                                
                            </Select>
                        </FormControl>
                    </div>
                    <div className={matches ? mobileClasses.inputDiv : desktopClasses.inputDiv} >
                        <Typography className={matches ? mobileClasses.inputLabel : desktopClasses.inputLabel} gutterBottom >
                            {t('address.customPostalCode')}
                        </Typography>
                        <TextField
                            type='text'
                            name='customPostCode'
                            variant='outlined'
                            className={classes.textField}
                            value={state.customPostCode}
                            onChange={handleChange}
                            disabled={state.postalCode !== ''}
                            required={!(state.postalCode !== '')}
                            fullWidth                                                    
                        />
                    </div>
                    <div className={matches ? mobileClasses.inputDiv : desktopClasses.inputDiv} >
                        <Typography className={matches ? mobileClasses.inputLabel : desktopClasses.inputLabel} gutterBottom >
                            {t('address.country')}
                        </Typography>
                        <FormControl variant='outlined' fullWidth>                            
                            <Select
                                value={state.country}
                                name='country'
                                className={classes.textField}
                                required
                                onChange={handleCountryChange}
                                fullWidth
                            >
                                <MenuItem value=''>
                                    <em>None</em>
                                </MenuItem>
                                {
                                    countries.length
                                    ? countries.map(option => <MenuItem key={countries.indexOf(option)} value={`/api/countries/${option.id}`}>{option.name}</MenuItem>)
                                    : <MenuItem value='' />
                                }                                
                            </Select>
                        </FormControl>
                    </div>
                    <div className={matches ? mobileClasses.inputDiv : desktopClasses.inputDiv} >
                        <Typography className={matches ? mobileClasses.inputLabel : desktopClasses.inputLabel} gutterBottom >
                            {t('address.county')}
                        </Typography>
                        <FormControl variant='outlined' fullWidth>                            
                            <Select
                                value={state.county}
                                name='county'
                                required
                                className={classes.textField}
                                onChange={handleCountyChange}
                                fullWidth
                            >
                                <MenuItem value=''>
                                    <em>None</em>
                                </MenuItem>
                                {
                                    counties.length
                                    ? counties.map(option => <MenuItem key={counties.indexOf(option)} value={`/api/counties/${option.id}`}>{option.name}</MenuItem>)
                                    : <MenuItem value='' />
                                }                                
                            </Select>
                        </FormControl>
                    </div>
                    <div className={matches ? mobileClasses.inputDiv : desktopClasses.inputDiv} >
                        <Typography className={matches ? mobileClasses.inputLabel : desktopClasses.inputLabel} gutterBottom >
                            {t('address.city')}
                        </Typography>
                        <FormControl variant='outlined' fullWidth>                            
                            <Select
                                value={state.city}
                                name='city'
                                className={classes.textField}
                                required={cities.leength > 0}
                                onChange={handleCityChange}
                                fullWidth
                            >
                                <MenuItem value=''>
                                    <em>None</em>
                                </MenuItem>
                                {
                                    cities.length
                                    ? cities.map(option => <MenuItem key={cities.indexOf(option)} value={`/api/cities/${option.id}`}>{option.name}</MenuItem>)
                                    : <MenuItem value='' />
                                }                                
                            </Select>
                        </FormControl>
                    </div>
                </div>
                <div className={matches ? mobileClasses.contButton : desktopClasses.contButton} >
                    <Button 
                        type='submit'
                        variant='outlined'
                        size='large' 
                        color='primary'
                        fullWidth
                        className={matches ? mobileClasses.button : desktopClasses.button}
                    >
                        {t('buttons.save')}
                    </Button>
                </div>
            </form>
        </Grid>
    )
}

const mapState = state => ({
    user: state.auth.user.data.iri,
    address: state.Address
})

const mapDispatch = {
    ...AddressActions,
    ...FetchActions
}

export default connect(mapState,mapDispatch)(Step2AddAddress)