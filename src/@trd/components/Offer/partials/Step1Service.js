import React, { useCallback } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import * as OfferActions from 'app/store/actions/OfferActions'

import CardCheckBox from '../../utils/CardCheckBox'
// import ExtraServiceSchedule from '../../utils/ExtraServiceSchedule'

import {
    Grid,
    Typography,
    useMediaQuery
} from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
    contTitle:{
        width: '100%',
        textAlign: 'center',
        marginBottom: '40px'
    }
})

const desktopStyles = makeStyles({
    container: {
        width: '100%',
        padding: '0 100px',
        marginBottom: '50px',
        padding: '0 100px'
    },
    title: {
        fontSize: '22px',
        fontWeight: 'bold',
        color: '#191919'
    },
})

const mobileStyles = makeStyles({
    container: {
        width: '100%',
        padding: '0 80px',
        marginBottom: '50px',
        padding: '0 15px'
    },
    title: {
        fontSize: '18px',
        fontWeight: 'bold',
        color: '#191919'
    },
})

const serviceIconOrder = [
    'planchar_ropa',
    'general_cleanning',
    'interior_frigorifico',
    'interior_ventanas',
    'interior_muebles',
    'limpieza_electrodomesticos',
    'compra_supermercado',
    'hacer_comida'
]

const Step1Service = ({ pointServices, SavePointService, title = 'general.chooseTasks' }) => {

    const {t} = useTranslation();

    const classes = useStyles();
    const desktopClasses = desktopStyles();
    const mobileClasses = mobileStyles();

    const matches = useMediaQuery('(max-width:600px)');

    const handleCheckbox = useCallback( ( key, service ) => {
            SavePointService({
                [key]: {
                    ...service,
                    checked: !service.checked
                }
            })
        },
        [ ],
    );

    return (
        <>
            <div className={ matches ? mobileClasses.container : desktopClasses.container }>

            {
                ( !!Object.values( pointServices ).length ) &&
                <>
                    <div className={ classes.contTitle }>
                        <Typography variant='h5' className={ matches ? mobileClasses.title : desktopClasses.title } align='center' gutterBottom > 
                            { t(title) }
                        </Typography>
                    </div>

                    <Grid
                        container
                        justify='space-around'
                        alignItems='center'
                        spacing={0}
                    >
                        {                        
                            Object.values( pointServices ).map( service => 
                                <CardCheckBox 
                                    n={ Object.values(pointServices).length } 
                                    key={ Object.values(pointServices).indexOf(service) }
                                    handleCheckbox={ handleCheckbox }
                                    service ={ service }
                                    name={ `service_${service.id}` }
                                />
                            )
                        }
                    </Grid>
                </>
            
            }

            </div>
        </>
    )
}

const mapState = state => ({
    pointServices: state.Offer.pointServices
})

const mapDispatch = {
    ...OfferActions
}

export default connect(mapState,mapDispatch)(Step1Service)