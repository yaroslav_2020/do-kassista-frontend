import React,{useState} from 'react'
import { connect } from 'react-redux'
import clsx from 'clsx'

import * as OfferActions from '../../../app/store/actions/OfferActions'

import { 
    Grid,
    FormControlLabel,
    Checkbox,
} from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles'

const urlIconCheck = 'data:image/svg+xml;charset=utf-8,%3Csvg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16"%3E%3Cpath fill-rule="evenodd" clip-rule="evenodd" d="M12 5c-.28 0-.53.11-.71.29L7 9.59l-2.29-2.3a1.003 1.003 0 00-1.42 1.42l3 3c.18.18.43.29.71.29s.53-.11.71-.29l5-5A1.003 1.003 0 0012 5z" fill="%23fff"/%3E%3C/svg%3E'

const useStyles = makeStyles({
    grid: {
        margin: '0 10px'
    },
    root: {
        '&:hover': {
            backgroundColor: 'transparent',
        },
    },
    icon: {
        borderRadius: 3,
        width: 16,
        height: 16,
        boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
        backgroundColor: '#fff',
        backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
        '$root.Mui-focusVisible &': {
            outline: '2px auto rgba(19,124,189,.6)',
            outlineOffset: 2,
        },
        'input:hover ~ &': {
            backgroundColor: '#ebf1f5',
        },
        'input:disabled ~ &': {
            boxShadow: 'none',
            background: 'rgba(206,217,224,.5)',
        },
    },
    checkedIcon: {
        backgroundColor: '#52BAF7',
        backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
        '&:before': {
            display: 'block',
            width: 16,
            height: 16,
            backgroundImage:
                `url('${urlIconCheck}')`,
            content: '""',
        },
        'input:hover ~ &': {
            backgroundColor: '#52BAF7',
        },
    }
})

const StyledCheckbox = props => {
    const classes = useStyles()

    return (
        <Checkbox
            className={classes.root}
            disableRipple
            color="default"
            checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
            icon={<span className={classes.icon} />}
            inputProps={{ 'aria-label': 'decorative checkbox' }}
            {...props}
        />
    )
}

const WeekCheckBoxes = props => {

    const classes = useStyles()

    const [state,setState] = useState({
                                monday: props.pointServices[props.service].days.indexOf('MONDAY') >= 0,
                                tuesday: props.pointServices[props.service].days.indexOf('TUESDAY') >= 0,
                                wednesday: props.pointServices[props.service].days.indexOf('WEDNESDAY') >= 0,
                                thursday: props.pointServices[props.service].days.indexOf('THURSDAY') >= 0,
                                friday: props.pointServices[props.service].days.indexOf('FRIDAY') >= 0,
                                saturday: props.pointServices[props.service].days.indexOf('SATURDAY') >= 0,
                                sunday: props.pointServices[props.service].days.indexOf('SUNDAY') >= 0,
                            })

    const days = [
        {
            day: 'MONDAY',
            dia: 'Lun',
            checked: state.monday
        },
        {
            day: 'TUESDAY',
            dia: 'Mar',
            checked: state.tuesday
        },
        {
            day: 'WEDNESDAY',
            dia: 'Mier',
            checked: state.wednesday
        },
        {
            day: 'THURSDAY',
            dia: 'Juev',
            checked: state.thursday
        },
        {
            day: 'FRIDAY',
            dia: 'Vie',
            checked: state.friday
        },
        {
            day: 'SATURDAY',
            dia: 'Sab',
            checked: state.saturday
        },
        {
            day: 'SUNDAY',
            dia: 'Dom',
            checked: state.sunday
        }
    ]

    const handleCheckChange = check => {
        let newDays = [...props.pointServices[props.service].days]
        let exists = newDays.indexOf(check.target.value) >= 0

        if(exists){
            newDays = newDays.filter(d => d !== check.target.value)
        }else{
            newDays.push(check.target.value)            
        }

        props.SavePointService({
            [props.service]: {
                ...props.pointServices[props.service],
                days: newDays
            }
        })
    }

    return (
        <Grid
            container
            justify='space-around'
            className={classes.grid}
            style={{
                maxHeight: '40px'
            }}
        >

            {
                days.length
                    ? days.map(day => {
                            if(props.periodPoints[day.day.toLowerCase()].checked)
                                return <Grid key={days.indexOf(day)} item xs={12} md={Math.floor(12/7)} sm={12}>
                                            <FormControlLabel
                                                value={day.day}
                                                control={<StyledCheckbox checked={props.pointServices[props.service].days.indexOf(day.day) >= 0} 
                                                            onChange={handleCheckChange} 
                                                        />}
                                                label={day.dia}
                                                labelPlacement="start"
                                                name={day.day.toLowerCase()}
                                            />
                                        </Grid>
                      })
                    : <Grid/>
            }
            
        </Grid>
    )
}


const mapState = state => ({
    periodPoints: state.Offer.periodPoints,
    pointServices: state.Offer.pointServices
})

const mapDispatch = {
    ...OfferActions
}

export default connect(mapState,mapDispatch)(WeekCheckBoxes)