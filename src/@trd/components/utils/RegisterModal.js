import React,{ useState , Fragment } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import * as loginActions from 'app/auth/store/actions/loginActions'

import FuseSplashScreen from '@fuse/core/FuseSplashScreen'

import {
    Modal,
    Grid,
    Card,
    CardContent,
    Typography,
    Divider,
    InputAdornment, 
    Icon,
    Button,
    Box,
    FormControlLabel,
    RadioGroup,
    FormControl,
    Radio
} from '@material-ui/core'

import { TextFieldFormsy } from '@fuse/core/formsy'
import Formsy from 'formsy-react'

import { makeStyles } from '@material-ui/core/styles'

const getModalStyle = () => {
    const top = 50
    const left = 50

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    }
}

const useStyles = makeStyles(theme => ({
    paper: {
        position: 'absolute',
        width: 400,
        backgroundColor: theme.palette.background.paper,
        border: '1px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
      }
}))

const RegisterModal = props => {
    const {t,i18n} = useTranslation()

    const classes = useStyles()

    const [modalStyle] = useState(getModalStyle)

    const [loginData,setLoginData] = useState({
                                        email: '',
                                        password: '',
                                        name: '',
                                        confirm_password: '',
                                        bankAccount: '',
                                        bankName: '',
                                        phone_number: ''
                                    })

    const [isValidForm,setIsValidForm] = useState(false)

    const handleSubmit = data => {
        props.SignUp({
            ...data,
            user_type: selectedValue,
            register_mode: true
        })
    }

    const disableButton = () => {
        setIsValidForm(false)
    }

    const enableButton = () => {
        setIsValidForm(true)
    }

    const handleChange = input => {
        setLoginData({
            ...loginData,
            [input.target.name]: input.target.value
        })
    }

    const options = [
        {
            value: 1,
            control: <Radio value={1} color='primary' />,
            label: t('register.options.client'),
            labelPlacement: 'end',
        },
        {
            value: 3,
            control: <Radio value={3} color='primary' />,
            label: t('register.options.independent'),
            labelPlacement: 'end'
        },
        {
            value: 4,
            control: <Radio value={4} color='primary' />,
            label: t('register.options.employee'),
            labelPlacement: 'end'
        }        
    ]

    const [selectedValue,setSelectedValue] = useState(options[0].value)

    const onChange = radio => {
        setSelectedValue(parseInt(radio.target.value))
    }

    const inputs = [
        {
            type: 'text',
            name: 'email',
            label: t('login.inputs.username'),
            value: loginData.email,
            icon: 'email',
        },
        {
            type: 'text',
            name: 'name',
            label: t('register.inputs.name'),
            value: loginData.name,
            icon: 'person_outline',
        },
        {
            type: 'text',
            name: 'phone_number',
            label: t('address.phoneNumber'),
            value: loginData.phone_number,
            icon: 'phone',
        },
        {
            type: 'password',
            name: 'password',
            label: t('login.inputs.password'),
            value: loginData.password,
            icon: 'vpn_key',
        },
        {
            type: 'password',
            name: 'confirm_password',
            label: t('register.inputs.confirmPassword'),
            value: loginData.confirm_password,
            icon: 'vpn_key',
        }
    ]    

    const IndependentOptions = [
        {
            type: 'text',
            name: 'bankName',
            label: t('register.inputs.bankName'),
            value: loginData.bankName,
            icon: 'account_balance',
        },
        {
            type: 'text',
            name: 'bankAccount',
            label: t('register.inputs.bankAccount'),
            value: loginData.bankAccount,
            icon: 'account_balance',
        },
    ]

    const handleRenderComponent = () => {
        if(!props.login.fetching){
            /**
             * if is not fetching show the login form
             */
            return (
                <Modal
                    open={props.open}
                    onClose={props.handleClose}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                >
                    <Card style={modalStyle} className={classes.paper}>
                        <CardContent>
                            <Grid
                                container
                                direction='column'
                                justify='center'
                                alignItems='center'
                                spacing={3}
                            >
                                <Box mb={2}>
                                    <Grid>
                                        <Typography variant='h5' >
                                            {t('register.title')}
                                        </Typography>
                                        <Divider/>
                                    </Grid>
                                </Box>
                                <Formsy
                                    onValidSubmit={handleSubmit}
                                    onValid={enableButton}
                                    onInvalid={disableButton}
                                    className="flex flex-col justify-center w-full"
                                    autoComplete='off'
                                >
                                    <FormControl component='fieldset'>
                                        <RadioGroup 
                                            name='pet' 
                                            row
                                            value={selectedValue}
                                            onChange={onChange}
                                        >
                                            {
                                                options.length
                                                    ? options.map(option => <FormControlLabel
                                                                                key={options.indexOf(option)} 
                                                                                {...option} 
                                                                            />)
                                                    : <FormControlLabel/>
                                            }
                                        </RadioGroup>
                                    </FormControl>
                                    {                                        
                                        inputs.length
                                            ? inputs.map(input => <Grid key={inputs.indexOf(input)}>
                                                                    <TextFieldFormsy
                                                                        className='mb-16'
                                                                        validations={{
                                                                            minLength: 4
                                                                        }}
                                                                        validationErrors={{
                                                                            minLength: 'Min character length is 4'
                                                                        }}
                                                                        InputProps={{
                                                                        endAdornment: <InputAdornment position='end'><Icon className='text-20' color='action'>{input.icon}</Icon></InputAdornment>
                                                                        }}
                                                                        variant='outlined'
                                                                        required
                                                                        onChange={handleChange}  
                                                                        fullWidth 
                                                                        {...input} 
                                                                    />
                                                                </Grid>) 
                                            : <div />
                                    }
                                    {
                                        selectedValue === 'USER_INEDPENDENT_WORKER' && IndependentOptions.length
                                            ? IndependentOptions.map(input => <Grid key={IndependentOptions.indexOf(input)}>
                                                                                    <TextFieldFormsy
                                                                                        className='mb-16'
                                                                                        validations={{
                                                                                            minLength: 4
                                                                                        }}
                                                                                        validationErrors={{
                                                                                            minLength: 'Min character length is 4'
                                                                                        }}
                                                                                        InputProps={{
                                                                                        endAdornment: <InputAdornment position='end'><Icon className='text-20' color='action'>{input.icon}</Icon></InputAdornment>
                                                                                        }}
                                                                                        variant='outlined'
                                                                                        required
                                                                                        onChange={handleChange}  
                                                                                        fullWidth 
                                                                                        {...input} 
                                                                                    />
                                                                                </Grid>)
                                            : <div />
                                    }
                                    <Grid>
                                        <Button
                                            type='submit'
                                            variant='contained'
                                            color='primary'
                                            className='w-full mx-auto mt-16 normal-case'
                                            aria-label='LOG IN'
                                            disabled={!isValidForm}
                                            value='legacy'
                                        >
                                            {t('buttons.register')}
                                        </Button>
                                    </Grid>
                                </Formsy>
                                    
                            </Grid>
                        </CardContent>
                    </Card>   
                </Modal>
            )
        }else{
            /**
             * if is fetching show the spinner
             */
            return <FuseSplashScreen/>
        }
    }

    return (        
        <Fragment>
            {
                handleRenderComponent()
            }
        </Fragment>
    )
}

const mapState = state => ({
    login: state.auth.login,
    user: state.auth.user
})

const mapDispatch = {
    ...loginActions
}

export default connect(mapState,mapDispatch)(RegisterModal)