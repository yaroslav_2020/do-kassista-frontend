import React from 'react'

import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers'

import { 
    createMuiTheme, 
    ThemeProvider,
} from '@material-ui/core/styles'

import DateFnsUtils from '@date-io/date-fns'

const muiTheme = createMuiTheme({
    overrides: {
        layoutPadding: '52px 39px',
        layoutPaddingTop: '52px',
        MuiInput: {
            root: {
                background: '#000'
            }
        }
    },
    palette: {
        primary: {
            light: '#52BAF7',
            main: '#52BAF7',
            dark: '#52BAF7',
            contrastText: '#fff',
        }
    },
    typography: {
        fontSize: 22,
    },
})

const DatePicker = ({ type, name, value, handlePicker, minDate, className, disabled, margin, label }) => {

    return (
        <MuiPickersUtilsProvider utils={DateFnsUtils} >
            <ThemeProvider theme={muiTheme}>
                {
                    type === 'date'
                        ?   <KeyboardDatePicker
                                disablePast                                
                                autoOk={ true }
                                disabled={ disabled }
                                value={ value }
                                name={ name }
                                onChange={ ( value ) => handlePicker( name, value ) }
                                minDate={ minDate ? minDate : new Date() }
                                className={ className ? className : '' }
                                inputVariant='outlined'
                                format='dd/MM/yyyy'
                                KeyboardButtonProps={{
                                    size: 'small',
                                }}    
                            />
                        :    <KeyboardTimePicker
                                disablePast
                                margin={ margin }
                                label={ label }
                                value={ value }
                                name={ name }
                                ampm={ false }
                                onChange={ ( value ) => handlePicker( name, value ) }
                                minDate={ minDate ? minDate : new Date() }
                                className={ className ? className : '' }
                                required
                                inputVariant='outlined'
                                format='HH:mm'
                                KeyboardButtonProps={{
                                    size: 'small'
                                }}
                            />
                }
            </ThemeProvider>
        </MuiPickersUtilsProvider>

    )
}

export default DatePicker;