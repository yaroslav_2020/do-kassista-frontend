import React,{ Fragment } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import * as OfferActions from '../../../app/store/actions/OfferActions'

import WeekCheckBoxes from './WeekCheckBoxes'

import {
    Typography,
    Grid,
    TextField,
    Box,
} from '@material-ui/core'

const ExtraServiceExtraServiceSchedule = props => {

    const {t} = useTranslation()

    const handleChange = input => {
        props.SavePointService({
            [props.service]: {
                ...props.pointServices[props.service],
                hours: input.target.value
            }
        })
    }

    return (
        <Fragment>
            <Typography variant='h6' align='left' gutterBottom>
                { props.pointServices[props.service].name }
            </Typography>
            <Typography variant='body1' align='left' gutterBottom>
                {t('ExtraServiceSchedule.selectDays')}
            </Typography>
            <Box mb={2} mt={2} style={{maxHeight: '80px'}}>
                <WeekCheckBoxes service={props.service} />
            </Box>
            <Grid
                container
                direction='row'
                justify='space-around'
                alignItems='center'
            >
                <Grid item xs={12} md={6} sm={12}>
                    <Typography variant='h6' align='left'>
                        {t('ExtraServiceSchedule.howManyHours')}
                    </Typography>
                    <br/>
                    <Typography variant='body1' align='left' gutterBottom>
                        {t('Advices.adviceFor150m2')}
                    </Typography>
                </Grid>
                <Grid item xs={12} md={6} sm={12}>
                    <TextField 
                        value={props.pointServices[props.service].hours}
                        onChange={handleChange}
                        type='number'
                        variant='outlined' 
                        inputProps={{
                            min: 0
                        }}    
                        name='hours'
                    />
                </Grid>
            </Grid>
        </Fragment>
    )
}

const mapState = state => ({
    pointServices: state.Offer.pointServices
})

const mapDispatch = {
    ...OfferActions
}

export default connect(mapState,mapDispatch)(ExtraServiceExtraServiceSchedule)