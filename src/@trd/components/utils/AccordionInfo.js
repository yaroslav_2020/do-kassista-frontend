import React from 'react'

import {
    Grid,
    Accordion,
    AccordionSummary,
    AccordionDetails,
    Table,
    TableBody,
    TableCell,
    TableRow,
    Typography,
    useMediaQuery
} from '@material-ui/core'

import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
    accordion: {
        transition: 'all 300ms ease',
        borderRadius: '19px',
        margin: '10px 0',
        boxShadow: 'none',
        border: '1px solid #E6E6E6',
        '&:before': {
            display: 'none',
            height: '0px'
        }
    },
    summary: {
        background: '#F4F4F4',
        transition: 'all 300ms ease',
        '& .MuiAccordionSummary-content': {
            textAlign: 'center',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        },
        '&:before': {
            display: 'none',
            height: '0px'
        }
    }
})

const desktopStyles = makeStyles({
    title: {
        fontSize: '22px',
        fontWeight: 'bold',
        color: '#2F2F2F'
    },
    info: {
        fontSize: '18px',
        fontWeight: 'normal',
        color: '#272727'
    }
})

const mobileStyles = makeStyles({
    title: {
        fontSize: '18px',
        fontWeight: 'bold',
        color: '#2F2F2F'
    },
    info: {
        fontSize: '14px',
        fontWeight: 'normal',
        color: '#272727'
    }
})

const AccordionInfo = props => {

    const handleChange = () => {
        props.setExpanded(props.name)
    }

    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const matches = useMediaQuery('(max-width:600px)')

    return (
        <Accordion 
            square 
            expanded={props.expanded === props.name} 
            onChange={handleChange} 
            className={classes.accordion}
        >
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                className={classes.summary}
                style={{
                    borderRadius: props.expanded === props.name ? '19px 19px 0 0' : '19px'
                }}
            >
                <Typography className={matches ? mobileClasses.title : desktopClasses.title } >{props.title}</Typography>
            </AccordionSummary>
            <AccordionDetails style={{overflow: 'hidden'}}>
                <Grid item xs={12}>
                    <Table>
                        <TableBody>
                            {
                                props.info.length
                                    ? props.info.map(info => <TableRow key={props.info.indexOf(info)}>
                                                                <TableCell className={matches ? mobileClasses.info : desktopClasses.info } component="th" scope="row">
                                                                    {info.key}
                                                                </TableCell>
                                                                <TableCell className={matches ? mobileClasses.info : desktopClasses.info } align="right">
                                                                    {
                                                                        Array.isArray(info.value) && info.value.length
                                                                            ? info.value.map(val => <Typography 
                                                                                                        key={info.value.indexOf(val)}  
                                                                                                        className={matches ? mobileClasses.info : desktopClasses.info }
                                                                                                        variant='body1'
                                                                                                    >
                                                                                                        {val}
                                                                                                    </Typography>)
                                                                            : info.value
                                                                    }
                                                                </TableCell>
                                                            </TableRow>)
                                    : <TableRow />
                            }
                        </TableBody>
                    </Table>
                </Grid>
            </AccordionDetails>
        </Accordion>
    )
}

export default AccordionInfo