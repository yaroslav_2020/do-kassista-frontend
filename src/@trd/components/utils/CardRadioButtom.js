import React from 'react'
import { connect } from 'react-redux'
import useMediaQuery from '@material-ui/core/useMediaQuery'

import {
    Radio,
    FormControlLabel,
    Icon,
} from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles'

import CleaningToolActive from 'app/assets/images/icons/cleaning_tool_active.svg'
import CleaningToolInactive from 'app/assets/images/icons/cleaning_tool_inactive.svg'

const desktopStyles = makeStyles(theme => ({
    card: {
        // minWidth: '170px',
        width: '180px',
        borderRadius: '8px',
        padding: '30px 20px',
        height: '105px',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
        margin: '0 10px',
        marginBottom: '30px'
    },
    radio: {
        display: 'none'
    },
    icon: {
        position: 'absolute',
        top: '0',
        left: '67px',
        width: '47px',
        height: '28px',
        borderRadius: '0 0 8px 8px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        color: 'black'
        
    },
    span: {
        fontSize: '17px',
        marginTop: '15px',
        fontWeight: 'medium',
        textAlign: 'center'
    }
}))

const CardRadioButtom = props => {
    const desktopClasses = desktopStyles()

    const matches = useMediaQuery('(max-width:960px)')

    return (
        <div 
            className={desktopClasses.card} 
            style={{
                background: props.selectedPeriod === props.value ? '#0097F6' : '#fff',
                color: props.selectedPeriod === props.value ? '#fff' : '#000000',
                boxShadow: props.selectedPeriod === props.value ? '0px 5px 10px rgba(0,151,246,0.22)' : '0px 5px 10px rgba(0,0,0,0.16)'
            }}
            onClick={props.onClick}
        >
            <div 
                className={desktopClasses.icon}
                style={{
                    background: props.selectedPeriod === props.value ? '#DEF2FF' : '#0097F6',
                    
                }}
            >
                <img src={props.selectedPeriod === props.value ? CleaningToolActive : CleaningToolInactive } alt='cleaning_tool' />
            </div>
            <span className={desktopClasses.span}>{props.label}</span>
         </div>  
    )
}

const mapState = state => ({
    selectedPeriod: state.Offer.period
})

export default connect(mapState,{})(CardRadioButtom)