import React from 'react'
import { connect } from 'react-redux'
import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import { logoutUser } from 'app/auth/store/userSlice'

import * as AlertActions from 'app/store/actions/AlertActions'

import AlertPopup from 'app/utils/AlertPopup'

// import {
//     Accordion,
//     AccordionSummary,
//     AccordionDetails,
//     Table,
//     TableBody,
//     TableCell,
//     TableRow,
//     Grid,
//     Typography
// } from '@material-ui/core'

// import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

import { makeStyles } from '@material-ui/core/styles'

import UserIcon from 'app/assets/images/icons/user-icon.svg'
import UserIcon2 from 'app/assets/images/icons/user-header.svg'
import RequestsIcon from 'app/assets/images/icons/requests.svg'
import LocationIcon from 'app/assets/images/icons/location.svg'
import PadlockIcon from 'app/assets/images/icons/padlock.svg'
import Schedule from 'app/assets/images/icons/calendar.svg'
import LogoutIcon from 'app/assets/images/icons/logout.svg'
import WorkerPayments from 'app/assets/images/icons/menu-worker-payments.svg'

const useStyles = makeStyles({

})

const desktopStyles = makeStyles({
    option: {
        height: "70px",
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "center",
        padding: "25px",
        borderBottom: "1px solid #EBEBEB",
        '& a': {
            fontSize: "19px",
            fontWeight: "500",
            color: "#242424",
            textDecoration: "none",
            marginLeft: "20px",
            cursor: 'pointer'
        },
        '& a:hover': {
            textDecoration: 'none'
        }
    }
})

// const mobileStyles = makeStyles({

// })

const AccordionMenu = props => {

    const {t} = useTranslation()
    
    const dispatch = useDispatch()

    const classes = useStyles()
    const desktopClasses = desktopStyles()

    const handleIcon = icon => {
        switch(icon){
            case 'user':
                return UserIcon
            case 'requests':
                return RequestsIcon
            case 'location':
                return LocationIcon
            case 'padlock':
                return PadlockIcon
            case 'logout':
                return LogoutIcon
            case 'payments':
                return WorkerPayments
            case 'schedule':
                return Schedule
            default: 
                return UserIcon
        }
    }

    return (
        <div className={desktopClasses.option}>
            {
                props.show && <AlertPopup 
                                    text={t('general.logoutConfirmation')}
                                    image={UserIcon2}
                                    confirmAction={() => dispatch(logoutUser())}
                                />
            }
            <img src={handleIcon(props.icon)} alt='menu-icon' />
            {
                props.link && <Link to={props.link}>{props.title}</Link>
            }
            {
                props.logOut && <a onClick={() => props.alertShow()}>{props.title}</a>
            }
        </div>
    )

    // return (
    //     <Accordion 
    //         square 
    //         expanded={props.expanded === props.name} style={{width: '100%'}} 
    //         onChange={props.handleChange(props.name)} 
    //         className={classes.accordion}
    //     >
    //         <AccordionSummary
    //             expandIcon={<ExpandMoreIcon />}
    //         >
    //             <Typography className={desktopClasses.summary} >{ props.title }</Typography>
    //         </AccordionSummary>
    //         <AccordionDetails>
    //             <Grid item xs={12}>
    //                 <Table>
    //                     <TableBody>
    //                         {
    //                             props.options.length
    //                                 ? props.options.map(option => <TableRow key={props.options.indexOf(option)}>
    //                                                                     <TableCell component="th" style={{textAlign: 'justify'}} scope="row">
    //                                                                         {
    //                                                                             option.link !== undefined
    //                                                                                 ? <Link to={option.link} onClick={option.onClick ? option.onClick : ()=>{}} style={{textDecoration: 'none',color:'#000'}} >
    //                                                                                       {option.option}
    //                                                                                   </Link>
    //                                                                                 : option.option
    //                                                                         }
    //                                                                     </TableCell>
    //                                                                 </TableRow>)
    //                                 : <TableRow />
    //                         }
    //                         {
    //                             props.logOut && <TableRow >
    //                                                 <TableCell component="th" style={{textAlign: 'justify'}} scope="row">
    //                                                     <Typography 
    //                                                         variant='body1' 
    //                                                         onClick={() => {
    //                                                             dispatch(logoutUser())
    //                                                         }}  
    //                                                         style={{cursor: 'pointer'}} 
    //                                                         gutterBottom
    //                                                     >
    //                                                         {t('buttons.logout')}
    //                                                     </Typography>
    //                                                 </TableCell>
    //                                             </TableRow>
    //                         }
    //                     </TableBody>
    //                 </Table>
    //             </Grid>
    //         </AccordionDetails>
    //     </Accordion>
    // )
}

const mapSate = state => ({
    show: state.Alert.show
})

const mapDispatch = ({
    ...AlertActions
})

export default connect(mapSate,mapDispatch)(AccordionMenu)