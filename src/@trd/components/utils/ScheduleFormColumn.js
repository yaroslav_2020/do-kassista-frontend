import React,{ useEffect } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'
import clsx from 'clsx'
import moment from 'moment'

import * as OfferActions from '../../../app/store/actions/OfferActions'

import {
    Checkbox,
    FormControlLabel,
    Typography,
    useMediaQuery
} from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles'

import checkboxIcon from 'app/assets/images/icons/checkbox-icon.svg'

const useStyles = makeStyles({
    root: {
        padding: '0',
        '&:hover': {
            backgroundColor: 'transparent',
        },
    },
    icon: {
        borderRadius: 3,
        width: 12,
        height: 12,
        boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
        backgroundColor: '#fff',
        backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
        '$root.Mui-focusVisible &': {
            outline: '2px auto rgba(19,124,189,.6)',
            outlineOffset: 2,
        },
        'input:hover ~ &': {
            backgroundColor: '#ebf1f5',
        },
        'input:disabled ~ &': {
            boxShadow: 'none',
            background: 'rgba(206,217,224,.5)',
        },
    },
    checkedIcon: {
        backgroundColor: '#000',
        backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
        '&:before': {
            display: 'block',
            width: 12,
            height: 12,
            backgroundImage:
                `url('${checkboxIcon}')`,
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            content: '""',
        },
        'input:hover ~ &': {
            backgroundColor: '#000',
        },
    },
    scheduleDay: {
        margin: '0px 3px'
    },
    textField: {
        height: '30px',
        '& input': {
            padding: '8px 5px',
            height: '14px',
        },
        borderRadius: '9px',
    },
    normalInput: {
        '& fieldset': {
            border: '1px solid #DBDBDB'
        },
        '& .MuiOutlinedInput-root:hover .MuiOutlinedInput-notchedOutline': {
            border: '1px solid #DBDBDB',
        }
    },
    disableButton: {
        background: '#fff',
        border: '1px solid #DBDBDB',
        color: '#DBDBDB !important'
    },
    blueInput: {
        background: '#BCE5FF',
        border: '1px solid #0097FE',
    },
    greenInput: {
        background: '#97EAA4',
        border: '1px solid #50A75D',
    },
})

const desktopStyles = makeStyles({
    checkBoxCont: {
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
        '& .MuiFormControlLabel-root': {
            margin: '0 3px',
        },
    },
    checkBoxLabel: {
        minWidth: 'auto', 
        textAlign: 'left',
        fontSize: '14px',
        color: '#191919',
        fontWeight: 'regular'
    },
    infoCont: {
        width: '100%'
    },
    infoLabel: {
        fontSize: '14px',
        color: '#191919',
        margin: '12px 0',
        fontWeight: 'regular'
    },
    scheduleInput: {
        fontSize: '12px',
        fontWeight: '500',
        color: '#2F2F2F',
        borderRadius: '9px',
        width: '90%',
        height: '32px',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: '4px',
        userSelect: 'none',
    }
})

const mobileStyles = makeStyles({
    checkBoxCont: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    checkBoxLabel: {
        minWidth: '80%', 
        textAlign: 'left',
        fontSize: '16px',
        color: '#191919',
        fontWeight: 'medium'
    },
    infoCont: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flexDirection: 'column',
        textAlign: 'left',
        width: '100%'
    },
    infoLabel: {
        fontSize: '16px',
        color: '#191919',
        fontWeight: 'regular'
    },
    scheduleInput: {
        fontSize: '15px',
        fontWeight: '500',
        color: '#000',
        borderRadius: '9px',
        width: '90px',
        height: '40px',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: '12px',
        marginRight: '12px',
        userSelect: 'none',
    }
})

const StyledCheckbox = props => {
    const classes = useStyles()

    return (
        <Checkbox
            className={classes.root}
            disableRipple
            color='default'
            checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
            icon={<span className={classes.icon} />}
            inputProps={{ 'aria-label': 'decorative checkbox' }}
            {...props}
        />
    )
}

const ScheduleFormColumn = props => {

    const {t} = useTranslation()

    const matches = useMediaQuery('(max-width:600px)')

    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()  

    const handleCheckChange = () => {        
        props.SavePeriodPoint({
            [props.day]: {
                ...props.periodPoints[props.day],
                checked: !props.periodPoints[props.day].checked,
            }
        })
    }

    useEffect(() => {
        let endHour = moment(`${moment().format('YYYY-MM-DD')} ${props.startHour}:00`,'YYYY-MM-DD HH:mm:ss')

        endHour.add(moment.duration(props.hoursNeeded,'hours'))

        props.SaveStep1({
            endHour: endHour.format('HH:mm')
        })
    },[props.hoursNeeded,props.startHour])

    return (
        <div 
            className={classes.scheduleDay} 
            style={{
                width: !matches ? '12.5%' : '90%',
                borderRight: (!matches && props.day !== 'sunday') ? '1px solid #DBDBDB' : 'none',
            }}
        >
            <div
                className={matches ? mobileClasses.checkBoxCont : desktopClasses.checkBoxCont}
            >
                <div className={matches ? mobileClasses.checkBoxLabel : desktopClasses.checkBoxLabel} onClick={handleCheckChange} >
                    <span >{ matches ? t(`Schedule.days.${props.day}`) : t(`Schedule.shortDays.${props.day}`)}</span>
                </div>
                <FormControlLabel
                    control={
                        <StyledCheckbox checked={props.periodPoints[props.day].checked} 
                                onChange={handleCheckChange} 
                                value={props.day}
                                disabled={props.disabled}
                        />
                    }
                    labelPlacement='start'
                    name={props.day}
                /> 
            </div>           
            <div 
                style={{
                    display: matches ? (props.periodPoints[props.day].checked ? 'flex' : 'none') : 'flex',
                    flexDirection: matches ? 'row' : 'column', 
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    width: '100%'
                }}
            >
                <div className={`${mobileClasses.infoCont}`} >
                    <Typography 
                        variant='body1' 
                        className={matches ? mobileClasses.infoLabel : desktopClasses.infoLabel} 
                        align={props.textAlign ? props.textAlign : 'left'}
                        style={{
                            opacity: (!matches && props.day !== 'monday') ? 0 : 1
                        }}
                    >
                        {t('Schedule.entry')}
                    </Typography>
                    <p 
                        className={
                            matches 
                                ? `${mobileClasses.scheduleInput} ${props.periodPoints[props.day].checked ? classes.blueInput : classes.disableButton}` 
                                : `${desktopClasses.scheduleInput} ${props.periodPoints[props.day].checked ? classes.blueInput : classes.disableButton}`
                        } 
                    >
                        {
                            props.startHour !== ''
                                ?   props.startHour
                                :   '00:00'
                        }
                    </p>

                </div>

                <div className={mobileClasses.infoCont} >
                    <Typography 
                        variant='body1' 
                        className={matches ? mobileClasses.infoLabel : desktopClasses.infoLabel} 
                        align={props.textAlign ? props.textAlign : 'left'}
                        style={{
                            opacity: (!matches && props.day !== 'monday') ? 0 : 1
                        }}
                    >
                        {t('Schedule.output')}
                    </Typography>
                    <p
                        className={
                            matches 
                                ? `${mobileClasses.scheduleInput} ${props.periodPoints[props.day].checked ? classes.greenInput : classes.disableButton}` 
                                : `${desktopClasses.scheduleInput} ${props.periodPoints[props.day].checked ? classes.greenInput : classes.disableButton}`
                        } 
                    >
                        {
                            props.endHour !== ''
                                ?   props.endHour
                                :   '00:00'
                        }
                    </p>

                </div>
            </div>
        </div>
    )
}

const mapState = state => ({
    periodPoints: state.Offer.periodPoints,
    startHour: state.Offer.startHour,
    hoursNeeded: state.Offer.hoursNeeded,
    endHour: state.Offer.endHour
})

const mapDispatch = {
    ...OfferActions
}

export default connect(mapState,mapDispatch)(ScheduleFormColumn)