import React from 'react'
// import useMediaQuery from '@material-ui/core/useMediaQuery'

import { makeStyles } from '@material-ui/core/styles'

// import CleaningToolActive from 'app/assets/images/icons/cleaning_tool_active.svg'
// import CleaningToolInactive from 'app/assets/images/icons/cleaning_tool_inactive.svg'

import GeneralCleanning from 'app/assets/images/icons/general_cleanning.svg'
import GeneralCleanningHover from 'app/assets/images/icons/general_cleanning_hover.svg'
import PlancharRopa from 'app/assets/images/icons/planchar_ropa.svg'
import PlancharRopaHover from 'app/assets/images/icons/planchar_ropa_hover.svg'
import LimpiezaElectrodomestico from 'app/assets/images/icons/limpieza_electrodomesticos.svg'
import LimpiezaElectrodomesticoHover from 'app/assets/images/icons/limpieza_electrodomesticos_hover.svg'
import InteriorMuebles from 'app/assets/images/icons/interior_muebles.svg'
import InteriorMueblesHover from 'app/assets/images/icons/interior_muebles_hover.svg'
import InteriorVentanas from 'app/assets/images/icons/interior_ventanas.svg'
import InteriorVentanasHover from 'app/assets/images/icons/interior_ventanas_hover.svg'
import HacerComida from 'app/assets/images/icons/hacer_comida.svg'
import HacerComidaHover from 'app/assets/images/icons/hacer_comida_hover.svg'
import InteriorFrigorifico from 'app/assets/images/icons/interior_frigorifico.svg'
import InteriorFrigorificoHover from 'app/assets/images/icons/interior_frigorifico_hover.svg'
import CompraSupermercado from 'app/assets/images/icons/compra_supermercado.svg'
import CompraSupermercadoHover from 'app/assets/images/icons/compra_supermercado_hover.svg'

const desktopStyles = makeStyles(theme => ({
    card: {
        // minWidth: '170px',
        width: '140px',
        borderRadius: '8px',
        padding: '30px 20px',
        height: '105px',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
        margin: '0 10px',
        marginBottom: '30px'
    },
    radio: {
        display: 'none'
    },
    icon: {
        position: 'absolute',
        top: '0',
        left: '50px',
        width: '47px',
        height: '28px',
        borderRadius: '0 0 8px 8px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    span: {
        fontSize: '15px',
        marginTop: '15px',
        fontWeight: 'medium',
        textAlign: 'center'
    }
}))

const handleInactiveIcon = icon => {
    switch(icon){
        case 'general_cleanning':
            return GeneralCleanningHover
        case 'planchar_ropa':
            return PlancharRopaHover
        case 'limpieza_electrodomesticos':
            return LimpiezaElectrodomesticoHover
        case 'interior_muebles':
            return InteriorMueblesHover
        case 'interior_ventanas':
            return InteriorVentanasHover
        case 'hacer_comida':
            return HacerComidaHover
        case 'interior_frigorifico':
            return InteriorFrigorificoHover
        case 'compra_supermercado':
            return CompraSupermercadoHover
        default: 
            return GeneralCleanningHover
    }
}

const handleActiveIcon = icon => {
    switch(icon){
        case 'general_cleanning':
            return GeneralCleanning
        case 'planchar_ropa':
            return PlancharRopa
        case 'limpieza_electrodomesticos':
            return LimpiezaElectrodomestico
        case 'interior_muebles':
            return InteriorMuebles
        case 'interior_ventanas':
            return InteriorVentanas
        case 'hacer_comida':
            return HacerComida
        case 'interior_frigorifico':
            return InteriorFrigorifico
        case 'compra_supermercado':
            return CompraSupermercado
        default: 
            return GeneralCleanning
    }
}

const CardChexBox = ({ service, handleCheckbox, name }) => {

    const desktopClasses = desktopStyles()

    // const matches = useMediaQuery('(max-width:600px)')

    return (
        <div 
            className={ desktopClasses.card } 
            style={{
                background: service.checked ? '#0097F6' : '#fff',
                color: service.checked ? '#fff' : '#000000',
                boxShadow: service.checked ? '0px 5px 10px rgba(0,151,246,0.22)' : '0px 5px 10px rgba(0,0,0,0.16)'
            }}
            onClick={ () => handleCheckbox( name, service ) }
        >
            <div 
                className={ desktopClasses.icon }
                style={{
                    background: service.checked ? '#DEF2FF' : '#0097F6',handleInactiveIcon,
                    alignSelf: 'auto'

                }}
            >
                <img src={ service.checked ?
                    handleInactiveIcon( service.icon ) 
                    : handleActiveIcon( service.icon ) }    
                    alt='cleaning_tool'
                />
            </div>
            <span className={ desktopClasses.span }>{ service.name }</span>
         </div>  
    )
}

export default React.memo(CardChexBox);