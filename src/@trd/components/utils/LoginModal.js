import React,{ useState , Fragment } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import * as loginActions from 'app/auth/store/actions/loginActions'

import FuseSplashScreen from '@fuse/core/FuseSplashScreen'

import {
    Modal,
    Grid,
    Card,
    CardContent,
    Typography,
    Divider,
    InputAdornment, 
    Icon,
    Button,
    Box
} from '@material-ui/core'

import { TextFieldFormsy } from '@fuse/core/formsy'
import Formsy from 'formsy-react'

import { makeStyles } from '@material-ui/core/styles'

const getModalStyle = () => {
    const top = 50
    const left = 50

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    }
}

const useStyles = makeStyles(theme => ({
    paper: {
        position: 'absolute',
        width: 400,
        backgroundColor: theme.palette.background.paper,
        border: '1px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
      }
}))

const LoginModal = props => {
    const {t} = useTranslation()

    const classes = useStyles()

    const [modalStyle] = useState(getModalStyle)

    const [loginData,setLoginData] = useState({
                                        email: '',
                                        password: ''
                                    })

    const [isValidForm,setIsValidForm] = useState(false)

    const handleSubmit = form => {
        props.Fetching()
        props.SignIn(loginData)
    }

    const disableButton = () => {
        setIsValidForm(false)
    }

    const enableButton = () => {
        setIsValidForm(true)
    }

    const handleChange = input => {
        setLoginData({
            ...loginData,
            [input.target.name]: input.target.value
        })
    }

    const inputs = [
        {
            className: 'mb-16',
            type: 'text',
            name: 'email',
            label: t('login.inputs.username'),
            validations: {
                minLength: 4
            },
            validationErrors: {
                minLength: 'Min character length is 4'
            },
            InputProps: {
                endAdornment: <InputAdornment position='end'><Icon className='text-20' color='action'>email</Icon></InputAdornment>
            },
            variant: 'outlined',
            required: true,
            value: loginData.email,
            onChange: handleChange
        },
        {
            className: 'mb-16',
            type: 'password',
            name: 'password',
            label: t('login.inputs.password'),
            validations: {
                minLength: 4
            },
            validationErrors: {
                minLength: 'Min character length is 4'
            },
            InputProps: {
                endAdornment: <InputAdornment position='end'><Icon className='text-20' color='action'>vpn_key</Icon></InputAdornment>
            },
            variant: 'outlined',
            required: true,
            value: loginData.password,
            onChange: handleChange
        }
    ]    

    const handleRenderComponent = () => {
        if(!props.login.fetching){
            /**
             * if is not fetching show the login form
             */
            return (
                <Modal
                    open={props.open}
                    onClose={props.handleClose}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                >
                    <Card style={modalStyle} className={classes.paper}>
                        <CardContent>
                            <Grid
                                container
                                direction='column'
                                justify='center'
                                alignItems='center'
                                spacing={3}
                            >
                                <Box mb={2}>
                                    <Grid>
                                        <Typography variant='h5' >
                                            {t('login.title')}
                                        </Typography>
                                        <Divider/>
                                    </Grid>
                                </Box>
                                <Formsy
                                    onValidSubmit={handleSubmit}
                                    onValid={enableButton}
                                    onInvalid={disableButton}
                                    className="flex flex-col justify-center w-full"
                                    autoComplete='off'
                                >
                                    {                                        
                                        inputs.length
                                            ? inputs.map(input => <Grid key={inputs.indexOf(input)}>
                                                                    <TextFieldFormsy  fullWidth {...input} />
                                                                </Grid>) 
                                            : <div></div>
                                    }
                                    <Grid>
                                        <Button
                                            type='submit'
                                            variant='contained'
                                            color='primary'
                                            className='w-full mx-auto mt-16 normal-case'
                                            aria-label='LOG IN'
                                            disabled={!isValidForm}
                                            value='legacy'
                                        >
                                           {t('buttons.login')}
                                        </Button>
                                    </Grid>
                                </Formsy>
                                    
                            </Grid>
                        </CardContent>
                    </Card>   
                </Modal>
            )
        }else{
            /**
             * if is fetching show the spinner
             */
            return <FuseSplashScreen/>
        }
    }

    return (        
        <Fragment>
            {
                handleRenderComponent()
            }
        </Fragment>
    )
}

const mapState = state => ({
    login: state.auth.login,
    user: state.auth.user
})

const mapDispatch = {
    ...loginActions
}

export default connect(mapState,mapDispatch)(LoginModal)