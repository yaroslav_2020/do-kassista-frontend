import React , { useState } from 'react'

import {
    Accordion,
    AccordionSummary,
    AccordionDetails,
    useMediaQuery
} from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles'

import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

const useStyles = makeStyles({
    accordion: {
        boxShadow: 'none',
        width: '100%',
        paddinG: '30px 25px'
    },
    details: {
        textAlign: 'justify'
    }
})

const desktopStyles = makeStyles({
    text: {
        fontSize: '17px',
        color: '#242424'
    },
    detailText: {
        fontSize: '14px',
        color: '#242424'
    }
})

const mobileStyles = makeStyles({
    text: {
        fontSize: '14px',
        color: '#242424'
    },
    detailText: {
        fontSize: '12px',
        color: '#242424'
    }
})

const FaqAccordion = props => {

    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const matches = useMediaQuery('(max-width:600px)')

    return (
        <Accordion className={classes.accordion} expanded={props.expanded === props.name} onChange={props.handleChange(props.name)}>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                className={matches ? mobileClasses.text : desktopClasses.text}
            >
                {
                    props.question
                }
            </AccordionSummary>
            <AccordionDetails 
                className={`${classes.details} ${matches ? mobileClasses.detailText : desktopClasses.detailText}`}
            >
                {
                    props.answer
                }
            </AccordionDetails>
        </Accordion>
    )
}

export default FaqAccordion