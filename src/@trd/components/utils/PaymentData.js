import React,{ useState , useEffect } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import * as PaymentActions from 'app/store/actions/PaymentActions'
import * as OfferActions from 'app/store/actions/OfferActions'

import GenericSnackbar from 'app/utils/GenericSnackbar'

import {
    Typography,
    Box,
    Grid,
    TextField
} from '@material-ui/core'

import {
    CardNumberElement,
    CardExpiryElement,
    CardCvcElement,
    useElements,
    useStripe,
    CardElement
} from '@stripe/react-stripe-js'

import { makeStyles } from '@material-ui/core/styles'
import './CardSectionStyles.css'

import PaymentLogos from 'app/assets/images/payment-logos.png'
import { createSetupSecretIntent } from 'app/utils/PaymentIntent'

const CARD_ELEMENT_OPTIONS = {
    style: {
      base: {
        color: "#32325d",
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: "antialiased",
        fontSize: "16px",
        "::placeholder": {
          color: "#aab7c4",
        },
      },
      invalid: {
        color: "#fa755a",
        iconColor: "#fa755a",
      },
    },
  };

const useStyles = makeStyles({
    cardContainer: {

    },
    cardInput: {
        border: '1px solid #DBDBDB',
        padding: '20px 25px',
        borderRadius: '9px',
        fontSize: '16px',
        marginBottom: '36px'
    },
    cardLabel: {
        fontSize: '20px',
        fontWeight: 'bold',
        color: '#272727',
        marginBottom: '12px'
    },
    textField: {
        '& .MuiInputBase-root fieldset': {
            border: '1px solid #DBDBDB',
            borderRadius: '9px',
            borderColor: '#DBDBDB'
        },
        '& .MuiInputBase-root fieldset:hover': {
            border: '1px solid #DBDBDB',
            borderRadius: '9px',
            borderColor: '#DBDBDB'
        },
        '& .MuiInputBase-root:focus fieldset': {
            border: '1px solid #DBDBDB',
            borderRadius: '9px',
            borderColor: '#DBDBDB'
        },
        '& .MuiInputBase-root:hover fieldset': {
            border: '1px solid #DBDBDB',
            borderRadius: '9px',
            borderColor: '#DBDBDB'
        },
        '& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline': {
            border: '1px solid #DBDBDB',
            borderRadius: '9px',
            borderColor: '#DBDBDB'
        }
    }
})

const desktopStyles = makeStyles({
    
})

const mobileStyles = makeStyles({

})

const PaymentData = props => {

    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()

    const {t} = useTranslation()

    const stripe = useStripe()
    const elements = useElements()

    const [error,setError] = useState({
        cardNumber: '',
        expiresDate: '',
        cvc: '',
        generalError: ''
    })

    const [stripeFormValid, setStripeFormValid] = useState({
        cardName: false,
        cardNumber: false,
        cardExpiry: false,
        cardCvc: false
    })

    const [snackbar,setSnackbar] = useState({
        open: false,
        message: '', 
        severity: 'success',
    })

    const handleCloseSnackbar = value => setSnackbar(value)

    // const stripePaymentMethodHandler = (result) => {

    //     console.log('enter result', result);
    //     if (result.error) {
    //         // Show error in payment form
    //     } else {
    //         // Otherwise send paymentMethod.id to your server (see Step 4)

    //     }
    // }

    useEffect(() => { 
        // if(props.paymentActive){            
        if(stripeFormValid.cardName && stripeFormValid.cardNumber && stripeFormValid.cardExpiry && stripeFormValid.cardCvc ){
            props.setFetching(true)
            
            const cardNumber = elements.getElement(CardNumberElement)

            createSetupSecretIntent({
                    // payment_method: result.paymentMethod.id,
                    // amount: props.pricePerService,
                    customer: props.clientEmail
            })            
            .then( result => {
                console.log('enter server response', result);

                if (result.data.setupIntent) {
                    stripe.confirmCardSetup(
                        result.data.setupIntent.client_secret,  //SERVER RESPONSE FOR OPERATION
                        {
                            payment_method: {card: cardNumber}                        
                        }
                    )
                    .then(response => {
    
                        console.log('enter confirmCardSetup response body', response);
                        if(response.error){
                            console.log('enter error', response);

                            setError({
                                ...error,
                                // generalError: t(`payment.stripe.error.${response.error.code}`)
                                generalError: response.error.message
                            })
    
                            setSnackbar({
                                open: true,
                                // message: t(`payment.stripe.error.${response.error.code}`), 
                                message: response.error.message, 
                                severity: 'error',
                            })
                        }else{
                            if (response.setupIntent.status === 'succeeded') {

                                console.log('enter response succeeded', response );


                                setError({
                                    ...error,
                                    generalError: ''
                                })
    
                                setSnackbar({
                                    open: true,
                                    message: t('payment.stripe.succeeded'), 
                                    severity: 'success',
                                })
                                props.setPaymentToken(response.setupIntent.payment_method);
                                props.pay();                                
                                // props.setPaymentToken(response.token.id)                                
                            }
                        }
                        props.setFetching(false)                        
                        // props.finish()
                    })
                    .catch(error => console.error(error))                    
                } else {
                    console.log('enter no entra en confirm');
                }

                // if (result.data.paymentIntents) {
                //     stripe.confirmCardPayment(
                //         result.data.paymentIntents.client_secret,  //SERVER RESPONSE FOR OPERATION
                //         {
                //             payment_method: {card: cardNumber}                        
                //         }
                //     )
                //     .then(response => {
    
                //         console.log('enter confirmCardPayment response body', response);
                //         // if(response.error){
                //         //     setError({
                //         //         ...error,
                //         //         // generalError: t(`payment.stripe.error.${response.error.code}`)
                //         //     })
    
                //         //     setSnackbar({
                //         //         open: true,
                //         //         message: t(`payment.stripe.error.${response.error.code}`), 
                //         //         severity: 'error',
                //         //     })
                //         // }else{
                //         //     setError({
                //         //         ...error,
                //         //         generalError: ''
                //         //     })
                //         //     props.setPaymentToken(response.token.id)
                //         // }
                        
                //         // props.finish()
                //     })
                //     .catch(error => console.error(error))                    
                // } else {
                //     console.log('enter no entra en confirm');
                // }

            }
            //       function(result) {
            //     // Handle server response (see Step 4)
            //     result.json().then(function(json) {
            //       handleServerResponse(json);
            //     })
            //   }
            );            

            // stripe.createPaymentMethod({
            //     type: 'card',
            //     card: cardNumber,
            //     billing_details: {
            //     // Include any additional collected billing details.
            //     name: props.cardHolderName,
            //     },
            // }).then(stripePaymentMethodHandler);

            
            // stripe.createToken(cardNumber)
            //     .then(response => {
            //         console.log("ENTER RESPONSE", response);
            //         if(response.error){
            //             setError({
            //                 ...error,
            //                 // generalError: t(`payment.stripe.error.${response.error.code}`)
            //             })

            //             setSnackbar({
            //                 open: true,
            //                 message: t(`payment.stripe.error.${response.error.code}`), 
            //                 severity: 'error',
            //             })
            //         }else{
            //             setError({
            //                 ...error,
            //                 generalError: ''
            //             })
            //             props.setPaymentToken(response.token.id)
            //         }
                    
            //         props.finish()
            //     })
            //     .catch(error => console.error(error))
        } else {
            props.finish();
        }
    },[stripeFormValid])

    // useEffect(() => { 
    //     if(props.paymentActive){
    //         let cardNumber = elements.getElement(CardNumberElement)
    //         stripe.confirmCardPayment(
    //                 INTENT_SECRET_FROM_STEP_1,  //SERVER RESPONSE FOR OPERATION
    //                 {
    //                     payment_method: {card: cardNumber}                        
    //                 }
    //             )
    //             .then(response => {
    //                 if(response.error){
    //                     setError({
    //                         ...error,
    //                         // generalError: t(`payment.stripe.error.${response.error.code}`)
    //                     })

    //                     setSnackbar({
    //                         open: true,
    //                         message: t(`payment.stripe.error.${response.error.code}`), 
    //                         severity: 'error',
    //                     })
    //                 }else{
    //                     setError({
    //                         ...error,
    //                         generalError: ''
    //                     })
    //                     props.setPaymentToken(response.token.id)
    //                 }
                    
    //                 props.finish()
    //             })
    //             .catch(error => console.error(error))
    //     }
    // },[props.paymentActive])

    const handleCardNumberChange = input => {     
        const cardNumber = elements.getElement(CardNumberElement)

        setStripeFormValid( c => ({
            ...c,
            [input.elementType]: input.complete 
        }))        

        setError({
            ...error,
            cardNumber: input.error !== undefined ? t(`payment.stripe.error.${input.error.code}`) : ''
        })
    }

    const handleExpiresDateChange = input => {

        setStripeFormValid( c => ({
            ...c,
            [input.elementType]: input.complete
        }))     

        setError({
            ...error,
            expiresDate: input.error !== undefined ? t(`payment.stripe.error.${input.error.code}`) : ''
        })
    }

    const handleCvcChange = input => {
        setStripeFormValid( c => ({
            ...c,
            [input.elementType]: input.complete
        }))
        setError({
            ...error,
            cvc: input.error !== undefined ? t(`payment.stripe.error.${input.error.code}`) : ''
        })
    }

    const handleChangeCardHolderName = input => {
        setStripeFormValid( c => ({
            ...c,
            cardName: input.target.value && true
        }))
    
        props.SaveStep1({
            cardHolderName: input.target.value
        })
    }

    return (
        <Box mt={2} mb={2} >
            <GenericSnackbar 
                handleClose={handleCloseSnackbar} 
                {...snackbar} 
            />
            <Grid
                container
                direction='row'
                justify='space-around'
                alignItems='center'
                spacing={2}
            >

                {/* <Grid item xs={12} md={12} sm={12} >
                    <Typography variant='h5' align='center' style={{fontWeight: 'bold'}} gutterBottom >
                        {t('payment.title')}
                    </Typography>                    
                </Grid>
                <Grid item xs={12} md={12} sm={12} >
                    <p className={classes.cardLabel} >{t('payment.customerName')}</p>
                    <CardElement options={CARD_ELEMENT_OPTIONS} />
                </Grid> */}




                <Grid item xs={12} md={12} sm={12} >
                    <Typography variant='h5' align='center' style={{fontWeight: 'bold'}} gutterBottom >
                        {t('payment.title')}
                    </Typography>                    
                </Grid>
                <Grid item xs={12} md={12} sm={12} >
                    <p className={classes.cardLabel} >{t('payment.customerName')}</p>
                    <TextField 
                        fullWidth
                        variant='outlined'
                        value={props.cardHolderName}
                        onChange={handleChangeCardHolderName}
                        className={classes.textField}
                    />
                </Grid>
                <Grid item xs={12} md={12} sm={12} >
                    <p className={classes.cardLabel} >{t('payment.card')}</p>
                    <div className={classes.cardInput} >
                        <CardNumberElement 
                            options={{
                                style: {
                                    base: {
                                        fontSize: '16px',
                                        fontWeight: '400',
                                        color: '#272727'
                                    }
                                },
                            }}

                            onChange={handleCardNumberChange}
                        />
                        <p>{error.cardNumber}</p>
                    </div>
                </Grid>
                <Grid item xs={12} md={6} sm={12} >
                    <p className={classes.cardLabel} >{t('payment.expires')}</p>
                    <div className={classes.cardInput} >
                        <CardExpiryElement 
                            options={{
                                style: {
                                    base: {
                                        fontSize: '18px'
                                    }
                                },
                            }}

                            onChange={handleExpiresDateChange}
                        />
                        <p>{error.expiresDate}</p>
                    </div>
                </Grid>
                <Grid item xs={12} md={6} sm={12} >
                    <p className={classes.cardLabel} >{t('payment.cvc')}</p>
                    <div className={classes.cardInput} >
                        <CardCvcElement 
                            options={{
                                style: {
                                    base: {
                                        fontSize: '18px'
                                    }
                                },
                            }}

                            onChange={handleCvcChange}
                        />
                        <p>{error.cvc}</p>
                    </div>
                </Grid>
                <Grid item xs={12} md={12} sm={12} >
                    <img src={PaymentLogos} />                        
                </Grid>
                <Grid item xs={12} md={6} sm={12} >
                    <Box mt={4}>
                        <Typography variant='body1' align='left' style={{color: 'red'}} >
                            {error.generalError}
                        </Typography>
                    </Box>
                </Grid>
            </Grid>
        </Box>
    )
} 

const mapState = state => ({
    paymentActive: state.Payment.active,
    cardHolderName: state.Offer.cardHolderName,    
    pricePerService: state.Offer.pricePerService,
    clientEmail: state.auth.user.data.email
})

const mapDispatch = {
    ...PaymentActions,
    ...OfferActions
}

export default connect(mapState,mapDispatch)(PaymentData)