import React , { Fragment , useState } from 'react'
import clsx from 'clsx'
import { connect } from 'react-redux'
import GenericSnackbar from 'app/utils/GenericSnackbar'
import { useTranslation } from 'react-i18next'

import {
    useMediaQuery,
    Stepper,
    Step,
    StepLabel,
    StepConnector
} from '@material-ui/core'

import { makeStyles, withStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
    container: {
        width: '100%',
        height: '80px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    mobileContainer: {
        width: '100%',
        padding: '0'
    },
    tab: {
        display: 'flex',
        flexDirection: 'row',
        height: '80px',
        width: '35%',
        display: 'flex',
        alignItems: 'center',
        cursor: 'pointer',
        userSelect: 'none',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    contImg: {
        minHeight: '100%',
        width: '32px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        cursor: 'pointer'
    },
    img: {
        minWidth: '24px',
        minHeight: '24px',
        cursor: 'pointer'
    },
    span: {
        minHeight: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        cursor: 'pointer',
        fontWeight: 'bold',
    },
    mobileSpan: {
        cursor: 'pointer',
        fontSize: '14px',
        fontWeight: 'bold',
    }
})

const ColorlibConnector = withStyles({
    alternativeLabel: {
        top: 22,
    },
    active: {
        '& $line': {
            background: '#0097F6',
        },
    },
    completed: {
        '& $line': {
            background: '#0097F6',
        },
    },
    line: {
        height: 1,
        border: 0,
        background: '#B4B4B4',
        borderRadius: 1,
    },
})(StepConnector)

const useColorlibStepIconStyles = makeStyles({
    root: {
        backgroundColor: '#fff',
        zIndex: 1,
        color: '#fff',
        width: 50,
        height: 50,
        display: 'flex',
        borderRadius: '50%',
        justifyContent: 'center',
        alignItems: 'center',
        border: '1px solid #B4B4B4'
    },
    active: {
        background: '#fff',
        border: '1px solid #0097F6',
    },
    completed: {
        background: '#fff',
        border: '1px solid #0097F6',
    },
    img: {
        width: '24px',
    }
})

const ColorlibStepIcon = (props,tab,iconMargin) => {
    const classes = useColorlibStepIconStyles()
    const { active, completed } = props

    const [snackbar,setSnackbar] = useState({
        open: false,
        message: '', 
        severity: 'success',
    })

    const handleCloseSnackbar = value => setSnackbar(value)

    return (
        <div
            className={clsx(classes.root, {
                [classes.active]: active,
                [classes.completed]: completed,
            })}
        >
            <img src={tab} style={{marginLeft: iconMargin ? '5px' : '0'}} className={classes.img} alt='tab' />
        </div>
    )
}

const AppBarOptions = props => {
    const classes = useStyles()

    const matches = useMediaQuery('(max-width:600px)')

    const { t } = useTranslation()

    const [snackbar,setSnackbar] = useState({
        open: false,
        message: '', 
        severity: 'success',
    })

    const handleCloseSnackbar = value => setSnackbar(value)

    const handleTabChange = id => {
        let validateForm1 = ((props.offer.offerBuilding.roomNumber > 0 ||
                                props.offer.offerBuilding.bathRoomNumber > 0 ||
                                props.offer.offerBuilding.kitchenNumber > 0 ||
                                props.offer.offerBuilding.livingNumber > 0) &&
                                ((props.offer.period === 'several_times_a_week' &&
                                Object.values(props.offer.periodPoints).filter(day => day.checked).length > 0) ||
                                props.offer.period !== 'several_times_a_week') &&
                                props.offer.pricePerService > 0 &&
                                props.offer.pricePerDay > 0)

        let validateForm2 = (props.offer.serviceAddress !== '' && props.offer.privacyPolicy)

        if(props.value === 0 && (id === 1 || id === 2) && validateForm1){
            props.onClick(id)             
        }else if(props.value === 0 && id === 2 && validateForm1 && validateForm2){
            props.onClick(id)        
        }else if(props.value === 1 && id === 2 && validateForm1 && validateForm2){
            props.onClick(id)        
        }else if(props.value === 1 && id === 0){
            props.onClick(id)        
        }else if(props.value === 2){
            props.onClick(id)      
        }else{
            setSnackbar({
                open: true,
                message: t('Advices.incompleteForm'), 
                severity: 'warning',
            })
        }
    }

    return (
        <div className={classes.container}>
            <GenericSnackbar 
                handleClose={handleCloseSnackbar} 
                {...snackbar} 
            />
            {
                props.options.map((option,id) => <div 
                                                    key={`tab${id}`} 
                                                    className={classes.tab} 
                                                    onClick={() => handleTabChange(id)}
                                                    style={{
                                                        color: (props.value === id) ? '#fff' : '#2F2F2F',
                                                        background: (props.value === id) ? '#0097F6' : '#F7F7F7',
                                                        borderTopLeftRadius: id === 0 ? '17px' : '0ox',
                                                        borderTopRightRadius: id === 2 ? '17px' : '0px',
                                                        borderLeft: id === 1 ? '1px solid #E0E0E0' : 'none',
                                                        borderRight: id === 1 ? '1px solid #E0E0E0' : 'none',
                                                        padding: matches ? '15px 10px' : '15px 50px'
                                                    }}
                                                >
                                                    <div 
                                                        className={classes.contImg}
                                                        style={{
                                                            marginRight: matches ? '5px' : '15px'
                                                        }}
                                                    >
                                                        <img 
                                                            className={classes.img} 
                                                            src={(props.value === id) ? option.active : option.inactive} 
                                                            alt='tab' 
                                                        />
                                                    </div>
                                                    <span 
                                                        className={classes.span}
                                                        style={{
                                                            fontSize: matches ? '12px' : '15px', 
                                                            color: (props.value === id) ? '#fff' : '#BEBEBE'
                                                        }}
                                                    >{option.label}</span>
                                                </div>)
            }
        </div>
    )
}

const mapState = state => ({
    offer: state.Offer
})

export default connect(mapState,{})(AppBarOptions)