import React , { Fragment, useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { connect } from 'react-redux'


import * as LoginActions from 'app/auth/store/actions/loginActions'

import {
    useMediaQuery,
    makeStyles
} from '@material-ui/core'

import Formsy from 'formsy-react'
import { TextFieldFormsy } from '@fuse/core/formsy'

const useStyles = makeStyles(theme => ({
    // loginContainer: {
    //     width: '60%',
    //     display: 'flex',
    //     flexDirection: 'column',
    //     justifyContent: 'center',
    //     alignItems: 'center',
    //     flexWrap: 'wrap',
    //     borderRadius: '16px',
    //     background: '#fff',
    //     padding: '45px 10px 25px 10px',
    //     boxShadow: '0px 5px 20px rgba(0,0,0,.06)',
    //     marginTop: '30px',
    //     '& a': {
    //         fontSize: '14px',
    //         fontWeight: '500',
    //         color: '#2F2F2F',
    //     },
    //     '& a:hover': {
    //         textDecoration: 'none'
    //     }
    // },
    // loginContainer: {
    //     width: '350px',
    //     boxShadow: '0px 5px 30px rgba(0,0,0,.09)',
    //     height: '500px',
    //     borderRadius: '3px',
    //     width: '#fff',
    //     padding: '50px 32px',
    //     display: 'flex',
    //     flexDirection: 'column',
    //     '& a': {
    //         fontSize: '14px',
    //         fontWeight: '500',
    //         color: '#2F2F2F',
    //     },
    //     '& a:hover': {
    //         textDecoration: 'none'
    //     }
    // },

}))

const desktopStyles = makeStyles(theme => ({
    textField: {
        marginBottom: '20px',
        '& .MuiInputBase-root': {
            border: '1px solid #D1D1D1'
        },
        '& .MuiInputBase-root:hover': {
            border: '1px solid #D1D1D1'
        }
    },
    label: {
        fontSize: '14px',
        fontWeight: '500',
        color: '#2F2F2F',
        marginBottom: '6px',
        textAlign: 'left',
        width:'100%'

    },
    forgetLabel: {
        marginTop: '20px',
        textAlign: 'right',
    },
    buttonBlue: {
        width: '100%',
        background: '#0097F6',
        color: '#fff',
        padding: '16px',
        fontSize: '17px',
        fontWeight: 'bold',
        borderRadius: '3px',
        marginBottom: '36px'
    },
}))

const mobileStyles = makeStyles(theme => ({
    textField: {
        marginBottom: '20px',
        '& .MuiInputBase-root': {
            border: '1px solid #D1D1D1'
        },
        '& .MuiInputBase-root:hover': {
            border: '1px solid #D1D1D1'
        }
    },
    title: {
        fontSize: '25px',
        fontWeight: 'bold',
        color: '#2F2F2F',
        marginBottom: '36px',
        textAlign: 'center'
    },
    label: {
        fontSize: '14px',
        fontWeight: '500',
        color: '#2F2F2F',
        marginBottom: '6px',
    },
    forgetLabel: {
        marginBottom: '20px',
    },
    buttonBlue: {
        background: '#0097F6',
        color: '#fff',
        padding: '16px 0',
        fontSize: '17px',
        fontWeight: 'bold',
        borderRadius: '3px',
        marginBottom: '36px'
    },
}))

const LoginWizard = props => {
    const classes = useStyles()
    const desktopClasses = desktopStyles()
    const mobileClasses = mobileStyles()
    
    const { t } = useTranslation()
    const history = useHistory()
    const matches = useMediaQuery('(max-width:600px)')
    
    const [loginData,setLoginData] = useState({
        email: '',
        password: ''
    })
    const [isValidForm,setIsValidForm] = useState(false)

    const handleChange = input => {
        setLoginData({
            ...loginData,
            [input.target.name]: input.target.value
        })
    }

    const handleSubmit = async form => {
        props.Fetching()
        
        let response = await props.SignIn(loginData)

        if(response.data)
            history.push('/offer')

    }

    const disableButton = () => {
        setIsValidForm(false)
    }

    const enableButton = () => {
        setIsValidForm(true)
    }
    
    return (
        <Fragment>
            <Formsy
                onValidSubmit={handleSubmit}
                onValid={enableButton}
                onInvalid={disableButton}
                className={classes.loginContainer}
            >
                <p className={matches ? mobileClasses.label : desktopClasses.label}>{t('general.email')}*</p>
                <TextFieldFormsy
                    fullWidth
                    variant='outlined'
                    name='email'
                    value={loginData.email}
                    onChange={handleChange}
                    className={matches ? mobileClasses.textField : desktopClasses.textField}
                />
                <p className={matches ? mobileClasses.label : desktopClasses.label}>{t('login.inputs.password')}*</p>
                <TextFieldFormsy
                    fullWidth
                    type='password'
                    variant='outlined'
                    name='password'
                    value={loginData.password}
                    onChange={handleChange}
                    className={matches ? mobileClasses.textField : desktopClasses.textField}
                />
                <div>
                    <Link to='/offer/password/reset' className={matches ? mobileClasses.forgetLabel : desktopClasses.forgetLabel} >{t('general.forgetPassword')}</Link>
                </div>
                <br/>
                <button 
                    className={matches ? mobileClasses.buttonBlue : desktopClasses.buttonBlue}
                    disabled={!isValidForm}    
                >{t('login.title').toUpperCase()}</button>
            </Formsy>
        </Fragment>
    )
}

const mapState = state => ({
    login: state.auth.login,
    user: state.auth.user
})

const mapDispatch = {
    ...LoginActions
}

export default connect(mapState,mapDispatch)(LoginWizard)